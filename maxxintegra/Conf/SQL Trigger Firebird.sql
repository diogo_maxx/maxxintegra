Triggers Firebird

INSERT = 

CREATE OR ALTER TRIGGER MAXXINTEGRA_/*TABELA_REG*/_AFTINS FOR /*TABELA_REG*/ 
ACTIVE AFTER INSERT POSITION 0 
AS declare variable CONTEM INTEGER; 
begin 
 
POST_EVENT '/*TABELA_REG*/_AFTINS'; 
 
select count(*) from maxx_integracao m 
where m.codigo_reg like /*CODIGO_REG*/ 
into :CONTEM; 
 
if (CONTEM <= 0) then 
   begin 
     INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, ENVIADO) 
     VALUES (/*CTABELA_REG*/, /*TABELA_PAI*/, /*CODIGO_REG*/, /*CODIGO_PAI*/, current_timestamp, 'I', 'N'); 
   end 
end;

UPDATE =

CREATE OR ALTER TRIGGER MAXXINTEGRA_/*TABELA_REG*/_AFTUPD FOR /*TABELA_REG*/ 
ACTIVE AFTER UPDATE POSITION 0 
AS declare variable CONTEM INTEGER; 
begin 
 
POST_EVENT '/*TABELA_REG*/_AFTUPD'; 
 
select count(*) from maxx_integracao m 
where m.codigo_reg like /*CODIGO_REG*/ 
into :CONTEM; 
 
if (CONTEM <= 0) then 
begin 
  INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, ENVIADO) 
  VALUES (/*CTABELA_REG*/, /*TABELA_PAI*/, /*CODIGO_REG*/, /*CODIGO_PAI*/, current_timestamp, 'A', 'N'); 
end 
end;

DELETE =

CREATE OR ALTER TRIGGER MAXXINTEGRA_/*TABELA_REG*/_BEFDEL FOR /*TABELA_REG*/ 
ACTIVE BEFORE DELETE POSITION 0 
AS declare variable CONTEM INTEGER; 
begin 
 
  POST_EVENT '/*TABELA_REG*/_BEFDEL'; 
 
select count(*) from maxx_integracao m 
where m.codigo_reg like /*CODIGO_REG*/ 
into :CONTEM; 
 
if (CONTEM <= 0) then 
begin 
  INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, ENVIADO) 
  VALUES (/*CTABELA_REG*/, /*TABELA_PAI*/, /*CODIGO_REG*/, /*CODIGO_PAI*/, current_timestamp, 'D', 'N'); 
end 
end; 