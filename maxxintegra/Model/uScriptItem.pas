unit uScriptItem;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons;

const
  SCRIPT_TABELASCRIPTITEM = ' CREATE TABLE scriptsitem ( ' +
                            '  idscriptsitem int(11) NOT NULL,' +
                            '  idscripts int(11) NOT NULL,' +
                            '  idtiposcripts int(11) DEFAULT NULL,' +
                            '  scri_tpcon char(1) DEFAULT NULL,' +
                            '  scri_endpoint LONGTEXT DEFAULT NULL,' +
                            '  scri_script LONGTEXT DEFAULT NULL,' +
                            '  dt_criacao TIMESTAMP DEFAULT 0,' +
                            '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                            '  usu_criacao INT(11) DEFAULT NULL,' +
                            '  usu_modificacao INT(11) DEFAULT NULL,' +
                            '  inativo CHAR(1) DEFAULT NULL,' +
                            '  status CHAR(1) DEFAULT NULL,' +
                            '  PRIMARY KEY (idscriptsitem)' +
                            ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';

  PREENCHE_TABELACAMPOS_SCRIPTITEM :  array[0..3] of array[0..1] of string = (('idscriptsitem',    'C�digo do script'),
                                                                              ('scri_tpcon',    'Tipo de conex�o do script'),
                                                                              ('scri_endpoint',   'Endpoint do webservice'),
                                                                              ('scri_script', 'Script'));

function SQLPesquisaScriptItem: String;



implementation

uses uMensagens;


function SQLPesquisaScriptItem: String;
begin
  Result := 'SELECT ' +
            '       s.idscripts "C�digo", ' +
            '       sis.sis_nome "Sistema integrado", ' +
            '       s.scr_padrao "Padr�o", ' +
            '       c.cli_fantasia "Cliente", ' +
            '       f.fil_fantasia "Filial" ' +
            'FROM &SCHEMA.sripts s ' +
            'left join &SCHEMA.sistemasintegrados sis on (sis.idsistemasintegrados = s.idsistemasintegrados) ' +
            'left join &SCHEMA.clientes c on (c.idclientes = s.idclientes) ' +
            'left join &SCHEMA.filiais f on (f.idfiliais = s.idfiliais and f.idclientes = s.idclientes) ' +
            'where s.status = ' + QuotedStr('A');
end;

end.
