unit uUsuario;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELAUSUARIOS = ' CREATE TABLE usuarios ( ' +
                   '  idusuarios int(11) NOT NULL,' +
                   '  usu_descricao varchar(100) DEFAULT NULL,' +
                   '  usu_nome varchar(100) DEFAULT NULL,' +
                   '  usu_senha varchar(40) DEFAULT NULL,' +
                   '  dt_criacao TIMESTAMP DEFAULT 0,' +
                   '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                   '  usu_criacao INT(11) DEFAULT NULL,' +
                   '  usu_modificacao INT(11) DEFAULT NULL,' +
                   '  inativo CHAR(1) DEFAULT NULL,' +
                   '  status CHAR(1) DEFAULT NULL,' +
                   '  PRIMARY KEY (idusuarios),' +
                   '  UNIQUE KEY idcampos_UNIQUE (idusuarios)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_USUARIO :  array[0..3] of array[0..1] of string = (('idusuarios', 'C�digo do usu�rio'),
                                                                            ('usu_descricao', 'Descri��o do usu�rio'),
                                                                            ('usu_nome', 'Nome ou login do usu�rio'),
                                                                            ('usu_senha ', 'Senha do usu�rio'));

procedure CriarUsuarioSistema(FDExec: TFDQuery);
function PesquisaUsuario: String;


implementation

uses uMensagens;

procedure CriarUsuarioSistema(FDExec: TFDQuery);
begin
  //Usuario sys senha: Maxx80131980
  FDExec.Close;
  FDExec.SQL.Clear;
  FDExec.SQL.Add('INSERT INTO ' + FDExec.Connection.Params.Database + '.usuarios ' +
                 '(idusuarios, ' +
                 'usu_descricao, ' +
                 'usu_nome, ' +
                 'usu_senha, ' +
                 'dt_criacao, ' +
                 'dt_modificacao, ' +
                 'usu_criacao, ' +
                 'usu_modificacao, ' +
                 'inativo, ' +
                 'status) ' +
                 'VALUES ' +
                 '(1, ' +
                 '''USUARIO NIVEL DE SISTEMA'', ' +
                 '''SYS'', ' +
                 '''F9DA0153D66EF7D26C579C89C18A705C'', ' +
                 'current_timestamp(), ' +
                 'current_timestamp(), ' +
                 'null, ' +
                 'null, ' +
                 '''N'', ' +
                 '''A'')');
  FDExec.ExecSQL;

  FDExec.Close;
  FDExec.SQL.Clear;
end;

function PesquisaUsuario: String;
begin
  Result := 'select idusuarios "C�digo", usu_descricao "Descri��o", usu_nome "Nome", ' +
            ' case ' +
            '   when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '   when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            ' end "Inativo" ' +
            'from &SCHEMA.usuarios u ' +
            ' where u.status = ' + QuotedStr('A') +
            '   and u.idusuarios <> 1 ' +
            'order by idusuarios ';
end;

end.
