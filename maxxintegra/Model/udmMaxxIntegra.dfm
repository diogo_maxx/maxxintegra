object dmMaxxIntegra: TdmMaxxIntegra
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 339
  Width = 690
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=maxxintegra'
      'User_Name=root'
      'Password=root'
      'Server=192.168.25.3'
      'DriverID=MySQL')
    BeforeConnect = FDConnectionBeforeConnect
    Left = 40
    Top = 16
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    Left = 596
    Top = 221
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrAppWait
    Left = 596
    Top = 165
  end
  object fdUsuario: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT  u.idusuarios,'
      '        u.usu_descricao,'
      '        u.usu_nome,'
      '        u.usu_senha,'
      '        u.dt_criacao,'
      '        u.dt_modificacao,'
      '        u.usu_criacao,'
      '        u.usu_modificacao,'
      '        u.inativo,'
      '        u.status'
      'FROM &SCHEMA.usuarios u'
      'where u.usu_nome = :USU_NOME')
    Left = 40
    Top = 72
    ParamData = <
      item
        Name = 'USU_NOME'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
      end>
    object fdUsuarioidusuarios: TIntegerField
      FieldName = 'idusuarios'
      Origin = 'idusuarios'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object fdUsuariousu_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_descricao'
      Origin = 'usu_descricao'
      Size = 100
    end
    object fdUsuariousu_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_nome'
      Origin = 'usu_nome'
      Size = 100
    end
    object fdUsuariousu_senha: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_senha'
      Origin = 'usu_senha'
      Size = 40
    end
    object fdUsuariodt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object fdUsuariodt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object fdUsuariousu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object fdUsuariousu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object fdUsuarioinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object fdUsuariostatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
  object fdNovoCodigo: TFDQuery
    CachedUpdates = True
    Connection = FDConnection
    SQL.Strings = (
      'SELECT c.codigos_tabela, c.codigos_campo, c.codigos_valor '
      'FROM &SCHEMA.codigos c'
      'where c.codigos_tabela = :CODIGOS_TABELA'
      '  and c.codigos_campo = :CODIGOS_CAMPO')
    Left = 40
    Top = 120
    ParamData = <
      item
        Name = 'CODIGOS_TABELA'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'CODIGOS_CAMPO'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
  end
  object cxTraducao: TcxLocalizer
    Left = 600
    Top = 16
  end
  object StyleGrid: TcxStyleRepository
    Left = 400
    Top = 24
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 16243131
    end
    object cxStyle2: TcxStyle
    end
  end
end
