unit uValidacao;

interface

uses RTTI, Classes, Forms, Variants, SysUtils, ExtCtrls, Data.DB, Vcl.StdCtrls, uMensagens, uUtilidades;

function ValidaObrigatorios(Form: TForm; Lista: TStringList): Boolean;

implementation

function ValidaObrigatorios(Form: TForm; Lista: TStringList): Boolean;
var
  I: Integer;
  Hint: String;
  Contexto: TRttiContext;
  Tipo: TRttiType;
begin
  Result := True;

  for I := 0 to Lista.Count -1 do
  begin
    if Form.FindComponent(Lista.Strings[I]) <> nil then
    begin
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Form.FindComponent(Lista.Strings[I]).ClassType);

        if Tipo.GetProperty('Enabled').GetValue(Form.FindComponent(Lista.Strings[I])).AsBoolean = true then
        begin
          if (Tipo is TRttiInstanceType) and ((TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) or
                                              (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomComboBox))) then
          begin
            //verificando os campos de dataware
            if Tipo.GetProperty('Field') <> nil then
            begin
              if RetornaValor(TField(Tipo.GetProperty('Field').GetValue(
                                                           Form.FindComponent(Lista.Strings[I])).AsObject), 'AsVariant') = Null then
                Result := False;

            end
            else
            //verificando os campos sem dataware
            //caso seja um campo numerico
            if Tipo.GetProperty('intValue') <> nil then
            begin
              if RetornaValor(Form.FindComponent(Lista.Strings[I]), 'Value') <= 0 then
                Result := False;
            end
            else
            //se for campo data
            if Tipo.GetProperty('Date') <> nil then
            begin
              if RetornaValor(Form.FindComponent(Lista.Strings[I]), 'Date') = 0 then
                Result := False;
            end
            else
            //demais campos
            if Tipo.GetProperty('Value') <> nil then
            begin
              if RetornaValor(Form.FindComponent(Lista.Strings[I]), 'Value') = Null then
                Result := False;
            end;
            //caso nenhum destes, verifica a propriedade text
            if Tipo.GetProperty('Text') <> nil then
            begin
              if (RetornaValor(Form.FindComponent(Lista.Strings[I]), 'Text') = Null) or
                 (RetornaValor(Form.FindComponent(Lista.Strings[I]), 'Text') = '') then
                Result := False;
            end;
          end;
        end;

        if not Result then
        begin
          Hint := TCustomEdit(Form.FindComponent(Lista.Strings[I])).Hint;
          if Hint = '' then
            Hint := TCustomEdit(Form.FindComponent(Lista.Strings[I])).Name;

          Mensagem('C', 'O campo ' + Hint + ' est� vazio!' + #13 + 'Verifique.');
          SetarFocoComponente(Form.FindComponent(Lista.Strings[I]));
          Abort;
        end;
      finally
        Contexto.Free;
      end;
    end;
  end;
end;

end.
