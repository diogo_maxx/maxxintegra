unit uMensagens;

interface

uses
  Vcl.Forms, Vcl.Controls, SysUtils;


function Mensagem(Tipo, Msg: String; Form: TForm = nil): Boolean;

implementation

uses ufrmMensagens;

function Mensagem(Tipo, Msg: String; Form: TForm = nil): Boolean;
begin
  try
    frmMensagens := TfrmMensagens.Create(Form);
    frmMensagens.TipoMensagem := Tipo;
    frmMensagens.Mensagem     := Msg;
    frmMensagens.ShowModal;

    Result := frmMensagens.ModalResult = mrOk;
  finally
    FreeAndNil(frmMensagens);
  end;
end;

end.
