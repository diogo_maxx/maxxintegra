///////////////////////////////////////////////////////////////////////////////////////
///                                                                                 ///
///   Projeto: MaxxIntegra - Integrador de dados de ERP's para nossas solu��es web  ///
///                                                                                 ///
/// Esta biblioteca tem a fun��o de centralizar toda os SQL's de busca e cria��o    ///
/// para a tabela de logradouro.                                                    ///
///                                                                                 ///
/// Desenvolvido por: Diogo Aires Cardoso (07-03-2018)                              ///
///                                                                                 ///
///////////////////////////////////////////////////////////////////////////////////////
unit uLogradouro;

interface

uses System.SysUtils, System.Variants, System.Classes, uPesquisas;


function SQLPesquisaLogradouro: String;
procedure PesquisaLogradouroPorCEP(CEP: String; Campos: Array of String; Componentes: Array of TComponent);

implementation

function SQLPesquisaLogradouro: String;
begin
  Result := 'SELECT ' +
            '    l.cep "CEP", ' +
            '    l.id_logradouro "C�digo", ' +
            '    l.tipo "Tipo", ' +
            '    l.descricao_sem_numero "Logradouro", ' +
            '    l.id_cidade "C�d. Cidade", ' +
            '    l.uf "UF", ' +
            '    l.descricao_cidade "Nome", ' +
            '    l.codigo_cidade_ibge "C�d. IBGE", ' +
            '    l.descricao_bairro "Bairro" ' +
            'FROM &SCHEMA.logradouro l ';
end;


procedure PesquisaLogradouroPorCEP(CEP: String; Campos: Array of String; Componentes: Array of TComponent);
begin
  CEP := StringReplace(CEP, '-', '', [rfReplaceAll, rfIgnoreCase]);
  Pesquisar(SQLPesquisaLogradouro, 'where cep = ' + QuotedStr(CEP), Campos, Componentes, False, False);
end;


end.
