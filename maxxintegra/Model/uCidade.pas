///////////////////////////////////////////////////////////////////////////////////////
///                                                                                 ///
///   Projeto: MaxxIntegra - Integrador de dados de ERP's para nossas solu��es web  ///
///                                                                                 ///
/// Esta biblioteca tem a fun��o de centralizar toda os SQL's de busca e cria��o    ///
/// para a tabela de cidade.                                                        ///
///                                                                                 ///
/// Desenvolvido por: Diogo Aires Cardoso (07-03-2018)                              ///
///                                                                                 ///
///////////////////////////////////////////////////////////////////////////////////////
unit uCidade;

interface

function SQLPesquisaCidade: String; overload;

implementation

function SQLPesquisaCidade: String;
begin
  Result := '  SELECT ' +
            '    id_cidade "C�digo", ' +
            '    descricao "Nome", ' +
            '    uf "UF", ' +
            '    codigo_ibge "C�d. IBGE", ' +
            '    ddd "DDD" ' +
            '  FROM &SCHEMA.cidade ';
end;


end.
