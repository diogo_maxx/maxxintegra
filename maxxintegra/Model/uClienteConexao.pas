unit uClienteConexao;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELACLIENTECONEXAO = 'CREATE TABLE clientes_conexoes ( ' +
                                'idclientes_conexao INT NOT NULL, ' +
                                'idclientes INT NOT NULL, ' +
                                'idfiliais INT NULL, ' +
                                'clicon_tipo CHAR(1) NULL DEFAULT ''R'', ' +
                                'clicon_ip VARCHAR(20) NULL, ' +
                                'clicon_porta VARCHAR(20) NULL, ' +
                                'clicon_usuario VARCHAR(45) NULL, ' +
                                'clicon_senha varchar(45) NULL, ' +
                                'dt_criacao TIMESTAMP DEFAULT 0, ' +
                                'dt_modificacao TIMESTAMP DEFAULT 0, ' +
                                'usu_criacao INT(11) DEFAULT NULL, ' +
                                'usu_modificacao INT(11) DEFAULT NULL, ' +
                                'inativo CHAR(1) DEFAULT NULL, ' +
                                'status CHAR(1) DEFAULT NULL, ' +
                                'PRIMARY KEY (idclientes_conexao)) ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_CLIENTECOENXAO :  array[0..5] of array[0..1] of string = (('idclientes_conexao', 'C�digo do conex�o do cliente'),
                                                                                   ('clicon_tipo',        'Tipo da conex�o : R - �rea de trabalho Remota | T - Team viewer'),
                                                                                   ('clicon_ip',          'IP do cliente ou ID do Team Viewer'),
                                                                                   ('clicon_porta',       'Porta para conex�o'),
                                                                                   ('clicon_usuario',     'Usu�rio da conex�o'),
                                                                                   ('clicon_senha',       'Senha da conex�o'));

implementation



end.
