unit uFormulario;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;

const
  SCRIPT_TABELAFORMULARIOS = 'CREATE TABLE maxxintegra.formularios ( ' +
                             '  idformularios INT NOT NULL AUTO_INCREMENT, ' +
                             '  form_pai INT NULL, ' +
                             '  form_nome VARCHAR(45) NULL, ' +
                             '  form_descricao VARCHAR(45) NULL, ' +
                             '  PRIMARY KEY (idformularios)) ' +
                             '  ENGINE=InnoDB DEFAULT CHARSET=utf8';

  PREENCHE_TABELACAMPOS_FORMULARIOS :  array[0..3] of array[0..1] of string = (('idformularios', 'C�digo do formul�rio'),
                                                                               ('form_pai', 'C�digo do formul�rio que o mesmo ser� vinculado'),
                                                                               ('form_nome', 'Nome no sistema para o formul�rio'),
                                                                               ('form_descricao', 'Descri��o do formul�rio'));

function PesquisaFormulario: String;

implementation

function PesquisaFormulario: String;
begin
  Result := 'SELECT f.idformularios, f.form_pai, f.form_nome, f.form_descricao, ' +
            '     (select f1.form_descricao ' +
            '      from maxxintegra.formularios f1 ' +
            '      where f1.idformularios = f.form_pai) formpai_descricao ' +
            'FROM maxxintegra.formularios f ';
end;

end.
