unit uUtilidades;

interface

uses RTTI, Classes, Forms, Variants, SysUtils, ExtCtrls, Data.DB, Vcl.StdCtrls, uMensagens, Controls, ComCtrls, RzTabs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  StrUtils, TypInfo;

   /// <summary>Fun��o que retorna o valor do componente</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="Componente">Qual componente ser� retornado o seu valor</param>
   /// <param name="Propriedade">A propriedade que ser� retornada. Por padr�o � vazio</param>
   function RetornaValor(Componente: TComponent; Propriedade: String = ''): Variant;
   /// <summary>Fun��o que coloca o foco no componente</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="Componente">Qual componente ser� setado o foco</param>
   procedure SetarFocoComponente(Componente: TComponent);
   /// <summary>Fun��o que seta algum valor no componente</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="Componente">Qual componente ser� setado o valor</param>
   /// <param name="Valor">O valor a ser setado no componente</param>
   procedure SetarValorComponente(Component: TComponent; Valor: Variant);
   /// <summary>Procedure para centralizar a crian��o das rotinas do sistema</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="FormClass">A classe do formul�rio a ser criado</param>
   /// <param name="Reference">Vari�vel de refer�ncia do formul�rio</param>
   procedure CriaForm(FormClass: TFormClass; var Reference);
   /// <summary>Procedure para formatar o campo dataware para melhor apresenta��o pro cliente</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="Tipo">Qual o tipo de formata��o F - F�sico | J - Jur�dico</param>
   /// <param name="Componente">Componente para ser formatado</param>
   procedure FormatarCPFCNPJ(Tipo: String; Componente: TComponent);
   /// <summary>Procedure para padronizar e centralizar a cria��o de novos registros no sistema</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="FDQry">Qual o FDQuery que vai ser inclu�do os novos registros</param>
   /// <param name="Tabela">Tabela do banco de dados</param>
   /// <param name="CampoID">ID da tabela do banco de dados</param>
   procedure NovoRegistro(FDQry: TFDQuery; Tabela, Campo: String);
   /// <summary>Procedure para padronizar e centralizar alterar o charcase do char digitado</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="CH">Char a ser alterado</param>
   function UppCase(Ch: Char): Char;

implementation

uses uConexao, uConstantes, udmMaxxIntegra;

function RetornaValor(Componente: TComponent; Propriedade: String = ''): Variant;
var
  Context: TRttiContext;
  Types: TRttiType;
begin
  if Propriedade <> '' then
  begin
    try
      Context := TRttiContext.Create;
      Types   := Context.GetType(Componente.ClassType);

      Result := Null;

      if Types.GetProperty(Propriedade) <> nil then
      begin
        try
          if not(VarIsEmpty(Types.GetProperty(Propriedade).GetValue(Componente).AsVariant) or
                 VarIsNull(Types.GetProperty(Propriedade).GetValue(Componente).AsVariant)) then
          begin
            if Types.GetProperty('DataType') <> nil then
            begin
              if TField(Componente).DataType in [ftBCD, ftFloat, ftFMTBcd, ftInteger] then
              begin
                if Types.GetProperty(Propriedade).GetValue(Componente).AsVariant <> 0 then
                   Result := Types.GetProperty(Propriedade).GetValue(Componente).AsVariant;
              end
              else if TField(Componente).DataType = ftString then
              begin
                if TField(Componente).AsString <> '' then
                  Result := Types.GetProperty(Propriedade).GetValue(Componente).AsVariant;
              end
              else Result := Types.GetProperty(Propriedade).GetValue(Componente).AsVariant;
            end
            else Result := Types.GetProperty(Propriedade).GetValue(Componente).AsVariant;
          end;
        except
          on E: EInvalidCast do
            Result := Types.GetProperty(Propriedade).GetValue(Componente).AsVariant;
        end;
      end;
    finally
      Context.Free;
    end;
  end
  else
  begin
    try
      Context := TRttiContext.Create;
      Types     := Context.GetType(Componente.ClassType);

      if Types.GetProperty('Field') <> nil then
        Result := TField(Types.GetProperty('Field').GetValue(Componente).AsObject).AsVariant
      else
      begin
        if Types.GetProperty('Checked') <> nil then
          Result := Types.GetProperty('Checked').GetValue(Componente).AsVariant
        else if Types.GetProperty('Date') <> nil then
          Result := Types.GetProperty('Date').GetValue(Componente).AsVariant
        else if Types.GetProperty('Value') <> nil then
          Result := Types.GetProperty('Value').GetValue(Componente).AsVariant
        else if Types.GetProperty('Text') <> nil then
          Result := Types.GetProperty('Text').GetValue(Componente).AsVariant;
      end;
    finally
      if VarIsEmpty(Result) or VarIsNull(Result) then
        Result := Null;

      Context.Free;
    end;
  end;
end;

procedure SetarFocoComponente(Componente: TComponent);
var
  Component: TComponent;
  Context: TRttiContext;
  Types: TRttiType;
begin
  try
    if (Componente <> nil) and (TWinControl(Componente).Parent <> nil) then
    begin
      Component := TWinControl(Componente).Parent;

      while not (TWinControl(Componente).CanFocus) do
      begin
        if Component <> nil then
        begin
          try
            Context := TRttiContext.Create;
            Types   := Context.GetType(Component.ClassType);

            if (Types is TRttiInstanceType) and (TRttiInstanceType(Types).MetaclassType.InheritsFrom(TWinControl)) then
            begin
              if Component is TTabSheet then
                TTabSheet(Component).PageControl.ActivePage := TTabSheet(Component);
              if Component is TRzTabSheet then
                TRzTabSheet(Component).PageControl.ActivePage := TRzTabSheet(Component);

              Component := TWinControl(Component).Parent;
            end;
          finally
            Context.Free;
          end;
        end
        else Break;
      end;

      if (TWinControl(Componente).Enabled) and
         (TWinControl(Componente).Visible) and
         (TWinControl(Componente).Parent.Visible) then
        TWinControl(Componente).SetFocus;
    end;
  except
  end;
end;

procedure SetarValorComponente(Component: TComponent; Valor: Variant);
var
  Contexto  : TRttiContext;
  Tipo      : TRttiType;
begin
  if Valor <> Null then
  begin
    try
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Component.ClassType);

        if not(VarIsEmpty(Valor) or VarIsNull(Valor)) then
        begin
          if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TField)) then
          begin
            if Component.ClassType <> TBlobField then
              Tipo.GetProperty('AsVariant').SetValue(Component, TValue.From(Valor))
            else TField(Component).Value := Valor;
          end
          else
          if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) then
          begin
            if Tipo.GetProperty('Field') = nil then
            begin
              if Tipo.GetProperty('Value') <> nil then
                 Tipo.GetProperty('Value').SetValue(Component, TValue.From(Valor))
              else if Tipo.GetProperty('Text') <> nil then
                 Tipo.GetProperty('Text').SetValue(Component, vartostr(Valor));
            end
            else
              SetarValorComponente(TField(Tipo.GetProperty('Field').GetValue(Component).AsObject), Valor);
          end;
        end
        else
        begin
          if Tipo.GetMethod('Parent') <> nil then
          begin
            if Tipo.GetMethod('Clear') <> nil then
              Tipo.GetMethod('Clear').Invoke(Component, []);
          end
          else
          begin
            if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TField)) then
              Tipo.GetProperty('AsVariant').SetValue(Component, TValue.From(''))
            else
            if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) then
            begin
              if Tipo.GetProperty('Field') = nil then
              begin
                if Tipo.GetProperty('Value') <> nil then
                   Tipo.GetProperty('Value').SetValue(Component, TValue.From(''))
                else if Tipo.GetProperty('Text') <> nil then
                   Tipo.GetProperty('Text').SetValue(Component, vartostr(''));
              end
              else
                SetarValorComponente(TField(Tipo.GetProperty('Field').GetValue(Component).AsObject), '');
            end;
          end;
        end;
      finally
        Contexto.Free;
      end;
    except
      on E : Exception do
      begin
        Mensagem('E', 'Erro ao setar o valor no componente: ' + E.Message + #13 + Tipo.GetProperty('Name').GetValue(Component).AsString);
        Contexto.Free;
      end;
    end;
  end;
end;

procedure CriaForm(FormClass: TFormClass; var Reference);
begin
  try
    if TForm(Reference) = nil then
    begin
      Application.CreateForm(FormClass, TForm(Reference));
      with  TForm(Reference) do
      begin
        Hide;
        Visible     := False;
        Position    := poMainFormCenter;
        FormStyle   := fsNormal;
        Show;
        BringToFront;
      end;
    end
    else begin
      with  TForm(Reference) do
      begin
        Show;
        BringToFront;
      end;
    end;
  except
    on E: Exception do
      Mensagem('E', 'N�o foi poss�vel criar o formul�rio: ' + E.Message);
  end;
end;

procedure FormatarCPFCNPJ(Tipo: String; Componente: TComponent);
begin
  if Componente is TField then
    TField(Componente).EditMask := ifthen(Tipo = 'F', MASK_CPF, MASK_CNPJ);
end;

procedure NovoRegistro(FDQry: TFDQuery; Tabela, Campo: String);
begin
  FDQry.FieldByName(Campo).AsInteger   := dmMaxxIntegra.NovoID(Tabela, Campo);

  if FDQry.FindField('usu_criacao') <> nil then
    FDQry.FieldByName('usu_criacao').AsInteger     := dmMaxxIntegra.fdUsuarioidusuarios.AsInteger;
  if FDQry.FindField('usu_modificacao') <> nil then
    FDQry.FieldByName('usu_modificacao').AsInteger := dmMaxxIntegra.fdUsuarioidusuarios.AsInteger;
  if FDQry.FindField('dt_criacao') <> nil then
    FDQry.FieldByName('dt_criacao').AsDateTime     := Now;
  if FDQry.FindField('dt_modificacao') <> nil then
    FDQry.FieldByName('dt_modificacao').AsDateTime := Now;
  if FDQry.FindField('inativo') <> nil then
    FDQry.FindField('inativo').AsString := 'N';
  if FDQry.FindField('status') <> nil then
    FDQry.FindField('status').AsString := 'A';
end;

function UppCase(Ch: Char): Char;
begin
  Result := Ch;
  case Result of
    'a'..'z':   Dec(Result, Ord('a') - Ord('A'));
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
    '�': Result := '�';
  end;
end;

end.
