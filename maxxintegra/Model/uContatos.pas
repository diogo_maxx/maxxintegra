unit uContatos;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons;


const
  SCRIPT_TABELACONTATOS = ' CREATE TABLE contatos ( ' +
                   '  idcontatos int(11) NOT NULL,' +
                   '  idtiposcontatos varchar(100) DEFAULT NULL,' +
                   '  cont_descricao varchar(100) DEFAULT NULL,' +
                   '  cont_contato varchar(100) DEFAULT NULL,' +
                   '  cont_observacao LONGTEXT NULL, ' +
                   '  idchave int(11) NOT NULL,' +
                   '  cont_tabela varchar(50) NOT NULL,' +
                   '  dt_criacao TIMESTAMP DEFAULT 0,' +
                   '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                   '  usu_criacao INT(11) DEFAULT NULL,' +
                   '  usu_modificacao INT(11) DEFAULT NULL,' +
                   '  inativo CHAR(1) DEFAULT NULL,' +
                   '  status CHAR(1) DEFAULT NULL,' +
                   '  PRIMARY KEY (idcontatos)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';

  PREENCHE_TABELACAMPOS_CONTATOS :  array[0..5] of array[0..1] of string = (('idcontatos',      'C�digo do contato'),
                                                                            ('cont_contato',    'Contato'),
                                                                            ('cont_descricao',  'Descri��o do contato'),
                                                                            ('cont_observacao', 'Observa��o do contato'),
                                                                            ('idchave',         'C�digo da tabela origem'),
                                                                            ('cont_tabela ',    'Nome da tabela origem'));

function SQLPesquisaContato: String;


implementation

uses uMensagens;


function SQLPesquisaContato: String;
begin
  Result := 'SELECT c.idcontatos "C�digo", tp.tpcon_descricao "Tipo", c.cont_contato "Descri��o" ' +
            'FROM &SCHEMA.contatos c ' +
            'LEFT JOIN &SCHEMA.tiposcontatos tp on (tp.idtiposcontatos = c.idtiposcontatos) ';
end;

end.
