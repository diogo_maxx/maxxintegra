unit uCampos;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELACAMPOS = ' CREATE TABLE campos ( ' +
                        '  idcampos int(11) NOT NULL AUTO_INCREMENT,' +
                        '  tabela varchar(45) DEFAULT NULL,' +
                        '  campo varchar(45) DEFAULT NULL,' +
                        '  descricao varchar(100) DEFAULT NULL,' +
                        '  PRIMARY KEY (idcampos),' +
                        '  UNIQUE KEY idcampos_UNIQUE (idcampos),' +
                        '  UNIQUE KEY campo_UNIQUE (campo)' +
                        ') ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8; ';


  PREENCHE_TABELACAMPOS :  array[0..6] of array[0..1] of string = (('dt_criacao', 'Data de cria��o do registro'),
                                                                   ('dt_modificacao', 'Data de modifica��o do registro'),
                                                                   ('usu_criacao', 'Usu�rio que criou o registro'),
                                                                   ('usu_modificacao', 'Usu�rio que modificou o registro'),
                                                                   ('inativo', 'Registro inativo ou n�o (S - Sim | N - N�o)'),
                                                                   ('status', 'Status do registro (A - Ativo | X - Exclu�do)'),
                                                                   ('versao', '1.0.0.0'));

implementation





end.
