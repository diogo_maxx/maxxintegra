unit uCliente;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELACLIENTE =  'CREATE TABLE clientes ( ' +
                          'idclientes INT NOT NULL, ' +
                          'cli_tipo CHAR(1) NULL DEFAULT ''J'', ' +
                          'cli_cpfcnpj VARCHAR(20) NULL, ' +
                          'cli_razao VARCHAR(45) NULL, ' +
                          'cli_fantasia VARCHAR(45) NULL, ' +
                          'cli_dtnasc DATE NULL, ' +
                          'cli_documento VARCHAR(45) NULL, ' +
                          'cli_logradouro VARCHAR(100) NULL, ' +
                          'cli_complemento VARCHAR(100) NULL, ' +
                          'cli_numero VARCHAR(10) NULL, ' +
                          'cli_bairro VARCHAR(100) NULL, ' +
                          'id_cidade INT(11) NULL, ' +
                          'cli_cep VARCHAR(9) NULL, ' +
                          'cli_logo LONGBLOB NULL, ' +
                          'dt_criacao TIMESTAMP DEFAULT 0, ' +
                          'dt_modificacao TIMESTAMP DEFAULT 0, ' +
                          'usu_criacao INT(11) DEFAULT NULL, ' +
                          'usu_modificacao INT(11) DEFAULT NULL, ' +
                          'inativo CHAR(1) DEFAULT NULL, ' +
                          'status CHAR(1) DEFAULT NULL, ' +
                          'PRIMARY KEY (idclientes)) ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_CLIENTE :  array[0..12] of array[0..1] of string = (('idclientes',      'C�digo do cliente'),
                                                                             ('cli_tipo',        'Tipo do cliente : F - F�sico | J - Jur�dico'),
                                                                             ('cli_cpfcnpj',     'CPF ou CNPJ do cliente'),
                                                                             ('cli_razao',       'Raz�o social do cliente'),
                                                                             ('cli_fantasia',    'Nome fantasia do cliente'),
                                                                             ('cli_dtnasc',      'Data de nascimento ou cria��o do cliente'),
                                                                             ('cli_documento',   'Inscri��o estadual do cliente'),
                                                                             ('cli_logradouro',  'Logradouro do cliente'),
                                                                             ('cli_complemento', 'Complemento do cliente'),
                                                                             ('cli_numero',      'N�mero do cliente'),
                                                                             ('cli_bairro',      'Bairro do cliente'),
                                                                             ('cli_cep',         'CEP do cliente'),
                                                                             ('cli_logo',        'Logomarca do cliente'));

function SQLPesquisaClientes: String;
function SQLPesquisaClientesLogo: String;

implementation

function SQLPesquisaClientes: String;
begin
  Result := 'SELECT ' +
            '     c.idclientes "C�digo", ' +
            '     case ' +
            '     when c.cli_tipo = ' + QuotedStr('J') + ' then ' + QuotedStr('JUR�DICA') +
            '       when c.cli_tipo = ' + QuotedStr('F') + ' then ' + QuotedStr('F�SICA') +
            '     end "Tipo", ' +
            '     c.cli_cpfcnpj "CPF/CNPJ", ' +
            '     c.cli_razao "Raz�o Social", ' +
            '     c.cli_fantasia "Nome", ' +
            '     case ' +
            '       when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '       when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            '     end "Inativo" ' +
            'FROM &SCHEMA.clientes c ' +
            'where c.status = ' + QuotedStr('A');
end;

function SQLPesquisaClientesLogo: String;
begin
  Result := 'SELECT ' +
            '     c.idclientes "C�digo", ' +
            '     case ' +
            '     when c.cli_tipo = ' + QuotedStr('J') + ' then ' + QuotedStr('JUR�DICA') +
            '       when c.cli_tipo = ' + QuotedStr('F') + ' then ' + QuotedStr('F�SICA') +
            '     end "Tipo", ' +
            '     c.cli_cpfcnpj "CPF/CNPJ", ' +
            '     c.cli_razao "Raz�o Social", ' +
            '     c.cli_fantasia "Nome", ' +
            '     case ' +
            '       when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '       when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            '     end "Inativo", ' +
            '     c.cli_logo "Logomarca" ' +
            'FROM &SCHEMA.clientes c ' +
            'where c.status = ' + QuotedStr('A');
end;


end.
