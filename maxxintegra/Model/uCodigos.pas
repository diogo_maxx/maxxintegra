unit uCodigos;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELACODIGOS = ' CREATE TABLE codigos ( ' +
                  '  codigos_tabela varchar(45)  NOT NULL,' +
                  '  codigos_campo varchar(45) NOT NULL,' +
                  '  codigos_valor int(11) DEFAULT 0,' +
                  '  PRIMARY KEY (codigos_tabela, codigos_campo)' +
                  ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_CODIGOS :  array[0..2] of array[0..1] of string = (('codigos_tabela', 'Tabela do campo c�digo'),
                                                                    ('codigos_campo', 'Campo c�digo da tabela'),
                                                                    ('codigos_valor', 'Valor do �ltimo ID do campo'));

procedure InserirCodigosPadroes(FDExec: TFDQuery);

implementation

procedure InserirCodigosPadroes(FDExec: TFDQuery);
begin
  FDExec.Close;
  FDExec.SQL.Clear;
  FDExec.SQL.Add('INSERT INTO ' + FDExec.Connection.Params.Database + '.codigos (codigos_tabela, codigos_campo, codigos_valor) VALUES (' +
                                                                                '''usuarios'', ''idusuarios'', ''1'')');
  FDExec.ExecSQL;

  FDExec.Close;
  FDExec.SQL.Clear;
end;
end.
