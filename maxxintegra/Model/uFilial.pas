unit uFilial;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELAFILIAL = ' CREATE TABLE filiais ( ' +
                        '  idfiliais int(11) NOT NULL, ' +
                        '  idclientes int(11) NOT NULL, ' +
                        '  fil_cnpj varchar(20) DEFAULT NULL, ' +
                        '  fil_razao varchar(45) DEFAULT NULL, ' +
                        '  fil_fantasia varchar(45) DEFAULT NULL, ' +
                        '  fil_dtcria date DEFAULT NULL, ' +
                        '  fil_ie varchar(45) DEFAULT NULL, ' +
                        '  fil_logradouro varchar(100) DEFAULT NULL, ' +
                        '  fil_complemento varchar(100) DEFAULT NULL, ' +
                        '  fil_numero varchar(10) DEFAULT NULL, ' +
                        '  fil_bairro varchar(100) DEFAULT NULL, ' +
                        '  id_cidade int(11) DEFAULT NULL, ' +
                        '  fil_cep varchar(9) DEFAULT NULL, ' +
                        '  fil_logo longblob, ' +
                        '  dt_criacao timestamp DEFAULT 0, ' +
                        '  dt_modificacao timestamp DEFAULT 0, ' +
                        '  usu_criacao int(11) DEFAULT NULL, ' +
                        '  usu_modificacao int(11) DEFAULT NULL, ' +
                        '  inativo char(1) DEFAULT NULL, ' +
                        '  status char(1) DEFAULT NULL, ' +
                        '  PRIMARY KEY (idfiliais) ' +
                        ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_FILIAL :  array[0..11] of array[0..1] of string =  (('idfiliais',       'C�digo da filial'),
                                                                             ('fil_cnpj',        'CNPJ da filial'),
                                                                             ('fil_razao',       'Raz�o social da filial'),
                                                                             ('fil_fantasia',    'Nome fantasia da filial'),
                                                                             ('fil_dtcria',      'Data de cria��o da filial'),
                                                                             ('fil_ie',          'Inscri��o estadual da filial'),
                                                                             ('fil_logradouro',  'Logradouro da filial'),
                                                                             ('fil_complemento', 'Complemento da filial'),
                                                                             ('fil_numero',      'N�mero da filial'),
                                                                             ('fil_bairro',      'Bairro da filial'),
                                                                             ('fil_cep',         'CEP da filial'),
                                                                             ('fil_logo',        'Logomarca da filial'));


function SQLPesquisaFilial(IDCLIENTE: String): String; overload;
function SQLPesquisaFilial: String; overload;
function SQLPesquisaFilialLogo: String;

implementation

function SQLPesquisaFilial(IDCLIENTE: String): String;
begin
  Result := 'SELECT ' +
            '     c.idfiliais "C�digo", ' +
            '     c.fil_cnpj "CNPJ", ' +
            '     c.fil_razao "Raz�o Social", ' +
            '     c.fil_fantasia "Nome", ' +
            '     case ' +
            '       when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '       when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            '     end "Inativo" ' +
            'FROM &SCHEMA.filiais c ' +
            'where c.idclientes = ' + IDCLIENTE + ' and status = ' + QuotedStr('A') ;
end;

function SQLPesquisaFilial: String;
begin
  Result := 'SELECT ' +
            '     c.idfiliais "C�digo", ' +
            '     c.fil_cnpj "CNPJ", ' +
            '     c.fil_razao "Raz�o Social", ' +
            '     c.fil_fantasia "Nome", ' +
            '     case ' +
            '       when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '       when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            '     end "Inativo" ' +
            'FROM &SCHEMA.filiais c ' +
            'where status = ' + QuotedStr('A');
end;

function SQLPesquisaFilialLogo: String;
begin
  Result := 'SELECT ' +
            '     c.idfiliais "C�digo", ' +
            '     c.fil_cnpj "CNPJ", ' +
            '     c.fil_razao "Raz�o Social", ' +
            '     c.fil_fantasia "Nome", ' +
            '     case ' +
            '       when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '       when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            '     end "Inativo", ' +
            '     c.fil_logo "Logomarca" ' +
            'FROM &SCHEMA.filiais c ' +
            'where status = ' + QuotedStr('A');
end;

end.



