unit uTipoContato;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons;

const
  SCRIPT_TABELATIPOSCONTATOS = ' CREATE TABLE tiposcontatos ( ' +
                               '  idtiposcontatos int(11) NOT NULL,' +
                               '  tpcon_descricao varchar(100) DEFAULT NULL,' +
                               '  dt_criacao TIMESTAMP DEFAULT 0,' +
                               '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                               '  usu_criacao INT(11) DEFAULT NULL,' +
                               '  usu_modificacao INT(11) DEFAULT NULL,' +
                               '  inativo CHAR(1) DEFAULT NULL,' +
                               '  status CHAR(1) DEFAULT NULL,' +
                               '  PRIMARY KEY (idtiposcontatos)' +
                               ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';

  PREENCHE_TABELACAMPOS_TIPOSCONTATOS :  array[0..1] of array[0..1] of string = (('idtiposcontatos',   'C�digo do tipo de contato'),
                                                                                 ('tpcon_descricao', 'Descri��o do contato'));

function SQLPesquisaTipoContato: String;



implementation

uses uMensagens;


function SQLPesquisaTipoContato: String;
begin
  Result := 'SELECT tp.idtiposcontatos "C�digo", tp.tpcon_descricao "Descri��o" ' +
            'FROM &SCHEMA.tiposcontatos tp ' +
            'where tp.status = ' + QuotedStr('A');
end;

end.
