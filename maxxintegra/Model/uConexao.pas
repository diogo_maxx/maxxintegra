unit uConexao;

interface

uses IniFiles, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem, RTTI, TypInfo;

function CriaConexao(FDCon: TFDConnection; Tansaction: TFDTransaction = nil; ComponenteRetorno: TComponent = nil): Boolean;
//procedure SetarValorComponente(Component: TComponent; Valor: Variant);

implementation

uses uMensagens, uUtilidades;

function CriaConexao(FDCon: TFDConnection; Tansaction: TFDTransaction = nil; ComponenteRetorno: TComponent = nil): Boolean;
var
  ArqIni: TIniFile;
  PASTACFG, DATABASE, HOST, PORTA: String;
begin
  try
    Result := True;
    try
      PASTACFG := StringReplace(UpperCase(gsAppPath), '\EXE\', '\Conf\', []);
      ArqIni := TIniFile.Create(PastaCFG + 'conf.cfg');

      DATABASE := ArqIni.ReadString('BANCO', 'Database', '');
      HOST     := ArqIni.ReadString('BANCO', 'Host',     '');
      PORTA    := ArqIni.ReadString('BANCO', 'Porta',    '');
    except
      on E: Exception do
      begin
        Result := False;
      end;
    end;

    if Result then
    begin
      try
        FDCon.Connected              := False;
        FDCon.LoginPrompt            := False;
        FDCon.UpdateOptions.LockWait := False;

        FDCon.Params.Clear;
        FDCon.Params.Add('DriverID=MySQL');
        FDCon.Params.Add('Database=' + DATABASE);
        FDCon.Params.Add('User_Name=root');
        FDCon.Params.Add('Password=root');
        FDCon.Params.Add('SERVER=' + HOST);
        FDCon.Params.Add('Port=' + PORTA);


        FDCon.Connected := True;
      except
        on E: Exception do
        begin
          if E.Message <> 'Operation aborted' then
          begin
            if Pos('10065', E.Message) > 0 then
            begin
              if ComponenteRetorno <> nil then
                SetarValorComponente(ComponenteRetorno, 'N�o foi poss�vel conectar ao banco de dados! ' + #13 + ' Verifique sua conex�o e tente novamente. ' + E.Message)
              else
                Mensagem('E', 'N�o foi poss�vel conectar ao banco de dados!' + #13 + ' Verifique sua conex�o e tente novamente. ' + E.Message, nil);
            end
            else
            begin
              if ComponenteRetorno <> nil then
                SetarValorComponente(ComponenteRetorno, E.Message)
              else Mensagem('E', E.Message, nil);
            end;
          end;
          Result := False;
        end;
      end;
    end;
  finally
    if ArqIni <> nil then
      FreeAndNil(ArqIni);
  end;
end;

//procedure SetarValorComponente(Component: TComponent; Valor: Variant);
//var
//  Contexto  : TRttiContext;
//  Tipo      : TRttiType;
//begin
//  if Valor <> Null then
//  begin
//    try
//      try
//        Contexto := TRttiContext.Create;
//        Tipo     := Contexto.GetType(Component.ClassType);
//
//        if not(VarIsEmpty(Valor) or VarIsNull(Valor)) then
//        begin
//          if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TField)) then
//            Tipo.GetProperty('AsVariant').SetValue(Component, TValue.From(Valor))
//          else
//          if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) then
//          begin
//            if Tipo.GetProperty('Field') = nil then
//            begin
//              if Tipo.GetProperty('Value') <> nil then
//                 Tipo.GetProperty('Value').SetValue(Component, TValue.From(Valor))
//              else if Tipo.GetProperty('Text') <> nil then
//                 Tipo.GetProperty('Text').SetValue(Component, vartostr(Valor));
//            end
//            else
//              SetarValorComponente(TField(Tipo.GetProperty('Field').GetValue(Component).AsObject), Valor);
//          end;
//        end
//        else
//        begin
//          if Tipo.GetMethod('Parent') <> nil then
//          begin
//            if Tipo.GetMethod('Clear') <> nil then
//              Tipo.GetMethod('Clear').Invoke(Component, []);
//          end
//          else
//          begin
//            if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TField)) then
//              Tipo.GetProperty('AsVariant').SetValue(Component, TValue.From(''))
//            else
//            if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) then
//            begin
//              if Tipo.GetProperty('Field') = nil then
//              begin
//                if Tipo.GetProperty('Value') <> nil then
//                   Tipo.GetProperty('Value').SetValue(Component, TValue.From(''))
//                else if Tipo.GetProperty('Text') <> nil then
//                   Tipo.GetProperty('Text').SetValue(Component, vartostr(''));
//              end
//              else
//                SetarValorComponente(TField(Tipo.GetProperty('Field').GetValue(Component).AsObject), '');
//            end;
//          end;
//        end;
//      finally
//        Contexto.Free;
//      end;
//    except
//      on E : Exception do
//        Mensagem('E', 'Erro SetarValorComponente: ' + e.Message + #13 + Tipo.GetProperty('Name').GetValue(Component).AsString, nil);
//    end;
//  end;
//end;



end.
