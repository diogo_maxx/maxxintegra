unit uSistemaIntegrado;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELASISTEMASINTEGRADOS = ' CREATE TABLE sistemasintegrados ( ' +
                   '  idsistemasintegrados int(11) NOT NULL,' +
                   '  idclientes int(11) DEFAULT NULL,' +
                   '  idfiliais int(11) DEFAULT NULL,' +
                   '  sis_nome varchar(100) DEFAULT NULL,' +
                   '  sis_tpcon char(1) DEFAULT NULL,' +
                   '  sis_driverid char(1) DEFAULT NULL,' +
                   '  sis_database varchar(100) DEFAULT NULL,' +
                   '  sis_schema varchar(100) DEFAULT NULL,' +
                   '  sis_username varchar(20) DEFAULT NULL,' +
                   '  sis_password varchar(50) DEFAULT NULL,' +
                   '  sis_port int(11) DEFAULT NULL,' +
                   '  sis_server varchar(100) DEFAULT NULL,' +
                   '  sis_protocol char(1) DEFAULT NULL,' +
                   '  sis_baseurl LONGTEXT DEFAULT NULL,' +
                   '  sis_endpointlogin LONGTEXT DEFAULT NULL,' +
                   '  sis_wsusername varchar(100) DEFAULT NULL, ' +
                   '  sis_wspassword varchar(100) DEFAULT NULL, ' +
                   '  sis_triggerinsert LONGTEXT DEFAULT NULL,' +
                   '  sis_triggerupdate LONGTEXT DEFAULT NULL,' +
                   '  sis_triggerdelete LONGTEXT DEFAULT NULL,' +
                   '  dt_criacao TIMESTAMP DEFAULT 0,' +
                   '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                   '  usu_criacao INT(11) DEFAULT NULL,' +
                   '  usu_modificacao INT(11) DEFAULT NULL,' +
                   '  inativo CHAR(1) DEFAULT NULL,' +
                   '  status CHAR(1) DEFAULT NULL,' +
                   '  PRIMARY KEY (idsistemasintegrados)' +
                   ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOS :  array[0..15] of array[0..1] of string = (('idsistemasintegrados', 'C�digo do sistema integrado'),
                                                                                        ('sis_nome',          'Nome do sistema'),
                                                                                        ('sis_baseurl',       'URL onde o servi�o est� rodando'),
                                                                                        ('sis_endpointlogin', 'Endpoint para realizar o login no servi�o'),
                                                                                        ('sis_wsusername',    'URL onde o servi�o est� rodando'),
                                                                                        ('sis_wspassword',    'Endpoint para realizar o login no servi�o'),
                                                                                        ('sis_driverid',      'Conex�o via banco do script - Driver de conex�o'),
                                                                                        ('sis_database',      'Conex�o via banco do script - database'),
                                                                                        ('sis_username',      'Conex�o via banco do script - nome de usu�rio'),
                                                                                        ('sis_password',      'Conex�o via banco do script - senha'),
                                                                                        ('sis_port',          'Conex�o via banco do script - porta'),
                                                                                        ('sis_server',        'Conex�o via banco do script - servidor'),
                                                                                        ('conf_triggerinsert', 'Script para cria��o da trigger after insert'),
                                                                                        ('conf_triggerupdate', 'Script para cria��o da trigger after update'),
                                                                                        ('conf_triggerdelete', 'Script para cria��o da trigger before delete'),
                                                                                        ('sis_protocol',      'Conex�o via banco do script - protocolo de conex�o'));

function PesquisaSistemasIntegrados: String;

implementation

function PesquisaSistemasIntegrados: String;
begin
  Result := 'select idsistemasintegrados "C�digo", sis_nome "Nome", ' +
            ' case ' +
            '   when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '   when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            ' end "Inativo" ' +
            'from &SCHEMA.sistemasintegrados s ' +
            ' where s.status = ' + QuotedStr('A');
end;


end.
