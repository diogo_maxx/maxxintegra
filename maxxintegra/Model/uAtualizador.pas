unit uAtualizador;

interface

uses IniFiles, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;



function VerificaVersao(FDQry: TFDQuery): Boolean;
function VerificarTabelas(FDQry: TFDQuery): Boolean;
function PreencheTabelaCampos(FDQry: TFDQuery): Boolean;
function PreencheTabelaFormularios(FDQry: TFDQuery): Boolean;
function AtualizarBanco(ComponenteRetorno: TComponent = nil): Boolean;

const
  TABLES : array[0..14] of String = ('campos', 'usuarios', 'codigos', 'formularios', 'sistemasintegrados',
                                    'clientes', 'filiais', 'clientes_conexoes', 'tiposcontatos', 'contatos', 'tiposcripts',
                                    'scripts', 'scriptsitem', 'sistemasintegrados_conf', 'conf_json');

implementation

uses uConstantes, uConexao, uCampos, uUsuario, ufrmLogin, uCodigos, uMensagens, uFormulario, uSistemaIntegrado,
  uCliente, uFilial, uClienteConexao, uContatos, uTipoContato, uTipoScript, uScript, uScriptItem, uSistemaIntegradoConf,
  uConfJSON;

function AtualizarBanco(ComponenteRetorno: TComponent = nil): Boolean;
var
  ArqIni: TIniFile;
  PASTACFG, DATABASE, HOST, PORTA: String;
  FDCon: TFDConnection;
  FDQry: TFDQuery;
begin
  try
    try
      Result := True;

      FDCon := TFDConnection.Create(nil);
      FDQry := TFDQuery.Create(nil);

      if CriaConexao(FDCon, nil, ComponenteRetorno) then
      begin

        FDQry.Connection := FDCon;

        if not VerificaVersao(FDQry) then
        begin
          if not VerificarTabelas(FDQry) then
            Abort;

          if not PreencheTabelaCampos(FDQry) then
            Abort;

          if not PreencheTabelaFormularios(FDQry) then
            Abort;

          //////////////////////////////////////////////////////
          ///  Atualizando informa��es de vers�o do sistema  ///
          //////////////////////////////////////////////////////
          FDQry.Close;
          FDQry.SQL.Clear;
          FDQry.SQL.Add('UPDATE ' + FDQry.Connection.Params.Database + '.campos ' +
                        'SET ' +
                        'descricao = ' + QuotedStr(Versao + ' ' + DATA_VERSAO + ' | Att=' + FormatDateTime('DD/MM/YYYY HH:MM', Now)) +
                        'WHERE campo = ' + QuotedStr('versao') +
                        '  and tabela is null');
          FDQry.ExecSQL;
        end;
      end
      else
        Result := False;
    finally
      FDQry.Close;
      FDCon.Connected := False;
      FreeAndNil(FDQry);
      FreeAndNil(FDCon);
    end;
  except
    on E: Exception do
    begin
      if E.Message <> 'Operation aborted' then
        Mensagem('E', E.Message, nil);
      Result := False;
    end;
  end;
end;

function VerificaVersao(FDQry: TFDQuery): Boolean;
var
  Vrs: TStringList;
begin
  try

    Result := True;
    //Verifica a vers�o se est� diferente

    Vrs := TStringList.Create;

    FDQry.Close;
    FDQry.SQL.Clear;
    FDQry.SQL.Add('select * from ' + FDQry.Connection.Params.Database + '.campos c ' +
                  'where c.campo = ' + QuotedStr('versao') +
                  '  and c.tabela is null');
    FDQry.Open;

    Vrs.StrictDelimiter := False;
    Vrs.Delimiter       := '|';
    Vrs.StrictDelimiter := True;
    Vrs.DelimitedText   := FDQry.FieldByName('descricao').AsString;

    if Trim(Vrs[0]) <> (VERSAO + ' ' + DATA_VERSAO) then
      Result := False;

  except
    on E: Exception do
      Result := False;
  end;
end;

function VerificarTabelas(FDQry: TFDQuery): Boolean;
var
  I: Integer;
  FDExec: TFDQuery;
procedure CriaTabela(Script: String);
begin
  if FDQry.IsEmpty then
  begin
    FDExec.Close;
    FDExec.SQL.Clear;
    FDExec.SQL.Add(Script);
    FDExec.ExecSQL;
  end;
end;

begin
  try
    Result := True;
    try
      FDExec            := TFDQuery.Create(nil);
      FDExec.Connection := FDQry.Connection;

      FDQry.Close;
      FDQry.SQL.Clear;
      FDQry.SQL.Add('select * from information_schema.tables ' +
                    'where table_schema = ' + QuotedStr(FDQry.Connection.Params.Database));
      FDQry.Open;

      FDQry.DisableControls;

      for I := 0 to Length(TABLES) -1 do
      begin
        //localizando se a tabela est� criada
        FDQry.Filtered := False;
        FDQry.Filter   := 'TABLE_NAME = ' + QuotedStr(TABLES[I]);
        FDQry.Filtered := True;

        case AnsiIndexStr(TABLES[I], TABLES) of
          0: //campos
            CriaTabela(SCRIPT_TABELACAMPOS);
          1: //usuario
          begin
            CriaTabela(SCRIPT_TABELAUSUARIOS);
            if FDQry.IsEmpty then
              CriarUsuarioSistema(FDExec);
          end;
          2: //codigos
          begin
            CriaTabela(SCRIPT_TABELACODIGOS);
            if FDQry.IsEmpty then
              InserirCodigosPadroes(FDExec);
          end;
          3: //formularios
            CriaTabela(SCRIPT_TABELAFORMULARIOS);
          4: //sistemasintegrados
            CriaTabela(SCRIPT_TABELASISTEMASINTEGRADOS);
          5: //cliente
            CriaTabela(SCRIPT_TABELACLIENTE);
          6: //filial
            CriaTabela(SCRIPT_TABELAFILIAL);
          7: //clientes conex�es
            CriaTabela(SCRIPT_TABELACLIENTECONEXAO);
          8: //tiposcontatos
            CriaTabela(SCRIPT_TABELATIPOSCONTATOS);
          9: //CONTATOS
            CriaTabela(SCRIPT_TABELACONTATOS);
          10: //tipo de script
            CriaTabela(SCRIPT_TABELATIPOSCRIPT);
          11: //script
            CriaTabela(SCRIPT_TABELASCRIPT);
          12: //script item
            CriaTabela(SCRIPT_TABELASCRIPTITEM);
          13: //sistemas integrados conf
            CriaTabela(SCRIPT_TABELASISTEMASINTEGRADOSCONF);
          14: //confjson
            CriaTabela(SCRIPT_TABELACONFJSON);
        end;
      end;
    except
      on E: Exception do
      begin
        Mensagem('E', 'Erro ao atualizar banco: ' + E.Message, nil);
        Result := False;
      end;
    end;
  finally
    FDQry.Filtered := False;
    FDQry.EnableControls;
    FDQry.Close;
    FreeAndNil(FDExec);
  end;
end;

function PreencheTabelaCampos(FDQry: TFDQuery): Boolean;
var
  FDExec: TFDQuery;
  I, J: Integer;

procedure GeraRegistroTabelaCampo(Tabela, Campo, Descricao: String);
begin
  FDQry.Filtered := False;
  FDQry.Filter   := 'TABELA ' + ifthen(Tabela = '', 'is null ', ' = ' + QuotedStr(Tabela))  + ' and CAMPO = ' + QuotedStr(Campo);
  FDQry.Filtered := True;

  if FDQry.IsEmpty then
  begin
    FDExec.Close;
    FDExec.SQL.Clear;
    FDExec.SQL.Add('INSERT INTO ' + FDExec.Connection.Params.Database + '.campos (tabela, campo, descricao) ' +
                   'VALUES (' + IfThen(Tabela <> '', QuotedStr(Tabela), 'null') + ', ' + QuotedStr(Campo) + ', ' + QuotedStr(Descricao) + ')');
    FDExec.ExecSQL;
  end;
end;

begin
  try
    Result := True;
    FDExec            := TFDQuery.Create(nil);
    FDExec.Connection := FDQry.Connection;
    try
      FDQry.Close;
      FDQry.SQL.Clear;
      FDQry.SQL.Add('select * from ' + FDQry.Connection.Params.Database + '.campos');
      FDQry.Open;

      FDQry.DisableControls;

      for I := 0 to Length(TABLES) -1 do
      begin
        case AnsiIndexStr(TABLES[I], TABLES) of
          0: //campos
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS) -1 do
              GeraRegistroTabelaCampo('', PREENCHE_TABELACAMPOS[J, 0], PREENCHE_TABELACAMPOS[J, 1]);
          end;
          1: //usuario
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_USUARIO) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_USUARIO[J, 0], PREENCHE_TABELACAMPOS_USUARIO[J, 1]);
          end;
          2: //campos
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_CODIGOS) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_CODIGOS[J, 0], PREENCHE_TABELACAMPOS_CODIGOS[J, 1]);
          end;
          3: //formularios
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_FORMULARIOS) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_FORMULARIOS[J, 0], PREENCHE_TABELACAMPOS_FORMULARIOS[J, 1]);
          end;
          4: //sistemasintegrados
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOS) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOS[J, 0], PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOS[J, 1]);

          end;
          5: //cliente
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_CLIENTE) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_CLIENTE[J, 0], PREENCHE_TABELACAMPOS_CLIENTE[J, 1]);
          end;
          6: //filial
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_FILIAL) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_FILIAL[J, 0], PREENCHE_TABELACAMPOS_FILIAL[J, 1]);
          end;
          7: //filial
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_CLIENTECOENXAO) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_CLIENTECOENXAO[J, 0], PREENCHE_TABELACAMPOS_CLIENTECOENXAO[J, 1]);
          end;
          8: //TIPOCONTATOS
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_TIPOSCONTATOS) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_TIPOSCONTATOS[J, 0], PREENCHE_TABELACAMPOS_TIPOSCONTATOS[J, 1]);
          end;
          9: //CONTATOS
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_CONTATOS) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_CONTATOS[J, 0], PREENCHE_TABELACAMPOS_CONTATOS[J, 1]);
          end;
          10: //tipo de scripts
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_TIPOSCRIPT) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_TIPOSCRIPT[J, 0], PREENCHE_TABELACAMPOS_TIPOSCRIPT[J, 1]);
          end;
          11: //scripts
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_SCRIPT) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_SCRIPT[J, 0], PREENCHE_TABELACAMPOS_SCRIPT[J, 1]);
          end;
          12: //scripts item
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_SCRIPTITEM) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_SCRIPTITEM[J, 0], PREENCHE_TABELACAMPOS_SCRIPTITEM[J, 1]);
          end;
          13: //sistemasintegradosCONF
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOSCONF) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOSCONF[J, 0], PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOSCONF[J, 1]);
          end;
          14: //confjson
          begin
            for J := 0 to Length(PREENCHE_TABELACAMPOS_CONFJSON) -1 do
              GeraRegistroTabelaCampo(TABLES[I], PREENCHE_TABELACAMPOS_CONFJSON[J, 0], PREENCHE_TABELACAMPOS_CONFJSON[J, 1]);
          end;

        end;
      end;
    except
      on E: Exception do
      begin
        Mensagem('E', 'Erro ao preencher a tabela de campos: ' + E.Message, nil);
        Result := False;
      end;
    end;
  finally
    FDQry.Filtered := False;
    FDQry.EnableControls;
    FDQry.Close;
    FreeAndNil(FDExec);
  end;
end;

function PreencheTabelaFormularios(FDQry: TFDQuery): Boolean;
procedure Executa(form_pai, form_nome, form_descricao: String);
begin
  FDQry.Close;
  FDQry.SQL.Clear;
  FDQry.SQL.Add('INSERT INTO ' + FDQry.Connection.Params.Database + '.formularios (form_pai, form_nome, form_descricao) ' +
                'VALUES (' + form_pai + ', ' + QuotedStr(form_nome) + ', ' + QuotedStr(form_descricao) + ')');
  FDQry.ExecSQL;
end;

begin
  try
    Result := True;
    try
      FDQry.Close;
      FDQry.SQL.Clear;
      FDQry.SQL.Add('delete from formularios');
      FDQry.ExecSQL;

      FDQry.Close;
      FDQry.SQL.Clear;
      FDQry.SQL.Add('ALTER TABLE formularios AUTO_INCREMENT = 1');
      FDQry.ExecSQL;

      Executa('null', 'Pesquisar', 'Pesquisar');
      Executa('null', 'Cadastros', 'Cadastros');
      Executa('null', 'Script', 'Script');
      Executa('2', 'frmCadUsuarios', 'Usuarios');
      Executa('2', 'frmCadSistemasIntegrados', 'Sistemas Integrados');
      Executa('2', 'frmCadClientes', 'Clientes');
      Executa('2', 'frmCadTiposContatos', 'Tipos de contato');
      Executa('2', 'frmCadTipoScript', 'Tipos de Scripts');
      Executa('3', 'frmMovMontaScript', 'Montar scripts');
    except
      on E: Exception do
      begin
        Mensagem('E', 'Erro ao preencher a tabela de formul�rios: ' + E.Message, nil);
        Result := False;
      end;
    end;
  finally
    FDQry.EnableControls;
    FDQry.Close;
  end;
end;


end.
