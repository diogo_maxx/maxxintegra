unit uTipoScript;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons;

const
  SCRIPT_TABELATIPOSCRIPT = ' CREATE TABLE tiposcripts ( ' +
                               '  idtiposcripts int(11) NOT NULL,' +
                               '  tpscr_descricao varchar(100) DEFAULT NULL,' +
                               '  dt_criacao TIMESTAMP DEFAULT 0,' +
                               '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                               '  usu_criacao INT(11) DEFAULT NULL,' +
                               '  usu_modificacao INT(11) DEFAULT NULL,' +
                               '  inativo CHAR(1) DEFAULT NULL,' +
                               '  status CHAR(1) DEFAULT NULL,' +
                               '  PRIMARY KEY (idtiposcripts)' +
                               ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';

  PREENCHE_TABELACAMPOS_TIPOSCRIPT :  array[0..1] of array[0..1] of string = (('idtiposcripts',   'C�digo do tipo do script'),
                                                                              ('tpscr_descricao', 'Descri��o do script'));

function SQLPesquisaTipoScript: String;



implementation

uses uMensagens;


function SQLPesquisaTipoScript: String;
begin
  Result := 'SELECT tp.idtiposcripts "C�digo", tp.tpscr_descricao "Descri��o" ' +
            'FROM &SCHEMA.tiposcripts tp ' +
            'where tp.status = ' + QuotedStr('A');
end;

end.
