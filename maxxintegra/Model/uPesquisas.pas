///////////////////////////////////////////////////////////////////////////////////////
///                                                                                 ///
///   Projeto: MaxxIntegra - Integrador de dados de ERP's para nossas solu��es web  ///
///                                                                                 ///
/// Esta biblioteca tem a fun��o de realizar as buscas de dados no banco de dados   ///
/// centralizando assim, toda a problem�tica em um �nico local                      ///
///                                                                                 ///
/// Desenvolvido por: Diogo Aires Cardoso (07-03-2018)                              ///
///                                                                                 ///
///////////////////////////////////////////////////////////////////////////////////////
unit uPesquisas;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, Forms, RTTI;

   /// <summary>Procedure que executa o formul�rio de pesquisa e retorna os valores passados como par�metro</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="SQL">Qual o sql que ser� feita a busca</param>
   /// <param name="Campos">Array dinamico que recebe os campos do SQL para retorno</param>
   /// <param name="Componentes">Array dinamico que recebe os componentes para retorno (Obrigat�rio ter a mesma posi��o que o Array de campos)</param>
   procedure Pesquisar(SQL: String; Campos: array of String; Componentes: array of TComponent); overload;
   /// <summary>Procedure que realiza a busca no banco e retorna os valores passados como par�metro</summary>
   /// <author>Diogo Cardoso</author>
   /// <param name="SQL">Qual o sql que ser� feita a busca</param>
   /// <param name="Filtro">Refinamento da busca</param>
   /// <param name="Campos">Array dinamico que recebe os campos do SQL para retorno</param>
   /// <param name="Componentes">Array dinamico que recebe os componentes para retorno (Obrigat�rio ter a mesma posi��o que o Array de campos)</param>
   /// <param name="ValidarInativo">Se ir� trazer os valores que est�o inativados ou n�o</param>
   /// <param name="ValidarStatus">Se ir� trazer os valores que est�o exclu�dos ou n�o</param>
   procedure Pesquisar(SQL, Filtro: String; Campos: array of String; Componentes: array of TComponent; ValidarInativo: Boolean = True; ValidarStatus: Boolean = True); overload;

implementation

uses ufrmPesquisa, uUtilidades, uConexao, uMensagens;

procedure Pesquisar(SQL: String; Campos: array of String; Componentes: array of TComponent);
var
  I: Integer;
  Contexto : TRttiContext;
  Tipo     : TRttiType;
  Metodo   : TValue;
begin
  try
    Application.CreateForm(TfrmPesquisa, frmPesquisa);

    frmPesquisa.fdPesquisa.Close;
    frmPesquisa.fdPesquisa.SQL.Clear;
    frmPesquisa.fdPesquisa.SQL.Add(SQL);
    frmPesquisa.ShowModal;

    if frmPesquisa.ModalResult = mrOk then
    begin
      for I := 0 to Length(Campos) -1 do
      begin
        /////////////////////////////////////////////////////////////////////////////////////
        ///  Se o campo for um c�digo, geralmente o que est� fazendo a busca...           ///
        ///  retira-se o evento de exit do mesmo para que n�o fa�a 2 pesquisas no banco   ///
        ///  e avan�a o foco da tela e seta o restante dos valores.                       ///
        /////////////////////////////////////////////////////////////////////////////////////
        if Campos[I] = 'C�digo' then
        begin
          try
            Contexto := TRttiContext.Create;
            Tipo     := Contexto.GetType(Componentes[I].ClassInfo);
            //retirando o evento
            if Tipo.GetProperty('OnExit') <> nil then
            begin
              if not Tipo.GetProperty('OnExit').GetValue(Componentes[i]).IsEmpty then
              begin
                 Metodo := Tipo.GetProperty('OnExit').GetValue(Componentes[i]);
                 Tipo.GetProperty('OnExit').SetValue(Componentes[i], nil);
              end;
            end;
            //setando o valor no componente
            if frmPesquisa.fdPesquisa.FindField(Campos[I]) <> nil then
              SetarValorComponente(Componentes[I], frmPesquisa.fdPesquisa.FindField(Campos[I]).AsVariant);
            //avan�ando o foco
            TForm(Componentes[I].Owner).Perform(CM_DialogKey, VK_TAB, 0);
            //seta novamente o evento
            if (not Metodo.IsEmpty) then
              Tipo.GetProperty('OnExit').SetValue(Componentes[i], Metodo);
          finally
            Contexto.Free;
          end;
        end
        else
        if frmPesquisa.fdPesquisa.FindField(Campos[I]) <> nil then
          SetarValorComponente(Componentes[I], frmPesquisa.fdPesquisa.FindField(Campos[I]).AsVariant);
      end;
    end;
  finally
    FreeAndNil(frmPesquisa);
  end;
end;

procedure Pesquisar(SQL, Filtro: String; Campos: array of String; Componentes: array of TComponent; ValidarInativo: Boolean = True; ValidarStatus: Boolean = True);
var
  Con   : TFDConnection;
  FDQry : TFDQuery;
  I     : Integer;
begin
  try
    Con   := TFDConnection.Create(nil);
    FDQry := TFDQuery.Create(nil);

    CriaConexao(Con);

    FDQry.Connection := Con;

    FDQry.Close;
    FDQry.SQL.Clear;
    FDQry.SQL.Add(SQL + Filtro + ifthen(ValidarInativo, ' and inativo = ' + QuotedStr('N'), '') +
                                 ifthen(ValidarStatus, ' and status = ' + QuotedStr('A'), ''));

    if FDQry.MacroByName('SCHEMA') <> nil then
      FDQry.MacroByName('SCHEMA').AsRaw := FDQry.Connection.Params.Database;

    FDQry.Open;

    if not FDQry.IsEmpty then
    begin
      for I := 0 to Length(Campos) -1 do
      begin
        if FDQry.FindField(Campos[I]) <> nil then
          SetarValorComponente(Componentes[I], FDQry.FindField(Campos[I]).AsVariant);
      end;
    end
    else
    begin
      Mensagem('C', 'N�o foi localizado nenhum registro!');
      for I := 0 to Length(Campos) -1 do
      begin
        if FDQry.FindField(Campos[I]) <> nil then
          SetarValorComponente(Componentes[I], '');
      end;
    end;

  finally
    Con.Close;
    FDQry.Close;
    FreeAndNil(Con);
    FreeAndNil(FDQry);
  end;
end;

end.
