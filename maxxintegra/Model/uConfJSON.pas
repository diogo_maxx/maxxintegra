unit uConfJSON;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELACONFJSON =
      'CREATE TABLE conf_json ( ' +
      '  idconf_json INT NOT NULL, ' +
      '  idsistemasintegrados INT NULL, ' +
      '  idconf INT NULL, ' +
      '  json_camposql VARCHAR(45) NULL, ' +
      '  json_campojson VARCHAR(45) NULL, ' +
      '  dt_criacao TIMESTAMP DEFAULT 0,' +
      '  dt_modificacao TIMESTAMP DEFAULT 0,' +
      '  usu_criacao INT(11) DEFAULT NULL,' +
      '  usu_modificacao INT(11) DEFAULT NULL,' +
      '  inativo CHAR(1) DEFAULT NULL,' +
      '  status CHAR(1) DEFAULT NULL,' +
      '  PRIMARY KEY (idconf_json) ' +
      ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_CONFJSON :  array[0..2] of array[0..1] of string = (('idconf_json',     'C�digo da configura��o'),
                                                                             ('json_camposql',        'Tabela origem'),
                                                                             ('json_campojson',     'Tabela pai da origem'));
function SQLPesquisaCONFJSON: String;

implementation

function SQLPesquisaCONFJSON: String;
begin
  Result := 'select idconf_json "C�digo", json_camposql "Nome", ' +
            ' case ' +
            '   when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '   when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            ' end "Inativo" ' +
            'from &SCHEMA.conf_json s ' +
            ' where s.status = ' + QuotedStr('A');
end;

end.
