unit udmMaxxIntegra;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, FireDAC.Phys.MySQLDef, FireDAC.Comp.UI, FireDAC.Phys.MySQL, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, cxClasses, cxLocalization, IWSystem, Threading, cxStyles;

type
  TdmMaxxIntegra = class(TDataModule)
    FDConnection: TFDConnection;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    fdUsuario: TFDQuery;
    fdUsuarioidusuarios: TIntegerField;
    fdUsuariousu_descricao: TStringField;
    fdUsuariousu_nome: TStringField;
    fdUsuariousu_senha: TStringField;
    fdUsuariodt_criacao: TSQLTimeStampField;
    fdUsuariodt_modificacao: TSQLTimeStampField;
    fdUsuariousu_criacao: TIntegerField;
    fdUsuariousu_modificacao: TIntegerField;
    fdUsuarioinativo: TStringField;
    fdUsuariostatus: TStringField;
    fdNovoCodigo: TFDQuery;
    cxTraducao: TcxLocalizer;
    StyleGrid: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    procedure FDConnectionBeforeConnect(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure PreencheMacros;
    { Private declarations }
  public
    { Public declarations }
    function NovoID(Tabela, Campo: String): Integer;
  end;

var
  dmMaxxIntegra: TdmMaxxIntegra;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uConexao;

{$R *.dfm}


procedure TdmMaxxIntegra.DataModuleCreate(Sender: TObject);
begin
  cxTraducao.LoadFromFile(UpperCase(gsAppPath) + 'dev.ini');
  cxTraducao.LanguageIndex := 1;
  cxTraducao.Active := true;

  TThread.Queue(nil,
  procedure
  begin
    CriaConexao(FDConnection);
  end);
end;

procedure TdmMaxxIntegra.FDConnectionBeforeConnect(Sender: TObject);
begin
  PreencheMacros;
end;

procedure TdmMaxxIntegra.PreencheMacros;
var
  I: Integer;
begin
  for I := 0 to ComponentCount -1 do
  begin
    if Components[I] is TFDQuery then
    begin
      if TFDQuery(Components[I]).FindMacro('SCHEMA') <> nil then
        TFDQuery(Components[I]).FindMacro('SCHEMA').AsRaw := FDConnection.Params.Database;
    end;
  end;
end;

function TdmMaxxIntegra.NovoID(Tabela, Campo: String): Integer;
begin
  try
    fdNovoCodigo.Close;
//    fdNovoCodigo.Params[0].AsString := QuotedStr(Tabela);
//    fdNovoCodigo.Params[1].AsString := QuotedStr(Campo);
    fdNovoCodigo.Params[0].AsString := Tabela;
    fdNovoCodigo.Params[1].AsString := Campo;
    fdNovoCodigo.Open;

    if fdNovoCodigo.IsEmpty then
    begin
      fdNovoCodigo.Append;
      fdNovoCodigo.FieldByName('CODIGOS_TABELA').AsString := Tabela;
      fdNovoCodigo.FieldByName('CODIGOS_CAMPO').AsString  := Campo;
      fdNovoCodigo.FieldByName('CODIGOS_VALOR').AsInteger := 1;
      Result := 1;
    end
    else
    begin
      fdNovoCodigo.Edit;
      fdNovoCodigo.FieldByName('CODIGOS_VALOR').AsInteger := fdNovoCodigo.FieldByName('CODIGOS_VALOR').AsInteger + 1;
      Result := fdNovoCodigo.FieldByName('CODIGOS_VALOR').AsInteger;
    end;

    fdNovoCodigo.Post;
    fdNovoCodigo.ApplyUpdates(0);
  finally
    fdNovoCodigo.Close;
  end;
end;


end.
