unit uConstantes;

interface

CONST
	VERSAO         = '1.0.0.2';
	DATA_VERSAO    = '15/02/2018';
	HORA_VERSAO    = '09:00:00';

  ////////////////////////////////////////////
  ///  MASCARAS PARA OS CAMPOS DO SISTEMA  ///
  ////////////////////////////////////////////
  MASK_CEP  = '99999\-999;0;_';
  MASK_CPF  = '999\.999\.999\-99;0;_';
  MASK_CNPJ = '99\.999\.999\/9999\-99;0;_';
  MASK_DATA = '99\/99\/9999;0;_';

  //////////////////////////////////////////
  ///  LAYOUT BARRA TITULO               ///
  //////////////////////////////////////////
  LAYOUT_BARRATITULO        = $00663E24;{ $00663E24;}//$00181818;
  LAYOUT_CORFONTBARRATITULO = $00F8F8F8;
  //////////////////////////////////////////
  ///  LAYOUT MENU                       ///
  //////////////////////////////////////////
  LAYOUT_CORMENUPRIMARIO    = $00663E24;//$00282828;
  LAYOUT_CORSELECIONADOMENU = $00D27619;//$00696969;
  //////////////////////////////////////////
  ///  LAYOUT FORMULARIO                 ///
  //////////////////////////////////////////
//  LAYOUT_COR_FORM            = $00303030;
//  LAYOUT_COR_FORM_TITULO     = $00282828;
//  LAYOUT_COR_FORM_FONTTITULO = $00F8F8F8;
  LAYOUT_COR_FORM            = $00F8F8F8;//$00EEEEEE;
  LAYOUT_COR_FORM_TITULO     = $00663E24;//$00282828;
  LAYOUT_COR_FORM_FONTTITULO = $00F8F8F8;
  //////////////////////////////////////////
  ///  LAYOUT EDIT HABILITADO            ///
  //////////////////////////////////////////
//  LAYOUT_COREDIT_NORMAL      = $00303030;
//  LAYOUT_CORFONT_EDIT_ATIVO  = $00CFCFCF;
  LAYOUT_COREDIT_NORMAL      = $00F8F8F8;
  LAYOUT_CORFONT_EDIT_ATIVO  = $00424242;
  //////////////////////////////////////////
  ///  LAYOUT EDIT DESABILITADO          ///
  //////////////////////////////////////////
//  LAYOUT_COREDIT_DESABILITADO = $00303030;
//  LAYOUT_CORFONT_EDIT_INATIVO = $006F6464;
  LAYOUT_COREDIT_DESABILITADO = $00F0F0F0;//$00AEAEAE;
  LAYOUT_CORFONT_EDIT_INATIVO = $006F6464;
  //////////////////////////////////////////
  ///  LAYOUT EDIT FOCO                  ///
  //////////////////////////////////////////
//  LAYOUT_COREDIT_SELECIONADO = $00303030;
//  LAYOUT_FRAME_SELECIONADO   = $00CFCFCF;
//  LAYOUT_FRAME               = $00CFCFCF;
  LAYOUT_COREDIT_SELECIONADO = $00F8F8F8;
  LAYOUT_FRAME_SELECIONADO   = $00D27619;//$00AEAEAE;
  LAYOUT_FRAME               = $00BDBDBD;
  //////////////////////////////////////////
  ///  LAYOUT BOTOES                     ///
  //////////////////////////////////////////
  LAYOUT_CORBOTAO_HABILITADO        = $00EEEEEE;
  LAYOUT_CORBOTAO_DESABILITADO      = $00663E24;//$00191919;
  LAYOUT_CORBOTAO_DESABILITADODET   = $00F0F0F0;
  LAYOUT_CORBOTAO_DESTAQUE          = $00D27619;//$1FD27619;//$00707070;
  LAYOUT_CORBOTAO_NORMAL            = $00F0F0F0;
  LAYOUT_CORBOTAO_FONTEHABILITADO   = $00181818;//$00F8F8F8;
  LAYOUT_CORBOTAO_FONTEDESABILITADO = $00181818;//$00F8F8F8;//$008E8E8E;
  LAYOUT_CORBOTAO_FONTEDESTAQUE     = $00F8F8F8;
  LAYOUT_CORBOTAO_FONTENORMAL       = $00181818;//$00424242;
  //////////////////////////////////////////
  ///  LAYOUT LABELS                     ///
  //////////////////////////////////////////
  LAYOUT_CORLABEL_NORMAL            = $00424242;
  LAYOUT_CORLABEL_OBRIGATORIO       = $00663E24;//$00D27619;

implementation

end.

