unit uSistemaIntegradoConf;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem;


const
  SCRIPT_TABELASISTEMASINTEGRADOSCONF =
      '  CREATE TABLE sistemasintegrados_conf ( ' +
      '  idconf INT NOT NULL, ' +
      '  idsistemasintegrados INT NULL, ' +
      '  idtiposcripts INT NULL, ' +
      '  conf_tabela VARCHAR(45) NULL, ' +
      '  conf_tabelapai VARCHAR(45) NULL, ' +
      '  conf_codigo VARCHAR(45) NULL, ' +
      '  conf_codigosql VARCHAR(45) NULL, ' +
      '  conf_codigopai VARCHAR(45) NULL, ' +
      '  conf_encarray CHAR(1) DEFAULT ''N'', ' +
      '  conf_scriptbusca LONGTEXT DEFAULT NULL,' +
      '  conf_prioridade char(1) DEFAULT ''N'',' +
      '  conf_baseurl LONGTEXT DEFAULT NULL, ' +
      '  conf_endpoint LONGTEXT DEFAULT NULL, ' +
      '  conf_endpointupdate LONGTEXT DEFAULT NULL, ' +
      '  conf_endpointdelete LONGTEXT DEFAULT NULL, ' +
      '  conf_nomejson varchar(45) null, ' +
      '  conf_maxqntpost LONGTEXT DEFAULT NULL, ' +
      '  dt_criacao TIMESTAMP DEFAULT 0,' +
      '  dt_modificacao TIMESTAMP DEFAULT 0,' +
      '  usu_criacao INT(11) DEFAULT NULL,' +
      '  usu_modificacao INT(11) DEFAULT NULL,' +
      '  inativo CHAR(1) DEFAULT NULL,' +
      '  status CHAR(1) DEFAULT NULL,' +
      '  PRIMARY KEY (idconf) ' +
      ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';


   PREENCHE_TABELACAMPOS_SISTEMASINTEGRADOSCONF :  array[0..6] of array[0..1] of string = (('idconf',             'C�digo da configura��o'),
                                                                                           ('conf_tabela',        'Tabela origem'),
                                                                                           ('conf_tabelapai',     'Tabela pai da origem'),
                                                                                           ('conf_codigo',        'Campo(s) chave(s) da tabela origem'),
                                                                                           ('conf_codigopai',     'Campo(s) chave(s) da tabela pai'),
                                                                                           ('conf_scriptbusca',   'Script para buscar as informa��es no BD'),
                                                                                           ('conf_prioridade',    'Prioridade para o envio para o portal'));
function SQLPesquisaSistemasIntegradosConf: String;

implementation

function SQLPesquisaSistemasIntegradosConf: String;
begin
  Result := 'select idsistemasintegrados "C�digo", sis_nome "Nome", ' +
            ' case ' +
            '   when inativo = ' + QuotedStr('N') + ' then ' + QuotedStr('N�O') +
            '   when inativo = ' + QuotedStr('S') + ' then ' + QuotedStr('SIM') +
            ' end "Inativo" ' +
            'from &SCHEMA.sistemasintegrados s ' +
            ' where s.status = ' + QuotedStr('A');
end;

end.
