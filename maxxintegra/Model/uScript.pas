unit uScript;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons;

const
  SCRIPT_TABELASCRIPT = ' CREATE TABLE scripts ( ' +
                        '  idscripts int(11) NOT NULL,' +
                        '  idsistemasintegrados int(11) NOT NULL,' +
                        '  idclientes int(11) DEFAULT NULL,' +
                        '  idfiliais int(11) DEFAULT NULL,' +
                        '  dt_criacao TIMESTAMP DEFAULT 0,' +
                        '  dt_modificacao TIMESTAMP DEFAULT 0,' +
                        '  usu_criacao INT(11) DEFAULT NULL,' +
                        '  usu_modificacao INT(11) DEFAULT NULL,' +
                        '  inativo CHAR(1) DEFAULT NULL,' +
                        '  status CHAR(1) DEFAULT NULL,' +
                        '  PRIMARY KEY (idscripts)' +
                        ') ENGINE=InnoDB DEFAULT CHARSET=utf8; ';

  PREENCHE_TABELACAMPOS_SCRIPT :  array[0..0] of array[0..1] of string = (('idscripts',    'C�digo da montagem do script'));

function SQLPesquisaScript: String;



implementation

uses uMensagens;


function SQLPesquisaScript: String;
begin
  Result := 'SELECT ' +
            '       s.idscripts "C�digo", ' +
            '       sis.sis_nome "Sistema integrado", ' +
            '       s.scr_padrao "Padr�o", ' +
            '       c.cli_fantasia "Cliente", ' +
            '       f.fil_fantasia "Filial" ' +
            'FROM &SCHEMA.scripts s ' +
            'left join &SCHEMA.sistemasintegrados sis on (sis.idsistemasintegrados = s.idsistemasintegrados) ' +
            'left join &SCHEMA.clientes c on (c.idclientes = s.idclientes) ' +
            'left join &SCHEMA.filiais f on (f.idfiliais = s.idfiliais and f.idclientes = s.idclientes) ' +
            'where s.status = ' + QuotedStr('A');
end;
end.
