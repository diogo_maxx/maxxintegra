program MaxxIntegra;

uses
  Vcl.Forms,
  ufrmPrincipal in 'Views\ufrmPrincipal.pas' {frmPrincipal},
  uConstantes in 'Model\uConstantes.pas',
  ufrmMaster in 'Views\ufrmMaster.pas' {frmMaster},
  ufrmMasterCad in 'Views\ufrmMasterCad.pas' {frmMasterCad},
  uLayoutFormulario in 'Model\uLayoutFormulario.pas',
  ufrmCadUsuarios in 'Views\ufrmCadUsuarios.pas' {frmCadUsuarios},
  uAtualizador in 'Model\uAtualizador.pas',
  uConexao in 'Model\uConexao.pas',
  uCampos in 'Model\uCampos.pas',
  uUsuario in 'Model\uUsuario.pas',
  ufrmLogin in 'Views\ufrmLogin.pas' {frmLogin},
  udmMaxxIntegra in 'Model\udmMaxxIntegra.pas' {dmMaxxIntegra: TDataModule},
  U_Cipher in 'Model\U_Cipher.pas',
  DCPcrypt in 'Model\DCPcrypt.pas',
  IDEA in 'Model\IDEA.pas',
  Md5 in 'Model\Md5.pas',
  Base64 in 'Model\Base64.pas',
  DCPconst in 'Model\DCPconst.pas',
  Sha1 in 'Model\Sha1.pas',
  uCodigos in 'Model\uCodigos.pas',
  ufrmMensagens in 'Views\ufrmMensagens.pas' {frmMensagens},
  uMensagens in 'Model\uMensagens.pas',
  ufrmPesquisa in 'Views\ufrmPesquisa.pas' {frmPesquisa},
  uValidacao in 'Model\uValidacao.pas',
  uUtilidades in 'Model\uUtilidades.pas',
  uFormulario in 'Model\uFormulario.pas',
  ufrmCadSistemasIntegrados in 'Views\ufrmCadSistemasIntegrados.pas' {frmCadSistemasIntegrados},
  uSistemaIntegrado in 'Model\uSistemaIntegrado.pas',
  ufrmCadClientes in 'Views\ufrmCadClientes.pas' {frmCadClientes},
  uCliente in 'Model\uCliente.pas',
  uPesquisas in 'Model\uPesquisas.pas',
  uCidade in 'Model\uCidade.pas',
  uLogradouro in 'Model\uLogradouro.pas',
  uFilial in 'Model\uFilial.pas',
  ufrmMasterDet in 'Views\ufrmMasterDet.pas' {frmMasterDet},
  ufrmCadClientesFiliais in 'Views\ufrmCadClientesFiliais.pas' {frmCadClientesFiliais},
  uClienteConexao in 'Model\uClienteConexao.pas',
  ufrmCadClientesConexao in 'Views\ufrmCadClientesConexao.pas' {frmCadClientesConexao},
  uContatos in 'Model\uContatos.pas',
  uTipoContato in 'Model\uTipoContato.pas',
  ufrmCadContatos in 'Views\ufrmCadContatos.pas' {frmCadContato},
  ufrmCadTipoScripts in 'Views\ufrmCadTipoScripts.pas' {frmCadTipoScripts},
  ufrmCadTiposContatos in 'Views\ufrmCadTiposContatos.pas' {frmCadTiposContatos},
  uTipoScript in 'Model\uTipoScript.pas',
  ufrmMovMontaScript in 'Views\ufrmMovMontaScript.pas' {frmMovMontaScript},
  uScript in 'Model\uScript.pas',
  ufrmMovMontaScriptItem in 'Views\ufrmMovMontaScriptItem.pas' {frmMovMontaScriptItem},
  uScriptItem in 'Model\uScriptItem.pas',
  uSistemaIntegradoConf in 'Model\uSistemaIntegradoConf.pas',
  ufrmCadSistemasIntegradosConf in 'Views\ufrmCadSistemasIntegradosConf.pas' {frmCadSistemasIntegradosConf},
  uConfJSON in 'Model\uConfJSON.pas',
  ufrmCadSistemasIntegradosConfJSON in 'Views\ufrmCadSistemasIntegradosConfJSON.pas' {frmCadSistemasIntegradosConfJSON},
  ufrmLog in 'Views\ufrmLog.pas' {frmLog},
  ufrmLogDetalhes in 'Views\ufrmLogDetalhes.pas' {frmLogDetalhes};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TdmMaxxIntegra, dmMaxxIntegra);
  Application.Run;
end.
