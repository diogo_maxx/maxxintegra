object frmMaster: TfrmMaster
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'frmMaster'
  ClientHeight = 454
  ClientWidth = 808
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnPesquisaImagem: TBitBtn
    Left = 240
    Top = 32584
    Width = 201
    Height = 25
    Caption = 'btnPesquisaImagem'
    Enabled = False
    Glyph.Data = {
      96090000424D9609000000000000360000002800000028000000140000000100
      18000000000060090000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFAFAFAA5A5A5C5C5C5FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFDFDFDCACACADDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFBFBFB9F9F9F606060A5A5A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
      C5C5C59B9B9BCACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFB9E9E9E6464649F
      9F9FFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDC5C5C59D9D9DC5C5C5FCFC
      FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6D6
      D6D6D7D7D7E9E9E9FFFFFFFDFDFD9E9E9E6060609F9F9FFBFBFBFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0E9E9E9EAEA
      EAF2F2F2FFFFFFFEFEFEC4C4C49B9B9BC5C5C5FEFEFEFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E8A6A6A66F6F6F505050535353757575
      B7B7B7B6B6B65454549F9F9FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFF2F2F2C7C7C7A4A4A4959595979797A7A7A7D0D0D0D4
      D4D4939393C5C5C5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFE4E4E4858585606060A3A3A3D1D1D1CECECE989898606060757575B5B5
      B5FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
      F0F0B3B3B39D9D9DC8C8C8E0E0E0DFDFDFC2C2C29E9E9EAAAAAAD4D4D4FEFEFE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE9D9D9D6464
      64CCCCCCFDFDFDFFFFFFFFFFFFFAFAFABFBFBF5F5F5FB7B7B7FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC2C2C2A1A1A1E4E4E4
      FFFFFFFFFFFFFFFFFFFEFEFEDCDCDC9E9E9ECFCFCFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD707070A8A8A8FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFBFBFB999999717171E6E6E6FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFEBEBEBA4A4A4CCCCCCFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFEFEFEC1C1C1A8A8A8F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFC4C4C4565656DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFCDCDCD545454D8D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFDFDFDF979797EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF
      DFDF979797EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      C3C3C3545454E3E3E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1D1D15151
      51D6D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDE96
      9696ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1969696E9E9E9
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D76C6C6CB2B2
      B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDA3A3A36F6F6FE6E6E6FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E8A1A1A1D2D2D2FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C8A5A5A5F1F1F1FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC969696696969DADADAFFFFFFFF
      FFFFFFFFFFFEFEFECDCDCD5F5F5FA6A6A6FEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDBDBDBDA5A5A5ECECECFFFFFFFFFFFFFFFF
      FFFFFFFFE4E4E49E9E9EC7C7C7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFDBDBDB7C7C7C696969B3B3B3E3E3E3DFDFDFA8A8A8
      656565868686E6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFEAEAEAADADADA4A4A4D2D2D2ECECECEAEAEACCCCCCA1A1A1B3
      B3B3F1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFDCDCDC9696966B6B6B5454545656567070709C9C9CE6E6E6FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFEBEBEBBDBDBDA2A2A2969696979797A4A4A4C2C2C2F1F1F1FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFDFDFDD7D7D7C2C2C2C4C4C4DCDCDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      E8E8E8DEDEDEDFDFDFEBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    NumGlyphs = 2
    TabOrder = 0
    TabStop = False
    Visible = False
  end
  object pnlBordas: TRzPanel
    Left = 0
    Top = 0
    Width = 808
    Height = 454
    Align = alClient
    BorderOuter = fsNone
    ParentColor = True
    TabOrder = 1
    object pnlMaster: TRzPanel
      Left = 0
      Top = 25
      Width = 808
      Height = 410
      Align = alClient
      BorderOuter = fsNone
      ParentColor = True
      TabOrder = 0
    end
    object pnlTitulo: TRzPanel
      Left = 0
      Top = 0
      Width = 808
      Height = 25
      Align = alTop
      BorderOuter = fsFlatRounded
      BorderSides = [sdBottom]
      TabOrder = 1
      OnMouseDown = pnlTituloMouseDown
      object spbFechar: TsSpeedButton
        AlignWithMargins = True
        Left = 775
        Top = 3
        Width = 30
        Height = 17
        Align = alRight
        Flat = True
        Glyph.Data = {
          46050000424D4605000000000000360000002800000012000000120000000100
          2000000000001005000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FFFF
          FF11FFFFFF40FFFFFF0100000000000000000000000000000000000000000000
          0000FFFFFF01FFFFFF40FFFFFF11000000000000000000000000000000000000
          000000000000FFFFFF3DFFFFFFFFFFFFFF96FFFFFF0100000000000000000000
          000000000000FFFFFF01FFFFFF96FFFFFFFFFFFFFF3D00000000000000000000
          0000000000000000000000000000FFFFFF01FFFFFF94FFFFFFFFFFFFFF9AFFFF
          FF020000000000000000FFFFFF01FFFFFF93FFFFFFFFFFFFFF9BFFFFFF020000
          0000000000000000000000000000000000000000000000000000FFFFFF01FFFF
          FF94FFFFFFFFFFFFFF9AFFFFFF02FFFFFF01FFFFFF93FFFFFFFFFFFFFF9BFFFF
          FF02000000000000000000000000000000000000000000000000000000000000
          000000000000FFFFFF01FFFFFF96FFFFFFFFFFFFFF96FFFFFF96FFFFFFFFFFFF
          FF96FFFFFF010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FFFFFF01FFFFFF94FFFFFFFFFFFF
          FFFFFFFFFF9BFFFFFF0200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FFFFFF01FFFF
          FF93FFFFFFFFFFFFFFFFFFFFFF9AFFFFFF020000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FFFF
          FF01FFFFFF96FFFFFFFFFFFFFF96FFFFFF96FFFFFFFFFFFFFF96FFFFFF010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FFFFFF01FFFFFF93FFFFFFFFFFFFFF9BFFFFFF02FFFFFF01FFFFFF94FFFF
          FFFFFFFFFF9AFFFFFF0200000000000000000000000000000000000000000000
          000000000000FFFFFF01FFFFFF93FFFFFFFFFFFFFF9BFFFFFF02000000000000
          0000FFFFFF01FFFFFF94FFFFFFFFFFFFFF9AFFFFFF0200000000000000000000
          0000000000000000000000000000FFFFFF3DFFFFFFFFFFFFFF96FFFFFF010000
          0000000000000000000000000000FFFFFF01FFFFFF96FFFFFFFFFFFFFF3D0000
          00000000000000000000000000000000000000000000FFFFFF11FFFFFF40FFFF
          FF01000000000000000000000000000000000000000000000000FFFFFF01FFFF
          FF40FFFFFF110000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000}
        OnClick = spbFecharClick
        ExplicitLeft = 780
        ExplicitTop = 0
        ExplicitHeight = 28
      end
      object lblTitulo: TLabel
        AlignWithMargins = True
        Left = 40
        Top = 3
        Width = 3
        Height = 13
        Margins.Left = 15
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object sImage1: TsImage
        Left = 0
        Top = 0
        Width = 25
        Height = 23
        Align = alLeft
        Center = True
        Picture.Data = {07544269746D617000000000}
        ImageIndex = 0
        SkinData.SkinSection = 'CHECKBOX'
      end
    end
    object stbSTATUS: TRzStatusBar
      Left = 0
      Top = 435
      Width = 808
      Height = 19
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdTop, sdRight, sdBottom]
      BorderWidth = 0
      Color = clBlack
      DoubleBuffered = False
      GradientColorStyle = gcsCustom
      ParentDoubleBuffered = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      object stpNomeTela: TRzStatusPane
        Left = 0
        Top = 0
        Width = 145
        Height = 19
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Caption = 'Nome da tela'
      end
      object stpCriacao: TRzStatusPane
        Left = 508
        Top = 0
        Width = 150
        Height = 19
        Align = alRight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Alignment = taCenter
        Caption = 'Cria'#231#227'o'
        ExplicitLeft = 458
        ExplicitTop = -8
      end
      object stpModificacao: TRzStatusPane
        Left = 658
        Top = 0
        Width = 150
        Height = 19
        Align = alRight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Alignment = taCenter
        Caption = 'Modifica'#231#227'o'
        ExplicitLeft = 408
        ExplicitHeight = 15
      end
    end
  end
end
