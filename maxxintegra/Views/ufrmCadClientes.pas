unit ufrmCadClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, RzRadChk, RzDBChk, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzBtnEdt, RzButton,
  RzSpnEdt, Vcl.Buttons, RzStatus, RzPanel, RzTabs, Vcl.ExtCtrls, acImage, sSpeedButton, RzDBEdit, RzRadGrp, RzDBRGrp,
  RzCmboBx, RzDBCmbo, RzDBBnEd, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxImage, cxDBEdit, Vcl.ExtDlgs, sDialogs, Vcl.Menus, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.VCLUI.Wait, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxImageComboBox, System.ImageList, Vcl.ImgList,
  acAlphaImageList, uUtilidades;

type
  TfrmCadClientes = class(TfrmMasterCad)
    FDMasteridclientes: TIntegerField;
    FDMastercli_tipo: TStringField;
    FDMastercli_cpfcnpj: TStringField;
    FDMastercli_razao: TStringField;
    FDMastercli_fantasia: TStringField;
    FDMastercli_dtnasc: TDateField;
    FDMastercli_documento: TStringField;
    FDMastercli_logradouro: TStringField;
    FDMastercli_complemento: TStringField;
    FDMastercli_numero: TStringField;
    FDMastercli_bairro: TStringField;
    FDMasterid_cidade: TIntegerField;
    FDMastercli_cep: TStringField;
    FDMastercli_logo: TBlobField;
    FDMasterdt_criacao: TSQLTimeStampField;
    FDMasterdt_modificacao: TSQLTimeStampField;
    FDMasterusu_criacao: TIntegerField;
    FDMasterusu_modificacao: TIntegerField;
    FDMasterinativo: TStringField;
    FDMasterstatus: TStringField;
    Label1: TLabel;
    mseCLI_RAZAO: TRzDBEdit;
    Label2: TLabel;
    mseCLI_FANTASIA: TRzDBEdit;
    Label3: TLabel;
    cbbCLI_TIPO: TRzDBComboBox;
    Label4: TLabel;
    mseCLI_CPFCNPJ: TRzDBEdit;
    Label5: TLabel;
    dteCLI_DTNASC: TRzDBDateTimeEdit;
    Label6: TLabel;
    mseCLI_DOCUMENTO: TRzDBEdit;
    Label7: TLabel;
    mseCLI_CEP: TRzDBEdit;
    Label8: TLabel;
    mseCLI_LOGRADOURO: TRzDBEdit;
    Label9: TLabel;
    mseCLI_COMPLEMENTO: TRzDBEdit;
    Label10: TLabel;
    mseCLI_NUMERO: TRzDBEdit;
    Label11: TLabel;
    mseCLI_BAIRRO: TRzDBEdit;
    Label12: TLabel;
    mseID_CIDADE: TRzDBButtonEdit;
    mseCIDADE_NOME: TRzDBEdit;
    Label13: TLabel;
    mseUF: TRzDBEdit;
    RzGroupBox1: TRzGroupBox;
    RzPanel2: TRzPanel;
    cxDBImage1: TcxDBImage;
    btnAddLogo: TsSpeedButton;
    btnRemLogo: TsSpeedButton;
    FDMastercidade_nome: TStringField;
    FDMastercidade_uf: TStringField;
    sOpenPictureDialog: TsOpenPictureDialog;
    TabSheet2: TRzTabSheet;
    RzPanel1: TRzPanel;
    btnAddFil: TRzBitBtn;
    btnEditFil: TRzBitBtn;
    btnDelFil: TRzBitBtn;
    cxGridFiliaisDBTableView: TcxGridDBTableView;
    cxGridFiliaisLevel: TcxGridLevel;
    cxGridFiliais: TcxGrid;
    FDFiliais: TFDQuery;
    dtsFiliais: TDataSource;
    FDFiliaisidfiliais: TIntegerField;
    FDFiliaisidclientes: TIntegerField;
    FDFiliaisfil_cnpj: TStringField;
    FDFiliaisfil_razao: TStringField;
    FDFiliaisfil_fantasia: TStringField;
    FDFiliaisfil_dtcria: TDateField;
    FDFiliaisfil_ie: TStringField;
    FDFiliaisfil_logradouro: TStringField;
    FDFiliaisfil_complemento: TStringField;
    FDFiliaisfil_numero: TStringField;
    FDFiliaisfil_bairro: TStringField;
    FDFiliaisid_cidade: TIntegerField;
    FDFiliaiscidade_nome: TStringField;
    FDFiliaiscidade_uf: TStringField;
    FDFiliaisfil_cep: TStringField;
    FDFiliaisfil_logo: TBlobField;
    FDFiliaisdt_criacao: TSQLTimeStampField;
    FDFiliaisdt_modificacao: TSQLTimeStampField;
    FDFiliaisusu_criacao: TIntegerField;
    FDFiliaisusu_modificacao: TIntegerField;
    FDFiliaisinativo: TStringField;
    FDFiliaisstatus: TStringField;
    cxGridFiliaisDBTableViewfil_cpfcnpj: TcxGridDBColumn;
    cxGridFiliaisDBTableViewfil_razao: TcxGridDBColumn;
    cxGridFiliaisDBTableViewfil_fantasia: TcxGridDBColumn;
    TabSheet3: TRzTabSheet;
    RzPanel3: TRzPanel;
    btnEditCon: TRzBitBtn;
    btnDelCon: TRzBitBtn;
    cxGridConexao: TcxGrid;
    cxGridConexaoDBTableView: TcxGridDBTableView;
    cxGridConexaoLevel: TcxGridLevel;
    btnAddCon: TRzBitBtn;
    FDConexao: TFDQuery;
    dtsConexao: TDataSource;
    FDConexaoidclientes_conexao: TIntegerField;
    FDConexaoidclientes: TIntegerField;
    FDConexaoidfiliais: TIntegerField;
    FDConexaoclicon_tipo: TStringField;
    FDConexaoclicon_ip: TStringField;
    FDConexaoclicon_porta: TStringField;
    FDConexaoclicon_usuario: TStringField;
    FDConexaoclicon_senha: TStringField;
    FDConexaodt_criacao: TSQLTimeStampField;
    FDConexaodt_modificacao: TSQLTimeStampField;
    FDConexaousu_criacao: TIntegerField;
    FDConexaousu_modificacao: TIntegerField;
    FDConexaoinativo: TStringField;
    FDConexaostatus: TStringField;
    cxGridConexaoDBTableViewclicon_tipo: TcxGridDBColumn;
    cxGridConexaoDBTableViewclicon_ip: TcxGridDBColumn;
    cxGridConexaoDBTableViewclicon_porta: TcxGridDBColumn;
    FDContato: TFDQuery;
    FDContatoidcontatos: TIntegerField;
    FDContatoidtiposcontatos: TStringField;
    FDContatotpcon_descricao: TStringField;
    FDContatocont_contato: TStringField;
    FDContatoidchave: TIntegerField;
    FDContatocont_tabela: TStringField;
    FDContatodt_criacao: TSQLTimeStampField;
    FDContatodt_modificacao: TSQLTimeStampField;
    FDContatousu_criacao: TIntegerField;
    FDContatousu_modificacao: TIntegerField;
    FDContatoinativo: TStringField;
    FDContatostatus: TStringField;
    dtsContato: TDataSource;
    TabSheet4: TRzTabSheet;
    RzPanel4: TRzPanel;
    btnAddContato: TRzBitBtn;
    btnEditContato: TRzBitBtn;
    btnDelContato: TRzBitBtn;
    cxGridContato: TcxGrid;
    cxGridContatoDBTableView: TcxGridDBTableView;
    cxGridContatoLevel: TcxGridLevel;
    cxGridContatoDBTableViewcont_contato: TcxGridDBColumn;
    cxGridContatoDBTableViewtpcon_descricao: TcxGridDBColumn;
    FDContatocont_descricao: TStringField;
    FDContatocont_observacao: TMemoField;
    procedure ConfigurarBotoesPadrao(Padroniza : Boolean = True); override;
    procedure FormCreate(Sender: TObject);
    procedure PesquisaMaster; override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure FDMasterAfterOpen(DataSet: TDataSet);
    procedure cbbCLI_TIPOExit(Sender: TObject);
    procedure FDMasterNewRecord(DataSet: TDataSet);
    procedure mseCLI_CEPExit(Sender: TObject);
    procedure btnAddLogoClick(Sender: TObject);
    procedure btnRemLogoClick(Sender: TObject);
    procedure mseID_CIDADEButtonClick(Sender: TObject);
    procedure btnAddFilClick(Sender: TObject);
    procedure FDFiliaisNewRecord(DataSet: TDataSet);
    procedure FDFiliaisBeforePost(DataSet: TDataSet);
    procedure mseID_CIDADEExit(Sender: TObject);
    procedure FDMasterAfterClose(DataSet: TDataSet);
    procedure btnEditFilClick(Sender: TObject);
    procedure btnDelFilClick(Sender: TObject);
    procedure btnAddConClick(Sender: TObject);
    procedure FDConexaoNewRecord(DataSet: TDataSet);
    procedure btnEditConClick(Sender: TObject);
    procedure btnDelConClick(Sender: TObject);
    procedure FDContatoNewRecord(DataSet: TDataSet);
    procedure btnAddContatoClick(Sender: TObject);
    procedure btnEditContatoClick(Sender: TObject);
    procedure btnDelContatoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadClientes: TfrmCadClientes;

implementation

{$R *.dfm}

uses udmMaxxIntegra, uCliente, uConstantes, uPesquisas, uCidade, uLogradouro, ufrmCadClientesFiliais,
  ufrmCadClientesConexao, ufrmCadContatos, ufrmPrincipal;

{ TfrmCadClientes }

procedure TfrmCadClientes.btnAddConClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadClientesConexao, frmCadClientesConexao);


    FDConexao.Append;
    FDConexaoidclientes.AsInteger := FDMasteridclientes.AsInteger;

    frmCadClientesConexao.ShowModal;

    if frmCadClientesConexao.ModalResult = mrOk then
      FDConexao.Post
    else FDConexao.Cancel;

  finally
    FreeAndNil(frmCadClientesConexao);
    ConfigurarBotoesPadrao(True);
  end;
end;

procedure TfrmCadClientes.btnAddContatoClick(Sender: TObject);
begin
  inherited;
  try
    FDContato.Append;
    FDContatocont_tabela.AsString := 'clientes';
    FDContatoidchave.AsInteger    := FDMasteridclientes.AsInteger;

    Application.CreateForm(TfrmCadContato, frmCadContato);

    frmCadContato.dtsContato.DataSet := FDContato;
    frmCadContato.ShowModal;

    if frmCadContato.ModalResult = mrOk then
      FDContato.Post
    else FDContato.Cancel;

  finally
    FreeAndNil(frmCadContato);
    ConfigurarBotoesPadrao(True);
  end;
end;

procedure TfrmCadClientes.btnAddFilClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadClientesFiliais, frmCadClientesFiliais);

    FDFiliais.Append;

    frmCadClientesFiliais.ShowModal;

    if frmCadClientesFiliais.ModalResult = mrOk then
      FDFiliais.Post
    else FDFiliais.Cancel;

  finally
    FreeAndNil(frmCadClientesFiliais);
    ConfigurarBotoesPadrao(True);
  end;
end;

procedure TfrmCadClientes.btnAddLogoClick(Sender: TObject);
begin
  inherited;
  try
    if sOpenPictureDialog.Execute then
      FDMastercli_logo.LoadFromFile(sOpenPictureDialog.FileName);
  finally

  end;
end;

procedure TfrmCadClientes.btnAlterarClick(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(cbbCLI_TIPO);
end;

procedure TfrmCadClientes.btnDelConClick(Sender: TObject);
begin
  inherited;
  FDConexao.Delete;
  ConfigurarBotoesPadrao(True);
end;

procedure TfrmCadClientes.btnDelContatoClick(Sender: TObject);
begin
  inherited;
  FDContato.Delete;
  ConfigurarBotoesPadrao(True);
end;

procedure TfrmCadClientes.btnDelFilClick(Sender: TObject);
begin
  inherited;
  FDFiliais.Delete;
  ConfigurarBotoesPadrao(True);
end;

procedure TfrmCadClientes.btnEditConClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadClientesConexao, frmCadClientesConexao);

    FDConexao.Edit;

    frmCadClientesConexao.ShowModal;

    if frmCadClientesConexao.ModalResult = mrOk then
      FDConexao.Post
    else FDConexao.Cancel;

  finally
    FreeAndNil(frmCadClientesConexao);
    ConfigurarBotoesPadrao(True);
  end;
end;

procedure TfrmCadClientes.btnEditContatoClick(Sender: TObject);
begin
  inherited;
  try
    FDContato.Edit;
    Application.CreateForm(TfrmCadContato, frmCadContato);

    frmCadContato.dtsContato.DataSet := FDContato;
    frmCadContato.ShowModal;

    if frmCadContato.ModalResult = mrOk then
      FDContato.Post
    else FDContato.Cancel;

  finally
    FreeAndNil(frmCadContato);
    ConfigurarBotoesPadrao(True);
  end;
end;

procedure TfrmCadClientes.btnEditFilClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadClientesFiliais, frmCadClientesFiliais);

    FDFiliais.Edit;

    frmCadClientesFiliais.ShowModal;

    if frmCadClientesFiliais.ModalResult = mrOk then
      FDFiliais.Post
    else FDFiliais.Cancel;

  finally
    FreeAndNil(frmCadClientesFiliais);
    ConfigurarBotoesPadrao(True);
  end;
end;

procedure TfrmCadClientes.btnNovoClick(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(cbbCLI_TIPO);
end;

procedure TfrmCadClientes.btnRemLogoClick(Sender: TObject);
begin
  inherited;
  FDMastercli_logo.Clear;
end;

procedure TfrmCadClientes.cbbCLI_TIPOExit(Sender: TObject);
begin
  inherited;
  FormatarCPFCNPJ(FDMastercli_tipo.AsString, FDMastercli_cpfcnpj);
end;

procedure TfrmCadClientes.ConfigurarBotoesPadrao(Padroniza: Boolean = True);
begin
  btnAddLogo.Enabled     := FDMaster.State in [dsEdit, dsInsert];
  btnRemLogo.Enabled     := FDMaster.State in [dsEdit, dsInsert];
  btnAddFil.Enabled      := FDMaster.State in [dsEdit, dsInsert];
  btnEditFil.Enabled     := (FDMaster.State in [dsEdit, dsInsert]) and (not FDFiliais.IsEmpty);
  btnDelFil.Enabled      := (FDMaster.State in [dsEdit, dsInsert]) and (not FDFiliais.IsEmpty);
  btnAddCon.Enabled      := FDMaster.State in [dsEdit, dsInsert];
  btnEditCon.Enabled     := (FDMaster.State in [dsEdit, dsInsert]) and (not FDConexao.IsEmpty);
  btnDelCon.Enabled      := (FDMaster.State in [dsEdit, dsInsert]) and (not FDConexao.IsEmpty);
  btnAddContato.Enabled  := FDMaster.State in [dsEdit, dsInsert];
  btnEditContato.Enabled := (FDMaster.State in [dsEdit, dsInsert]) and (not FDContato.IsEmpty);
  btnDelContato.Enabled  := (FDMaster.State in [dsEdit, dsInsert]) and (not FDContato.IsEmpty);
  inherited;
end;

procedure TfrmCadClientes.FDConexaoNewRecord(DataSet: TDataSet);
begin
  inherited;
  NovoRegistro(FDConexao, 'clientes_conexoes', 'idclientes_conexao');
end;

procedure TfrmCadClientes.FDContatoNewRecord(DataSet: TDataSet);
begin
  inherited;
  NovoRegistro(FDContato, 'contatos', 'idcontatos');
end;

procedure TfrmCadClientes.FDFiliaisBeforePost(DataSet: TDataSet);
begin
  inherited;
  if DataSet.State = dsEdit then
  begin
    if DataSet.FindField('usu_modificacao') <> nil then
      DataSet.FieldByName('usu_modificacao').AsInteger := dmMaxxIntegra.fdUsuarioidusuarios.AsInteger;
    if DataSet.FindField('dt_modificacao') <> nil then
      DataSet.FieldByName('dt_modificacao').AsDateTime := Now;
  end;
end;

procedure TfrmCadClientes.FDFiliaisNewRecord(DataSet: TDataSet);
begin
  inherited;
  NovoRegistro(FDFiliais, 'filiais', 'idfiliais');
end;

procedure TfrmCadClientes.FDMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  FDFiliais.Close;
  FDConexao.Close;
  FDContato.Close;
end;

procedure TfrmCadClientes.FDMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if not FDMaster.IsEmpty then
    FormatarCPFCNPJ(FDMastercli_tipo.AsString, FDMastercli_cpfcnpj);

  FDMastercli_cep.EditMask := MASK_CEP;
  FDMastercli_dtnasc.EditMask := MASK_DATA;
  FDFiliais.Close;
  FDFiliais.Open;

  FDConexao.Close;
  FDConexao.Open;

  FDContato.Close;
  FDContato.Open;
end;

procedure TfrmCadClientes.FDMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  FDMastercli_tipo.AsString     := 'F';
  FDMastercli_dtnasc.AsDateTime := Now;
end;

procedure TfrmCadClientes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(frmCadClientes);

  frmPrincipal.pnlClientes.BorderSides := [];

end;

procedure TfrmCadClientes.FormCreate(Sender: TObject);
begin
  inherited;
  TABELABD := 'clientes';
  CAMPOID  := 'idclientes';

  ListaObrigatorios.Add('cbbCLI_TIPO');
  ListaObrigatorios.Add('mseCLI_FANTASIA');

  FDFiliaisfil_cnpj.EditMask := MASK_CNPJ;
  FDFiliaisfil_cep.EditMask  := MASK_CEP;

end;

procedure TfrmCadClientes.mseCLI_CEPExit(Sender: TObject);
begin
  inherited;
  if FDMastercli_cep.AsString <> '' then
    PesquisaLogradouroPorCEP(FDMastercli_cep.AsString, ['CEP', 'Logradouro', 'Bairro', 'C�d. Cidade', 'Nome', 'UF'],
                                                       [FDMastercli_cep, FDMastercli_logradouro, FDMastercli_bairro, FDMasterid_cidade, FDMastercidade_nome, FDMastercidade_uf]);
end;

procedure TfrmCadClientes.mseID_CIDADEButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaCidade, ['C�digo', 'Nome', 'UF'], [mseID_CIDADE, mseCIDADE_NOME, mseUF]);
end;

procedure TfrmCadClientes.mseID_CIDADEExit(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaCidade, 'where id_cidade = ' + mseID_CIDADE.Text, ['C�digo', 'Nome', 'UF'], [FDMasterid_cidade, FDMastercidade_nome, FDMastercidade_uf], False, False);
end;

procedure TfrmCadClientes.PesquisaMaster;
begin
  SCRIPT_BUSCA := SQLPesquisaClientes;
  inherited;
end;

end.
