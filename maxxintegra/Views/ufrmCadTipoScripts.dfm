inherited frmCadTipoScripts: TfrmCadTipoScripts
  Caption = 'Tipos de scripts'
  ClientHeight = 205
  ClientWidth = 440
  ExplicitWidth = 440
  ExplicitHeight = 205
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 440
    Height = 205
    ExplicitWidth = 440
    ExplicitHeight = 205
    inherited pnlMaster: TRzPanel
      Width = 440
      Height = 161
      ExplicitWidth = 440
      ExplicitHeight = 161
      inherited pgMaster: TRzPageControl
        Width = 434
        Height = 70
        ExplicitWidth = 434
        ExplicitHeight = 70
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          ExplicitTop = 20
          ExplicitWidth = 434
          ExplicitHeight = 50
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object msetpscr_descricao: TRzDBEdit
            Left = 4
            Top = 18
            Width = 389
            Height = 21
            DataSource = dtsMaster
            DataField = 'tpscr_descricao'
            TabOrder = 0
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 440
        ExplicitWidth = 440
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 384
          DataField = 'inativo'
          DataSource = dtsMaster
          ExplicitLeft = 384
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 123
        Width = 440
        ExplicitTop = 123
        ExplicitWidth = 440
        inherited btnCancelar: TRzRapidFireButton
          Left = 291
          ExplicitLeft = 291
        end
        inherited btnSalvar: TRzBitBtn
          Left = 367
          ExplicitLeft = 367
        end
        inherited btnNovo: TRzBitBtn
          Left = -89
          ExplicitLeft = -89
        end
        inherited btnAlterar: TRzBitBtn
          Left = -13
          ExplicitLeft = -13
        end
        inherited btnDeletar: TRzBitBtn
          Left = 63
          ExplicitLeft = 63
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 139
          ExplicitLeft = 139
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 215
          ExplicitLeft = 215
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 440
      ExplicitWidth = 440
      inherited spbFechar: TsSpeedButton
        Left = 407
        ExplicitLeft = 407
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 186
      Width = 440
      ExplicitTop = 186
      ExplicitWidth = 440
      inherited stpCriacao: TRzStatusPane
        Left = 140
        ExplicitLeft = 37
        ExplicitTop = 0
      end
      inherited stpModificacao: TRzStatusPane
        Left = 290
        ExplicitLeft = 237
      end
    end
  end
  inherited FDMaster: TFDQuery
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      
        'SELECT t.idtiposcripts, t.tpscr_descricao, t.dt_criacao, t.dt_mo' +
        'dificacao, t.usu_criacao,'
      '       t.usu_modificacao, t.inativo, t.status'
      'FROM &SCHEMA.tiposcripts t'
      'where t.idtiposcripts = :IDTIPOSCRIPTS'
      '  and t.status = '#39'A'#39)
    Top = 40
    ParamData = <
      item
        Name = 'IDTIPOSCRIPTS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDMasteridtiposcripts: TIntegerField
      FieldName = 'idtiposcripts'
      Origin = 'idtiposcripts'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDMastertpscr_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'tpscr_descricao'
      Origin = 'tpscr_descricao'
      Size = 100
    end
    object FDMasterdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDMasterdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDMasterusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDMasterusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDMasterinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDMasterstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
end
