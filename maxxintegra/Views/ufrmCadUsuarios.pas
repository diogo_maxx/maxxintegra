unit ufrmCadUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, RzButton, RzSpnEdt, Vcl.StdCtrls, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls,
  RzPanel, Vcl.Mask, RzEdit, RzDBEdit, RzRadChk, RzDBChk, RzBtnEdt, RzStatus, acImage, RzTabs, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.VCLUI.Wait;

type
  TfrmCadUsuarios = class(TfrmMasterCad)
    Label1: TLabel;
    mseUSU_DESCRICAO: TRzDBEdit;
    Label2: TLabel;
    mseUSU_NOME: TRzDBEdit;
    Label3: TLabel;
    mseUSU_SENHA: TRzDBEdit;
    Label4: TLabel;
    mseUSU_CONFSENHA: TRzMaskEdit;
    FDMasteridusuarios: TIntegerField;
    FDMasterusu_descricao: TStringField;
    FDMasterusu_nome: TStringField;
    FDMasterusu_senha: TStringField;
    FDMasterdt_criacao: TSQLTimeStampField;
    FDMasterdt_modificacao: TSQLTimeStampField;
    FDMasterusu_criacao: TIntegerField;
    FDMasterusu_modificacao: TIntegerField;
    FDMasterinativo: TStringField;
    FDMasterstatus: TStringField;
    procedure FDMasterAfterOpen(DataSet: TDataSet);
    procedure btnNovoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PesquisaMaster; override;
    procedure btnAlterarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadUsuarios: TfrmCadUsuarios;

implementation

{$R *.dfm}

uses udmMaxxIntegra, uUsuario, U_Cipher, uUtilidades, uMensagens;


procedure TfrmCadUsuarios.btnAlterarClick(Sender: TObject);
begin
  inherited;
  mseUSU_DESCRICAO.SetFocus;
end;

procedure TfrmCadUsuarios.btnNovoClick(Sender: TObject);
begin
  inherited;
  mseUSU_DESCRICAO.SetFocus;
end;

procedure TfrmCadUsuarios.btnSalvarClick(Sender: TObject);

function ValidaSenha: Boolean;
begin
  Result := True;
  if MD5Hash(FDMasterusu_senha.AsString) <> MD5Hash(mseUSU_CONFSENHA.Text) then
  begin
    Mensagem('E', 'As senhas n�o conferem!');
    mseUSU_CONFSENHA.Clear;
    SetarFocoComponente(mseUSU_SENHA);
    Result := False;
  end
  else FDMasterusu_senha.AsString := MD5Hash(FDMasterusu_senha.AsString);
end;

begin
  if FDMaster.State = dsInsert then
  begin
    if not ValidaSenha then
      Abort;
  end
  else
  begin
    if FDMasterusu_senha.OldValue <> FDMasterusu_senha.NewValue then
    begin
      if not ValidaSenha then
        Abort;
    end;
  end;

  inherited;
end;

procedure TfrmCadUsuarios.FDMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  mseUSU_CONFSENHA.Text := FDMasterusu_senha.AsString;
end;

procedure TfrmCadUsuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(frmCadUsuarios);
end;

procedure TfrmCadUsuarios.FormCreate(Sender: TObject);
begin
  inherited;
  TABELABD := 'usuarios';
  CAMPOID  := 'idusuarios';

  ListaObrigatorios.Add('mseUSU_DESCRICAO');
  ListaObrigatorios.Add('mseUSU_NOME');
  ListaObrigatorios.Add('mseUSU_SENHA');
  ListaObrigatorios.Add('mseUSU_CONFSENHA');


end;

procedure TfrmCadUsuarios.PesquisaMaster;
begin
  SCRIPT_BUSCA := PesquisaUsuario;
  inherited;
end;

end.
