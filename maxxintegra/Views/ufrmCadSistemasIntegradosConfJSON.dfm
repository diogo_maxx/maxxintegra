inherited frmCadSistemasIntegradosConfJSON: TfrmCadSistemasIntegradosConfJSON
  Caption = 'Sistemas integrados - Configura'#231#227'o do JSON'
  ClientHeight = 143
  ClientWidth = 559
  ExplicitWidth = 559
  ExplicitHeight = 143
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 559
    Height = 143
    inherited pnlMaster: TRzPanel
      Width = 555
      Height = 95
      ExplicitTop = 28
      object Label1: TLabel [0]
        Left = 4
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Campo SQL'
        FocusControl = msejson_camposql
      end
      object Label2: TLabel [1]
        Left = 207
        Top = 4
        Width = 74
        Height = 13
        Caption = 'Converter para'
        FocusControl = msejson_campojson
      end
      inherited pnlBotoes: TRzPanel
        Top = 57
        Width = 555
        inherited btnCancelar: TRzRapidFireButton
          Left = 406
        end
        inherited btnOK: TRzBitBtn
          Left = 482
        end
      end
      object msejson_camposql: TRzDBEdit
        Left = 4
        Top = 18
        Width = 197
        Height = 21
        DataSource = frmCadSistemasIntegrados.dtsConfJSON
        DataField = 'json_camposql'
        TabOrder = 1
      end
      object msejson_campojson: TRzDBEdit
        Left = 207
        Top = 18
        Width = 197
        Height = 21
        DataSource = frmCadSistemasIntegrados.dtsConfJSON
        DataField = 'json_campojson'
        TabOrder = 2
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 555
      inherited spbFechar: TsSpeedButton
        Left = 522
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 122
      Width = 555
      inherited stpCriacao: TRzStatusPane
        Left = 195
        Width = 180
        ExplicitLeft = 205
        ExplicitTop = 0
        ExplicitWidth = 180
      end
      inherited stpModificacao: TRzStatusPane
        Left = 375
        Width = 180
        ExplicitLeft = 380
        ExplicitWidth = 180
        ExplicitHeight = 19
      end
    end
  end
end
