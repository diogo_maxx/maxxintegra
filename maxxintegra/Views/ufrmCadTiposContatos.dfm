inherited frmCadTiposContatos: TfrmCadTiposContatos
  Caption = 'Tipos de contatos'
  ClientHeight = 214
  ClientWidth = 506
  ExplicitWidth = 506
  ExplicitHeight = 214
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 506
    Height = 214
    inherited pnlMaster: TRzPanel
      Width = 506
      Height = 170
      inherited pgMaster: TRzPageControl
        Width = 500
        Height = 79
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object msetpcon_descricao: TRzDBEdit
            Left = 4
            Top = 18
            Width = 477
            Height = 21
            DataSource = dtsMaster
            DataField = 'tpcon_descricao'
            TabOrder = 0
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 506
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 450
          DataField = 'inativo'
          DataSource = dtsMaster
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 132
        Width = 506
        inherited btnCancelar: TRzRapidFireButton
          Left = 357
        end
        inherited btnSalvar: TRzBitBtn
          Left = 433
        end
        inherited btnNovo: TRzBitBtn
          Left = -23
        end
        inherited btnAlterar: TRzBitBtn
          Left = 53
        end
        inherited btnDeletar: TRzBitBtn
          Left = 129
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 205
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 281
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 506
      inherited spbFechar: TsSpeedButton
        Left = 473
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 195
      Width = 506
      inherited stpCriacao: TRzStatusPane
        Left = 206
      end
      inherited stpModificacao: TRzStatusPane
        Left = 356
      end
    end
  end
  inherited dtsMaster: TDataSource
    Left = 376
    Top = 112
  end
  inherited FDMaster: TFDQuery
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      'SELECT '
      '    t.idtiposcontatos, '
      '    t.tpcon_descricao, '
      '    t.dt_criacao, '
      '    t.dt_modificacao, '
      '    t.usu_criacao,'
      '    t.usu_modificacao, '
      '    t.inativo,'
      '    t.status'
      'FROM &SCHEMA.tiposcontatos t'
      'where t.idtiposcontatos = :IDTIPOSCONTRATOS')
    Left = 376
    Top = 56
    ParamData = <
      item
        Name = 'IDTIPOSCONTRATOS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDMasteridtiposcontatos: TIntegerField
      FieldName = 'idtiposcontatos'
      Origin = 'idtiposcontatos'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDMastertpcon_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'tpcon_descricao'
      Origin = 'tpcon_descricao'
      Size = 100
    end
    object FDMasterdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDMasterdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDMasterusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDMasterusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDMasterinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDMasterstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
end
