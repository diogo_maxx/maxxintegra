inherited frmPesquisa: TfrmPesquisa
  Caption = 'Realize sua busca'
  ClientHeight = 348
  ClientWidth = 725
  KeyPreview = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  ExplicitWidth = 725
  ExplicitHeight = 348
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 725
    Height = 348
    BorderOuter = fsGroove
    ExplicitWidth = 725
    ExplicitHeight = 348
    inherited pnlMaster: TRzPanel
      Left = 2
      Top = 27
      Width = 721
      Height = 300
      ExplicitLeft = 2
      ExplicitTop = 27
      ExplicitWidth = 721
      ExplicitHeight = 300
      object RzPanel1: TRzPanel
        Left = 0
        Top = 0
        Width = 721
        Height = 262
        Align = alClient
        BorderOuter = fsNone
        ParentColor = True
        TabOrder = 0
        object cxGridPesquisa: TcxGrid
          AlignWithMargins = True
          Left = 3
          Top = 60
          Width = 715
          Height = 199
          Align = alClient
          TabOrder = 0
          object cxGridPesquisaDBTableView: TcxGridDBTableView
            OnDblClick = cxGridPesquisaDBTableViewDblClick
            OnKeyPress = cxGridPesquisaDBTableViewKeyPress
            Navigator.Buttons.CustomButtons = <>
            FilterBox.Visible = fvNever
            DataController.DataSource = dtsPesquisa
            DataController.Filter.Options = [fcoCaseInsensitive, fcoSoftCompare]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.ImmediateEditor = False
            OptionsCustomize.ColumnFiltering = False
            OptionsSelection.CellSelect = False
            OptionsSelection.MultiSelect = True
            OptionsView.GridLines = glNone
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
          end
          object cxGridPesquisaLevel: TcxGridLevel
            GridView = cxGridPesquisaDBTableView
          end
        end
        object RzPanel3: TRzPanel
          Left = 0
          Top = 0
          Width = 721
          Height = 57
          Align = alTop
          BorderOuter = fsNone
          TabOrder = 1
          object Label1: TLabel
            Left = 4
            Top = 8
            Width = 42
            Height = 13
            Caption = 'Pesquisa'
          end
          object msePESQUISA: TRzMaskEdit
            Left = 4
            Top = 22
            Width = 501
            Height = 21
            TabOrder = 0
            Text = ''
            TextHint = 'Digite sua pesquisa aqui...'
            TextHintVisibleOnFocus = True
            OnKeyPress = msePESQUISAKeyPress
            OnKeyUp = msePESQUISAKeyUp
          end
        end
      end
      object pnlBotoes: TRzPanel
        Left = 0
        Top = 262
        Width = 721
        Height = 38
        Align = alBottom
        BorderOuter = fsFlat
        BorderSides = [sdBottom]
        TabOrder = 1
        object btnOK: TRzBitBtn
          AlignWithMargins = True
          Left = 648
          Top = 3
          Width = 70
          Height = 31
          HelpType = htKeyword
          HelpKeyword = 'Padrao'
          Align = alRight
          Caption = '&Ok'
          TabOrder = 0
          OnClick = btnOKClick
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Left = 2
      Top = 2
      Width = 721
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 721
      inherited spbFechar: TsSpeedButton
        Left = 688
        ExplicitLeft = 692
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Left = 2
      Top = 327
      Width = 721
      ExplicitLeft = 2
      ExplicitTop = 327
      ExplicitWidth = 721
      inherited stpCriacao: TRzStatusPane
        Visible = False
      end
      inherited stpModificacao: TRzStatusPane
        Visible = False
      end
    end
  end
  object dtsPesquisa: TDataSource
    DataSet = fdPesquisa
    Left = 128
    Top = 233
  end
  object fdPesquisa: TFDQuery
    FilterOptions = [foCaseInsensitive]
    Left = 56
    Top = 232
  end
end
