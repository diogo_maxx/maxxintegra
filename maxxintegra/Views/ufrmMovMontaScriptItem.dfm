inherited frmMovMontaScriptItem: TfrmMovMontaScriptItem
  Caption = 'Montar Script - Item'
  ClientHeight = 386
  ClientWidth = 572
  ExplicitWidth = 572
  ExplicitHeight = 386
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 572
    Height = 386
    ExplicitWidth = 572
    ExplicitHeight = 386
    inherited pnlMaster: TRzPanel
      Width = 568
      Height = 338
      ExplicitWidth = 568
      ExplicitHeight = 338
      object Label1: TLabel [0]
        Left = 4
        Top = 4
        Width = 64
        Height = 13
        Caption = 'Tipo de script'
        FocusControl = mseidtiposcripts
      end
      object Label2: TLabel [1]
        Left = 4
        Top = 84
        Width = 27
        Height = 13
        Caption = 'Script'
        FocusControl = mmoscri_script
      end
      object Label3: TLabel [2]
        Left = 4
        Top = 44
        Width = 108
        Height = 13
        Caption = 'Webservice - Endpoint'
        FocusControl = msetpscr_descricao
      end
      inherited pnlBotoes: TRzPanel
        Top = 300
        Width = 568
        ExplicitTop = 300
        ExplicitWidth = 568
        inherited btnCancelar: TRzRapidFireButton
          Left = 419
          ExplicitLeft = 419
        end
        inherited btnOK: TRzBitBtn
          Left = 495
          ExplicitLeft = 495
        end
      end
      object mseidtiposcripts: TRzDBButtonEdit
        Left = 4
        Top = 18
        Width = 99
        Height = 21
        DataSource = dtsScriptItem
        DataField = 'idtiposcripts'
        TabOrder = 1
        OnExit = mseidtiposcriptsExit
        AltBtnWidth = 15
        ButtonWidth = 15
        OnButtonClick = mseidtiposcriptsButtonClick
      end
      object msetpscr_descricao: TRzDBEdit
        Left = 103
        Top = 18
        Width = 294
        Height = 21
        TabStop = False
        DataSource = dtsScriptItem
        DataField = 'tpscr_descricao'
        Enabled = False
        TabOrder = 2
      end
      object mmoscri_script: TRzDBMemo
        Left = 4
        Top = 98
        Width = 557
        Height = 191
        DataField = 'scri_script'
        DataSource = dtsScriptItem
        TabOrder = 3
      end
      object RzDBCheckBox1: TRzDBCheckBox
        Left = 508
        Top = 20
        Width = 53
        Height = 15
        DataField = 'inativo'
        DataSource = dtsScriptItem
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        Caption = 'Inativo'
        TabOrder = 4
      end
      object mmoscri_endpoint: TRzDBMemo
        Left = 4
        Top = 58
        Width = 557
        Height = 22
        DataField = 'scri_endpoint'
        DataSource = dtsScriptItem
        TabOrder = 5
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 568
      ExplicitWidth = 568
      inherited spbFechar: TsSpeedButton
        Left = 535
        ExplicitLeft = 535
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 365
      Width = 568
      ExplicitTop = 365
      ExplicitWidth = 568
      inherited stpCriacao: TRzStatusPane
        Left = 168
        Width = 200
        ExplicitLeft = 352
        ExplicitTop = 0
        ExplicitWidth = 200
      end
      inherited stpModificacao: TRzStatusPane
        Left = 368
        Width = 200
        ExplicitLeft = 502
        ExplicitWidth = 200
        ExplicitHeight = 19
      end
    end
  end
  object dtsScriptItem: TDataSource
    Left = 74
    Top = 227
  end
end
