inherited frmCadScripts: TfrmCadScripts
  Caption = 'Scrips'
  ClientHeight = 212
  ClientWidth = 437
  ExplicitWidth = 437
  ExplicitHeight = 212
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 437
    Height = 212
    inherited pnlMaster: TRzPanel
      Width = 437
      Height = 168
      inherited pgMaster: TRzPageControl
        Width = 431
        Height = 77
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitWidth = 913
          ExplicitHeight = 553
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
          end
          object RzDBEdit1: TRzDBEdit
            Left = 4
            Top = 18
            Width = 389
            Height = 21
            TabOrder = 0
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 437
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 381
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 130
        Width = 437
        inherited btnCancelar: TRzRapidFireButton
          Left = 288
        end
        inherited btnSalvar: TRzBitBtn
          Left = 364
        end
        inherited btnNovo: TRzBitBtn
          Left = -92
        end
        inherited btnAlterar: TRzBitBtn
          Left = -16
        end
        inherited btnDeletar: TRzBitBtn
          Left = 60
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 136
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 212
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 437
      inherited spbFechar: TsSpeedButton
        Left = 404
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 193
      Width = 437
      inherited stpCriacao: TRzStatusPane
        Left = 137
        ExplicitLeft = 37
        ExplicitTop = 0
      end
      inherited stpModificacao: TRzStatusPane
        Left = 287
        ExplicitLeft = 237
      end
    end
  end
end
