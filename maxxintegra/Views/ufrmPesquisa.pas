unit ufrmPesquisa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMaster, Vcl.StdCtrls, Vcl.Buttons, RzStatus, RzPanel, Vcl.ExtCtrls, acImage,
  sSpeedButton, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Vcl.Mask, RzEdit, RzSpnEdt, RzButton,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Threading, StrUtils,
  cxDataControllerConditionalFormattingRulesManagerDialog;

type
  TfrmPesquisa = class(TfrmMaster)
    RzPanel1: TRzPanel;
    cxGridPesquisa: TcxGrid;
    cxGridPesquisaDBTableView: TcxGridDBTableView;
    cxGridPesquisaLevel: TcxGridLevel;
    RzPanel3: TRzPanel;
    Label1: TLabel;
    msePESQUISA: TRzMaskEdit;
    dtsPesquisa: TDataSource;
    fdPesquisa: TFDQuery;
    pnlBotoes: TRzPanel;
    btnOK: TRzBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cxGridPesquisaDBTableViewDblClick(Sender: TObject);
    procedure msePESQUISAKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure msePESQUISAKeyPress(Sender: TObject; var Key: Char);
    procedure cxGridPesquisaDBTableViewKeyPress(Sender: TObject; var Key: Char);
  private
    procedure ConfigurarFormulario; override;
    procedure ConfigurarGrid;
    { Private declarations }
  public

  end;

var
  frmPesquisa: TfrmPesquisa;
  Con: TFDConnection;

implementation

{$R *.dfm}

uses uConstantes, uConexao, uMensagens;

procedure TfrmPesquisa.btnOKClick(Sender: TObject);
begin
  inherited;
  Close;
  ModalResult := mrOk;
end;

procedure TfrmPesquisa.ConfigurarFormulario;
begin
  inherited;

  pnlBotoes.Color := LAYOUT_COR_FORM_TITULO;
end;


procedure TfrmPesquisa.FormDestroy(Sender: TObject);
begin
  inherited;
  Con.Close;
  FreeAndNil(Con);
end;

procedure TfrmPesquisa.FormShow(Sender: TObject);
//var
//  I: Integer;
begin
  inherited;
  msePESQUISA.SetFocus;

  cxGridPesquisaDBTableView.OptionsView.NoDataToDisplayInfoText := 'Preenchendo informa��es...';

  TThread.Queue(nil,
  procedure
  begin
    try
      Con                   := TFDConnection.Create(nil);
      fdPesquisa.Connection := Con;

      if CriaConexao(Con) then
      begin
        fdPesquisa.Macros.MacroByName('SCHEMA').AsRaw := fdPesquisa.Connection.Params.Database;
        fdPesquisa.Open;
        cxGridPesquisaDBTableView.DataController.CreateAllItems;
      end;
    except
      on E: Exception do
        Mensagem('E', 'Erro ao realizar a pesquisa: ' + E.Message, nil);
    end;
    ConfigurarGrid;
  end);
end;

procedure TfrmPesquisa.msePESQUISAKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then
    cxGridPesquisa.SetFocus
  else if Key = #27 then
    Close;

end;

procedure TfrmPesquisa.msePESQUISAKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  AItemList: TcxFilterCriteriaItemList;
  I: Integer;
begin
  inherited;
  with cxGridPesquisaDBTableView.DataController.Filter do
  begin
    if msePESQUISA.Text <> '' then
    begin
      Active := False;
      BeginUpdate;
      try
        Root.Clear;
        Root.BoolOperatorKind := fboOr;

        AItemList := Root.AddItemList(fboOr);

        for I := 0 to cxGridPesquisaDBTableView.ColumnCount -1 do
          AItemList.AddItem(cxGridPesquisaDBTableView.Columns[I], foLike, '%' + msePESQUISA.Text + '%', msePESQUISA.Text);
      finally
        EndUpdate;
        Active := True;
      end;
    end
    else
    begin
      Active := False;
      BeginUpdate;
      Root.Clear;
      EndUpdate;
    end;
  end;
end;

procedure TfrmPesquisa.ConfigurarGrid;
var
  I: Integer;
begin
  for I := 0 to cxGridPesquisaDBTableView.ColumnCount -1 do
  begin
    cxGridPesquisaDBTableView.Columns[I].HeaderAlignmentHorz := TAlignment.taCenter;

    case AnsiIndexStr(cxGridPesquisaDBTableView.Columns[I].Caption, ['C�digo', 'Descri��o', 'Nome', 'Status', 'Inativo']) of
      0: cxGridPesquisaDBTableView.Columns[I].Width := 70;
      1: cxGridPesquisaDBTableView.Columns[I].Width := 200;
      2: cxGridPesquisaDBTableView.Columns[I].Width := 200;
      3: cxGridPesquisaDBTableView.Columns[I].Width := 70;
      4: cxGridPesquisaDBTableView.Columns[I].Width := 70;
    else
      begin
        if cxGridPesquisaDBTableView.Columns[I].DataBinding.ValueType = 'String' then
          cxGridPesquisaDBTableView.Columns[I].Width := 150;
      end;
    end;
  end;

  if fdPesquisa.FindField('C�digo') <> nil then
    fdPesquisa.FieldByName('C�digo').Alignment  := TAlignment.taCenter;
  if fdPesquisa.FindField('Status') <> nil then
    fdPesquisa.FieldByName('Status').Alignment  := TAlignment.taCenter;
  if fdPesquisa.FindField('Inativo') <> nil then
    fdPesquisa.FieldByName('Inativo').Alignment := TAlignment.taCenter;


  if fdPesquisa.IsEmpty then
    cxGridPesquisaDBTableView.OptionsView.NoDataToDisplayInfoText := '<No data to display>';

  if fdPesquisa.Fields.Count <= 4 then
    cxGridPesquisaDBTableView.OptionsView.ColumnAutoWidth := True;
end;

procedure TfrmPesquisa.cxGridPesquisaDBTableViewDblClick(Sender: TObject);
begin
  inherited;
  btnOK.Click;
end;

procedure TfrmPesquisa.cxGridPesquisaDBTableViewKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then
    btnOK.Click;
end;

end.
