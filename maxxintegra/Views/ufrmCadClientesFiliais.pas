unit ufrmCadClientesFiliais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterDet, Vcl.StdCtrls, Vcl.Buttons, RzStatus, RzPanel, RzButton, RzSpnEdt,
  Vcl.ExtCtrls, acImage, sSpeedButton, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxImage, cxDBEdit, RzDBBnEd, Vcl.Mask, RzEdit, RzDBEdit;

type
  TfrmCadClientesFiliais = class(TfrmMasterDet)
    dteFIL_DTCRIA: TRzDBDateTimeEdit;
    Label5: TLabel;
    mseFIL_IE: TRzDBEdit;
    Label6: TLabel;
    Label4: TLabel;
    mseFIL_CNPJ: TRzDBEdit;
    mseFIL_RAZAO: TRzDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    mseFIL_FANTASIA: TRzDBEdit;
    Label7: TLabel;
    mseFIL_CEP: TRzDBEdit;
    mseFIL_LOGRADOURO: TRzDBEdit;
    Label8: TLabel;
    mseFIL_COMPLEMENTO: TRzDBEdit;
    Label9: TLabel;
    mseFIL_NUMERO: TRzDBEdit;
    Label10: TLabel;
    mseUF: TRzDBEdit;
    Label13: TLabel;
    mseCIDADE_NOME: TRzDBEdit;
    mseID_CIDADE: TRzDBButtonEdit;
    Label12: TLabel;
    mseFIL_BAIRRO: TRzDBEdit;
    Label11: TLabel;
    RzGroupBox1: TRzGroupBox;
    RzPanel2: TRzPanel;
    btnAddLogo: TsSpeedButton;
    btnRemLogo: TsSpeedButton;
    cxDBImage1: TcxDBImage;
    procedure mseID_CIDADEButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAddLogoClick(Sender: TObject);
    procedure btnRemLogoClick(Sender: TObject);
    procedure mseID_CIDADEExit(Sender: TObject);
    procedure mseFIL_CEPExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadClientesFiliais: TfrmCadClientesFiliais;

implementation

{$R *.dfm}

uses ufrmCadClientes, uPesquisas, uCidade, uUtilidades, uLogradouro;

procedure TfrmCadClientesFiliais.btnAddLogoClick(Sender: TObject);
begin
  inherited;
  try
    if frmCadClientes.sOpenPictureDialog.Execute then
      frmCadClientes.FDFiliaisfil_logo.LoadFromFile(frmCadClientes.sOpenPictureDialog.FileName);
  finally

  end;
end;

procedure TfrmCadClientesFiliais.btnRemLogoClick(Sender: TObject);
begin
  inherited;
  frmCadClientes.FDFiliaisfil_logo.Clear;
end;

procedure TfrmCadClientesFiliais.FormCreate(Sender: TObject);
begin
  inherited;
  ListaObrigatorios.Add('mseFIL_RAZAO');
  ListaObrigatorios.Add('mseFIL_FANTASIA');
  ListaObrigatorios.Add('mseFIL_CNPJ');

end;

procedure TfrmCadClientesFiliais.FormShow(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(dteFIL_DTCRIA);

  stpCriacao.Caption     := 'Cria��o: ' + FormatDateTime('DD/MM/YYYY HH:MM',  frmCadClientes.FDFiliais.FieldByName('dt_criacao').AsDateTime);
  stpModificacao.Caption := 'Atualiza��o: ' + FormatDateTime('DD/MM/YYYY HH:MM', frmCadClientes.FDFiliais.FieldByName('dt_modificacao').AsDateTime);
end;

procedure TfrmCadClientesFiliais.mseFIL_CEPExit(Sender: TObject);
begin
  inherited;
  if frmCadClientes.FDFiliaisfil_cep.AsString <> '' then
    PesquisaLogradouroPorCEP(frmCadClientes.FDFiliaisfil_cep.AsString, ['CEP', 'Logradouro', 'Bairro', 'C�d. Cidade', 'Nome', 'UF'],
                                                       [frmCadClientes.FDFiliaisfil_cep, frmCadClientes.FDFiliaisfil_logradouro,
                                                        frmCadClientes.FDFiliaisfil_bairro, frmCadClientes.FDFiliaisid_cidade,
                                                        frmCadClientes.FDFiliaiscidade_nome, frmCadClientes.FDFiliaiscidade_uf]);
end;

procedure TfrmCadClientesFiliais.mseID_CIDADEButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaCidade, ['C�digo', 'Nome', 'UF'], [mseID_CIDADE, mseCIDADE_NOME, mseUF]);
end;

procedure TfrmCadClientesFiliais.mseID_CIDADEExit(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaCidade, 'where id_cidade = ' + mseID_CIDADE.Text, ['C�digo', 'Nome', 'UF'], [mseID_CIDADE, mseCIDADE_NOME, mseUF], False, False);
end;

end.
