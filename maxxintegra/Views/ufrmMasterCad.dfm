inherited frmMasterCad: TfrmMasterCad
  Caption = 'Formul'#225'rio principal de cadastros'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    inherited pnlMaster: TRzPanel
      object pgMaster: TRzPageControl
        AlignWithMargins = True
        Left = 3
        Top = 50
        Width = 802
        Height = 319
        Hint = ''
        ActivePage = TabSheet1
        Align = alClient
        BoldCurrentTab = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ShowFullFrame = False
        ShowShadow = False
        SoftCorners = True
        TabIndex = 0
        TabOrder = 0
        Transparent = True
        FixedDimension = 19
        object TabSheet1: TRzTabSheet
          Caption = '&1 - Dados Gerais'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object pnlCODIGO: TRzPanel
        Left = 0
        Top = 0
        Width = 808
        Height = 47
        Align = alTop
        BorderOuter = fsNone
        BorderSides = [sdBottom]
        TabOrder = 1
        object lblCodigo: TLabel
          Left = 4
          Top = 4
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          Transparent = True
        end
        object mseCODIGO: TRzButtonEdit
          Left = 4
          Top = 18
          Width = 99
          Height = 21
          Text = ''
          TabOrder = 0
          OnKeyPress = mseCODIGOKeyPress
          AltBtnWidth = 15
          ButtonWidth = 15
          OnButtonClick = mseCODIGOButtonClick
        end
        object cbxINATIVO: TRzDBCheckBox
          AlignWithMargins = True
          Left = 752
          Top = 3
          Width = 53
          Height = 41
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Align = alRight
          AlignmentVertical = avCenter
          Caption = 'Inativo'
          TabOrder = 1
          ExplicitHeight = 15
        end
      end
      object pnlBotoes: TRzPanel
        Left = 0
        Top = 372
        Width = 808
        Height = 38
        Align = alBottom
        BorderOuter = fsFlat
        BorderSides = [sdBottom]
        TabOrder = 2
        object btnCancelar: TRzRapidFireButton
          AlignWithMargins = True
          Left = 659
          Top = 3
          Width = 70
          Height = 31
          Align = alRight
          Caption = '&Cancelar'
          OnClick = btnCancelarClick
          ExplicitLeft = 679
          ExplicitTop = 4
        end
        object btnSalvar: TRzBitBtn
          AlignWithMargins = True
          Left = 735
          Top = 3
          Width = 70
          Height = 31
          HelpType = htKeyword
          HelpKeyword = 'Padrao'
          Align = alRight
          Caption = '&Salvar'
          TabOrder = 5
          OnClick = btnSalvarClick
        end
        object btnNovo: TRzBitBtn
          AlignWithMargins = True
          Left = 279
          Top = 3
          Width = 70
          Height = 31
          HelpType = htKeyword
          HelpKeyword = 'Padrao'
          Align = alRight
          Caption = '&Novo'
          TabOrder = 0
          OnClick = btnNovoClick
        end
        object btnAlterar: TRzBitBtn
          AlignWithMargins = True
          Left = 355
          Top = 3
          Width = 70
          Height = 31
          Align = alRight
          Caption = '&Alterar'
          TabOrder = 1
          OnClick = btnAlterarClick
        end
        object btnDeletar: TRzBitBtn
          AlignWithMargins = True
          Left = 431
          Top = 3
          Width = 70
          Height = 31
          Align = alRight
          Caption = '&Deletar'
          TabOrder = 2
          OnClick = btnDeletarClick
        end
        object btnPesquisar: TRzBitBtn
          AlignWithMargins = True
          Left = 507
          Top = 3
          Width = 70
          Height = 31
          Align = alRight
          Caption = '&Pesquisar'
          TabOrder = 3
          OnClick = btnPesquisarClick
        end
        object btnOpcoes: TRzBitBtn
          AlignWithMargins = True
          Left = 583
          Top = 3
          Width = 70
          Height = 31
          Align = alRight
          Caption = '&Op'#231#245'es'
          TabOrder = 4
          Visible = False
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      inherited spbFechar: TsSpeedButton
        ExplicitLeft = 775
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      inherited stpNomeTela: TRzStatusPane
        ExplicitHeight = 15
      end
      inherited stpModificacao: TRzStatusPane
        ExplicitHeight = 19
      end
    end
  end
  object dtsMaster: TDataSource
    DataSet = FDMaster
    Left = 168
    Top = 272
  end
  object FDMaster: TFDQuery
    AfterOpen = FDMasterAfterOpen
    AfterCancel = FDMasterAfterCancel
    OnNewRecord = FDMasterNewRecord
    OnPostError = FDMasterPostError
    CachedUpdates = True
    OnReconcileError = FDMasterReconcileError
    UpdateOptions.AssignedValues = [uvUpdateMode, uvLockWait]
    UpdateOptions.LockWait = True
    Left = 168
    Top = 216
  end
  object Con: TFDConnection
    Left = 688
    Top = 32
  end
  object FDSchemaAdapter: TFDSchemaAdapter
    Left = 720
    Top = 96
  end
end
