unit ufrmMenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.WinXCtrls, System.ImageList, Vcl.ImgList, acAlphaImageList,
  Vcl.CategoryButtons, Vcl.StdCtrls, Vcl.Mask, RzEdit, RTTI, TypInfo, RzCommon, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, StrUtils;

type
  TfrmMenu = class(TForm)
    spvCategorias: TSplitView;
    caMenuDiversos: TCategoryButtons;
    imgModulos: TsAlphaImageList;
    pnlGambiarra1: TPanel;
    procedure FormShow(Sender: TObject);
    procedure caMenuDiversosMouseEnter(Sender: TObject);
    procedure caMenuDiversosMouseLeave(Sender: TObject);
  private
    procedure PadronizarMenu;
    procedure CriarMenus;
    procedure ClickRotinas(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
    procedure AbrirMenu(Abrir: Boolean  = False);
  end;

var
  frmMenu: TfrmMenu;
  LASTMENU: String;

implementation

{$R *.dfm}

uses uConstantes, ufrmMaster, ufrmPrincipal, ufrmMasterCad, ufrmCadUsuarios, uLayoutFormulario, uConexao, uFormulario,
  uUtilidades, uMensagens, ufrmCadSistemasIntegrados, ufrmCadClientes, ufrmCadTiposContatos, ufrmCadTipoScripts, ufrmMovMontaScript;

procedure TfrmMenu.AbrirMenu(Abrir: Boolean = False);
begin
  spvCategorias.Visible := True;
  if Abrir then
    spvCategorias.Open
  else
    spvCategorias.Close;

  caMenuDiversos.OnMouseLeave := caMenuDiversosMouseLeave;
end;

procedure TfrmMenu.FormShow(Sender: TObject);
var
  I: Integer;
begin
  CriarMenus;
  PadronizarMenu;
  AbrirMenu;
end;

procedure TfrmMenu.PadronizarMenu;
begin
  spvCategorias.Color           := LAYOUT_CORMENUPRIMARIO;
  caMenuDiversos.Color          := LAYOUT_CORMENUPRIMARIO;
  caMenuDiversos.HotButtonColor := LAYOUT_CORSELECIONADOMENU;
end;

procedure TfrmMenu.caMenuDiversosMouseEnter(Sender: TObject);
begin
  AbrirMenu(True);
end;

procedure TfrmMenu.caMenuDiversosMouseLeave(Sender: TObject);
begin
  AbrirMenu(False);
end;


procedure TfrmMenu.CriarMenus;
var
  I: Integer;
begin
  for I := 0 to Pred(caMenuDiversos.Categories[0].Items.Count) do
    caMenuDiversos.Categories[0].Items[I].OnClick := ClickRotinas;

end;

procedure TfrmMenu.ClickRotinas(Sender: TObject);
const
  ArrayFormularios : Array[0..5] of String = ('frmCadUsuarios', 'frmCadSistemasIntegrados',
                                              'frmCadClientes', 'frmCadTiposContatos', 'frmCadTipoScripts',
                                              'frmMovMontaScript');
begin
  try
    try
      case AnsiIndexStr(TButtonItem(TCategoryButtons(Sender).selecteditem).Hint, ArrayFormularios) of
        0: CriaForm(TfrmCadUsuarios, frmCadUsuarios);
        1: CriaForm(TfrmCadSistemasIntegrados, frmCadSistemasIntegrados);
        2: CriaForm(TfrmCadClientes, frmCadClientes);
        3: CriaForm(TfrmCadTiposContatos, frmCadTiposContatos);
        4: CriaForm(TfrmCadTipoScripts, frmCadTipoScripts);
        5: CriaForm(TfrmMovMontaScript, frmMovMontaScript);
      end;
    except
      on E: Exception do
        Mensagem('E', 'ClickRotinas: ' + E.Message);
    end;
  finally
    AbrirMenu(False);
  end;
end;


end.
