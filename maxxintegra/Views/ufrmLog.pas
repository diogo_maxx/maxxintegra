unit ufrmLog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IPPeerClient, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  REST.Response.Adapter, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, Vcl.ExtCtrls, RzPanel;

type
  TfrmLog = class(TForm)
    RESTClientTabela: TRESTClient;
    RESTRequestTabela: TRESTRequest;
    RESTResponseTabela: TRESTResponse;
    RESTResponseDataSetAdapterTabela: TRESTResponseDataSetAdapter;
    FDTabela: TFDMemTable;
    FDStatus: TFDMemTable;
    FDLog: TFDMemTable;
    dtsTabela: TDataSource;
    dtsStatus: TDataSource;
    dtsLog: TDataSource;
    RESTClientStatus: TRESTClient;
    RESTRequestStatus: TRESTRequest;
    RESTResponseStatus: TRESTResponse;
    RESTResponseDataSetAdapterStatus: TRESTResponseDataSetAdapter;
    RESTClientLog: TRESTClient;
    RESTRequestLog: TRESTRequest;
    RESTResponseLog: TRESTResponse;
    RESTResponseDataSetAdapterLog: TRESTResponseDataSetAdapter;
    cxGridLogDBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    RzPanel1: TRzPanel;
    RzGroupBox2: TRzGroupBox;
    cxGrid2: TcxGrid;
    cxGridStatusDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    RzGroupBox1: TRzGroupBox;
    cxGrid1: TcxGrid;
    cxGridTabelasDBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    procedure FormShow(Sender: TObject);
    procedure FDTabelaAfterScroll(DataSet: TDataSet);
    procedure cxGridStatusDBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridLogDBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLog: TfrmLog;


  CONST
    STATUSCOD = '192.168.25.3:1091/busca/distinct/statusCod/{TABELA}';
    TABELA = '192.168.25.3:1091/busca/distinct/tabela';
    REGISTROS = 'localhost:1091/busca/all?tabela={TABELA}&statusCod={STATUSCOD}';


implementation

{$R *.dfm}

uses uLayoutFormulario, ufrmLogDetalhes, ufrmPrincipal;

procedure TfrmLog.cxGridLogDBTableView1CellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  try
    Application.CreateForm(TfrmLogDetalhes, frmLogDetalhes);

    frmLogDetalhes.ShowModal;
  finally
    FreeAndNil(frmLogDetalhes);
  end;
end;

procedure TfrmLog.cxGridStatusDBTableView1CellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  FDLog.Close;

  cxGridLogDBTableView1.DataController.GridView.ClearItems;
  cxGrid2.Refresh;

  RESTClientLog.BaseURL := REGISTROS;
//  RESTRequestLog.Resource := '';

  RESTRequestLog.Params.AddUrlSegment('TABELA', FDTabela.Fields[0].AsString);
  RESTRequestLog.Params.AddUrlSegment('STATUSCOD', FDStatus.Fields[0].AsString);

  RESTRequestLog.Execute;

  RESTResponseDataSetAdapterLog.Active := True;
  FDLog.Open;

  FDLog.IndexFieldNames := 'horaintegracao; horasincronismo:D';

  with cxGridLogDBTableView1.CreateColumn do
  begin
    DataBinding.FieldName := 'registro';
    Caption               := 'Registro';
    HeaderAlignmentHorz   := TAlignment.taCenter;
  end;

  with cxGridLogDBTableView1.CreateColumn do
  begin
    DataBinding.FieldName := 'horaintegracao';
    Caption               := 'Hora integra��o';
    DataBinding.ValueType := 'DateTime';
    Width                 := 200;
    HeaderAlignmentHorz   := TAlignment.taCenter;
  end;

  with cxGridLogDBTableView1.CreateColumn do
  begin
    DataBinding.FieldName := 'horasincronismo';
    Caption               := 'Hora sincronismo';
    DataBinding.ValueType := 'DateTime';
    Width                 := 200;
    HeaderAlignmentHorz   := TAlignment.taCenter;
  end;

  with cxGridLogDBTableView1.CreateColumn do
  begin
    DataBinding.FieldName := 'mensagem';
    Caption               := 'Mensagem';
    HeaderAlignmentHorz   := TAlignment.taCenter;
  end;

  with cxGridLogDBTableView1.CreateColumn do
  begin
    DataBinding.FieldName := 'tiporequisicao';
    Caption               := 'Tipo de requisi��o';
    GroupIndex            := 0;
    Visible               := False;
    HeaderAlignmentHorz   := TAlignment.taCenter;
  end;

//  cxGridLogDBTableView1.DataController.CreateAllItems;
//  cxGridLogDBTableView1.Columns[0].Caption := 'C�digo de resposta';
end;

procedure TfrmLog.FDTabelaAfterScroll(DataSet: TDataSet);
begin
  FDStatus.Close;

  cxGridStatusDBTableView1.DataController.GridView.ClearItems;

  RESTClientStatus.BaseURL := '192.168.25.3:1091/';
  RESTRequestStatus.Resource := 'busca/distinct/statusCod/{TABELA}';
  RESTRequestStatus.Params.AddUrlSegment('TABELA', FDTabela.Fields[0].AsString);

  RESTRequestStatus.Execute;

  RESTResponseDataSetAdapterStatus.Active := True;
  FDStatus.Open;


  cxGridStatusDBTableView1.DataController.CreateAllItems;
  cxGridStatusDBTableView1.Columns[0].Caption := 'C�digo de resposta';

end;

procedure TfrmLog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmPrincipal.pnlLog.BorderSides := [];
end;

procedure TfrmLog.FormShow(Sender: TObject);
begin
  PadronizarTela(Self);

  FDTabela.AfterScroll := nil;

  RESTClientTabela.BaseURL := TABELA;
  RESTRequestTabela.Execute;

  RESTResponseDataSetAdapterTabela.Active := True;

  FDTabela.Open;

  cxGridTabelasDBTableView1.DataController.CreateAllItems;
  cxGridTabelasDBTableView1.Columns[0].Caption := 'Tabelas';

  FDTabela.AfterScroll := FDTabelaAfterScroll;

end;

end.
