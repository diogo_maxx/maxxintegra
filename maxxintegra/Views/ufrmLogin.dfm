inherited frmLogin: TfrmLogin
  Caption = 'MaxxIntegra - Login do sistema'
  ClientHeight = 165
  ClientWidth = 653
  ExplicitWidth = 653
  ExplicitHeight = 165
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 653
    Height = 165
    BorderOuter = fsFlatBold
    ExplicitWidth = 515
    ExplicitHeight = 455
    inherited pnlMaster: TRzPanel
      Left = 2
      Top = 27
      Width = 649
      Height = 117
      ExplicitLeft = 250
      ExplicitTop = 147
      ExplicitWidth = 858
      ExplicitHeight = 293
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 80
        Height = 13
        Caption = 'Nome de usu'#225'rio'
      end
      object Label2: TLabel
        Left = 16
        Top = 58
        Width = 81
        Height = 13
        Caption = 'Senha de acesso'
      end
      object btnSair: TRzRapidFireButton
        Left = 248
        Top = 64
        Width = 75
        Height = 35
        Caption = 'Sair'
        OnClick = btnSairClick
      end
      object mseUSUARIO: TRzMaskEdit
        Left = 16
        Top = 31
        Width = 210
        Height = 21
        TabOrder = 0
        Text = ''
      end
      object mseSENHA: TRzMaskEdit
        Left = 16
        Top = 73
        Width = 210
        Height = 21
        PasswordChar = '*'
        TabOrder = 1
        Text = ''
      end
      object btnEntrar: TRzBitBtn
        Left = 248
        Top = 23
        Height = 35
        HelpType = htKeyword
        HelpKeyword = 'Padrao'
        ModalResult = 1
        Caption = 'Entrar'
        TabOrder = 2
        OnClick = btnEntrarClick
      end
      object mmoResultados: TRzMemo
        AlignWithMargins = True
        Left = 339
        Top = 3
        Width = 307
        Height = 111
        Align = alRight
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 3
        ExplicitLeft = 344
        ExplicitTop = 6
        ExplicitHeight = 93
      end
    end
    inherited pnlTitulo: TRzPanel
      Left = 2
      Top = 2
      Width = 649
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 511
      inherited spbFechar: TsSpeedButton
        Left = 616
        ExplicitLeft = 480
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Left = 2
      Top = 144
      Width = 649
      Visible = False
      ExplicitLeft = 2
      ExplicitTop = 434
      ExplicitWidth = 511
      inherited stpNomeTela: TRzStatusPane
        ExplicitHeight = 0
      end
      inherited stpCriacao: TRzStatusPane
        Visible = False
      end
      inherited stpModificacao: TRzStatusPane
        Left = 499
        ExplicitLeft = 363
        ExplicitHeight = 0
      end
    end
  end
end
