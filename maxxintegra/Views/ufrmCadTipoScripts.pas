unit ufrmCadTipoScripts;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB, FireDAC.Comp.DataSet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, sSpeedButton, RzButton,
  Vcl.Buttons, RzSpnEdt, RzRadChk, RzDBChk, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzBtnEdt, RzTabs, RzDBEdit, RzDBBnEd, Vcl.DBCtrls;

type
  TfrmCadTipoScripts = class(TfrmMasterCad)
    Label1: TLabel;
    msetpscr_descricao: TRzDBEdit;
    FDMasteridtiposcripts: TIntegerField;
    FDMastertpscr_descricao: TStringField;
    FDMasterdt_criacao: TSQLTimeStampField;
    FDMasterdt_modificacao: TSQLTimeStampField;
    FDMasterusu_criacao: TIntegerField;
    FDMasterusu_modificacao: TIntegerField;
    FDMasterinativo: TStringField;
    FDMasterstatus: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadTipoScripts: TfrmCadTipoScripts;

implementation

{$R *.dfm}

uses udmMaxxIntegra, uTipoScript, uUtilidades, ufrmPrincipal;

procedure TfrmCadTipoScripts.btnAlterarClick(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(msetpscr_descricao);
end;

procedure TfrmCadTipoScripts.btnNovoClick(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(msetpscr_descricao);
end;

procedure TfrmCadTipoScripts.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(frmCadTipoScripts);

  frmPrincipal.pnlTabelas.BorderSides := [];
end;

procedure TfrmCadTipoScripts.FormCreate(Sender: TObject);
begin
  inherited;
  TABELABD     := 'tiposcripts';
  CAMPOID      := 'idtiposcripts';
  SCRIPT_BUSCA := SQLPesquisaTipoScript;

  ListaObrigatorios.Add('msetpscr_descricao');
end;

end.
