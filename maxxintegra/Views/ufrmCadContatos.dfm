inherited frmCadContato: TfrmCadContato
  ActiveControl = mseidtiposcontatos
  Caption = 'Contatos'
  ClientHeight = 328
  ClientWidth = 340
  ExplicitWidth = 340
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 340
    Height = 328
    ExplicitWidth = 340
    ExplicitHeight = 328
    inherited pnlMaster: TRzPanel
      Width = 336
      Height = 280
      ExplicitWidth = 336
      ExplicitHeight = 280
      object Label1: TLabel [0]
        Left = 4
        Top = 44
        Width = 75
        Height = 13
        Caption = 'Tipo de contato'
        FocusControl = mseidtiposcontatos
      end
      object Label2: TLabel [1]
        Left = 4
        Top = 84
        Width = 75
        Height = 13
        Caption = 'Tipo de contato'
        FocusControl = msecont_contato
      end
      object Label3: TLabel [2]
        Left = 4
        Top = 4
        Width = 46
        Height = 13
        Caption = 'Descri'#231#227'o'
        FocusControl = msecont_descricao
      end
      object Label4: TLabel [3]
        Left = 4
        Top = 124
        Width = 58
        Height = 13
        Caption = 'Observa'#231#227'o'
        FocusControl = mmocont_observacao
      end
      inherited pnlBotoes: TRzPanel
        Top = 242
        Width = 336
        TabOrder = 4
        ExplicitTop = 242
        ExplicitWidth = 336
        inherited btnCancelar: TRzRapidFireButton
          Left = 187
          ExplicitLeft = 317
        end
        inherited btnOK: TRzBitBtn
          Left = 263
          ExplicitLeft = 263
        end
      end
      object mseidtiposcontatos: TRzDBButtonEdit
        Left = 4
        Top = 58
        Width = 99
        Height = 21
        DataSource = dtsContato
        DataField = 'idtiposcontatos'
        TabOrder = 1
        OnExit = mseidtiposcontatosExit
        ButtonGlyph.Data = {
          FE040000424DFE040000000000003604000028000000140000000A0000000100
          080000000000C800000000000000000000000001000000000000000000003300
          00006600000099000000CC000000FF0000000033000033330000663300009933
          0000CC330000FF33000000660000336600006666000099660000CC660000FF66
          000000990000339900006699000099990000CC990000FF99000000CC000033CC
          000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
          0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
          330000333300333333006633330099333300CC333300FF333300006633003366
          33006666330099663300CC663300FF6633000099330033993300669933009999
          3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
          330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
          66006600660099006600CC006600FF0066000033660033336600663366009933
          6600CC336600FF33660000666600336666006666660099666600CC666600FF66
          660000996600339966006699660099996600CC996600FF99660000CC660033CC
          660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
          6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
          990000339900333399006633990099339900CC339900FF339900006699003366
          99006666990099669900CC669900FF6699000099990033999900669999009999
          9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
          990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
          CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
          CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
          CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
          CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
          CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
          FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
          FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
          FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
          FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
          000000808000800000008000800080800000C0C0C00080808000191919004C4C
          4C00B2B2B200E5E5E5005A1E1E00783C3C0096646400C8969600FFC8C800465F
          82005591B9006EB9D7008CD2E600B4E6F000D8E9EC0099A8AC00646F7100E2EF
          F100C56A31000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000232323232323
          0101232323232323232381812323232323232305040301232323232323818181
          812323232323DF0403DB01232323232381818181812323235E5E5ED7DB012323
          2323818181EE81812323235EB3B3885E5E2323232381ACACAC818123232389B3
          B3B3B3885E23232381ACACACACAC8123232389B3B3B3B3B35E23232381ACACAC
          ACAC8123232389B3D7B3B3B35E23232381ACACACACAC812323232389B3B3B35E
          232323232381ACACAC8123232323232389898923232323232323818181232323
          2323}
        ButtonKind = bkCustom
        AltBtnWidth = 15
        ButtonWidth = 15
        OnButtonClick = mseidtiposcontatosButtonClick
      end
      object msetpcon_descricao: TRzDBEdit
        Left = 103
        Top = 58
        Width = 223
        Height = 21
        TabStop = False
        DataSource = dtsContato
        DataField = 'tpcon_descricao'
        Enabled = False
        TabOrder = 2
      end
      object msecont_contato: TRzDBEdit
        Left = 4
        Top = 98
        Width = 322
        Height = 21
        DataSource = dtsContato
        DataField = 'cont_contato'
        TabOrder = 3
      end
      object msecont_descricao: TRzDBEdit
        Left = 4
        Top = 18
        Width = 322
        Height = 21
        DataSource = dtsContato
        DataField = 'cont_descricao'
        TabOrder = 0
      end
      object mmocont_observacao: TRzDBMemo
        Left = 4
        Top = 138
        Width = 322
        Height = 89
        DataField = 'cont_observacao'
        DataSource = dtsContato
        TabOrder = 5
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 336
      ExplicitWidth = 336
      inherited spbFechar: TsSpeedButton
        Left = 303
        ExplicitLeft = 433
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 307
      Width = 336
      ExplicitTop = 307
      ExplicitWidth = 336
      inherited stpNomeTela: TRzStatusPane
        Width = 120
        ExplicitWidth = 120
      end
      inherited stpCriacao: TRzStatusPane
        Left = 120
        Visible = False
        ExplicitLeft = 126
        ExplicitTop = 0
      end
      inherited stpModificacao: TRzStatusPane
        Left = 270
        Visible = False
        ExplicitLeft = 296
        ExplicitHeight = 19
      end
    end
  end
  object dtsContato: TDataSource
    Left = 234
    Top = 179
  end
end
