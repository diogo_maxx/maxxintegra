unit ufrmMovMontaScript;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB, FireDAC.Comp.DataSet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, sSpeedButton, RzButton,
  Vcl.Buttons, RzSpnEdt, RzRadChk, RzDBChk, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzBtnEdt, RzTabs, RzDBEdit, RzDBBnEd, RzCmboBx, RzDBCmbo, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxCustomListBox, cxCheckListBox, cxImage, cxDBEdit, cxDBCheckListBox,
  Vcl.DBCtrls, cxListBox, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxGDIPlusClasses, acPNG, StrUtils, cxImageComboBox;

type
  TfrmMovMontaScript = class(TfrmMasterCad)
    Panel2: TPanel;
    Label1: TLabel;
    mseidsistemasintegrados: TRzDBButtonEdit;
    msesis_nome: TRzDBEdit;
    Label2: TLabel;
    mseidclientes: TRzDBButtonEdit;
    msecli_fantasia: TRzDBEdit;
    Label3: TLabel;
    mseidfiliais: TRzDBButtonEdit;
    msefil_fantasia: TRzDBEdit;
    FDScriptsItem: TFDQuery;
    dtsScriptsItem: TDataSource;
    FDScriptsItemidscriptsitem: TIntegerField;
    FDScriptsItemidscripts: TIntegerField;
    FDScriptsItemidtiposcripts: TIntegerField;
    FDScriptsItemtpscr_descricao: TStringField;
    FDScriptsItemscri_tpcon: TStringField;
    FDScriptsItemscri_endpoint: TMemoField;
    FDScriptsItemscri_script: TMemoField;
    FDScriptsItemdt_criacao: TSQLTimeStampField;
    FDScriptsItemdt_modificacao: TSQLTimeStampField;
    FDScriptsItemusu_criacao: TIntegerField;
    FDScriptsItemusu_modificacao: TIntegerField;
    FDScriptsIteminativo: TStringField;
    FDScriptsItemstatus: TStringField;
    RzGroupBox1: TRzGroupBox;
    Panel3: TPanel;
    RzPanel1: TRzPanel;
    btnAddScript: TRzBitBtn;
    btnEditScript: TRzBitBtn;
    btnDelScript: TRzBitBtn;
    btnImpScript: TRzBitBtn;
    cxGridFiliais: TcxGrid;
    cxGridFiliaisDBTableView: TcxGridDBTableView;
    cxGridFiliaisDBTableViewtpscr_descricao: TcxGridDBColumn;
    cxGridFiliaisLevel: TcxGridLevel;
    FDMasteridscripts: TIntegerField;
    FDMasteridsistemasintegrados: TIntegerField;
    FDMasteridclientes: TIntegerField;
    FDMasteridfiliais: TIntegerField;
    FDMasterdt_criacao: TSQLTimeStampField;
    FDMasterdt_modificacao: TSQLTimeStampField;
    FDMasterusu_criacao: TIntegerField;
    FDMasterusu_modificacao: TIntegerField;
    FDMasterinativo: TStringField;
    FDMasterstatus: TStringField;
    FDMastersis_nome: TStringField;
    FDMastercli_fantasia: TStringField;
    FDMastercli_logo: TBlobField;
    FDMasterfil_fantasia: TStringField;
    FDMasterfil_logo: TBlobField;
    procedure mseidsistemasintegradosButtonClick(Sender: TObject);
    procedure mseidsistemasintegradosExit(Sender: TObject);
    procedure mseidclientesButtonClick(Sender: TObject);
    procedure mseidclientesExit(Sender: TObject);
    procedure mseidfiliaisExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAddScriptClick(Sender: TObject);
    procedure FDMasterAfterOpen(DataSet: TDataSet);
    procedure FDMasterAfterClose(DataSet: TDataSet);
    procedure btnEditScriptClick(Sender: TObject);
    procedure btnDelScriptClick(Sender: TObject);
    procedure cbbscr_tpconExit(Sender: TObject);

  private
    { Private declarations }
    procedure ConfigurarBotoesPadrao(Padroniza : Boolean = True); override;
  public
    { Public declarations }
  end;

var
  frmMovMontaScript: TfrmMovMontaScript;

implementation

{$R *.dfm}

uses udmMaxxIntegra, uPesquisas, uSistemaIntegrado, uCliente, uFilial, uScript, uUtilidades, ufrmMovMontaScriptItem, uLayoutFormulario;

procedure TfrmMovMontaScript.btnAddScriptClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmMovMontaScriptItem, frmMovMontaScriptItem);
    frmMovMontaScriptItem.dtsScriptItem.DataSet := FDScriptsItem;


    FDScriptsItem.Append;
    NovoRegistro(FDScriptsItem, 'scriptsitem', 'idscriptsitem');
    FDScriptsItemidscripts.AsInteger := FDMasteridscripts.AsInteger;
    FDScriptsIteminativo.AsString    := 'N';

    frmMovMontaScriptItem.ShowModal;

    if frmMovMontaScriptItem.ModalResult = mrOk then
      FDScriptsItem.Post
    else FDScriptsItem.Cancel;

    ConfigurarBotoesPadrao(True);
  finally
    FreeAndNil(frmMovMontaScriptItem);
  end;
end;

procedure TfrmMovMontaScript.btnDelScriptClick(Sender: TObject);
begin
  inherited;
  FDScriptsItem.Delete;
  ConfigurarBotoesPadrao(True);
end;

procedure TfrmMovMontaScript.btnEditScriptClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmMovMontaScriptItem, frmMovMontaScriptItem);
    frmMovMontaScriptItem.dtsScriptItem.DataSet := FDScriptsItem;

    FDScriptsItem.Edit;

    frmMovMontaScriptItem.ShowModal;

    if frmMovMontaScriptItem.ModalResult = mrOk then
      FDScriptsItem.Post
    else FDScriptsItem.Cancel;

    ConfigurarBotoesPadrao(True);
  finally
    FreeAndNil(frmMovMontaScriptItem);
  end;
end;

procedure TfrmMovMontaScript.cbbscr_tpconExit(Sender: TObject);
  procedure ExcluiObrigatorios(Campo: String);
  begin
    if ListaObrigatorios.IndexOf(Campo) >= 0 then
      ListaObrigatorios.Delete(ListaObrigatorios.IndexOf(Campo));
  end;

begin
  inherited;
  PadronizarTela(Self, ListaObrigatorios);
  Self.Refresh;
  Self.Repaint;
end;

procedure TfrmMovMontaScript.ConfigurarBotoesPadrao(Padroniza: Boolean);
begin
  btnAddScript.Enabled  := (FDMaster.State in [dsInsert, dsEdit]);
  btnEditScript.Enabled := (FDMaster.State in [dsInsert, dsEdit]) and (not FDScriptsItem.IsEmpty);
  btnDelScript.Enabled  := (FDMaster.State in [dsInsert, dsEdit]) and (not FDScriptsItem.IsEmpty);
  btnImpScript.Enabled  := (FDMaster.State in [dsInsert, dsEdit]);
  inherited;
end;

procedure TfrmMovMontaScript.FDMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  FDScriptsItem.Close;
end;

procedure TfrmMovMontaScript.FDMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FDScriptsItem.Close;
  FDScriptsItem.Open;
end;

procedure TfrmMovMontaScript.FormCreate(Sender: TObject);
begin
  inherited;
  TABELABD     := 'scripts';
  CAMPOID      := 'idscripts';
  SCRIPT_BUSCA := SQLPesquisaScript;

  ListaObrigatorios.Add('mseidsistemasintegrados');
end;

procedure TfrmMovMontaScript.mseidclientesButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaClientes, ['C�digo', 'Nome'], [mseidclientes, msecli_fantasia]);
  if mseidclientes.Text <> '' then
    Pesquisar(SQLPesquisaClientesLogo, ' and idclientes = ' + mseidclientes.Text, ['Logomarca'], [FDMastercli_logo]);

end;

procedure TfrmMovMontaScript.mseidclientesExit(Sender: TObject);
begin
  inherited;
  if mseidclientes.Text <> '' then
  Pesquisar(SQLPesquisaClientesLogo, ' and idclientes = ' + mseidclientes.text, ['C�digo', 'Nome', 'Logomarca'],
                                                                                          [mseidclientes, msecli_fantasia, FDMastercli_logo]);
end;

procedure TfrmMovMontaScript.mseidfiliaisExit(Sender: TObject);
begin
  inherited;
  if mseidfiliais.Text <> '' then
    Pesquisar(SQLPesquisaFilialLogo, ' and idfiliais = ' + mseidfiliais.Text, ['C�digo', 'Nome', 'Logomarca'],
                                                                               [mseidfiliais, msefil_fantasia, FDMasterfil_logo]);
end;

procedure TfrmMovMontaScript.mseidsistemasintegradosButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(PesquisaSistemasIntegrados, ['C�digo', 'Nome'], [mseidsistemasintegrados, msesis_nome]);
end;

procedure TfrmMovMontaScript.mseidsistemasintegradosExit(Sender: TObject);
begin
  inherited;
  if mseidsistemasintegrados.Text <> '' then
    Pesquisar(PesquisaSistemasIntegrados, ' and idsistemasintegrados = ' + mseidsistemasintegrados.Text, ['C�digo', 'Nome'], [mseidsistemasintegrados, msesis_nome]);
end;

end.
