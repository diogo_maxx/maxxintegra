inherited frmCadSistemasIntegrados: TfrmCadSistemasIntegrados
  Caption = 'Sistemas integrados'
  ClientHeight = 406
  ClientWidth = 652
  ExplicitWidth = 652
  ExplicitHeight = 406
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 652
    Height = 406
    ExplicitWidth = 652
    ExplicitHeight = 406
    inherited pnlMaster: TRzPanel
      Width = 652
      Height = 362
      ExplicitWidth = 652
      ExplicitHeight = 362
      inherited pgMaster: TRzPageControl
        Width = 646
        Height = 271
        ActivePage = TabSheet4
        TabIndex = 1
        ExplicitWidth = 646
        ExplicitHeight = 271
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          ExplicitTop = 20
          ExplicitWidth = 646
          ExplicitHeight = 251
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 130
            Height = 13
            Caption = 'Nome do sistema integrado'
            FocusControl = mseSIS_NOME
          end
          object Label2: TLabel
            Left = 375
            Top = 5
            Width = 79
            Height = 13
            Caption = 'Tipo de conex'#227'o'
            FocusControl = cbbsis_tpcon
          end
          object Label14: TLabel
            Left = 4
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Cliente'
            FocusControl = mseidclientes
          end
          object Label15: TLabel
            Left = 4
            Top = 84
            Width = 20
            Height = 13
            Caption = 'Filial'
            FocusControl = mseidfiliais
          end
          object mseSIS_NOME: TRzDBEdit
            Left = 4
            Top = 18
            Width = 365
            Height = 21
            DataSource = dtsMaster
            DataField = 'sis_nome'
            TabOrder = 0
          end
          object cbbsis_tpcon: TRzDBComboBox
            Left = 375
            Top = 18
            Width = 173
            Height = 21
            DataField = 'sis_tpcon'
            DataSource = dtsMaster
            Enabled = False
            TabOrder = 1
            TabStop = False
            Items.Strings = (
              'BANCO DE DADOS'
              'WEBSERVICE')
            Values.Strings = (
              'B'
              'W')
          end
          object mseidclientes: TRzDBButtonEdit
            Left = 4
            Top = 58
            Width = 99
            Height = 21
            DataSource = dtsMaster
            DataField = 'idclientes'
            TabOrder = 2
            OnExit = mseidclientesExit
            AltBtnWidth = 15
            ButtonWidth = 15
            OnButtonClick = mseidclientesButtonClick
          end
          object msecli_fantasia: TRzDBEdit
            Left = 103
            Top = 58
            Width = 266
            Height = 21
            TabStop = False
            DataSource = dtsMaster
            DataField = 'cli_fantasia'
            Enabled = False
            TabOrder = 3
          end
          object mseidfiliais: TRzDBButtonEdit
            Left = 4
            Top = 98
            Width = 99
            Height = 21
            DataSource = dtsMaster
            DataField = 'idfiliais'
            TabOrder = 4
            OnExit = mseidfiliaisExit
            AltBtnWidth = 15
            ButtonWidth = 15
            OnButtonClick = mseidfiliaisButtonClick
          end
          object msefil_fantasia: TRzDBEdit
            Left = 103
            Top = 98
            Width = 266
            Height = 21
            TabStop = False
            DataSource = dtsMaster
            DataField = 'fil_fantasia'
            Enabled = False
            TabOrder = 5
          end
        end
        object TabSheet4: TRzTabSheet
          Caption = '&2 - Banco de dados'
          Padding.Top = 3
          object RzPageControl3: TRzPageControl
            AlignWithMargins = True
            Left = 3
            Top = 6
            Width = 640
            Height = 242
            Hint = ''
            ActivePage = TabSheet10
            Align = alClient
            ShowFullFrame = False
            ShowShadow = False
            TabIndex = 1
            TabOrder = 0
            FixedDimension = 19
            object TabSheet9: TRzTabSheet
              Caption = '&2.1 - Conex'#227'o'
              object Label17: TLabel
                Left = 135
                Top = 4
                Width = 39
                Height = 13
                Caption = 'Protocol'
                FocusControl = cbbsis_protocol
              end
              object Label18: TLabel
                Left = 316
                Top = 3
                Width = 32
                Height = 13
                Caption = 'Server'
                FocusControl = msesis_server
              end
              object Label19: TLabel
                Left = 4
                Top = 44
                Width = 20
                Height = 13
                Caption = 'Port'
                FocusControl = msesis_port
              end
              object Label20: TLabel
                Left = 75
                Top = 44
                Width = 46
                Height = 13
                Caption = 'Database'
                FocusControl = msesis_database
              end
              object Label21: TLabel
                Left = 4
                Top = 84
                Width = 51
                Height = 13
                Caption = 'User name'
                FocusControl = msesis_username
              end
              object Label22: TLabel
                Left = 160
                Top = 84
                Width = 46
                Height = 13
                Caption = 'Password'
                FocusControl = msesis_password
              end
              object lbldriverid: TLabel
                Left = 4
                Top = 2
                Width = 43
                Height = 13
                Caption = 'Driver ID'
                Color = clBtnFace
                FocusControl = cbbsis_driverid
                ParentColor = False
              end
              object Label4: TLabel
                Left = 316
                Top = 44
                Width = 41
                Height = 13
                Caption = 'SCHEMA'
                FocusControl = msesis_schema
              end
              object cbbsis_protocol: TRzDBComboBox
                Left = 135
                Top = 17
                Width = 175
                Height = 21
                DataField = 'sis_protocol'
                DataSource = dtsMaster
                TabOrder = 0
                Items.Strings = (
                  '<LOCAL>'
                  '<TCP/IP>')
                Values.Strings = (
                  'L'
                  'I')
              end
              object msesis_server: TRzDBEdit
                Left = 316
                Top = 17
                Width = 235
                Height = 21
                DataSource = dtsMaster
                DataField = 'sis_server'
                TabOrder = 1
              end
              object msesis_port: TRzDBEdit
                Left = 4
                Top = 58
                Width = 65
                Height = 21
                DataSource = dtsMaster
                DataField = 'sis_port'
                TabOrder = 2
              end
              object msesis_database: TRzDBEdit
                Left = 75
                Top = 58
                Width = 235
                Height = 21
                DataSource = dtsMaster
                DataField = 'sis_database'
                TabOrder = 3
              end
              object msesis_username: TRzDBEdit
                Left = 4
                Top = 97
                Width = 150
                Height = 21
                DataSource = dtsMaster
                DataField = 'sis_username'
                TabOrder = 5
              end
              object msesis_password: TRzDBEdit
                Left = 160
                Top = 97
                Width = 150
                Height = 21
                DataSource = dtsMaster
                DataField = 'sis_password'
                PasswordChar = '*'
                TabOrder = 6
              end
              object cbbsis_driverid: TRzDBComboBox
                Left = 4
                Top = 17
                Width = 125
                Height = 21
                DataField = 'sis_driverid'
                DataSource = dtsMaster
                TabOrder = 7
                Items.Strings = (
                  'FIREBIRD'
                  'MYSQL'
                  'SQL SERVER'
                  'ORACLE')
                Values.Strings = (
                  'F'
                  'M'
                  'S'
                  'O')
              end
              object msesis_schema: TRzDBEdit
                Left = 316
                Top = 58
                Width = 235
                Height = 21
                DataSource = dtsMaster
                DataField = 'sis_schema'
                TabOrder = 4
              end
            end
            object TabSheet10: TRzTabSheet
              Caption = '&2.2 - Tabelas e campos'
              object cxGridFiliais: TcxGrid
                AlignWithMargins = True
                Left = 106
                Top = 3
                Width = 531
                Height = 216
                Align = alClient
                BorderStyle = cxcbsNone
                TabOrder = 0
                object cxGridFiliaisDBTableView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = dtsSistemasIntegradosConf
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsView.ColumnAutoWidth = True
                  OptionsView.GroupByBox = False
                  object cxGridFiliaisDBTableViewconf_tabela: TcxGridDBColumn
                    Caption = 'Tabela origem'
                    DataBinding.FieldName = 'tpscr_descricao'
                    Width = 200
                  end
                  object cxGridFiliaisDBTableViewconf_tabelapai: TcxGridDBColumn
                    Caption = 'Tabela pai origem'
                    DataBinding.FieldName = 'conf_tabelapai'
                    Width = 200
                  end
                  object cxGridFiliaisDBTableViewconf_codigo: TcxGridDBColumn
                    Caption = 'Chaves prim'#225'rias origem'
                    DataBinding.FieldName = 'conf_codigo'
                    Width = 200
                  end
                  object cxGridFiliaisDBTableViewconf_codigopai: TcxGridDBColumn
                    Caption = 'Chaves prim'#225'rias pai'
                    DataBinding.FieldName = 'conf_codigopai'
                    Width = 200
                  end
                  object cxGridFiliaisDBTableViewconf_prioridade: TcxGridDBColumn
                    Caption = 'Prioridade'
                    DataBinding.FieldName = 'conf_prioridade'
                  end
                end
                object cxGridFiliaisLevel: TcxGridLevel
                  GridView = cxGridFiliaisDBTableView
                end
              end
              object RzPanel1: TRzPanel
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 100
                Height = 216
                Margins.Right = 0
                Align = alLeft
                BorderOuter = fsFlat
                BorderSides = [sdRight]
                Padding.Right = 3
                TabOrder = 1
                object btnAddTab: TRzBitBtn
                  Left = 0
                  Top = 0
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Padrao'
                  Align = alTop
                  Caption = 'Adicionar'
                  TabOrder = 0
                  OnClick = btnAddTabClick
                end
                object btnEditTab: TRzBitBtn
                  Left = 0
                  Top = 27
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Normal'
                  Align = alTop
                  Caption = 'Editar'
                  TabOrder = 1
                  OnClick = btnEditTabClick
                end
                object btnDelTab: TRzBitBtn
                  Left = 0
                  Top = 54
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Normal'
                  Align = alTop
                  Caption = 'Deletar'
                  TabOrder = 2
                  OnClick = btnDelTabClick
                end
              end
            end
            object TabSheet11: TRzTabSheet
              Caption = '&2.3 - Triggers'
              object RzPageControl4: TRzPageControl
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 634
                Height = 216
                Hint = ''
                ActivePage = RzTabSheet3
                Align = alClient
                BoldCurrentTab = True
                ShowFullFrame = False
                ShowShadow = False
                TabIndex = 2
                TabOrder = 0
                FixedDimension = 19
                object RzTabSheet1: TRzTabSheet
                  Caption = '&2.3.1 - Insert'
                  object mmosis_triggerinsert: TRzDBMemo
                    AlignWithMargins = True
                    Left = 3
                    Top = 3
                    Width = 628
                    Height = 190
                    Align = alClient
                    DataField = 'sis_triggerinsert'
                    DataSource = dtsMaster
                    ParentShowHint = False
                    ScrollBars = ssVertical
                    ShowHint = True
                    TabOrder = 0
                  end
                end
                object RzTabSheet2: TRzTabSheet
                  Caption = '&2.3.2 - Update'
                  object mmosis_triggerupdate: TRzDBMemo
                    AlignWithMargins = True
                    Left = 3
                    Top = 3
                    Width = 628
                    Height = 190
                    Align = alClient
                    DataField = 'sis_triggerupdate'
                    DataSource = dtsMaster
                    ScrollBars = ssVertical
                    TabOrder = 0
                  end
                end
                object RzTabSheet3: TRzTabSheet
                  Caption = '&2.3.3 - Delete'
                  object mmosis_triggerdelete: TRzDBMemo
                    AlignWithMargins = True
                    Left = 3
                    Top = 3
                    Width = 628
                    Height = 190
                    Align = alClient
                    DataField = 'sis_triggerdelete'
                    DataSource = dtsMaster
                    ScrollBars = ssVertical
                    TabOrder = 0
                  end
                end
              end
            end
          end
        end
        object TabSheet5: TRzTabSheet
          Caption = '&3 - Servi'#231'o'
          object Label3: TLabel
            Left = 4
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Base URL'
            FocusControl = mmosis_baseurl
          end
          object Label11: TLabel
            Left = 320
            Top = 4
            Width = 42
            Height = 13
            Caption = 'EndPoint'
            FocusControl = mmosis_endpoint
          end
          object Label13: TLabel
            Left = 320
            Top = 64
            Width = 30
            Height = 13
            Caption = 'Senha'
            FocusControl = mmosis_baseurl
          end
          object Label12: TLabel
            Left = 4
            Top = 64
            Width = 36
            Height = 13
            Caption = 'Usu'#225'rio'
            FocusControl = mmosis_baseurl
          end
          object mmosis_baseurl: TRzDBMemo
            Left = 4
            Top = 18
            Width = 310
            Height = 41
            DataField = 'sis_baseurl'
            DataSource = dtsMaster
            TabOrder = 0
            WordWrap = False
          end
          object mmosis_endpoint: TRzDBMemo
            Left = 320
            Top = 18
            Width = 310
            Height = 41
            DataField = 'sis_endpointlogin'
            DataSource = dtsMaster
            TabOrder = 1
            WordWrap = False
          end
          object msesis_wspassword: TRzDBEdit
            Left = 320
            Top = 78
            Width = 228
            Height = 21
            DataSource = dtsMaster
            DataField = 'sis_wspassword'
            PasswordChar = '*'
            TabOrder = 3
          end
          object msesis_wsusername: TRzDBEdit
            Left = 4
            Top = 78
            Width = 310
            Height = 21
            DataSource = dtsMaster
            DataField = 'sis_wsusername'
            TabOrder = 2
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 652
        ExplicitWidth = 652
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 596
          DataField = 'inativo'
          DataSource = dtsMaster
          ExplicitLeft = 596
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 324
        Width = 652
        ExplicitTop = 324
        ExplicitWidth = 652
        inherited btnCancelar: TRzRapidFireButton
          Left = 503
          ExplicitLeft = 311
        end
        inherited btnSalvar: TRzBitBtn
          Left = 579
          ExplicitLeft = 579
        end
        inherited btnNovo: TRzBitBtn
          Left = 123
          ExplicitLeft = 123
        end
        inherited btnAlterar: TRzBitBtn
          Left = 199
          ExplicitLeft = 199
        end
        inherited btnDeletar: TRzBitBtn
          Left = 275
          ExplicitLeft = 275
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 351
          ExplicitLeft = 351
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 427
          Caption = '&Exportar'
          Visible = True
          OnClick = btnOpcoesClick
          ExplicitLeft = 427
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 652
      ExplicitWidth = 652
      inherited spbFechar: TsSpeedButton
        Left = 619
        ExplicitLeft = 427
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 387
      Width = 652
      ExplicitTop = 387
      ExplicitWidth = 652
      inherited stpCriacao: TRzStatusPane
        Left = 252
        Width = 200
        ExplicitLeft = 425
        ExplicitTop = 0
        ExplicitWidth = 200
      end
      inherited stpModificacao: TRzStatusPane
        Left = 452
        Width = 200
        ExplicitLeft = 575
        ExplicitWidth = 200
      end
    end
  end
  inherited dtsMaster: TDataSource
    Left = 320
    Top = 152
  end
  inherited FDMaster: TFDQuery
    AfterClose = FDMasterAfterClose
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      'SELECT '
      '       m.*,'
      '       c.cli_fantasia,'
      '       f.fil_fantasia'
      'FROM &SCHEMA.sistemasintegrados m'
      'left join &SCHEMA.clientes c on (c.idclientes = m.idclientes)'
      
        'left join &SCHEMA.filiais f on (f.idfiliais = m.idfiliais and f.' +
        'idclientes = m.idclientes)'
      'where m.idsistemasintegrados = :IDSISTEMASINTEGRADOS    ')
    Left = 320
    Top = 104
    ParamData = <
      item
        Name = 'IDSISTEMASINTEGRADOS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = 'maxxintegra'
        Name = 'SCHEMA'
      end>
    object FDMasteridsistemasintegrados: TIntegerField
      FieldName = 'idsistemasintegrados'
      Origin = 'idsistemasintegrados'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDMasteridclientes: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idclientes'
      Origin = 'idclientes'
    end
    object FDMasteridfiliais: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idfiliais'
      Origin = 'idfiliais'
    end
    object FDMastersis_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_nome'
      Origin = 'sis_nome'
      Size = 100
    end
    object FDMastersis_tpcon: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_tpcon'
      Origin = 'sis_tpcon'
      FixedChar = True
      Size = 1
    end
    object FDMastersis_driverid: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_driverid'
      Origin = 'sis_driverid'
      FixedChar = True
      Size = 1
    end
    object FDMastersis_database: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_database'
      Origin = 'sis_database'
      Size = 100
    end
    object FDMastersis_username: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_username'
      Origin = 'sis_username'
    end
    object FDMastersis_password: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_password'
      Origin = 'sis_password'
      Size = 50
    end
    object FDMastersis_port: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'sis_port'
      Origin = 'sis_port'
    end
    object FDMastersis_server: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_server'
      Origin = 'sis_server'
      Size = 100
    end
    object FDMastersis_protocol: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_protocol'
      Origin = 'sis_protocol'
      FixedChar = True
      Size = 1
    end
    object FDMastersis_baseurl: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'sis_baseurl'
      Origin = 'sis_baseurl'
      BlobType = ftMemo
    end
    object FDMastersis_endpointlogin: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'sis_endpointlogin'
      Origin = 'sis_endpointlogin'
      BlobType = ftMemo
    end
    object FDMastersis_wsusername: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_wsusername'
      Origin = 'sis_wsusername'
      Size = 100
    end
    object FDMastersis_wspassword: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_wspassword'
      Origin = 'sis_wspassword'
      Size = 100
    end
    object FDMastersis_triggerinsert: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'sis_triggerinsert'
      Origin = 'sis_triggerinsert'
      BlobType = ftMemo
    end
    object FDMastersis_triggerupdate: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'sis_triggerupdate'
      Origin = 'sis_triggerupdate'
      BlobType = ftMemo
    end
    object FDMastersis_triggerdelete: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'sis_triggerdelete'
      Origin = 'sis_triggerdelete'
      BlobType = ftMemo
    end
    object FDMasterdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDMasterdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDMasterusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDMasterusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDMasterinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDMasterstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
    object FDMastercli_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_fantasia'
      Origin = 'cli_fantasia'
      ProviderFlags = []
      Size = 45
    end
    object FDMasterfil_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_fantasia'
      Origin = 'fil_fantasia'
      ProviderFlags = []
      Size = 45
    end
    object FDMastersis_schema: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_schema'
      Origin = 'sis_schema'
      Size = 100
    end
  end
  object FDSistemasIntegradosConf: TFDQuery
    AfterOpen = FDSistemasIntegradosConfAfterOpen
    AfterClose = FDSistemasIntegradosConfAfterClose
    MasterSource = dtsMaster
    MasterFields = 'idsistemasintegrados'
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      'SELECT '
      '       s.*,'
      '       tp.tpscr_descricao'
      'FROM &SCHEMA.sistemasintegrados_conf s'
      
        'left join &SCHEMA.tiposcripts tp on (tp.idtiposcripts = s.idtipo' +
        'scripts)'
      'where s.idsistemasintegrados = :IDSISTEMASINTEGRADOS'
      '  and s.status = '#39'A'#39
      'order by conf_prioridade, conf_tabela')
    Left = 208
    Top = 112
    ParamData = <
      item
        Name = 'IDSISTEMASINTEGRADOS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDSistemasIntegradosConfidconf: TIntegerField
      FieldName = 'idconf'
      Origin = 'idconf'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDSistemasIntegradosConfidsistemasintegrados: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idsistemasintegrados'
      Origin = 'idsistemasintegrados'
    end
    object FDSistemasIntegradosConfidtiposcripts: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idtiposcripts'
      Origin = 'idtiposcripts'
    end
    object FDSistemasIntegradosConfconf_tabela: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_tabela'
      Origin = 'conf_tabela'
      Size = 45
    end
    object FDSistemasIntegradosConfconf_tabelapai: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_tabelapai'
      Origin = 'conf_tabelapai'
      Size = 45
    end
    object FDSistemasIntegradosConfconf_codigo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_codigo'
      Origin = 'conf_codigo'
      Size = 45
    end
    object FDSistemasIntegradosConfconf_codigosql: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_codigosql'
      Origin = 'conf_codigosql'
      Size = 45
    end
    object FDSistemasIntegradosConfconf_codigopai: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_codigopai'
      Origin = 'conf_codigopai'
      Size = 45
    end
    object FDSistemasIntegradosConfconf_scriptbusca: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'conf_scriptbusca'
      Origin = 'conf_scriptbusca'
      BlobType = ftMemo
    end
    object FDSistemasIntegradosConfconf_prioridade: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_prioridade'
      Origin = 'conf_prioridade'
      FixedChar = True
      Size = 1
    end
    object FDSistemasIntegradosConfconf_baseurl: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'conf_baseurl'
      Origin = 'conf_baseurl'
      BlobType = ftMemo
    end
    object FDSistemasIntegradosConfconf_endpoint: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'conf_endpoint'
      Origin = 'conf_endpoint'
      BlobType = ftMemo
    end
    object FDSistemasIntegradosConfconf_maxqntpost: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'conf_maxqntpost'
      Origin = 'conf_maxqntpost'
      BlobType = ftMemo
    end
    object FDSistemasIntegradosConfdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDSistemasIntegradosConfdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDSistemasIntegradosConfusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDSistemasIntegradosConfusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDSistemasIntegradosConfinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDSistemasIntegradosConfstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
    object FDSistemasIntegradosConftpscr_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'tpscr_descricao'
      Origin = 'tpscr_descricao'
      ProviderFlags = []
      Size = 100
    end
    object FDSistemasIntegradosConfconf_endpointupdate: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'conf_endpointupdate'
      Origin = 'conf_endpointupdate'
      BlobType = ftMemo
    end
    object FDSistemasIntegradosConfconf_endpointdelete: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'conf_endpointdelete'
      Origin = 'conf_endpointdelete'
      BlobType = ftMemo
    end
    object FDSistemasIntegradosConfconf_nomejson: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_nomejson'
      Origin = 'conf_nomejson'
      Size = 45
    end
    object FDSistemasIntegradosConfconf_encarray: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'conf_encarray'
      Origin = 'conf_encarray'
      FixedChar = True
      Size = 1
    end
  end
  object dtsSistemasIntegradosConf: TDataSource
    DataSet = FDSistemasIntegradosConf
    Left = 203
    Top = 164
  end
  object RzBalloonHints1: TRzBalloonHints
    Bitmaps.TransparentColor = clOlive
    CaptionWidth = 500
    CenterThreshold = 0
    Color = 15921906
    FrameColor = cl3DDkShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clInfoText
    Font.Height = -9
    Font.Name = 'Tahoma'
    Font.Style = []
    ShowBalloon = False
    Left = 536
    Top = 184
  end
  object FDConfJSON: TFDQuery
    OnNewRecord = FDConfJSONNewRecord
    MasterSource = dtsSistemasIntegradosConf
    MasterFields = 'idsistemasintegrados;idconf'
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      'select c.*'
      'from &SCHEMA.conf_json c'
      'where c.idsistemasintegrados = :IDSISTEMASINTEGRADOS'
      '  and c.idconf = :IDCONF')
    Left = 200
    Top = 224
    ParamData = <
      item
        Name = 'IDSISTEMASINTEGRADOS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'IDCONF'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDConfJSONidconf_json: TIntegerField
      FieldName = 'idconf_json'
      Origin = 'idconf_json'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDConfJSONidsistemasintegrados: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idsistemasintegrados'
      Origin = 'idsistemasintegrados'
    end
    object FDConfJSONidconf: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idconf'
      Origin = 'idconf'
    end
    object FDConfJSONjson_camposql: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'json_camposql'
      Origin = 'json_camposql'
      Size = 45
    end
    object FDConfJSONjson_campojson: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'json_campojson'
      Origin = 'json_campojson'
      Size = 45
    end
    object FDConfJSONdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDConfJSONdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDConfJSONusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDConfJSONusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDConfJSONinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDConfJSONstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
  object dtsConfJSON: TDataSource
    DataSet = FDConfJSON
    Left = 200
    Top = 280
  end
end
