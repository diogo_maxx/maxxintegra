unit ufrmMasterCad;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMaster, Vcl.StdCtrls, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls, RzPanel,
  RzButton, RzSpnEdt, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Mask, RzEdit, RzBtnEdt, RzRadChk, RzDBChk, RzStatus, acImage, RzTabs, RTTI, TypInfo,
  RzCmboBx, RzDBCmbo, Vcl.Menus, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.VCLUI.Wait;

type
  TfrmMasterCad = class(TfrmMaster)
    pnlBotoes: TRzPanel;
    btnSalvar: TRzBitBtn;
    btnNovo: TRzBitBtn;
    btnAlterar: TRzBitBtn;
    btnDeletar: TRzBitBtn;
    btnPesquisar: TRzBitBtn;
    btnOpcoes: TRzBitBtn;
    btnCancelar: TRzRapidFireButton;
    dtsMaster: TDataSource;
    FDMaster: TFDQuery;
    pgMaster: TRzPageControl;
    TabSheet1: TRzTabSheet;
    Con: TFDConnection;
    pnlCODIGO: TRzPanel;
    lblCodigo: TLabel;
    mseCODIGO: TRzButtonEdit;
    cbxINATIVO: TRzDBCheckBox;
    FDSchemaAdapter: TFDSchemaAdapter;
    procedure ConfigurarBotoesPadrao(Padroniza : Boolean = True); virtual;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure FDMasterAfterOpen(DataSet: TDataSet);
    procedure FDMasterNewRecord(DataSet: TDataSet);
    procedure btnCancelarClick(Sender: TObject);
    procedure FDMasterAfterCancel(DataSet: TDataSet);
    procedure PesquisaMaster; virtual;
    procedure mseCODIGOButtonClick(Sender: TObject);
    procedure mseCODIGOKeyPress(Sender: TObject; var Key: Char);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FDMasterReconcileError(DataSet: TFDDataSet; E: EFDException; UpdateKind: TFDDatSRowState;
      var Action: TFDDAptReconcileAction);
    procedure FDMasterPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure btnDeletarClick(Sender: TObject);
    procedure ChamaPopUP(Botao: TRzBitBtn; PopUP: TPopupMenu);
  private
    { Private declarations }
    procedure ConfigurarFormulario; override;
    procedure LimparCache(Sender : TFDDataSet; AErrors: Integer);
  public
    { Public declarations }
    SCRIPT_BUSCA: String;
    procedure HabilitaCampos(Valor: Boolean);
  end;

var
  frmMasterCad: TfrmMasterCad;
  TABELABD, CAMPOID: String;

implementation

{$R *.dfm}

uses uConstantes, uLayoutFormulario, udmMaxxIntegra, ufrmMensagens, uMensagens, ufrmPesquisa, uUsuario, uConexao,
  uValidacao, uUtilidades, uPesquisas;

procedure TfrmMasterCad.btnAlterarClick(Sender: TObject);
begin
  inherited;
  try
    FDMaster.Edit;

    ConfigurarBotoesPadrao(False);
    HabilitaCampos(True);
  except
    on E: Exception do
      Mensagem('E', E.Message);
  end;
end;

procedure TfrmMasterCad.btnCancelarClick(Sender: TObject);
begin
  inherited;
  pgMaster.ActivePageIndex := 0;

  FDSchemaAdapter.CancelUpdates;

  FDMaster.Close;
  FDMaster.Params[0].Clear;
  FDMaster.Open;

  ConfigurarBotoesPadrao(False);
  HabilitaCampos(False);
  SetarFocoComponente(mseCODIGO);
end;

procedure TfrmMasterCad.btnDeletarClick(Sender: TObject);
begin
  inherited;
  if FDMaster.State = dsBrowse then
  begin
    if Mensagem('Q', 'Deseja realmente deletar o registro?') then
    begin
      if FDMaster.FindField('status') <> nil then
      begin
        FDMaster.Close;
        FDMaster.Params[0].Value := mseCODIGO.Text;
        FDMaster.Open;

        FDMaster.Edit;
        FDMaster.FieldByName('status').AsString := 'X';
        FDMaster.Post;

        if FDMaster.ApplyUpdates(0) = 0 then
          Mensagem('I', 'Registro exclu�do com sucesso!')
        else
        begin
          Mensagem('E', 'Falha ao excluir registro!');
          Abort;
        end;
      end;
    end;
  end;

  FDMaster.Close;

  SetarFocoComponente(mseCODIGO);
end;

procedure TfrmMasterCad.btnNovoClick(Sender: TObject);
begin
  try
    FDMaster.Append;

    ConfigurarBotoesPadrao(False);
    HabilitaCampos(True);
  except
    on E: Exception do
      Mensagem('E', E.Message);
  end;
end;

procedure TfrmMasterCad.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  mseCODIGO.Button.Click;
end;

procedure TfrmMasterCad.btnSalvarClick(Sender: TObject);
begin
  inherited;
  if ValidaObrigatorios(Self, ListaObrigatorios) then
  begin
    if FDMaster.State = dsEdit then
    begin
      if FDMaster.FindField('usu_modificacao') <> nil then
        FDMaster.FieldByName('usu_modificacao').AsInteger := dmMaxxIntegra.fdUsuarioidusuarios.AsInteger;
      if FDMaster.FindField('dt_modificacao') <> nil then
        FDMaster.FieldByName('dt_modificacao').AsDateTime := Now;
    end;
    try
      FDMaster.Post;
      if FDSchemaAdapter.ApplyUpdates(0) = 0 then
        Mensagem('I', 'Registro inclu�do com sucesso!')
      else
      begin
        Mensagem('E', 'Falha ao incluir registro!');
        Abort;
      end;
    except
      on E: Exception do
        Mensagem('E', 'Salvar: ' + E.Message);
    end;


    ConfigurarBotoesPadrao;
    HabilitaCampos(False);
    SetarFocoComponente(mseCODIGO);
  end;
end;

procedure TfrmMasterCad.ConfigurarBotoesPadrao(Padroniza : Boolean = True);
begin
  if FDMaster.State in [dsBrowse, dsInactive] then
  begin
    btnNovo.Enabled      := True;
    btnAlterar.Enabled   := (not FDMaster.IsEmpty) and (FDMaster.FieldByName('status').AsString <> 'X');
    btnDeletar.Enabled   := (not FDMaster.IsEmpty) and (FDMaster.FieldByName('status').AsString <> 'X');
    btnPesquisar.Enabled := True;
    btnCancelar.Visible  := False;
    btnCancelar.Enabled  := False;
    btnSalvar.Visible    := False;
    btnSalvar.Enabled    := False;
    btnPesquisar.Visible := True;
    btnDeletar.Visible   := True;
    btnAlterar.Visible   := True;
    btnNovo.Visible      := True;
  end
  else if FDMaster.State in [dsEdit, dsInsert] then
  begin
    btnNovo.Visible      := False;
    btnAlterar.Visible   := False;
    btnDeletar.Visible   := False;
    btnPesquisar.Visible := False;
    btnOpcoes.Visible    := False;
    btnNovo.Enabled      := False;
    btnAlterar.Enabled   := False;
    btnDeletar.Enabled   := False;
    btnPesquisar.Enabled := False;
    btnSalvar.Visible    := True;
    btnSalvar.Enabled    := True;
    btnCancelar.Visible  := True;
    btnCancelar.Enabled  := True;
  end;

  if Padroniza then
    PadronizarBotao(Self);
end;

procedure TfrmMasterCad.ConfigurarFormulario;
begin
  inherited;
  pnlBotoes.Color := LAYOUT_COR_FORM_TITULO;
  pnlCODIGO.Color := LAYOUT_COR_FORM;

  stpCriacao.Caption     := '';
  stpModificacao.Caption := '';
  ConfigurarBotoesPadrao;

  pgMaster.ActivePageIndex := 0;
end;

procedure TfrmMasterCad.FDMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  stpCriacao.Caption     := '';
  stpModificacao.Caption := '';
end;

procedure TfrmMasterCad.FDMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if not FDMaster.IsEmpty then
  begin
    stpCriacao.Caption     := 'Cria��o: ' + FormatDateTime('DD/MM/YYYY HH:MM', FDMaster.FieldByName('dt_criacao').AsDateTime);
    stpModificacao.Caption := 'Atualiza��o: ' + FormatDateTime('DD/MM/YYYY HH:MM', FDMaster.FieldByName('dt_modificacao').AsDateTime);
  end
  else
  begin
    stpCriacao.Caption     := '';
    stpModificacao.Caption := '';
  end;
end;

procedure TfrmMasterCad.FDMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  FDMaster.FieldByName(CAMPOID).AsInteger := dmMaxxIntegra.NovoID(TABELABD, CAMPOID);
  mseCODIGO.Text := FDMaster.FieldByName(CAMPOID).AsString;

  if FDMaster.FindField('usu_criacao') <> nil then
    FDMaster.FieldByName('usu_criacao').AsInteger     := dmMaxxIntegra.fdUsuarioidusuarios.AsInteger;
  if FDMaster.FindField('usu_modificacao') <> nil then
    FDMaster.FieldByName('usu_modificacao').AsInteger := dmMaxxIntegra.fdUsuarioidusuarios.AsInteger;
  if FDMaster.FindField('dt_criacao') <> nil then
    FDMaster.FieldByName('dt_criacao').AsDateTime     := Now;
  if FDMaster.FindField('dt_modificacao') <> nil then
    FDMaster.FieldByName('dt_modificacao').AsDateTime := Now;
  if FDMaster.FindField('inativo') <> nil then
    FDMaster.FindField('inativo').AsString := 'N';
  if FDMaster.FindField('status') <> nil then
    FDMaster.FindField('status').AsString := 'A';

  stpCriacao.Caption     := 'Cria��o: ' + FormatDateTime('DD/MM/YYYY HH:MM', FDMaster.FieldByName('dt_criacao').AsDateTime);
  stpModificacao.Caption := 'Atualiza��o: ' + FormatDateTime('DD/MM/YYYY HH:MM', FDMaster.FieldByName('dt_modificacao').AsDateTime);
end;

procedure TfrmMasterCad.FDMasterPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  inherited;
  Mensagem('E', 'Falha ao dar post: ' + E.Message);
end;

procedure TfrmMasterCad.FDMasterReconcileError(DataSet: TFDDataSet; E: EFDException; UpdateKind: TFDDatSRowState;
  var Action: TFDDAptReconcileAction);
begin
  inherited;
  Mensagem('E', 'Falha ao gravar no banco de dados: ' + E.Message);
end;

procedure TfrmMasterCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    Con.Close;
    FreeAndNil(frmMasterCad);
  except
  end;
  inherited;
end;

procedure TfrmMasterCad.FormCreate(Sender: TObject);
var
  I,J: Integer;
begin
  inherited;
  CriaConexao(Con);

  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TFDQuery then
    begin
      TFDQuery(Components[I]).Connection    := Con;
      TFDQuery(Components[I]).CachedUpdates := True;
      TFDQuery(Components[I]).SchemaAdapter := FDSchemaAdapter; //funcionando como master-detail que s� da apply no banco ap�s apply no master

      //Configurando Mestre-Detalhe
      if (TFDQuery(Components[I]).MasterSource <> nil) and
         (TFDQuery(Components[I]).DetailFields = '')  and
         (TFDQuery(Components[I]).IndexFieldNames = '') then
      begin
        TFDQuery(Components[I]).DetailFields               := TFDQuery(Components[I]).MasterFields;
        TFDQuery(Components[I]).IndexFieldNames            := TFDQuery(Components[I]).MasterFields;
        TFDQuery(Components[I]).FetchOptions.DetailCascade := True;
        TFDQuery(Components[I]).FetchOptions.Cache         := [fiDetails, fiBlobs, fiMeta];
      end;
      //seta a procedure de p�s apply para comitar todos os registros.
      TFDQuery(Components[I]).AfterApplyUpdates := LimparCache;


      if TFDQuery(Components[I]).MacroByName('SCHEMA') <> nil then
        TFDQuery(Components[I]).MacroByName('SCHEMA').AsRaw := Con.Params.Database;
    end;
  end;
end;

procedure TfrmMasterCad.FormShow(Sender: TObject);
begin
  HabilitaCampos(False);
  inherited;
  mseCODIGO.SetFocus;

  FDMaster.Close;
  FDMaster.Open;
end;

procedure TfrmMasterCad.HabilitaCampos(Valor: Boolean);
var
  I        : Integer;
  Contexto : TRttiContext;
  Tipo     : TRttiType;
begin
  for I := 0 to ComponentCount -1 do
  begin
    if ((Components[I] is TRzCustomEdit) or
       (Components[I] is TRzCustomCheckBox) or
       (Components[I] is TRzCustomComboBox) or
       (Components[I] is TCustomMemo)) then
    begin
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Components[I].ClassInfo);

        if Tipo.GetProperty('Name').GetValue(Components[I]).AsString = 'mseCODIGO' then
          Tipo.GetProperty('Enabled').SetValue(Components[I], not Valor)
        else
        if Tipo.GetProperty('TabStop') <> nil then
        begin
          if Tipo.GetProperty('TabStop').GetValue(Components[I]).AsBoolean then
          begin
            if Tipo.GetProperty('Enabled') <> nil then
              Tipo.GetProperty('Enabled').SetValue(Components[I], Valor);
          end;
//          else
//          if Tipo.GetProperty('Enabled') <> nil then
//            Tipo.GetProperty('Enabled').SetValue(Components[I], Valor);
        end;
      finally
        Contexto.Free;
      end;
    end;
  end;
  PadronizarTela(Self, ListaObrigatorios);
end;


procedure TfrmMasterCad.LimparCache(Sender: TFDDataSet; AErrors: Integer);
begin
  TFDQuery(Sender).CommitUpdates;
end;

procedure TfrmMasterCad.mseCODIGOButtonClick(Sender: TObject);
begin
  inherited;
  PesquisaMaster;

  if mseCODIGO.Text <> '' then
  begin
    FDMaster.Close;
    FDMaster.Params[0].Value := StrToInt(mseCODIGO.Text);
    FDMaster.Open;
  end
  else
  begin
    FDMaster.Close;
    FDMaster.Params[0].Clear;
    FDMaster.Open;
  end;

  mseCODIGO.SetFocus;
  ConfigurarBotoesPadrao;
end;

procedure TfrmMasterCad.mseCODIGOKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (Key = #13) then
  begin
    if mseCODIGO.Text <> '' then
    begin
      if (Self.Name = 'frmCadUsuarios') and (mseCODIGO.Text = '1') then
      begin
        Mensagem('C', 'O usu�rio selecionado tem n�vel de permiss�o de sistema.' + #13 + ' N�o � poss�vel editar o mesmo.');
        SetarFocoComponente(mseCODIGO);
        mseCODIGO.Clear;
        Abort;
      end;

      FDMaster.Close;
      FDMaster.Params[0].Value := mseCODIGO.Text;
      FDMaster.Open;

      if (FDMaster.IsEmpty) and (mseCODIGO.Text <> '') then
      begin
        Mensagem('C', 'N�o foi localizado registro com este c�digo', Self);

        FDMaster.Close;
        FDMaster.Params[0].Clear;
        FDMaster.Open;
      end;

      mseCODIGO.SetFocus;
      ConfigurarBotoesPadrao;
    end;
  end;
end;

procedure TfrmMasterCad.PesquisaMaster;
begin
  Pesquisar(SCRIPT_BUSCA, ['C�digo'], [mseCODIGO]);
end;

procedure TfrmMasterCad.ChamaPopUP(Botao: TRzBitBtn; PopUP: TPopupMenu);
begin
   PopUP.PopUp(Self.Left + pnlBotoes.Left +  Botao.Left + 5,
               Self.Top + pnlBotoes.Top + Botao.Top + Botao.Height);
end;


end.
