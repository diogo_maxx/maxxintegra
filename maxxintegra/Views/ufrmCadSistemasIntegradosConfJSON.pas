unit ufrmCadSistemasIntegradosConfJSON;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterDet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, Vcl.StdCtrls,
  sSpeedButton, RzButton, Vcl.Buttons, RzSpnEdt, Vcl.Mask, RzEdit, RzDBEdit;

type
  TfrmCadSistemasIntegradosConfJSON = class(TfrmMasterDet)
    Label1: TLabel;
    msejson_camposql: TRzDBEdit;
    Label2: TLabel;
    msejson_campojson: TRzDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadSistemasIntegradosConfJSON: TfrmCadSistemasIntegradosConfJSON;

implementation

{$R *.dfm}

uses ufrmCadSistemasIntegrados, uUtilidades;

procedure TfrmCadSistemasIntegradosConfJSON.FormCreate(Sender: TObject);
begin
  inherited;
  ListaObrigatorios.Add('msejson_campojson');
  ListaObrigatorios.Add('msejson_camposql');
end;

procedure TfrmCadSistemasIntegradosConfJSON.FormShow(Sender: TObject);
begin
  inherited;
  msejson_campojson.CharCase := ecNormal;
  SetarFocoComponente(msejson_camposql);
end;

end.
