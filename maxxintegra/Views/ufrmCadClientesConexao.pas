unit ufrmCadClientesConexao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterDet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, Vcl.StdCtrls,
  sSpeedButton, RzButton, Vcl.Buttons, RzSpnEdt, RzCmboBx, RzDBCmbo, Vcl.Mask, RzEdit, RzDBEdit;

type
  TfrmCadClientesConexao = class(TfrmMasterDet)
    Label1: TLabel;
    cbbclicon_tipo: TRzDBComboBox;
    Label2: TLabel;
    mseclicon_ip: TRzDBEdit;
    Label3: TLabel;
    mseclicon_porta: TRzDBEdit;
    Label4: TLabel;
    mseclicon_usuario: TRzDBEdit;
    Label5: TLabel;
    mseclicon_senha: TRzDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadClientesConexao: TfrmCadClientesConexao;

implementation

{$R *.dfm}

uses ufrmCadClientes;

procedure TfrmCadClientesConexao.FormCreate(Sender: TObject);
begin
  inherited;
  ListaObrigatorios.Add('cbbclicon_tipo');
  ListaObrigatorios.Add('mseclicon_ip');
end;

end.
