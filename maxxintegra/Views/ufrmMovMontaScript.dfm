inherited frmMovMontaScript: TfrmMovMontaScript
  Caption = 'Montar Script'
  ClientHeight = 458
  ClientWidth = 577
  ExplicitWidth = 577
  ExplicitHeight = 458
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 577
    Height = 458
    ExplicitWidth = 577
    ExplicitHeight = 458
    inherited pnlMaster: TRzPanel
      Width = 577
      Height = 414
      ExplicitWidth = 577
      ExplicitHeight = 414
      inherited pgMaster: TRzPageControl
        Width = 571
        Height = 323
        ExplicitWidth = 571
        ExplicitHeight = 323
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          ExplicitTop = 20
          ExplicitWidth = 571
          ExplicitHeight = 303
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 571
            Height = 129
            Align = alTop
            BevelOuter = bvNone
            Padding.Top = 4
            ParentColor = True
            TabOrder = 0
            object Label1: TLabel
              Left = 4
              Top = 4
              Width = 88
              Height = 13
              Caption = 'Sistema Integrado'
              FocusControl = mseidsistemasintegrados
            end
            object Label2: TLabel
              Left = 4
              Top = 44
              Width = 33
              Height = 13
              Caption = 'Cliente'
              FocusControl = mseidclientes
            end
            object Label3: TLabel
              Left = 4
              Top = 84
              Width = 20
              Height = 13
              Caption = 'Filial'
              FocusControl = mseidfiliais
            end
            object mseidsistemasintegrados: TRzDBButtonEdit
              Left = 4
              Top = 18
              Width = 99
              Height = 21
              DataSource = dtsMaster
              DataField = 'idsistemasintegrados'
              TabOrder = 0
              OnExit = mseidsistemasintegradosExit
              AltBtnWidth = 15
              ButtonWidth = 15
              OnButtonClick = mseidsistemasintegradosButtonClick
            end
            object msesis_nome: TRzDBEdit
              Left = 103
              Top = 18
              Width = 300
              Height = 21
              TabStop = False
              DataSource = dtsMaster
              DataField = 'sis_nome'
              Enabled = False
              TabOrder = 1
            end
            object mseidclientes: TRzDBButtonEdit
              Left = 4
              Top = 58
              Width = 99
              Height = 21
              DataSource = dtsMaster
              DataField = 'idclientes'
              TabOrder = 2
              OnExit = mseidclientesExit
              AltBtnWidth = 15
              ButtonWidth = 15
              OnButtonClick = mseidclientesButtonClick
            end
            object msecli_fantasia: TRzDBEdit
              Left = 103
              Top = 58
              Width = 300
              Height = 21
              TabStop = False
              DataSource = dtsMaster
              DataField = 'cli_fantasia'
              Enabled = False
              TabOrder = 3
            end
            object mseidfiliais: TRzDBButtonEdit
              Left = 4
              Top = 98
              Width = 99
              Height = 21
              DataSource = dtsMaster
              DataField = 'idfiliais'
              TabOrder = 4
              OnExit = mseidfiliaisExit
              AltBtnWidth = 15
              ButtonWidth = 15
            end
            object msefil_fantasia: TRzDBEdit
              Left = 103
              Top = 98
              Width = 300
              Height = 21
              TabStop = False
              DataSource = dtsMaster
              DataField = 'fil_fantasia'
              Enabled = False
              TabOrder = 5
            end
          end
          object RzGroupBox1: TRzGroupBox
            Left = 0
            Top = 137
            Width = 571
            Height = 166
            Align = alBottom
            Caption = 'Montar Scripts'
            TabOrder = 1
            object Panel3: TPanel
              Left = 1
              Top = 14
              Width = 569
              Height = 151
              Align = alClient
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object RzPanel1: TRzPanel
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 100
                Height = 145
                Margins.Right = 0
                Align = alLeft
                BorderOuter = fsFlat
                BorderSides = [sdRight]
                Padding.Right = 3
                TabOrder = 0
                object btnAddScript: TRzBitBtn
                  Left = 0
                  Top = 0
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Padrao'
                  Align = alTop
                  Caption = 'Adicionar'
                  TabOrder = 0
                  OnClick = btnAddScriptClick
                end
                object btnEditScript: TRzBitBtn
                  Left = 0
                  Top = 27
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Normal'
                  Align = alTop
                  Caption = 'Editar'
                  TabOrder = 1
                  OnClick = btnEditScriptClick
                end
                object btnDelScript: TRzBitBtn
                  Left = 0
                  Top = 54
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Normal'
                  Align = alTop
                  Caption = 'Deletar'
                  TabOrder = 2
                  OnClick = btnDelScriptClick
                end
                object btnImpScript: TRzBitBtn
                  Left = 0
                  Top = 81
                  Width = 96
                  Height = 27
                  HelpType = htKeyword
                  HelpKeyword = 'Padrao'
                  Align = alTop
                  Caption = 'Importar'
                  TabOrder = 3
                end
              end
              object cxGridFiliais: TcxGrid
                AlignWithMargins = True
                Left = 106
                Top = 3
                Width = 460
                Height = 145
                Align = alClient
                BorderStyle = cxcbsNone
                TabOrder = 1
                object cxGridFiliaisDBTableView: TcxGridDBTableView
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = dtsScriptsItem
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsView.GroupByBox = False
                  object cxGridFiliaisDBTableViewtpscr_descricao: TcxGridDBColumn
                    Caption = 'Tipo do script'
                    DataBinding.FieldName = 'tpscr_descricao'
                    Width = 304
                  end
                end
                object cxGridFiliaisLevel: TcxGridLevel
                  GridView = cxGridFiliaisDBTableView
                end
              end
            end
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 577
        ExplicitWidth = 577
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 521
          ExplicitLeft = 521
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 376
        Width = 577
        ExplicitTop = 376
        ExplicitWidth = 577
        inherited btnCancelar: TRzRapidFireButton
          Left = 428
          ExplicitLeft = 782
        end
        inherited btnSalvar: TRzBitBtn
          Left = 504
          ExplicitLeft = 504
        end
        inherited btnNovo: TRzBitBtn
          Left = 48
          ExplicitLeft = 48
        end
        inherited btnAlterar: TRzBitBtn
          Left = 124
          ExplicitLeft = 124
        end
        inherited btnDeletar: TRzBitBtn
          Left = 200
          ExplicitLeft = 200
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 276
          ExplicitLeft = 276
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 352
          ExplicitLeft = 352
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 577
      ExplicitWidth = 577
      inherited spbFechar: TsSpeedButton
        Left = 544
        ExplicitLeft = 898
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 439
      Width = 577
      ExplicitTop = 439
      ExplicitWidth = 577
      inherited stpCriacao: TRzStatusPane
        Left = 77
        Width = 250
        ExplicitLeft = 491
        ExplicitTop = 0
        ExplicitWidth = 250
      end
      inherited stpModificacao: TRzStatusPane
        Left = 327
        Width = 250
        ExplicitLeft = 691
        ExplicitWidth = 250
      end
    end
  end
  inherited dtsMaster: TDataSource
    Left = 424
    Top = 304
  end
  inherited FDMaster: TFDQuery
    AfterClose = FDMasterAfterClose
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      'SELECT '
      '       s.*,'
      '       sis.sis_nome,'
      '       c.cli_fantasia,'
      '       c.cli_logo,'
      '       f.fil_fantasia,'
      '       f.fil_logo'
      'FROM &SCHEMA.scripts s'
      
        'left join &SCHEMA.sistemasintegrados sis on (sis.idsistemasinteg' +
        'rados = s.idsistemasintegrados)'
      'left join &SCHEMA.clientes c on (c.idclientes = s.idclientes)'
      
        'left join &SCHEMA.filiais f on (f.idfiliais = s.idfiliais and f.' +
        'idclientes = s.idclientes)'
      'where s.idscripts = :IDSCRIPTS'
      '  and s.status = '#39'A'#39)
    Left = 184
    Top = 312
    ParamData = <
      item
        Name = 'IDSCRIPTS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDMasteridscripts: TIntegerField
      FieldName = 'idscripts'
      Origin = 'idscripts'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDMasteridsistemasintegrados: TIntegerField
      FieldName = 'idsistemasintegrados'
      Origin = 'idsistemasintegrados'
      Required = True
    end
    object FDMasteridclientes: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idclientes'
      Origin = 'idclientes'
    end
    object FDMasteridfiliais: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idfiliais'
      Origin = 'idfiliais'
    end
    object FDMasterdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDMasterdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDMasterusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDMasterusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDMasterinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDMasterstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
    object FDMastersis_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sis_nome'
      Origin = 'sis_nome'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object FDMastercli_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_fantasia'
      Origin = 'cli_fantasia'
      ProviderFlags = []
      ReadOnly = True
      Size = 45
    end
    object FDMastercli_logo: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'cli_logo'
      Origin = 'cli_logo'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDMasterfil_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_fantasia'
      Origin = 'fil_fantasia'
      ProviderFlags = []
      ReadOnly = True
      Size = 45
    end
    object FDMasterfil_logo: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'fil_logo'
      Origin = 'fil_logo'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  inherited Con: TFDConnection
    Left = 744
    Top = 24
  end
  inherited FDSchemaAdapter: TFDSchemaAdapter
    Left = 688
    Top = 24
  end
  object FDScriptsItem: TFDQuery
    MasterSource = dtsMaster
    MasterFields = 'idscripts'
    Connection = dmMaxxIntegra.FDConnection
    SQL.Strings = (
      'SELECT '
      '        s.idscriptsitem,'
      '        s.idscripts,'
      '        s.idtiposcripts,'
      '        t.tpscr_descricao,'
      '        s.scri_tpcon,'
      '        s.scri_endpoint,'
      '        s.scri_script,'
      '        s.dt_criacao,'
      '        s.dt_modificacao,'
      '        s.usu_criacao,'
      '        s.usu_modificacao,'
      '        s.inativo,'
      '        s.status'
      'FROM &SCHEMA.scriptsitem s'
      'left join tiposcripts t on (t.idtiposcripts = s.idtiposcripts)'
      'where s.idscripts = :IDSCRIPTS'
      '  and s.status = '#39'A'#39)
    Left = 176
    Top = 264
    ParamData = <
      item
        Name = 'IDSCRIPTS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDScriptsItemidscriptsitem: TIntegerField
      FieldName = 'idscriptsitem'
      Origin = 'idscriptsitem'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDScriptsItemidscripts: TIntegerField
      FieldName = 'idscripts'
      Origin = 'idscripts'
      Required = True
    end
    object FDScriptsItemidtiposcripts: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idtiposcripts'
      Origin = 'idtiposcripts'
    end
    object FDScriptsItemtpscr_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'tpscr_descricao'
      Origin = 'tpscr_descricao'
      ProviderFlags = []
      Size = 100
    end
    object FDScriptsItemscri_tpcon: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'scri_tpcon'
      Origin = 'scri_tpcon'
      Size = 100
    end
    object FDScriptsItemscri_endpoint: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'scri_endpoint'
      Origin = 'scri_endpoint'
      BlobType = ftMemo
    end
    object FDScriptsItemscri_script: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'scri_script'
      Origin = 'scri_script'
      BlobType = ftMemo
    end
    object FDScriptsItemdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDScriptsItemdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDScriptsItemusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDScriptsItemusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDScriptsIteminativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDScriptsItemstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
  object dtsScriptsItem: TDataSource
    DataSet = FDScriptsItem
    Left = 424
    Top = 256
  end
end
