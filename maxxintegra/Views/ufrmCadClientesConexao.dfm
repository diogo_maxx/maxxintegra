inherited frmCadClientesConexao: TfrmCadClientesConexao
  Caption = 'Clientes - Conex'#227'o'
  ClientHeight = 176
  ClientWidth = 483
  ExplicitWidth = 483
  ExplicitHeight = 176
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 483
    Height = 176
    ExplicitWidth = 483
    ExplicitHeight = 176
    inherited pnlMaster: TRzPanel
      Width = 479
      Height = 128
      ExplicitWidth = 479
      ExplicitHeight = 128
      object Label1: TLabel [0]
        Left = 4
        Top = 4
        Width = 20
        Height = 13
        Caption = 'Tipo'
        FocusControl = cbbclicon_tipo
      end
      object Label2: TLabel [1]
        Left = 194
        Top = 4
        Width = 39
        Height = 13
        Caption = 'IP ou ID'
        FocusControl = mseclicon_ip
      end
      object Label3: TLabel [2]
        Left = 359
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Porta'
        FocusControl = mseclicon_porta
      end
      object Label4: TLabel [3]
        Left = 4
        Top = 44
        Width = 34
        Height = 13
        Caption = 'Usu'#225'ro'
        FocusControl = mseclicon_usuario
      end
      object Label5: TLabel [4]
        Left = 194
        Top = 44
        Width = 30
        Height = 13
        Caption = 'Senha'
        FocusControl = mseclicon_senha
      end
      inherited pnlBotoes: TRzPanel
        Top = 90
        Width = 479
        ExplicitTop = 90
        ExplicitWidth = 479
        inherited btnCancelar: TRzRapidFireButton
          Left = 330
          ExplicitLeft = 330
        end
        inherited btnOK: TRzBitBtn
          Left = 406
          ExplicitLeft = 406
        end
      end
      object cbbclicon_tipo: TRzDBComboBox
        Left = 4
        Top = 17
        Width = 181
        Height = 21
        DataField = 'clicon_tipo'
        DataSource = frmCadClientes.dtsConexao
        Style = csDropDownList
        TabOrder = 1
        Items.Strings = (
          #193'rea de trabalho remota'
          'Team Viewer')
        Values.Strings = (
          'R'
          'T')
      end
      object mseclicon_ip: TRzDBEdit
        Left = 194
        Top = 18
        Width = 159
        Height = 21
        DataSource = frmCadClientes.dtsConexao
        DataField = 'clicon_ip'
        TabOrder = 2
      end
      object mseclicon_porta: TRzDBEdit
        Left = 359
        Top = 18
        Width = 93
        Height = 21
        DataSource = frmCadClientes.dtsConexao
        DataField = 'clicon_porta'
        TabOrder = 3
      end
      object mseclicon_usuario: TRzDBEdit
        Left = 4
        Top = 58
        Width = 181
        Height = 21
        DataSource = frmCadClientes.dtsConexao
        DataField = 'clicon_usuario'
        TabOrder = 4
      end
      object mseclicon_senha: TRzDBEdit
        Left = 194
        Top = 58
        Width = 159
        Height = 21
        DataSource = frmCadClientes.dtsConexao
        DataField = 'clicon_senha'
        TabOrder = 5
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 479
      ExplicitWidth = 479
      inherited spbFechar: TsSpeedButton
        Left = 446
        ExplicitLeft = 446
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 155
      Width = 479
      ExplicitTop = 155
      ExplicitWidth = 479
      inherited stpCriacao: TRzStatusPane
        Left = 139
        Width = 170
        ExplicitLeft = 159
        ExplicitTop = 0
        ExplicitWidth = 170
      end
      inherited stpModificacao: TRzStatusPane
        Left = 309
        Width = 170
        ExplicitLeft = 319
        ExplicitWidth = 170
        ExplicitHeight = 19
      end
    end
  end
end
