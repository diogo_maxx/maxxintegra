inherited frmCadClientes: TfrmCadClientes
  Caption = 'Clientes'
  ClientHeight = 363
  ClientWidth = 653
  ExplicitWidth = 653
  ExplicitHeight = 363
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 653
    Height = 363
    ExplicitWidth = 653
    ExplicitHeight = 363
    inherited pnlMaster: TRzPanel
      Width = 653
      Height = 319
      ExplicitWidth = 653
      ExplicitHeight = 319
      inherited pgMaster: TRzPageControl
        Width = 647
        Height = 228
        ActivePage = TabSheet3
        TabIndex = 2
        ExplicitWidth = 647
        ExplicitHeight = 228
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          Caption = '&1 - Dados gerais'
          ExplicitWidth = 647
          ExplicitHeight = 208
          object Label1: TLabel
            Left = 137
            Top = 43
            Width = 59
            Height = 13
            Caption = 'Raz'#227'o social'
            FocusControl = mseCLI_RAZAO
          end
          object Label2: TLabel
            Left = 4
            Top = 84
            Width = 78
            Height = 13
            Caption = 'Nome / Fantasia'
            FocusControl = mseCLI_FANTASIA
          end
          object Label3: TLabel
            Left = 4
            Top = 4
            Width = 20
            Height = 13
            Caption = 'Tipo'
            FocusControl = cbbCLI_TIPO
          end
          object Label4: TLabel
            Left = 4
            Top = 44
            Width = 54
            Height = 13
            Caption = 'CPF / CNPJ'
            FocusControl = mseCLI_CPFCNPJ
          end
          object Label5: TLabel
            Left = 137
            Top = 4
            Width = 95
            Height = 13
            Caption = 'Data de nascimento'
            FocusControl = dteCLI_DTNASC
          end
          object Label6: TLabel
            Left = 250
            Top = 4
            Width = 54
            Height = 13
            Caption = 'Documento'
            FocusControl = mseCLI_DOCUMENTO
          end
          object Label7: TLabel
            Left = 4
            Top = 124
            Width = 19
            Height = 13
            Caption = 'CEP'
            FocusControl = mseCLI_CEP
          end
          object Label8: TLabel
            Left = 137
            Top = 124
            Width = 55
            Height = 13
            Caption = 'Logradouro'
            FocusControl = mseCLI_LOGRADOURO
          end
          object Label9: TLabel
            Left = 404
            Top = 124
            Width = 65
            Height = 13
            Caption = 'Complemento'
            FocusControl = mseCLI_COMPLEMENTO
          end
          object Label10: TLabel
            Left = 547
            Top = 124
            Width = 37
            Height = 13
            Caption = 'N'#250'mero'
            FocusControl = mseCLI_NUMERO
          end
          object Label11: TLabel
            Left = 4
            Top = 164
            Width = 28
            Height = 13
            Caption = 'Bairro'
            FocusControl = mseCLI_BAIRRO
          end
          object Label12: TLabel
            Left = 233
            Top = 164
            Width = 33
            Height = 13
            Caption = 'Cidade'
            FocusControl = mseID_CIDADE
          end
          object Label13: TLabel
            Left = 580
            Top = 164
            Width = 13
            Height = 13
            Caption = 'UF'
            FocusControl = mseUF
          end
          object mseCLI_RAZAO: TRzDBEdit
            Left = 137
            Top = 57
            Width = 261
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_razao'
            TabOrder = 4
          end
          object mseCLI_FANTASIA: TRzDBEdit
            Left = 4
            Top = 98
            Width = 394
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_fantasia'
            TabOrder = 5
          end
          object cbbCLI_TIPO: TRzDBComboBox
            Left = 4
            Top = 17
            Width = 127
            Height = 21
            DataField = 'cli_tipo'
            DataSource = dtsMaster
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clDefault
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            KeepSearchCase = True
            ParentFont = False
            TabOrder = 0
            OnExit = cbbCLI_TIPOExit
            Items.Strings = (
              'F'#237'sico'
              'Jur'#237'dico')
            Values.Strings = (
              'F'
              'J')
          end
          object mseCLI_CPFCNPJ: TRzDBEdit
            Left = 4
            Top = 57
            Width = 127
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_cpfcnpj'
            TabOrder = 3
          end
          object dteCLI_DTNASC: TRzDBDateTimeEdit
            Left = 137
            Top = 18
            Width = 107
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_dtnasc'
            TabOrder = 1
            EditType = etDate
          end
          object mseCLI_DOCUMENTO: TRzDBEdit
            Left = 250
            Top = 18
            Width = 148
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_documento'
            TabOrder = 2
          end
          object mseCLI_CEP: TRzDBEdit
            Left = 4
            Top = 137
            Width = 127
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_cep'
            TabOrder = 6
            OnExit = mseCLI_CEPExit
          end
          object mseCLI_LOGRADOURO: TRzDBEdit
            Left = 137
            Top = 137
            Width = 261
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_logradouro'
            TabOrder = 7
          end
          object mseCLI_COMPLEMENTO: TRzDBEdit
            Left = 404
            Top = 137
            Width = 137
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_complemento'
            TabOrder = 8
          end
          object mseCLI_NUMERO: TRzDBEdit
            Left = 547
            Top = 137
            Width = 90
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_numero'
            TabOrder = 9
          end
          object mseCLI_BAIRRO: TRzDBEdit
            Left = 4
            Top = 177
            Width = 223
            Height = 21
            DataSource = dtsMaster
            DataField = 'cli_bairro'
            TabOrder = 10
          end
          object mseID_CIDADE: TRzDBButtonEdit
            Left = 233
            Top = 177
            Width = 99
            Height = 21
            DataSource = dtsMaster
            DataField = 'id_cidade'
            TabOrder = 11
            OnExit = mseID_CIDADEExit
            AltBtnWidth = 15
            ButtonWidth = 15
            OnButtonClick = mseID_CIDADEButtonClick
          end
          object mseCIDADE_NOME: TRzDBEdit
            Left = 331
            Top = 177
            Width = 243
            Height = 21
            TabStop = False
            DataSource = dtsMaster
            DataField = 'cidade_nome'
            Enabled = False
            TabOrder = 12
          end
          object mseUF: TRzDBEdit
            Left = 580
            Top = 177
            Width = 57
            Height = 21
            TabStop = False
            DataSource = dtsMaster
            DataField = 'cidade_uf'
            Enabled = False
            TabOrder = 13
          end
          object RzGroupBox1: TRzGroupBox
            Left = 404
            Top = 4
            Width = 233
            Height = 115
            Caption = 'Logomarca'
            ParentColor = True
            TabOrder = 14
            object RzPanel2: TRzPanel
              AlignWithMargins = True
              Left = 4
              Top = 17
              Width = 49
              Height = 94
              Align = alClient
              BorderOuter = fsNone
              ParentColor = True
              TabOrder = 0
              object btnAddLogo: TsSpeedButton
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 43
                Height = 30
                Align = alTop
                Flat = True
                Glyph.Data = {
                  C6070000424DC607000000000000360000002800000016000000160000000100
                  2000000000009007000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000162594339655A4344655A4344655A4344655A4344655A
                  4344655A4344655A4344655A4344655A4344655A4344655A4344655A43446259
                  433900000001000000000000000000000000000000000000000000000000645A
                  4594645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645B46920000
                  00000000000000000000000000000000000000000000645A44D8645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A44D800000000000000000000
                  0000000000000000000000000000635A45DA645A45FF645A45FF645A45FF645A
                  45FF645A45EE645A446968584820685848206559466A645A45EF645A45FF645A
                  45FF645A45FF645A45FF635A45DA000000000000000000000000000000000000
                  000000000000635A45DA645A45FF645A45FF645A45FF645A45EE635A421F7F7F
                  7F02625A4741645C44407F7F7F0268584820645A45EF645A45FF645A45FF645A
                  45FF635A45DA000000000000000000000000000000000000000000000000635A
                  45DA645A45FF645A45FF645A45FF655B456855555503655A45B9645A45FF645A
                  45FF645A45B87F7F7F026559466A645A45FF645A45FF645A45FF635A45DA0000
                  00000000000000000000000000000000000000000000635A45DA645A45FF645A
                  45FF645A45FF68584820625A4741645A45FF645A45FF645A45FF645A45FF645C
                  4440645D4621645A45FF645A45FF645A45FF635A45DA00000000000000000000
                  0000000000000000000000000000635A45DA645A45FF645A45FF645A45FF6858
                  4820625A4741645A45FF645A45FF645A45FF645A45FF645C4440645D4621645A
                  45FF645A45FF645A45FF635A45DA000000000000000000000000000000000000
                  000000000000635A45DA645A45FF645A45FF645A45FF6359456755555503645A
                  45BA645A45FF645A45FF655A45B97F7F7F02645A4469645A45FF645A45FF645A
                  45FF635A45DA000000000000000000000000000000000000000000000000635A
                  45DA645A45FF645A45FF645A45FF645A45ED665D441E55555503625A4741625A
                  474155555503635A421F645A45EE645A45FF645A45FF645A45FF635A45DA0000
                  00000000000000000000000000000000000000000000635A45DA645A45FF645A
                  45FF645A45FF645A45FF645A45ED63594567635A421F68584820655B4568645A
                  45EE645A45FF645A45FF645A45FF645A45FF635A45DA00000000000000000000
                  0000000000000000000000000000645A44D8645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A44D8000000000000000000000000000000000000
                  000000000000635B4495645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF655A45930000000000000000000000000000000000000000000000007F7F
                  7F026558463A655A4344655A4344635945B1645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF635945B1655A4344655A43446558463A7F7F7F020000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000055555506645B45A6645A45DD645A45DD645A45DD645A45DD645B
                  45A6555555060000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000000}
                OnClick = btnAddLogoClick
                DisabledGlyphKind = [dgBlended]
              end
              object btnRemLogo: TsSpeedButton
                AlignWithMargins = True
                Left = 3
                Top = 61
                Width = 43
                Height = 30
                Align = alBottom
                Flat = True
                Glyph.Data = {
                  C6070000424DC607000000000000360000002800000016000000160000000100
                  2000000000009007000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000006666
                  3305695A4B11695A4B11695A4B11695A4B11695A4B11695A4B11666633050000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000006359464D645A45F5645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45F4655B464C000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000000645A46B0645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A46B00000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000635A45B6645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF635A45B600000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000635A45B6645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF635A
                  45B6000000000000000000000000000000000000000000000000000000000000
                  000000000000000000000000000000000000635A45B6645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF635A45B6000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000000635A45B6645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF635A45B60000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000635A45B6645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF635A45B600000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000635A45B6645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF635A
                  45B6000000000000000000000000000000000000000000000000000000000000
                  000000000000000000000000000000000000635A45B6645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF635A45B6000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000000635A45B6645A45FF645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF635A45B60000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000645A45AA645A45EE645A45EE645A45EE645A45EE645A45EE645A45EE645A
                  45EE645A45EE645A45AA00000000000000000000000000000000000000000000
                  00000000000000000000000000000000000000000000615A4322655A4344655A
                  4344655A4344655A4344655A4344655A4344655A4344655A4344655A4344655A
                  4344615A43220000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000645A4680645A45FF645A45FF645A45FF645A
                  45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A45FF645A46800000
                  0000000000000000000000000000000000000000000000000000000000000000
                  000000000000615A4322655A4344655A43446459468F645A45FF645A45FF645A
                  45FF645A45FF6459468F655A4344655A4344615A432200000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000695A4B11695A4B11695A4B11695A4B110000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  0000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000000}
                OnClick = btnRemLogoClick
                DisabledGlyphKind = [dgBlended]
                ExplicitLeft = 6
                ExplicitTop = 67
              end
            end
            object cxDBImage1: TcxDBImage
              AlignWithMargins = True
              Left = 59
              Top = 17
              TabStop = False
              Align = alRight
              DataBinding.DataField = 'cli_logo'
              DataBinding.DataSource = dtsMaster
              Enabled = False
              Properties.GraphicClassName = 'TdxPNGImage'
              Properties.GraphicTransparency = gtTransparent
              Style.BorderStyle = ebsNone
              TabOrder = 1
              Transparent = True
              Height = 94
              Width = 170
            end
          end
        end
        object TabSheet2: TRzTabSheet
          Caption = '&2 - Filiais'
          object RzPanel1: TRzPanel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 100
            Height = 202
            Margins.Right = 0
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Padding.Right = 3
            TabOrder = 0
            object btnAddFil: TRzBitBtn
              Left = 0
              Top = 0
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Padrao'
              Align = alTop
              Caption = 'Adicionar'
              TabOrder = 0
              OnClick = btnAddFilClick
            end
            object btnEditFil: TRzBitBtn
              Left = 0
              Top = 27
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Editar'
              TabOrder = 1
              OnClick = btnEditFilClick
            end
            object btnDelFil: TRzBitBtn
              Left = 0
              Top = 54
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Deletar'
              TabOrder = 2
              OnClick = btnDelFilClick
            end
          end
          object cxGridFiliais: TcxGrid
            AlignWithMargins = True
            Left = 106
            Top = 3
            Width = 538
            Height = 202
            Align = alClient
            BorderStyle = cxcbsNone
            TabOrder = 1
            object cxGridFiliaisDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dtsFiliais
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object cxGridFiliaisDBTableViewfil_cpfcnpj: TcxGridDBColumn
                Caption = 'CNPJ'
                DataBinding.FieldName = 'fil_cnpj'
                Width = 122
              end
              object cxGridFiliaisDBTableViewfil_razao: TcxGridDBColumn
                Caption = 'Raz'#227'o social'
                DataBinding.FieldName = 'fil_razao'
                Width = 200
              end
              object cxGridFiliaisDBTableViewfil_fantasia: TcxGridDBColumn
                Caption = 'Fantasia'
                DataBinding.FieldName = 'fil_fantasia'
                Width = 200
              end
            end
            object cxGridFiliaisLevel: TcxGridLevel
              GridView = cxGridFiliaisDBTableView
            end
          end
        end
        object TabSheet3: TRzTabSheet
          Caption = '&3 - Conex'#227'o'
          object RzPanel3: TRzPanel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 100
            Height = 202
            Margins.Right = 0
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Padding.Right = 3
            TabOrder = 0
            object btnAddCon: TRzBitBtn
              Left = 0
              Top = 0
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Padrao'
              Align = alTop
              Caption = 'Adicionar'
              TabOrder = 0
              OnClick = btnAddConClick
            end
            object btnEditCon: TRzBitBtn
              Left = 0
              Top = 27
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Editar'
              TabOrder = 1
              OnClick = btnEditConClick
            end
            object btnDelCon: TRzBitBtn
              Left = 0
              Top = 54
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Deletar'
              TabOrder = 2
              OnClick = btnDelConClick
            end
          end
          object cxGridConexao: TcxGrid
            AlignWithMargins = True
            Left = 106
            Top = 3
            Width = 538
            Height = 202
            Align = alClient
            BorderStyle = cxcbsNone
            TabOrder = 1
            object cxGridConexaoDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dtsConexao
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object cxGridConexaoDBTableViewclicon_tipo: TcxGridDBColumn
                Caption = 'Tipo'
                DataBinding.FieldName = 'clicon_tipo'
                PropertiesClassName = 'TcxImageComboBoxProperties'
                Properties.Items = <
                  item
                    Description = 'AREA DE TRABALHO REMOTA'
                    ImageIndex = 0
                    Value = 'R'
                  end
                  item
                    Description = 'TEAM VIEWER'
                    Value = 'T'
                  end>
                Width = 162
              end
              object cxGridConexaoDBTableViewclicon_ip: TcxGridDBColumn
                Caption = 'IP / ID'
                DataBinding.FieldName = 'clicon_ip'
                Width = 150
              end
              object cxGridConexaoDBTableViewclicon_porta: TcxGridDBColumn
                Caption = 'Porta'
                DataBinding.FieldName = 'clicon_porta'
                Width = 150
              end
            end
            object cxGridConexaoLevel: TcxGridLevel
              GridView = cxGridConexaoDBTableView
            end
          end
        end
        object TabSheet4: TRzTabSheet
          Caption = '&4 - Contato'
          object RzPanel4: TRzPanel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 100
            Height = 202
            Margins.Right = 0
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Padding.Right = 3
            TabOrder = 0
            object btnAddContato: TRzBitBtn
              Left = 0
              Top = 0
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Padrao'
              Align = alTop
              Caption = 'Adicionar'
              TabOrder = 0
              OnClick = btnAddContatoClick
            end
            object btnEditContato: TRzBitBtn
              Left = 0
              Top = 27
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Editar'
              TabOrder = 1
              OnClick = btnEditContatoClick
            end
            object btnDelContato: TRzBitBtn
              Left = 0
              Top = 54
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Deletar'
              TabOrder = 2
              OnClick = btnDelContatoClick
            end
          end
          object cxGridContato: TcxGrid
            AlignWithMargins = True
            Left = 106
            Top = 3
            Width = 538
            Height = 202
            Align = alClient
            BorderStyle = cxcbsNone
            TabOrder = 1
            object cxGridContatoDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dtsContato
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object cxGridContatoDBTableViewtpcon_descricao: TcxGridDBColumn
                Caption = 'Tipo de contato'
                DataBinding.FieldName = 'tpcon_descricao'
                Width = 100
              end
              object cxGridContatoDBTableViewcont_contato: TcxGridDBColumn
                Caption = 'Contato'
                DataBinding.FieldName = 'cont_contato'
                Width = 230
              end
            end
            object cxGridContatoLevel: TcxGridLevel
              GridView = cxGridContatoDBTableView
            end
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 653
        ExplicitWidth = 653
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 597
          DataField = 'inativo'
          DataSource = dtsMaster
          ExplicitLeft = 597
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 281
        Width = 653
        ExplicitTop = 281
        ExplicitWidth = 653
        inherited btnCancelar: TRzRapidFireButton
          Left = 504
          ExplicitLeft = 504
        end
        inherited btnSalvar: TRzBitBtn
          Left = 580
          ExplicitLeft = 580
        end
        inherited btnNovo: TRzBitBtn
          Left = 124
          ExplicitLeft = 124
        end
        inherited btnAlterar: TRzBitBtn
          Left = 200
          ExplicitLeft = 200
        end
        inherited btnDeletar: TRzBitBtn
          Left = 276
          ExplicitLeft = 276
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 352
          ExplicitLeft = 352
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 428
          ExplicitLeft = 428
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 653
      ExplicitWidth = 653
      inherited spbFechar: TsSpeedButton
        Left = 620
        ExplicitLeft = 620
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 344
      Width = 653
      ExplicitTop = 344
      ExplicitWidth = 653
      inherited stpCriacao: TRzStatusPane
        Left = 253
        Width = 200
        ExplicitLeft = 353
        ExplicitTop = 0
        ExplicitWidth = 200
      end
      inherited stpModificacao: TRzStatusPane
        Left = 453
        Width = 200
        ExplicitLeft = 503
        ExplicitWidth = 200
      end
    end
  end
  inherited dtsMaster: TDataSource
    Left = 416
    Top = 32
  end
  inherited FDMaster: TFDQuery
    AfterClose = FDMasterAfterClose
    Connection = Con
    FetchOptions.AssignedValues = [evCache, evDetailCascade]
    FetchOptions.Cache = [fiDetails]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'SELECT '
      '   c.idclientes,'
      '   c.cli_tipo,'
      '   c.cli_cpfcnpj,'
      '   c.cli_razao,'
      '   c.cli_fantasia,'
      '   c.cli_dtnasc,'
      '   c.cli_documento,'
      '   c.cli_logradouro,'
      '   c.cli_complemento,'
      '   c.cli_numero,'
      '   c.cli_bairro,'
      '   c.id_cidade,'
      '   cid.descricao "cidade_nome",'
      '   cid.uf "cidade_uf",'
      '   c.cli_cep,'
      '   c.cli_logo,'
      '   c.dt_criacao,'
      '   c.dt_modificacao,'
      '   c.usu_criacao,'
      '   c.usu_modificacao,'
      '   c.inativo,'
      '   c.status'
      'FROM &SCHEMA.clientes c'
      'left join &SCHEMA.cidade cid on (cid.id_cidade = c.id_cidade)'
      'where c.idclientes = :IDCLIENTES')
    Left = 344
    Top = 32
    ParamData = <
      item
        Name = 'IDCLIENTES'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDMasteridclientes: TIntegerField
      FieldName = 'idclientes'
      Origin = 'idclientes'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDMastercli_tipo: TStringField
      FieldName = 'cli_tipo'
      Origin = 'cli_tipo'
      ProviderFlags = [pfInUpdate]
      FixedChar = True
      Size = 1
    end
    object FDMastercli_cpfcnpj: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_cpfcnpj'
      Origin = 'cli_cpfcnpj'
      ProviderFlags = [pfInUpdate]
    end
    object FDMastercli_razao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_razao'
      Origin = 'cli_razao'
      ProviderFlags = [pfInUpdate]
      Size = 45
    end
    object FDMastercli_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_fantasia'
      Origin = 'cli_fantasia'
      ProviderFlags = [pfInUpdate]
      Size = 45
    end
    object FDMastercli_dtnasc: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'cli_dtnasc'
      Origin = 'cli_dtnasc'
      ProviderFlags = [pfInUpdate]
    end
    object FDMastercli_documento: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_documento'
      Origin = 'cli_documento'
      ProviderFlags = [pfInUpdate]
      Size = 45
    end
    object FDMastercli_logradouro: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_logradouro'
      Origin = 'cli_logradouro'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object FDMastercli_complemento: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_complemento'
      Origin = 'cli_complemento'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object FDMastercli_numero: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_numero'
      Origin = 'cli_numero'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object FDMastercli_bairro: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_bairro'
      Origin = 'cli_bairro'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object FDMasterid_cidade: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id_cidade'
      Origin = 'id_cidade'
      ProviderFlags = [pfInUpdate]
    end
    object FDMastercli_cep: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cli_cep'
      Origin = 'cli_cep'
      ProviderFlags = [pfInUpdate]
      Size = 9
    end
    object FDMastercli_logo: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'cli_logo'
      Origin = 'cli_logo'
      ProviderFlags = [pfInUpdate]
    end
    object FDMasterdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
      ProviderFlags = [pfInUpdate]
    end
    object FDMasterdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
      ProviderFlags = [pfInUpdate]
    end
    object FDMasterusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
      ProviderFlags = [pfInUpdate]
    end
    object FDMasterusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
      ProviderFlags = [pfInUpdate]
    end
    object FDMasterinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      ProviderFlags = [pfInUpdate]
      FixedChar = True
      Size = 1
    end
    object FDMasterstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      ProviderFlags = [pfInUpdate]
      FixedChar = True
      Size = 1
    end
    object FDMastercidade_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cidade_nome'
      Origin = 'descricao'
      ProviderFlags = []
      Size = 100
    end
    object FDMastercidade_uf: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cidade_uf'
      Origin = 'uf'
      ProviderFlags = []
      Size = 2
    end
  end
  object sOpenPictureDialog: TsOpenPictureDialog [5]
    Filter = 'PNG graphics (*.png)|*.png'
    Options = [ofHideReadOnly, ofNoNetworkButton, ofEnableSizing, ofDontAddToRecent]
    Left = 144
    Top = 8
  end
  object FDFiliais: TFDQuery [6]
    BeforePost = FDFiliaisBeforePost
    OnNewRecord = FDFiliaisNewRecord
    MasterSource = dtsMaster
    MasterFields = 'idclientes'
    DetailFields = 'idclientes'
    Connection = Con
    FetchOptions.AssignedValues = [evMode, evCache, evDetailCascade]
    FetchOptions.Cache = [fiBlobs]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'SELECT '
      '   f.idfiliais,'
      '   f.idclientes,'
      '   f.fil_cnpj,'
      '   f.fil_razao,'
      '   f.fil_fantasia,'
      '   f.fil_dtcria,'
      '   f.fil_ie,'
      '   f.fil_logradouro,'
      '   f.fil_complemento,'
      '   f.fil_numero,'
      '   f.fil_bairro,'
      '   f.id_cidade,'
      '   cid.descricao "cidade_nome",'
      '   cid.uf "cidade_uf",'
      '   f.fil_cep,'
      '   f.fil_logo,'
      '   f.dt_criacao,'
      '   f.dt_modificacao,'
      '   f.usu_criacao,'
      '   f.usu_modificacao,'
      '   f.inativo,'
      '   f.status'
      'FROM &SCHEMA.filiais f'
      'left join &SCHEMA.cidade cid on (cid.id_cidade = f.id_cidade)'
      'where f.idclientes = :IDCLIENTES')
    Left = 488
    Top = 32
    ParamData = <
      item
        Name = 'IDCLIENTES'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDFiliaisidfiliais: TIntegerField
      FieldName = 'idfiliais'
      Origin = 'idfiliais'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDFiliaisidclientes: TIntegerField
      FieldName = 'idclientes'
      Origin = 'idclientes'
      Required = True
    end
    object FDFiliaisfil_cnpj: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_cnpj'
      Origin = 'fil_cnpj'
    end
    object FDFiliaisfil_razao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_razao'
      Origin = 'fil_razao'
      Size = 45
    end
    object FDFiliaisfil_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_fantasia'
      Origin = 'fil_fantasia'
      Size = 45
    end
    object FDFiliaisfil_dtcria: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'fil_dtcria'
      Origin = 'fil_dtcria'
    end
    object FDFiliaisfil_ie: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_ie'
      Origin = 'fil_ie'
      Size = 45
    end
    object FDFiliaisfil_logradouro: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_logradouro'
      Origin = 'fil_logradouro'
      Size = 100
    end
    object FDFiliaisfil_complemento: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_complemento'
      Origin = 'fil_complemento'
      Size = 100
    end
    object FDFiliaisfil_numero: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_numero'
      Origin = 'fil_numero'
      Size = 10
    end
    object FDFiliaisfil_bairro: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_bairro'
      Origin = 'fil_bairro'
      Size = 100
    end
    object FDFiliaisid_cidade: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id_cidade'
      Origin = 'id_cidade'
    end
    object FDFiliaiscidade_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cidade_nome'
      Origin = 'descricao'
      ProviderFlags = []
      Size = 100
    end
    object FDFiliaiscidade_uf: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cidade_uf'
      Origin = 'uf'
      ProviderFlags = []
      Size = 2
    end
    object FDFiliaisfil_cep: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fil_cep'
      Origin = 'fil_cep'
      Size = 9
    end
    object FDFiliaisfil_logo: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'fil_logo'
      Origin = 'fil_logo'
    end
    object FDFiliaisdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDFiliaisdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDFiliaisusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDFiliaisusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDFiliaisinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDFiliaisstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
  object dtsFiliais: TDataSource [7]
    DataSet = FDFiliais
    Left = 547
    Top = 31
  end
  object FDConexao: TFDQuery [8]
    OnNewRecord = FDConexaoNewRecord
    MasterSource = dtsMaster
    MasterFields = 'idclientes'
    DetailFields = 'idclientes'
    Connection = Con
    FetchOptions.AssignedValues = [evCache, evDetailCascade]
    FetchOptions.Cache = [fiBlobs]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'SELECT * FROM &SCHEMA.clientes_conexoes c'#13#10#10
      'where c.idclientes = :IDCLIENTES')
    Left = 488
    Top = 88
    ParamData = <
      item
        Name = 'IDCLIENTES'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDConexaoidclientes_conexao: TIntegerField
      FieldName = 'idclientes_conexao'
      Origin = 'idclientes_conexao'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDConexaoidclientes: TIntegerField
      FieldName = 'idclientes'
      Origin = 'idclientes'
      Required = True
    end
    object FDConexaoidfiliais: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'idfiliais'
      Origin = 'idfiliais'
    end
    object FDConexaoclicon_tipo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'clicon_tipo'
      Origin = 'clicon_tipo'
      FixedChar = True
      Size = 1
    end
    object FDConexaoclicon_ip: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'clicon_ip'
      Origin = 'clicon_ip'
    end
    object FDConexaoclicon_porta: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'clicon_porta'
      Origin = 'clicon_porta'
    end
    object FDConexaoclicon_usuario: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'clicon_usuario'
      Origin = 'clicon_usuario'
      Size = 45
    end
    object FDConexaoclicon_senha: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'clicon_senha'
      Origin = 'clicon_senha'
      Size = 45
    end
    object FDConexaodt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDConexaodt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDConexaousu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDConexaousu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDConexaoinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDConexaostatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
  object dtsConexao: TDataSource [9]
    DataSet = FDConexao
    Left = 488
    Top = 136
  end
  object FDContato: TFDQuery
    OnNewRecord = FDContatoNewRecord
    IndexFieldNames = 'idchave'
    MasterSource = dtsMaster
    MasterFields = 'idclientes'
    DetailFields = 'idchave'
    Connection = Con
    SQL.Strings = (
      'SELECT c.idcontatos, c.idtiposcontatos, tp.tpcon_descricao,'
      '       c.cont_descricao, c.cont_observacao,'
      
        '       c.cont_contato, c.idchave, c.cont_tabela, c.dt_criacao, c' +
        '.dt_modificacao,'
      '       c.usu_criacao, c.usu_modificacao, c.inativo, c.status'
      'FROM &SCHEMA.contatos c'
      
        'left join &SCHEMA.tiposcontatos tp on (tp.idtiposcontatos = c.id' +
        'tiposcontatos)'
      'where c.idchave = :IDCLIENTES'
      '  and c.cont_tabela = '#39'clientes'#39
      '  and c.status = '#39'A'#39)
    Left = 568
    Top = 88
    ParamData = <
      item
        Name = 'IDCLIENTES'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
    object FDContatoidcontatos: TIntegerField
      FieldName = 'idcontatos'
      Origin = 'idcontatos'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDContatoidtiposcontatos: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'idtiposcontatos'
      Origin = 'idtiposcontatos'
      Size = 100
    end
    object FDContatotpcon_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'tpcon_descricao'
      Origin = 'tpcon_descricao'
      ProviderFlags = []
      Size = 100
    end
    object FDContatocont_contato: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cont_contato'
      Origin = 'cont_contato'
      Size = 100
    end
    object FDContatoidchave: TIntegerField
      FieldName = 'idchave'
      Origin = 'idchave'
      Required = True
    end
    object FDContatocont_tabela: TStringField
      FieldName = 'cont_tabela'
      Origin = 'cont_tabela'
      Required = True
      Size = 50
    end
    object FDContatodt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDContatodt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDContatousu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDContatousu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDContatoinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDContatostatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
    object FDContatocont_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cont_descricao'
      Origin = 'cont_descricao'
      Size = 100
    end
    object FDContatocont_observacao: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'cont_observacao'
      Origin = 'cont_observacao'
      BlobType = ftMemo
    end
  end
  object dtsContato: TDataSource
    DataSet = FDContato
    Left = 568
    Top = 136
  end
end
