unit ufrmLogDetalhes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, sPanel, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzDBEdit, RzCmboBx,
  RzDBCmbo, Vcl.DBCtrls;

type
  TfrmLogDetalhes = class(TForm)
    sPanel1: TsPanel;
    Label1: TLabel;
    RzDBEdit1: TRzDBEdit;
    Label2: TLabel;
    RzDBComboBox1: TRzDBComboBox;
    Label3: TLabel;
    RzDBEdit2: TRzDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RzDBEdit3: TRzDBEdit;
    Label7: TLabel;
    RzDBEdit4: TRzDBEdit;
    Label8: TLabel;
    RzDBMemo1: TRzDBMemo;
    Label9: TLabel;
    RzDBMemo2: TRzDBMemo;
    Label10: TLabel;
    RzDBMemo3: TRzDBMemo;
    RzDBEdit5: TRzDBEdit;
    RzDBEdit6: TRzDBEdit;
    Label11: TLabel;
    RzDBMemo4: TRzDBMemo;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogDetalhes: TfrmLogDetalhes;

implementation

{$R *.dfm}

uses ufrmLog;

procedure TfrmLogDetalhes.FormShow(Sender: TObject);
begin
  if frmLog.FDLog.FieldByName('statuscod').AsInteger < 300 then
    RzDBEdit3.Font.Color := $00FF9428
  else
    RzDBEdit3.Font.Color := $005E5EFF;
end;

end.
