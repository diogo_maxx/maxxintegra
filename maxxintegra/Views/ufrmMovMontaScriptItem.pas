unit ufrmMovMontaScriptItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterDet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, Vcl.StdCtrls, sSpeedButton, RzButton, Vcl.Buttons,
  RzSpnEdt, Vcl.Mask, RzEdit, RzDBEdit, RzDBBnEd, Vcl.DBCtrls, Data.DB, StrUtils, RzRadChk, RzDBChk;

type
  TfrmMovMontaScriptItem = class(TfrmMasterDet)
    Label1: TLabel;
    mseidtiposcripts: TRzDBButtonEdit;
    msetpscr_descricao: TRzDBEdit;
    mmoscri_script: TRzDBMemo;
    Label2: TLabel;
    Label3: TLabel;
    dtsScriptItem: TDataSource;
    RzDBCheckBox1: TRzDBCheckBox;
    mmoscri_endpoint: TRzDBMemo;
    procedure mseidtiposcriptsExit(Sender: TObject);
    procedure mseidtiposcriptsButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMovMontaScriptItem: TfrmMovMontaScriptItem;

implementation

{$R *.dfm}

uses uPesquisas, uTipoScript, uUtilidades;

procedure TfrmMovMontaScriptItem.FormShow(Sender: TObject);
begin
  mmoscri_endpoint.Enabled := dtsScriptItem.DataSet.FieldByName('scri_tpcon').AsString <> 'B';
  mmoscri_script.Enabled   := dtsScriptItem.DataSet.FieldByName('scri_tpcon').AsString = 'B';
  ListaObrigatorios.Add(ifthen(dtsScriptItem.DataSet.FieldByName('scri_tpcon').AsString = 'B', 'mmoscri_script', 'msescri_endpoint'));
  ListaObrigatorios.Add('mseidtiposcripts');
  inherited;

  SetarFocoComponente(mseidtiposcripts);
end;

procedure TfrmMovMontaScriptItem.mseidtiposcriptsButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaTipoScript, ['C�digo', 'Descri��o'], [mseidtiposcripts, msetpscr_descricao]);
end;

procedure TfrmMovMontaScriptItem.mseidtiposcriptsExit(Sender: TObject);
begin
  inherited;
  if mseidtiposcripts.Text <> '' then
    Pesquisar(SQLPesquisaTipoScript, ' and idtiposcripts = ' + mseidtiposcripts.Text, ['C�digo', 'Descri��o'], [mseidtiposcripts, msetpscr_descricao]);
end;

end.
