inherited frmCadSistemasIntegradosConf: TfrmCadSistemasIntegradosConf
  Caption = 'Sistemas integrados - Configura'#231#227'o do sistema'
  ClientHeight = 407
  ClientWidth = 737
  ExplicitWidth = 737
  ExplicitHeight = 407
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 737
    Height = 407
    ExplicitWidth = 737
    ExplicitHeight = 407
    inherited pnlMaster: TRzPanel
      Width = 733
      Height = 359
      ExplicitWidth = 733
      ExplicitHeight = 359
      inherited pnlBotoes: TRzPanel
        Top = 321
        Width = 733
        ExplicitTop = 321
        ExplicitWidth = 733
        inherited btnCancelar: TRzRapidFireButton
          Left = 584
          ExplicitLeft = 485
        end
        inherited btnOK: TRzBitBtn
          Left = 660
          ExplicitLeft = 660
        end
      end
      object RzPageControl1: TRzPageControl
        Left = 0
        Top = 0
        Width = 733
        Height = 321
        Hint = ''
        ActivePage = TabSheet1
        Align = alClient
        BoldCurrentTab = True
        ShowFullFrame = False
        ShowShadow = False
        TabIndex = 0
        TabOrder = 1
        FixedDimension = 19
        object TabSheet1: TRzTabSheet
          Caption = '&1 - Dados gerais'
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 64
            Height = 13
            Caption = 'Tipo de script'
            FocusControl = mseidtiposcripts
          end
          object Label2: TLabel
            Left = 4
            Top = 44
            Width = 67
            Height = 13
            Caption = 'Tabela origem'
            FocusControl = mseconf_tabela
          end
          object Label3: TLabel
            Left = 319
            Top = 44
            Width = 49
            Height = 13
            Caption = 'Tabela pai'
            FocusControl = mseconf_tabelapai
          end
          object Label4: TLabel
            Left = 4
            Top = 84
            Width = 105
            Height = 13
            Caption = 'Campos chave origem'
            FocusControl = mseconf_codigo
          end
          object Label5: TLabel
            Left = 319
            Top = 84
            Width = 87
            Height = 13
            Caption = 'Campos chave pai'
            FocusControl = mseconf_codigopai
          end
          object Label6: TLabel
            Left = 319
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Prioridade'
            FocusControl = cbbconf_prioridade
          end
          object Label7: TLabel
            Left = 4
            Top = 124
            Width = 127
            Height = 13
            Caption = 'Campos chave origem SQL'
            FocusControl = mseconf_codigosql
          end
          object Label8: TLabel
            Left = 319
            Top = 124
            Width = 103
            Height = 13
            Caption = 'Nome da chave JSON'
            FocusControl = mseconf_nomejson
          end
          object mseidtiposcripts: TRzDBButtonEdit
            Left = 4
            Top = 18
            Width = 99
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'idtiposcripts'
            TabOrder = 0
            OnExit = mseidtiposcriptsExit
            AltBtnWidth = 15
            ButtonWidth = 15
            OnButtonClick = mseidtiposcriptsButtonClick
          end
          object msetpscr_descricao: TRzDBEdit
            Left = 102
            Top = 18
            Width = 211
            Height = 21
            TabStop = False
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'tpscr_descricao'
            Enabled = False
            TabOrder = 2
          end
          object mseconf_tabela: TRzDBEdit
            Left = 4
            Top = 58
            Width = 309
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'conf_tabela'
            TabOrder = 3
          end
          object mseconf_tabelapai: TRzDBEdit
            Left = 319
            Top = 58
            Width = 309
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'conf_tabelapai'
            TabOrder = 4
          end
          object mseconf_codigo: TRzDBEdit
            Left = 4
            Top = 98
            Width = 309
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'conf_codigo'
            TabOrder = 5
          end
          object mseconf_codigopai: TRzDBEdit
            Left = 319
            Top = 98
            Width = 309
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'conf_codigopai'
            TabOrder = 6
          end
          object cbbconf_prioridade: TRzDBComboBox
            Left = 319
            Top = 17
            Width = 145
            Height = 21
            DataField = 'conf_prioridade'
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            TabOrder = 1
            Items.Strings = (
              'Top'
              'Alta'
              'Normal'
              'Baixa')
            Values.Strings = (
              'A'
              'B'
              'C'
              'D')
          end
          object RzPageControl2: TRzPageControl
            AlignWithMargins = True
            Left = 3
            Top = 165
            Width = 727
            Height = 133
            Hint = ''
            ActivePage = TabSheet6
            Align = alBottom
            ShowFullFrame = False
            ShowShadow = False
            TabIndex = 0
            TabOrder = 9
            FixedDimension = 19
            object TabSheet6: TRzTabSheet
              Caption = '&1.1 - Base URL do Webservice'
              object mmoconf_baseurl: TRzDBMemo
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 721
                Height = 107
                Align = alClient
                DataField = 'conf_baseurl'
                DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
                TabOrder = 0
                WordWrap = False
              end
            end
            object TabSheet7: TRzTabSheet
              Caption = '&1.2 - Endpoint - POST'
              object mmoconf_endpoint: TRzDBMemo
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 721
                Height = 107
                Align = alClient
                DataField = 'conf_endpoint'
                DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
                TabOrder = 0
                WordWrap = False
              end
            end
            object TabSheet2: TRzTabSheet
              Caption = '&1.3 - Enpoint - PUT'
              object mmoconf_endpointupdate: TRzDBMemo
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 721
                Height = 107
                Align = alClient
                DataField = 'conf_endpointupdate'
                DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
                TabOrder = 0
              end
            end
            object TabSheet3: TRzTabSheet
              Caption = '&1.4 - Endpoint - DELETE'
              object mmoconf_endpointdelete: TRzDBMemo
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 721
                Height = 107
                Align = alClient
                DataField = 'conf_endpointdelete'
                DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
                TabOrder = 0
              end
            end
            object TabSheet8: TRzTabSheet
              Caption = '&1.5 - Quant. por requisi'#231#227'o'
              object RzDBMemo4: TRzDBMemo
                AlignWithMargins = True
                Left = 3
                Top = 3
                Width = 721
                Height = 107
                Align = alClient
                DataField = 'conf_maxqntpost'
                DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
                TabOrder = 0
              end
            end
          end
          object mseconf_codigosql: TRzDBEdit
            Left = 4
            Top = 138
            Width = 309
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'conf_codigosql'
            TabOrder = 7
          end
          object mseconf_nomejson: TRzDBEdit
            Left = 319
            Top = 138
            Width = 309
            Height = 21
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            DataField = 'conf_nomejson'
            TabOrder = 8
          end
          object cbxconf_encarray: TRzDBCheckBox
            Left = 473
            Top = 19
            Width = 139
            Height = 15
            DataField = 'conf_encarray'
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            ValueChecked = 'S'
            ValueUnchecked = 'N'
            Caption = 'Encapsular em um array?'
            TabOrder = 10
          end
        end
        object TabSheet5: TRzTabSheet
          Caption = '&2 - SQL busca'
          object mmoconf_scriptbusca: TRzDBMemo
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 727
            Height = 295
            Align = alClient
            DataField = 'conf_scriptbusca'
            DataSource = frmCadSistemasIntegrados.dtsSistemasIntegradosConf
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet9: TRzTabSheet
          Caption = '&3 - Conf. JSON'
          object RzPanel1: TRzPanel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 100
            Height = 295
            Margins.Right = 0
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Padding.Right = 3
            TabOrder = 0
            object btnAddConf: TRzBitBtn
              Left = 0
              Top = 0
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Padrao'
              Align = alTop
              Caption = 'Adicionar'
              TabOrder = 0
              OnClick = btnAddConfClick
            end
            object btnEditConf: TRzBitBtn
              Left = 0
              Top = 27
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Editar'
              TabOrder = 1
              OnClick = btnEditConfClick
            end
            object btnDelConf: TRzBitBtn
              Left = 0
              Top = 54
              Width = 96
              Height = 27
              HelpType = htKeyword
              HelpKeyword = 'Normal'
              Align = alTop
              Caption = 'Deletar'
              TabOrder = 2
              OnClick = btnDelConfClick
            end
          end
          object cxGridFiliais: TcxGrid
            AlignWithMargins = True
            Left = 106
            Top = 3
            Width = 624
            Height = 295
            Align = alClient
            BorderStyle = cxcbsNone
            TabOrder = 1
            object cxGridFiliaisDBTableView: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = frmCadSistemasIntegrados.dtsConfJSON
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.ColumnAutoWidth = True
              OptionsView.GroupByBox = False
              object cxGridFiliaisDBTableViewjson_camposql: TcxGridDBColumn
                Caption = 'Campo no SQL'
                DataBinding.FieldName = 'json_camposql'
                Width = 150
              end
              object cxGridFiliaisDBTableViewjson_campojson: TcxGridDBColumn
                Caption = 'Campo no JSON'
                DataBinding.FieldName = 'json_campojson'
                Width = 150
              end
              object cxGridFiliaisDBTableViewinativo: TcxGridDBColumn
                Caption = 'Inativo'
                DataBinding.FieldName = 'inativo'
                Width = 30
              end
            end
            object cxGridFiliaisLevel: TcxGridLevel
              GridView = cxGridFiliaisDBTableView
            end
          end
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 733
      ExplicitWidth = 733
      inherited spbFechar: TsSpeedButton
        Left = 700
        ExplicitLeft = 601
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 386
      Width = 733
      ExplicitTop = 386
      ExplicitWidth = 733
      inherited stpNomeTela: TRzStatusPane
        Width = 193
        ExplicitWidth = 193
      end
      inherited stpCriacao: TRzStatusPane
        Left = 333
        Width = 200
        ExplicitLeft = 334
        ExplicitTop = 0
        ExplicitWidth = 200
      end
      inherited stpModificacao: TRzStatusPane
        Left = 533
        Width = 200
        ExplicitLeft = 484
        ExplicitWidth = 200
        ExplicitHeight = 19
      end
    end
  end
end
