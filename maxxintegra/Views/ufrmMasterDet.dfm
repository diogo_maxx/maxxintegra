inherited frmMasterDet: TfrmMasterDet
  Caption = 'frmMasterDet'
  ClientHeight = 392
  ClientWidth = 656
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  ExplicitWidth = 656
  ExplicitHeight = 392
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 656
    Height = 392
    BorderOuter = fsGroove
    BorderShadow = clWhite
    ExplicitWidth = 656
    ExplicitHeight = 392
    inherited pnlMaster: TRzPanel
      Left = 2
      Top = 27
      Width = 652
      Height = 344
      ExplicitLeft = 2
      ExplicitTop = 27
      ExplicitWidth = 652
      ExplicitHeight = 344
      object pnlBotoes: TRzPanel
        Left = 0
        Top = 306
        Width = 652
        Height = 38
        Align = alBottom
        BorderOuter = fsFlat
        BorderSides = [sdBottom]
        TabOrder = 0
        object btnCancelar: TRzRapidFireButton
          AlignWithMargins = True
          Left = 503
          Top = 3
          Width = 70
          Height = 31
          Align = alRight
          Caption = '&Cancelar'
          OnClick = btnCancelarClick
          ExplicitLeft = 679
          ExplicitTop = 4
        end
        object btnOK: TRzBitBtn
          AlignWithMargins = True
          Left = 579
          Top = 3
          Width = 70
          Height = 31
          HelpType = htKeyword
          HelpKeyword = 'Padrao'
          ModalResult = 1
          Align = alRight
          Caption = '&OK'
          TabOrder = 0
          OnClick = btnOKClick
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Left = 2
      Top = 2
      Width = 652
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 652
      inherited spbFechar: TsSpeedButton
        Left = 619
        ExplicitLeft = 623
      end
      inherited lblTitulo: TLabel
        Height = 17
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Left = 2
      Top = 371
      Width = 652
      ExplicitLeft = 2
      ExplicitTop = 371
      ExplicitWidth = 652
      inherited stpCriacao: TRzStatusPane
        Left = 352
        ExplicitLeft = 356
      end
      inherited stpModificacao: TRzStatusPane
        Left = 502
        ExplicitLeft = 506
      end
    end
  end
end
