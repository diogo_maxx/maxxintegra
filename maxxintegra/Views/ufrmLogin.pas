unit ufrmLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMaster, Vcl.StdCtrls, Vcl.Buttons, RzStatus, RzPanel, Vcl.ExtCtrls, acImage,
  sSpeedButton, acPNG, Vcl.Mask, RzEdit, RzButton, RzSpnEdt, Vcl.ComCtrls, RzTreeVw, System.ImageList, Vcl.ImgList,
  acAlphaImageList, Threading, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.Diagnostics, System.SyncObjs;

Type
  TTaskHelper = class helper for TTask
  private type
    TUnsafeTaskEx = record
    private
      [Unsafe]
      // preciso de um record UNSAFE para nao incrementar o RefCount da Interface
      FTask: TTask;
    public
      property Value: TTask read FTask write FTask;
    end;
  public
    class function WaitForAllEx(AArray: Array of ITask;
    ATimeOut: int64 = INFINITE): boolean;
  end;

type
  TfrmLogin = class(TfrmMaster)
    Label1: TLabel;
    Label2: TLabel;
    mseUSUARIO: TRzMaskEdit;
    mseSENHA: TRzMaskEdit;
    btnEntrar: TRzBitBtn;
    btnSair: TRzRapidFireButton;
    mmoResultados: TRzMemo;
    procedure FormShow(Sender: TObject);
    procedure btnEntrarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.dfm}

uses uConstantes, uAtualizador, uLayoutFormulario, uConexao, udmMaxxIntegra, U_Cipher, uMensagens;

procedure TfrmLogin.btnEntrarClick(Sender: TObject);
begin
  inherited;

  try
    if dmMaxxIntegra = nil then
      dmMaxxIntegra := TdmMaxxIntegra.Create(nil);

    if CriaConexao(dmMaxxIntegra.FDConnection) then
    begin
      dmMaxxIntegra.fdUsuario.Close;
      dmMaxxIntegra.fdUsuario.Params[0].AsString := mseUSUARIO.Text;
      dmMaxxIntegra.fdUsuario.Open;

      if dmMaxxIntegra.fdUsuario.IsEmpty then
      begin
        Mensagem('C', 'Usu�rio inv�lido!', Self);
        mseUSUARIO.SetFocus;
        Abort;
      end;

      if dmMaxxIntegra.fdUsuariousu_senha.AsString <> MD5Hash(mseSENHA.Text) then
      begin
        Mensagem('C', 'Senha inv�lida!', Self);
        mseSENHA.SetFocus;
        Abort;
      end;
    end;

    Self.Close;
    ModalResult := mrOk;
  finally

  end;
end;

procedure TfrmLogin.btnSairClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmLogin.FormShow(Sender: TObject);
var
  IAtualiza: ITask;
begin
  inherited;

  IAtualiza := TTask.Create(
  procedure()
  begin
    AtualizarBanco(mmoResultados);
  end);
  IAtualiza.Start;

  {$IFDEF DEBUG}
       mseUSUARIO.Text := 'sys';
       mseSENHA.Text   := 'Maxx80131980';
  {$ENDIF}
end;

{ TTaskHelper }

class function TTaskHelper.WaitForAllEx(AArray: array of ITask; ATimeOut: int64): boolean;
var
  FEvent: TEvent;
  task: TUnsafeTaskEx;
  i: integer;
  taskInter: TArray<TUnsafeTaskEx>;
  completou: boolean;
  Canceled, Exceptions: boolean;
  ProcCompleted: TProc<ITask>;
  LHandle: THandle;
  LStop: TStopwatch;
begin
  LStop := TStopwatch.StartNew;
  ProcCompleted := procedure(ATask: ITask)
    begin
      FEvent.SetEvent;
    end;

  Canceled := false;
  Exceptions := false;
  result := true;
  try
    for i := low(AArray) to High(AArray) do
    begin
      task.Value := TTask(AArray[i]);
      if task.Value = nil then
        raise EArgumentNilException.Create('Wait Nil Task');

      completou := task.Value.IsComplete;
      if not completou then
      begin
        taskInter := taskInter + [task];
      end
      else
      begin
        if task.Value.HasExceptions then
          Exceptions := true
        else if task.Value.IsCanceled then
          Canceled := true;
      end;
    end;

    try
      FEvent := TEvent.Create();
      for task in taskInter do
      begin
        try
          FEvent.ResetEvent;
          if LStop.ElapsedMilliseconds > ATimeOut then
            break;
          LHandle := FEvent.Handle;
          task.Value.AddCompleteEvent(ProcCompleted);
          while not task.Value.IsComplete do
          begin
            try
              if LStop.ElapsedMilliseconds > ATimeOut then
                break;
                  if MsgWaitForMultipleObjectsEx(1, LHandle,
                    ATimeOut - LStop.ElapsedMilliseconds, QS_ALLINPUT, 0)
                    = WAIT_OBJECT_0 + 1 then
                    application.ProcessMessages;
            finally
            end;
          end;
          if task.Value.IsComplete then
          begin
            if task.Value.HasExceptions then
              Exceptions := true
            else if task.Value.IsCanceled then
              Canceled := true;
          end;
        finally
          task.Value.removeCompleteEvent(ProcCompleted);

        end;
      end;
    finally
      FEvent.Free;
    end;
  except
    result := false;
  end;

  if (not Exceptions and not Canceled) then
    Exit;
  if Exceptions or Canceled then
    raise EOperationCancelled.Create
      ('One Or More Tasks HasExceptions/Canceled');

end;

end.
