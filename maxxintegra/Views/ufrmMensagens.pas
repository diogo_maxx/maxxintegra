unit ufrmMensagens;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls, RzPanel, RzButton,
  RzSpnEdt, RzEdit, acImage, acPNG, StrUtils, System.ImageList, Vcl.ImgList, acAlphaImageList;

type
  TfrmMensagens = class(TForm)
    pnlTitulo: TRzPanel;
    spbFechar: TsSpeedButton;
    lblTitulo: TLabel;
    pnlMaster: TRzPanel;
    btnNao: TRzRapidFireButton;
    btnOk: TRzBitBtn;
    mmoMensagem: TRzMemo;
    imgIcone: TsImage;
    sAlphaImageList1: TsAlphaImageList;
    procedure FormShow(Sender: TObject);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnNaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    TipoMensagem: String;
    Mensagem    : String;
  end;

var
  frmMensagens: TfrmMensagens;

const
  COR_ERRO       = $004D4ED4;
  COR_CUIDADO    = $0051B9FF;
  COR_INFORMACAO = $00D27619;
  COR_QUESTAO    = $00757575;

implementation

{$R *.dfm}

uses uLayoutFormulario, uConstantes, uUtilidades;

procedure TfrmMensagens.btnNaoClick(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;

procedure TfrmMensagens.FormShow(Sender: TObject);

procedure ConfigurarTela(const Cor: Integer);
begin
  pnlTitulo.Color      := Cor;

  btnOk.Color          := Cor;
  btnOk.FrameColor     := Cor;
  btnOk.HotTrackColor  := Cor;
  btnOk.HighlightColor := Cor;
  btnOk.Font.Color     := LAYOUT_CORFONTBARRATITULO;

  btnNao.Flat          := True;
  btnNao.Font.Color    := Cor;
  btnNao.Font.Style    := [fsBold];

  mmoMensagem.Color         := LAYOUT_COR_FORM;
  mmoMensagem.ReadOnlyColor := LAYOUT_COR_FORM;
  mmoMensagem.Font.Color    := LAYOUT_CORLABEL_NORMAL;

  SetarFocoComponente(btnOk);
end;

begin
  try
    pnlMaster.Color := LAYOUT_COR_FORM;

    case AnsiIndexStr(TipoMensagem, ['E', 'C', 'Q', 'I']) of
      0:
      begin
        lblTitulo.Caption   := 'Ops... tem algo errado!';
        ConfigurarTela(COR_ERRO);


        imgIcone.ImageIndex := 0;
        btnOk.Visible       := True;
        btnNao.Visible      := False;
        btnOk.Caption       := '&OK';
      end;
      1:
      begin
        lblTitulo.Caption   := 'Fique atento!';
        ConfigurarTela(COR_CUIDADO);

        imgIcone.ImageIndex := 2;
        btnOk.Visible       := True;
        btnNao.Visible      := False;
        btnOk.Caption       := '&OK';
      end;
      2:
      begin
        lblTitulo.Caption   := 'Deseja realmente realizar esta a��o?';
        ConfigurarTela(COR_QUESTAO);

        imgIcone.ImageIndex := 3;
        btnOk.Visible       := True;
        btnNao.Visible      := True;
        btnOk.Caption       := '&Sim';
      end;
      3:
      begin
        lblTitulo.Caption   := 'Est� tudo certo!';
        ConfigurarTela(COR_INFORMACAO);

        imgIcone.ImageIndex := 1;
        btnOk.Visible       := True;
        btnNao.Visible      := False;
        btnOk.Caption       := '&OK';
      end;
    end;

    mmoMensagem.Lines.Add(Mensagem);
  finally

  end;
end;

procedure TfrmMensagens.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  if Button = mbleft then
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

end.
