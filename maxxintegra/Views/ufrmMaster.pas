unit ufrmMaster;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, RzEdit, RzButton, RzSpnEdt, RzDBEdit, RTTI, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxGraphics;

type
  TfrmMaster = class(TForm)
    pnlTitulo: TRzPanel;
    spbFechar: TsSpeedButton;
    lblTitulo: TLabel;
    pnlMaster: TRzPanel;
    stbSTATUS: TRzStatusBar;
    stpNomeTela: TRzStatusPane;
    stpCriacao: TRzStatusPane;
    stpModificacao: TRzStatusPane;
    sImage1: TsImage;
    btnPesquisaImagem: TBitBtn;
    pnlBordas: TRzPanel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure spbFecharClick(Sender: TObject);
    procedure ConfigurarFormulario; virtual;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
   /// <summary>Procedure para padronizar e centralizar a diferenciação das cores das colunas quando selecionadas</summary>
   /// <author>Diogo Cardoso</author>
    procedure CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
             AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean); virtual;
   /// <summary>Procedure para padronizar e centralizar quando no memo dar 2 enters sair</summary>
   /// <author>Diogo Cardoso</author>
    procedure KeyPress(Sender: TObject; var Key: Char); virtual;

  private
    ContEnter: Integer;
    { Private declarations }
  public
    { Public declarations }
    ListaObrigatorios: TStringList;
  end;

var
  frmMaster: TfrmMaster;

implementation

{$R *.dfm}

uses uConstantes, uLayoutFormulario, ufrmPrincipal, uMensagens, uUtilidades;

procedure TfrmMaster.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(ListaObrigatorios);
end;

procedure TfrmMaster.FormCreate(Sender: TObject);
begin
  ListaObrigatorios := TStringList.Create;
end;

procedure TfrmMaster.FormKeyPress(Sender: TObject; var Key: Char);
begin
  try
    if Self.ActiveControl <> nil then
    begin
      if (Key = #13) and
         (Self.ActiveControl.ClassType <> TRzDBMemo) and
         (Self.ActiveControl.ClassType <> TMemo) and
         (not(Self.ActiveControl is TCustomMemo)) and
         (TEdit(Self.ActiveControl).Name <> 'mseCODIGO') then
      begin
         Perform(CM_DialogKey, VK_TAB, 0);
         Key := #0;
      end;
    end;
  except
  end;
end;

procedure TfrmMaster.FormShow(Sender: TObject);
begin
  ConfigurarFormulario;

  Self.Refresh;
  Self.Repaint;
  Application.ProcessMessages;

end;


procedure TfrmMaster.KeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
     inc(ContEnter)
  else ContEnter := 0;

  if (ContEnter < 2) then
    Key := UppCase(Key)
  else Perform(CM_DialogKey, VK_TAB, 0);
end;

procedure TfrmMaster.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  if Button = mbleft then
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

procedure TfrmMaster.spbFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMaster.ConfigurarFormulario;
begin
  Color                := LAYOUT_COR_FORM;
  lblTitulo.Font.Color := LAYOUT_COR_FORM_FONTTITULO;
  pnlTitulo.Color      := LAYOUT_COR_FORM_TITULO;

  lblTitulo.Caption    := Self.Caption;
  stpNomeTela.Caption  := Self.Name;

  stbSTATUS.Height := 15;
  stbSTATUS.GradientColorStart := LAYOUT_COR_FORM_TITULO;
  stbSTATUS.GradientColorStop  := LAYOUT_COR_FORM_TITULO;
  stbSTATUS.VisualStyle        := TRzVisualStyle.vsGradient;
  stpNomeTela.Font.Color       := LAYOUT_COR_FORM_FONTTITULO;
  stpCriacao.Font.Color        := LAYOUT_COR_FORM_FONTTITULO;
  stpModificacao.Font.Color    := LAYOUT_COR_FORM_FONTTITULO;

  PadronizarTela(Self, ListaObrigatorios);

end;

procedure TfrmMaster.CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if AViewInfo.Selected then
    ACanvas.SetBrushColor($00FBDEBB);
end;




end.
