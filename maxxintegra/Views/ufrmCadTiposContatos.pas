unit ufrmCadTiposContatos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, Data.DB, FireDAC.Comp.DataSet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, sSpeedButton, RzButton,
  Vcl.Buttons, RzSpnEdt, RzRadChk, RzDBChk, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzBtnEdt, RzTabs, RzDBEdit;

type
  TfrmCadTiposContatos = class(TfrmMasterCad)
    Label1: TLabel;
    msetpcon_descricao: TRzDBEdit;
    FDMasteridtiposcontatos: TIntegerField;
    FDMastertpcon_descricao: TStringField;
    FDMasterdt_criacao: TSQLTimeStampField;
    FDMasterdt_modificacao: TSQLTimeStampField;
    FDMasterusu_criacao: TIntegerField;
    FDMasterusu_modificacao: TIntegerField;
    FDMasterinativo: TStringField;
    FDMasterstatus: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
  private
    procedure PesquisaMaster; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadTiposContatos: TfrmCadTiposContatos;

implementation

{$R *.dfm}

uses udmMaxxIntegra, uTipoContato, uUtilidades;

procedure TfrmCadTiposContatos.btnAlterarClick(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(msetpcon_descricao);
end;

procedure TfrmCadTiposContatos.btnNovoClick(Sender: TObject);
begin
  inherited;
  SetarFocoComponente(msetpcon_descricao);
end;

procedure TfrmCadTiposContatos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(frmCadTiposContatos);
end;

procedure TfrmCadTiposContatos.FormCreate(Sender: TObject);
begin
  inherited;
  TABELABD := 'tiposcontatos';
  CAMPOID  := 'idtiposcontatos';
  ListaObrigatorios.Add('msetpcon_descricao');
end;

procedure TfrmCadTiposContatos.PesquisaMaster;
begin
  SCRIPT_BUSCA := SQLPesquisaTipoContato;
  inherited;
end;

end.
