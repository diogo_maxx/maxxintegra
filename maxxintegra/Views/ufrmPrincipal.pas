unit ufrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls, Threading, acPNG, RzBckgnd,
  dxGDIPlusClasses, acImage, System.ImageList, Vcl.ImgList, acAlphaImageList, FireDAC.Phys.MySQLDef, FireDAC.UI.Intf,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Stan.Intf, FireDAC.Phys, FireDAC.Phys.MySQL, Vcl.ComCtrls, RzPanel,
  RzStatus, RzCommon, RzButton, Vcl.CategoryButtons;

type
  TfrmPrincipal = class(TForm)
    pnlTitulo: TPanel;
    btnMinimizar: TsSpeedButton;
    btnClose: TsSpeedButton;
    RzPanel2: TRzPanel;
    sImage2: TsImage;
    pnlDesktop: TPanel;
    pnlBordas: TRzPanel;
    sAlphaImageList1: TsAlphaImageList;
    pnlClientes: TRzPanel;
    btnClientes: TRzToolButton;
    pnlConfIntegra: TRzPanel;
    btnConfIntegra: TRzToolButton;
    pnlTabelas: TRzPanel;
    btnTabelas: TRzToolButton;
    RzBackground1: TRzBackground;
    pnlLog: TRzPanel;
    btnLog: TRzToolButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCloseClick(Sender: TObject);
    procedure pnlTituloMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnClientesClick(Sender: TObject);
    procedure btnConfIntegraClick(Sender: TObject);
    procedure btnTabelasClick(Sender: TObject);
    procedure btnMinimizarClick(Sender: TObject);
    procedure btnLogClick(Sender: TObject);
  private
    procedure ReajustarTela;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses uConstantes, uAtualizador, ufrmLogin, udmMaxxIntegra, uConexao, ufrmCadClientes,
  ufrmCadSistemasIntegradosConf, ufrmCadSistemasIntegrados, ufrmCadTipoScripts, ufrmLog;

procedure TfrmPrincipal.btnClientesClick(Sender: TObject);
begin
  if frmCadSistemasIntegrados <> nil then
    frmCadSistemasIntegrados.Hide;

  if frmCadTipoScripts <> nil then
    frmCadTipoScripts.Hide;

  if frmCadClientes = nil then
  begin
    Application.CreateForm(TfrmCadClientes, frmCadClientes);
    frmCadClientes.Parent      := pnlDesktop;
    frmCadClientes.WindowState := wsMaximized;

    frmCadClientes.Show;

    pnlClientes.BorderSides := [sdBottom];
  end
  else
  begin
    if frmCadClientes.Visible then
      frmCadClientes.Hide
    else
    begin
      frmCadClientes.Show;
      frmCadClientes.BringToFront;
    end;
  end;
end;

procedure TfrmPrincipal.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmPrincipal.btnConfIntegraClick(Sender: TObject);
begin
  if frmCadClientes <> nil then
    frmCadClientes.Hide;

  if frmCadTipoScripts <> nil then
    frmCadTipoScripts.Hide;


  if frmCadSistemasIntegrados = nil then
  begin
    Application.CreateForm(TfrmCadSistemasIntegrados, frmCadSistemasIntegrados);
    frmCadSistemasIntegrados.Parent      := pnlDesktop;
    frmCadSistemasIntegrados.WindowState := wsMaximized;

    frmCadSistemasIntegrados.Show;

    pnlConfIntegra.BorderSides := [sdBottom];
  end
  else
  begin
    if frmCadSistemasIntegrados.Visible then
      frmCadSistemasIntegrados.Hide
    else
      frmCadSistemasIntegrados.Show;
  end;

  Application.CreateForm(TfrmLog, frmLog);
  frmLog.Show;

end;

procedure TfrmPrincipal.btnMinimizarClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TfrmPrincipal.btnTabelasClick(Sender: TObject);
begin
  if frmCadClientes <> nil then
    frmCadClientes.Hide;

  if frmCadSistemasIntegrados <> nil then
    frmCadSistemasIntegrados.Hide;

  if frmCadTipoScripts = nil then
  begin
    Application.CreateForm(TfrmCadTipoScripts, frmCadTipoScripts);
    frmCadTipoScripts.Parent      := pnlDesktop;
    frmCadTipoScripts.WindowState := wsMaximized;

    frmCadTipoScripts.Show;

    pnlTabelas.BorderSides := [sdBottom];
  end
  else
  begin
    if frmCadTipoScripts.Visible then
      frmCadTipoScripts.Hide
    else
      frmCadTipoScripts.Show;
  end;
end;

procedure TfrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    Application.Terminate;
  finally
  end;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
//  {$IFDEF DEBUG}
//    dmMaxxIntegra.fdUsuario.Close;
//    dmMaxxIntegra.fdUsuario.Params[0].AsString := 'SYS';
//    dmMaxxIntegra.fdUsuario.Open;
//  {$ELSE}
//    try
//      Application.CreateForm(TfrmLogin, frmLogin);
//      frmLogin.Position := poScreenCenter;
//      frmLogin.ShowModal;
//
//      if frmLogin.ModalResult <> mrOk then
//        Self.Close;
//    finally
//    end;
//  {$ENDIF}

  if dmMaxxIntegra = nil then dmMaxxIntegra := TdmMaxxIntegra.Create(nil);

  if CriaConexao(dmMaxxIntegra.FDConnection) then
  begin
    dmMaxxIntegra.fdUsuario.Close;
    dmMaxxIntegra.fdUsuario.Params[0].AsString := '1';
    dmMaxxIntegra.fdUsuario.Open;
  end;

  ReajustarTela;
//  frmMenu.Show;
end;

procedure TfrmPrincipal.pnlTituloMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  if Button = mbleft then
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

procedure TfrmPrincipal.ReajustarTela;
begin
//  Self.WindowState := wsMaximized;
//  Self.Align       := alClient;


  /////////////////////////////////////////
  ///  Menu                             ///
  /////////////////////////////////////////
//  if frmMenu = nil then
//    Application.CreateForm(TfrmMenu, frmMenu);

//  frmMenu.Left   := 0;
//  frmMenu.Top    := pnlTitulo.Height;
//  frmMenu.Height := Self.ClientHeight - pnlTitulo.Height;
//  frmMenu.Show;

  pnlTitulo.Color      := LAYOUT_BARRATITULO;
  pnlTitulo.Font.Color := LAYOUT_CORFONTBARRATITULO;
end;

procedure TfrmPrincipal.btnLogClick(Sender: TObject);
begin
  if frmCadSistemasIntegrados <> nil then
    frmCadSistemasIntegrados.Hide;

  if frmCadTipoScripts <> nil then
    frmCadTipoScripts.Hide;

  if frmLog = nil then
  begin
    Application.CreateForm(TfrmLog, frmLog);
    frmLog.Parent      := pnlDesktop;
    frmLog.WindowState := wsMaximized;

    frmLog.Show;

    pnlLog.BorderSides := [sdBottom];
  end
  else
  begin
    if frmLog.Visible then
      frmLog.Hide
    else
    begin
      frmLog.Show;
      frmLog.BringToFront;
    end;
  end;
end;

end.
