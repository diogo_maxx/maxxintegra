unit ufrmCadSistemasIntegrados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, RzRadChk, RzDBChk, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzBtnEdt, RzButton,
  RzSpnEdt, Vcl.Buttons, RzStatus, RzPanel, RzTabs, Vcl.ExtCtrls, acImage, sSpeedButton, RzDBEdit, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.VCLUI.Wait, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzCmboBx, RzDBCmbo, Vcl.DBCtrls, RzBHints,
  RzDBBnEd, StrUtils;

const
  HINT_TRIGGER = 'Preenchimento dos campos da tela: ' + #$D#$A +
                 '- "Campos chave origem" e "Campos chave pai": caso haja mais de um valor, utilizar o separador '';''. ' + #$D#$A +
                 ' ' + #$D#$A +
                 'Como montar os scripts da busca: ' + #$D#$A +
                 'TAGS: ' + #$D#$A +
                 '   - &SCHEMA: Referente ao schema (se for necess�rio) para realizar a pesquisa; ' + #$D#$A +
                 '   - &FILTRO: Referente aos filtros para busca dos registros. *OBRIGAT�RIO ' + #$D#$A +
                 ' ' + #$D#$A +
                 'Como montar os scripts das triggers: ' + #$D#$A +
                 'TAGS: ' + #$D#$A +
                 '   - /*TABELA_REG*/    : Utilizada para referenciar a tabela origem; ' + #$D#$A +
                 '   - /*CTABELA_REG*/  : Utilizada para referenciar os campos chave da tabela origem; ' + #$D#$A +
                 '   - /*TABELA_PAI*/    : Utilizada para referenciar a tabela pai; ' + #$D#$A +
                 '   - /*CODIGO_PAI*/    : Utilizada para referenciar os campos chave da tabela pai; ' + #$D#$A +
                 ' ' + #$D#$A +
                 'O servi�o do lado do cliente j� est� preconfigurado para substituir estas TAGS com os valores informados neste cadastro. ' + #$D#$A +
                 'Padr�o para o nome da trigger: MAXXINTEGRA_/*TABELA_REG*/_TIPO TRIGGER. ' + #$D#$A +
                 ' ' + #$D#$A +
                 'TIPO TRIGGER (Composto por 6 caracteres concatenados): ' + #$D#$A +
                 '   - AFT   : Identificando que executar� ap�s (AFTER) o registro ser inclu�do na tabela; ' + #$D#$A +
                 '   - BEF    : Identificando que executar� antes (BEFORE) o registro ser inclu�do na tabela; ' + #$D#$A +
                 '   - INS    : Identificando que executar� quando inserir (INSERT) o registro; ' + #$D#$A +
                 '   - UPD   : Identificando que executar� quando atualizar (UPDATE) o registro; ' + #$D#$A +
                 '   - DEL    : Identificando que executar� quando deletar (DELETE) o registro.';

  TRIGGER_INS = 'CREATE OR ALTER TRIGGER MAXX_/*TABELA_REG*/_AFTINS FOR /*TABELA_REG*/ ' + #$D#$A +
                'ACTIVE AFTER INSERT POSITION 0 ' + #$D#$A +
                'AS declare variable CONTEM INTEGER; ' + #$D#$A +
                'begin ' + #$D#$A +
                ' ' + #$D#$A +
                'POST_EVENT ''/*TABELA_REG*/_AFTINS''; ' + #$D#$A +
                ' ' + #$D#$A +
                'select count(*) from maxx_integracao m ' + #$D#$A +
                'where m.codigo_reg like /*CODIGO_REG*/ ' + #$D#$A +
                'into :CONTEM; ' + #$D#$A +
                ' ' + #$D#$A +
                   'if (CONTEM <= 0) then ' + #$D#$A +
                '   begin ' + #$D#$A +
                '     INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, EXEC) ' + #$D#$A +
                '     VALUES (/*CTABELA_REG*/, /*TABELA_PAI*/, /*CODIGO_REG*/, /*CODIGO_PAI*/, current_timestamp, ''I'', ''N''); ' + #$D#$A +
                '   end ' +  #$D#$A +
                'end;';

  TRIGGER_UPD = 'CREATE OR ALTER TRIGGER MAXX_/*TABELA_REG*/_AFTUPD FOR /*TABELA_REG*/ ' + #$D#$A +
                'ACTIVE AFTER UPDATE POSITION 0 ' + #$D#$A +
                'AS declare variable CONTEM INTEGER; ' + #$D#$A +
                'begin ' + #$D#$A +
                ' ' + #$D#$A +
                'POST_EVENT ''/*TABELA_REG*/_AFTUPD''; ' + #$D#$A +
                ' ' + #$D#$A +
                'select count(*) from maxx_integracao m ' + #$D#$A +
                'where m.codigo_reg like /*CODIGO_REG*/ ' + #$D#$A +
                'into :CONTEM; ' + #$D#$A +
                ' ' + #$D#$A +
                'if (CONTEM <= 0) then ' + #$D#$A +
                'begin ' + #$D#$A +
                '  INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, EXEC) ' + #$D#$A +
                '  VALUES (/*CTABELA_REG*/, /*TABELA_PAI*/, /*CODIGO_REG*/, /*CODIGO_PAI*/, current_timestamp, ''A'', ''N''); ' + #$D#$A +
                'end ' + #$D#$A +
                'end;';

  TRIGGER_DEL = 'CREATE OR ALTER TRIGGER MAXX_/*TABELA_REG*/_BEFDEL FOR /*TABELA_REG*/ ' + #$D#$A +
                'ACTIVE BEFORE DELETE POSITION 0 ' + #$D#$A +
                'AS declare variable CONTEM INTEGER; ' + #$D#$A +
                'begin ' + #$D#$A +
                ' ' + #$D#$A +
                '  POST_EVENT ''/*TABELA_REG*/_BEFDEL''; ' + #$D#$A +
                ' ' + #$D#$A +
                'select count(*) from maxx_integracao m ' + #$D#$A +
                'where m.codigo_reg like /*CODIGO_REG*/ ' + #$D#$A +
                'into :CONTEM; ' + #$D#$A +
                ' ' + #$D#$A +
                'if (CONTEM <= 0) then ' + #$D#$A +
                'begin ' + #$D#$A +
                '  INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, EXEC) ' + #$D#$A +
                '  VALUES (/*CTABELA_REG*/, /*TABELA_PAI*/, /*CODIGO_REG*/, /*CODIGO_PAI*/, current_timestamp, ''D'', ''N''); ' + #$D#$A +
                'end ' + #$D#$A +
                'end; ';

type
  TfrmCadSistemasIntegrados = class(TfrmMasterCad)
    Label1: TLabel;
    mseSIS_NOME: TRzDBEdit;
    FDSistemasIntegradosConf: TFDQuery;
    dtsSistemasIntegradosConf: TDataSource;
    Label2: TLabel;
    cbbsis_tpcon: TRzDBComboBox;
    TabSheet4: TRzTabSheet;
    RzPanel1: TRzPanel;
    btnAddTab: TRzBitBtn;
    btnEditTab: TRzBitBtn;
    btnDelTab: TRzBitBtn;
    cxGridFiliais: TcxGrid;
    cxGridFiliaisDBTableView: TcxGridDBTableView;
    cxGridFiliaisDBTableViewconf_tabela: TcxGridDBColumn;
    cxGridFiliaisDBTableViewconf_tabelapai: TcxGridDBColumn;
    cxGridFiliaisDBTableViewconf_codigo: TcxGridDBColumn;
    cxGridFiliaisDBTableViewconf_codigopai: TcxGridDBColumn;
    cxGridFiliaisLevel: TcxGridLevel;
    RzBalloonHints1: TRzBalloonHints;
    Label14: TLabel;
    mseidclientes: TRzDBButtonEdit;
    msecli_fantasia: TRzDBEdit;
    Label15: TLabel;
    mseidfiliais: TRzDBButtonEdit;
    msefil_fantasia: TRzDBEdit;
    cbbsis_protocol: TRzDBComboBox;
    Label17: TLabel;
    Label18: TLabel;
    msesis_server: TRzDBEdit;
    msesis_port: TRzDBEdit;
    Label19: TLabel;
    msesis_database: TRzDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    msesis_username: TRzDBEdit;
    msesis_password: TRzDBEdit;
    Label22: TLabel;
    RzPageControl3: TRzPageControl;
    TabSheet9: TRzTabSheet;
    TabSheet10: TRzTabSheet;
    TabSheet11: TRzTabSheet;
    RzPageControl4: TRzPageControl;
    RzTabSheet1: TRzTabSheet;
    mmosis_triggerinsert: TRzDBMemo;
    RzTabSheet2: TRzTabSheet;
    mmosis_triggerupdate: TRzDBMemo;
    RzTabSheet3: TRzTabSheet;
    mmosis_triggerdelete: TRzDBMemo;
    TabSheet5: TRzTabSheet;
    Label3: TLabel;
    mmosis_baseurl: TRzDBMemo;
    Label11: TLabel;
    mmosis_endpoint: TRzDBMemo;
    Label13: TLabel;
    msesis_wspassword: TRzDBEdit;
    Label12: TLabel;
    msesis_wsusername: TRzDBEdit;
    FDSistemasIntegradosConfidconf: TIntegerField;
    FDSistemasIntegradosConfidsistemasintegrados: TIntegerField;
    FDSistemasIntegradosConfidtiposcripts: TIntegerField;
    FDSistemasIntegradosConfconf_tabela: TStringField;
    FDSistemasIntegradosConfconf_tabelapai: TStringField;
    FDSistemasIntegradosConfconf_codigo: TStringField;
    FDSistemasIntegradosConfconf_codigosql: TStringField;
    FDSistemasIntegradosConfconf_codigopai: TStringField;
    FDSistemasIntegradosConfconf_scriptbusca: TMemoField;
    FDSistemasIntegradosConfconf_prioridade: TStringField;
    FDSistemasIntegradosConfconf_baseurl: TMemoField;
    FDSistemasIntegradosConfconf_endpoint: TMemoField;
    FDSistemasIntegradosConfconf_maxqntpost: TMemoField;
    FDSistemasIntegradosConfdt_criacao: TSQLTimeStampField;
    FDSistemasIntegradosConfdt_modificacao: TSQLTimeStampField;
    FDSistemasIntegradosConfusu_criacao: TIntegerField;
    FDSistemasIntegradosConfusu_modificacao: TIntegerField;
    FDSistemasIntegradosConfinativo: TStringField;
    FDSistemasIntegradosConfstatus: TStringField;
    FDSistemasIntegradosConftpscr_descricao: TStringField;
    FDMasteridsistemasintegrados: TIntegerField;
    FDMasteridclientes: TIntegerField;
    FDMasteridfiliais: TIntegerField;
    FDMastersis_nome: TStringField;
    FDMastersis_tpcon: TStringField;
    FDMastersis_driverid: TStringField;
    FDMastersis_database: TStringField;
    FDMastersis_username: TStringField;
    FDMastersis_password: TStringField;
    FDMastersis_port: TIntegerField;
    FDMastersis_server: TStringField;
    FDMastersis_protocol: TStringField;
    FDMastersis_baseurl: TMemoField;
    FDMastersis_endpointlogin: TMemoField;
    FDMastersis_wsusername: TStringField;
    FDMastersis_wspassword: TStringField;
    FDMastersis_triggerinsert: TMemoField;
    FDMastersis_triggerupdate: TMemoField;
    FDMastersis_triggerdelete: TMemoField;
    FDMasterdt_criacao: TSQLTimeStampField;
    FDMasterdt_modificacao: TSQLTimeStampField;
    FDMasterusu_criacao: TIntegerField;
    FDMasterusu_modificacao: TIntegerField;
    FDMasterinativo: TStringField;
    FDMasterstatus: TStringField;
    lbldriverid: TLabel;
    cbbsis_driverid: TRzDBComboBox;
    FDMastercli_fantasia: TStringField;
    FDMasterfil_fantasia: TStringField;
    FDConfJSON: TFDQuery;
    dtsConfJSON: TDataSource;
    FDConfJSONidconf_json: TIntegerField;
    FDConfJSONidsistemasintegrados: TIntegerField;
    FDConfJSONidconf: TIntegerField;
    FDConfJSONjson_camposql: TStringField;
    FDConfJSONjson_campojson: TStringField;
    FDConfJSONdt_criacao: TSQLTimeStampField;
    FDConfJSONdt_modificacao: TSQLTimeStampField;
    FDConfJSONusu_criacao: TIntegerField;
    FDConfJSONusu_modificacao: TIntegerField;
    FDConfJSONinativo: TStringField;
    FDConfJSONstatus: TStringField;
    FDSistemasIntegradosConfconf_endpointupdate: TMemoField;
    FDSistemasIntegradosConfconf_endpointdelete: TMemoField;
    FDSistemasIntegradosConfconf_nomejson: TStringField;
    Label4: TLabel;
    msesis_schema: TRzDBEdit;
    FDMastersis_schema: TStringField;
    cxGridFiliaisDBTableViewconf_prioridade: TcxGridDBColumn;
    FDSistemasIntegradosConfconf_encarray: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure PesquisaMaster; override;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnAddTabClick(Sender: TObject);
    procedure FDMasterAfterClose(DataSet: TDataSet);
    procedure btnEditTabClick(Sender: TObject);
    procedure btnDelTabClick(Sender: TObject);
    procedure mseidclientesExit(Sender: TObject);
    procedure mseidclientesButtonClick(Sender: TObject);
    procedure mseidfiliaisExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FDMasterNewRecord(DataSet: TDataSet);
    procedure mseidfiliaisButtonClick(Sender: TObject);
    procedure FDConfJSONNewRecord(DataSet: TDataSet);
    procedure FDMasterAfterOpen(DataSet: TDataSet);
    procedure btnDeletarClick(Sender: TObject);
    procedure FDSistemasIntegradosConfAfterClose(DataSet: TDataSet);
    procedure FDSistemasIntegradosConfAfterOpen(DataSet: TDataSet);
    procedure btnOpcoesClick(Sender: TObject);

  private
    { Private declarations }
    procedure ConfigurarBotoesPadrao(Padroniza : Boolean = True); override;
  public
    { Public declarations }
  end;

var
  frmCadSistemasIntegrados: TfrmCadSistemasIntegrados;

implementation

{$R *.dfm}

uses uSistemaIntegrado, udmMaxxIntegra, ufrmCadSistemasIntegradosConf, uUtilidades, uLayoutFormulario, uPesquisas,
  uCliente, uFilial, ufrmPrincipal, uMensagens;

procedure TfrmCadSistemasIntegrados.btnAddTabClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadSistemasIntegradosConf, frmCadSistemasIntegradosConf);

    FDSistemasIntegradosConf.Append;

    NovoRegistro(FDSistemasIntegradosConf, 'sistemasintegrados_conf', 'idconf');

    FDSistemasIntegradosConfidsistemasintegrados.AsInteger := FDMasteridsistemasintegrados.AsInteger;
    FDSistemasIntegradosConfconf_baseurl.AsString          := FDMastersis_baseurl.AsString;

    frmCadSistemasIntegradosConf.ShowModal;

    if frmCadSistemasIntegradosConf.ModalResult = mrOk then
      FDSistemasIntegradosConf.Post
    else FDSistemasIntegradosConf.Cancel;
  finally
    FreeAndNil(frmCadSistemasIntegradosConf);
  end;
end;

procedure TfrmCadSistemasIntegrados.btnAlterarClick(Sender: TObject);
begin
  inherited;
  msesis_wsusername.CharCase := ecNormal;
  msesis_password.CharCase   := ecNormal;
  msesis_schema.CharCase     := ecNormal;

  SetarFocoComponente(mseSIS_NOME);
end;

procedure TfrmCadSistemasIntegrados.btnDeletarClick(Sender: TObject);
begin
  inherited;
  msesis_wsusername.CharCase := ecNormal;
  msesis_password.CharCase   := ecNormal;
  msesis_schema.CharCase     := ecNormal;
end;

procedure TfrmCadSistemasIntegrados.btnDelTabClick(Sender: TObject);
begin
  inherited;
  FDSistemasIntegradosConf.Delete;
end;

procedure TfrmCadSistemasIntegrados.btnEditTabClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadSistemasIntegradosConf, frmCadSistemasIntegradosConf);

    FDSistemasIntegradosConf.Edit;

    frmCadSistemasIntegradosConf.ShowModal;

    if frmCadSistemasIntegradosConf.ModalResult = mrOk then
      FDSistemasIntegradosConf.Post
    else FDSistemasIntegradosConf.Cancel;
  finally
    FreeAndNil(frmCadSistemasIntegradosConf);
  end;
end;

procedure TfrmCadSistemasIntegrados.btnNovoClick(Sender: TObject);
begin
  inherited;
  msesis_wsusername.CharCase := ecNormal;
  msesis_password.CharCase   := ecNormal;
  msesis_schema.CharCase     := ecNormal;

  SetarFocoComponente(mseSIS_NOME);
end;

procedure TfrmCadSistemasIntegrados.btnOpcoesClick(Sender: TObject);
var
  FDCabecalho, FDConf, FDJson: TFDQuery;
  I: Integer;
  Key : Char;
begin
  inherited;
  try
    try
      if Mensagem('Q', 'Deseja realmente exportar estas informa��es para outro registro?') then
      begin

        FDCabecalho := TFDQuery.Create(nil);
        FDConf      := TFDQuery.Create(nil);
        FDJson      := TFDQuery.Create(nil);

        FDCabecalho.Connection := Con;
        FDConf.Connection      := Con;
        FDJson.Connection      := Con;

        FDCabecalho.Close;
        FDCabecalho.SQL.Clear;
        FDCabecalho.CachedUpdates := True;
        FDCabecalho.SQL.Add(
            'SELECT m.* ' +
            'FROM &SCHEMA.sistemasintegrados m ' +
            'where m.idsistemasintegrados = :IDSISTEMASINTEGRADOS');
        FDCabecalho.Params[0].DataType  := ftInteger;
        FDCabecalho.Params[0].ParamType := ptInput;
        FDCabecalho.Open();

        FDConf.Close;
        FDConf.CachedUpdates := True;
        FDConf.SQL.Clear;
        FDConf.SQL.Add(
            'SELECT s.* ' +
            'FROM &SCHEMA.sistemasintegrados_conf s ' +
            'where s.idsistemasintegrados = :IDSISTEMASINTEGRADOS');
        FDConf.Params[0].DataType  := ftInteger;
        FDConf.Params[0].ParamType := ptInput;
        FDConf.Open();

        FDJson.Close;
        FDJson.CachedUpdates := True;
        FDJson.SQL.Clear;
        FDJson.SQL.Add(
            'select c.* ' +
            'from &SCHEMA.conf_json c ' +
            'where c.idsistemasintegrados = :IDSISTEMASINTEGRADOS');
        FDJson.Params[0].DataType  := ftInteger;
        FDJson.Params[0].ParamType := ptInput;
        FDJson.Open();
        /////////////////////////////////////////
        ///  Iniciando replica do cabe�alho   ///
        /////////////////////////////////////////
        FDCabecalho.Append;
        NovoRegistro(FDCabecalho, 'sistemasintegrados',      'idsistemasintegrados');

        for I := 0 to FDCabecalho.FieldCount -1 do
        begin
          if FDCabecalho.Fields[I].FieldName <> 'idsistemasintegrados' then
            FDCabecalho.Fields[I].Value := FDMaster.FieldByName(FDCabecalho.Fields[I].FieldName).Value;
        end;
        FDCabecalho.Post;

        FDSistemasIntegradosConf.First;
        while not FDSistemasIntegradosConf.Eof do
        begin
          FDConf.Append;
          NovoRegistro(FDConf, 'sistemasintegrados_conf', 'idconf');

          for I := 0 to FDConf.FieldCount -1 do
          begin
            if FDConf.Fields[I].FieldName <> 'idconf' then
            begin
              if FDConf.Fields[I].FieldName = 'idsistemasintegrados' then
                FDConf.Fields[I].Value := FDCabecalho.FieldByName('idsistemasintegrados').Value
              else
                FDConf.Fields[I].Value := FDSistemasIntegradosConf.FieldByName(FDConf.Fields[I].FieldName).Value;
            end;
          end;
          FDConf.Post;

          FDConfJSON.First;
          while not FDConfJSON.Eof do
          begin
            FDJson.Append;
            NovoRegistro(FDJson,      'conf_json',               'idconf_json');
            for I := 0 to FDJson.FieldCount -1 do
            begin
              if FDJson.Fields[I].FieldName <> 'idconf_json' then
              begin
                if FDJson.Fields[I].FieldName = 'idsistemasintegrados' then
                  FDJson.Fields[I].Value := FDConf.FieldByName('idsistemasintegrados').AsString
                else if FDJson.Fields[I].FieldName = 'idconf' then
                  FDJson.Fields[I].Value := FDConf.FieldByName('idconf').AsString
                else
                  FDJson.Fields[I].Value := FDConfJSON.FieldByName(FDJson.Fields[I].FieldName).Value;
              end;
            end;
            FDJson.Post;

            FDConfJSON.Next;
          end;
          FDSistemasIntegradosConf.Next;
        end;

        FDCabecalho.ApplyUpdates(0);
        FDConf.ApplyUpdates(0);
        FDJson.ApplyUpdates(0);

        mseCODIGO.Text := IntToStr(FDCabecalho.FieldByName('idsistemasintegrados').AsInteger);

        Key := #13;
        mseCODIGOKeyPress(Sender, Key);
      end;
    except
      on E: Exception do
        Mensagem('E', E.Message);
    end;
  finally

  end;
end;

procedure TfrmCadSistemasIntegrados.ConfigurarBotoesPadrao(Padroniza: Boolean);
begin
  btnAddTab.Enabled  := (FDMaster.State in [dsInsert, dsEdit]);
  btnEditTab.Enabled := (FDMaster.State in [dsInsert, dsEdit]) and (not FDSistemasIntegradosConf.IsEmpty);
  btnDelTab.Enabled  := (FDMaster.State in [dsInsert, dsEdit]) and (not FDSistemasIntegradosConf.IsEmpty);
  inherited;

  msesis_password.CharCase   := ecNormal;
  msesis_wsusername.CharCase := ecNormal;
end;

procedure TfrmCadSistemasIntegrados.FDConfJSONNewRecord(DataSet: TDataSet);
begin
  inherited;
  NovoRegistro(FDConfJSON, 'conf_json', 'idconf_json');
  FDConfJSONidsistemasintegrados.AsInteger := FDMasteridsistemasintegrados.AsInteger;
  FDConfJSONidconf.AsInteger               := FDSistemasIntegradosConfidconf.AsInteger;
end;

procedure TfrmCadSistemasIntegrados.FDMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
//  FDConfJSON.Open;
  FDSistemasIntegradosConf.Close;
//  FDSistemasIntegradosConf.Open;
end;

procedure TfrmCadSistemasIntegrados.FDMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  FDSistemasIntegradosConf.Open;

end;

procedure TfrmCadSistemasIntegrados.FDMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  FDMastersis_triggerinsert.AsString := TRIGGER_INS;
  FDMastersis_triggerupdate.AsString := TRIGGER_UPD;
  FDMastersis_triggerdelete.AsString := TRIGGER_DEL;
  FDMastersis_tpcon.AsString         := 'B';
end;

procedure TfrmCadSistemasIntegrados.FDSistemasIntegradosConfAfterClose(DataSet: TDataSet);
begin
  inherited;
    FDConfJSON.Close;
end;

procedure TfrmCadSistemasIntegrados.FDSistemasIntegradosConfAfterOpen(DataSet: TDataSet);
begin
  inherited;
    FDConfJSON.Open;
end;

procedure TfrmCadSistemasIntegrados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(frmCadSistemasIntegrados);
  frmPrincipal.pnlConfIntegra.BorderSides := [];

end;

procedure TfrmCadSistemasIntegrados.FormCreate(Sender: TObject);
begin
  TABELABD := 'sistemasintegrados';
  CAMPOID  := 'idsistemasintegrados';

  inherited;

  ListaObrigatorios.Add('mseSIS_NOME');
  ListaObrigatorios.Add('cbbsis_driverid');
  ListaObrigatorios.Add('cbbsis_protocol');
//  ListaObrigatorios.Add('msesis_server');
//  ListaObrigatorios.Add('msesis_port');
  ListaObrigatorios.Add('msesis_database');
  ListaObrigatorios.Add('msesis_username');
  ListaObrigatorios.Add('msesis_password');
//  ListaObrigatorios.Add('cbbsis_tpcon');
  ListaObrigatorios.Add('mmosis_baseurl');
  ListaObrigatorios.Add('mmosis_endpoint');
  ListaObrigatorios.Add('msesis_wsusername');
  ListaObrigatorios.Add('msesis_wspassword');

  Application.HintHidePause := 20000;
end;

procedure TfrmCadSistemasIntegrados.FormShow(Sender: TObject);
begin
  inherited;
  pgMaster.ActivePageIndex       := 0;
  RzPageControl3.ActivePageIndex := 0;
  RzPageControl4.ActivePageIndex := 0;

  mmosis_triggerinsert.Hint := HINT_TRIGGER;
  mmosis_triggerupdate.Hint := HINT_TRIGGER;
  mmosis_triggerdelete.Hint := HINT_TRIGGER;


end;

procedure TfrmCadSistemasIntegrados.mseidclientesButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaClientes, ['C�digo', 'Nome'], [mseidclientes, msecli_fantasia]);

end;

procedure TfrmCadSistemasIntegrados.mseidclientesExit(Sender: TObject);
begin
  inherited;
  if mseidclientes.Text <> '' then
  Pesquisar(SQLPesquisaClientesLogo, ' and idclientes = ' + mseidclientes.text, ['C�digo', 'Nome'],
                                                                                [mseidclientes, msecli_fantasia]);
end;

procedure TfrmCadSistemasIntegrados.mseidfiliaisButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(ifthen(mseidclientes.Text <> '', SQLPesquisaFilial(mseidclientes.Text), SQLPesquisaFilial),
                                             ['C�digo', 'Nome'], [mseidfiliais, msefil_fantasia]);
end;

procedure TfrmCadSistemasIntegrados.mseidfiliaisExit(Sender: TObject);
begin
  inherited;
  if mseidfiliais.Text <> '' then
    Pesquisar(SQLPesquisaFilialLogo, ' and idfiliais = ' + mseidfiliais.Text, ['C�digo', 'Nome'],
                                                                              [mseidfiliais, msefil_fantasia]);
end;

procedure TfrmCadSistemasIntegrados.PesquisaMaster;
begin
  SCRIPT_BUSCA := PesquisaSistemasIntegrados;
  inherited;
end;

end.
