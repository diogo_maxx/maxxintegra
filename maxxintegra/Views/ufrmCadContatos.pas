unit ufrmCadContatos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterDet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, Vcl.StdCtrls, sSpeedButton, RzButton, Vcl.Buttons,
  RzSpnEdt, Data.DB, Vcl.Mask, RzEdit, RzDBEdit, RzDBBnEd, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxImage, cxDBEdit, Vcl.DBCtrls;

type
  TfrmCadContato = class(TfrmMasterDet)
    dtsContato: TDataSource;
    Label1: TLabel;
    mseidtiposcontatos: TRzDBButtonEdit;
    msetpcon_descricao: TRzDBEdit;
    Label2: TLabel;
    msecont_contato: TRzDBEdit;
    Label3: TLabel;
    msecont_descricao: TRzDBEdit;
    Label4: TLabel;
    mmocont_observacao: TRzDBMemo;
    procedure mseidtiposcontatosButtonClick(Sender: TObject);
    procedure mseidtiposcontatosExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  frmCadContato: TfrmCadContato;

implementation

{$R *.dfm}

uses uPesquisas, uTipoContato;

procedure TfrmCadContato.FormCreate(Sender: TObject);
begin
  inherited;
  ListaObrigatorios.Add('msecont_descricao');
  ListaObrigatorios.Add('mseidtiposcontatos');
  ListaObrigatorios.Add('msecont_contatos');
end;

procedure TfrmCadContato.FormShow(Sender: TObject);
begin
  inherited;
  msecont_descricao.SetFocus;
end;

procedure TfrmCadContato.mseidtiposcontatosButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaTipoContato, ['C�digo', 'Descri��o'], [mseidtiposcontatos, msetpcon_descricao]);
end;

procedure TfrmCadContato.mseidtiposcontatosExit(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaTipoContato, ' and idtiposcontatos = ' + mseidtiposcontatos.Text,
           ['C�digo', 'Descri��o'],
           [mseidtiposcontatos, msetpcon_descricao]);
end;

end.

