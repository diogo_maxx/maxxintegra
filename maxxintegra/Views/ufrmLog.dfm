object frmLog: TfrmLog
  Left = 0
  Top = 0
  HelpType = htKeyword
  BorderIcons = [biSystemMenu]
  BorderStyle = bsNone
  Caption = 'frmLog'
  ClientHeight = 579
  ClientWidth = 910
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid3: TcxGrid
    AlignWithMargins = True
    Left = 192
    Top = 3
    Width = 715
    Height = 573
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 195
    ExplicitTop = 8
    ExplicitWidth = 701
    ExplicitHeight = 483
    object cxGridLogDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnCellDblClick = cxGridLogDBTableView1CellDblClick
      DataController.DataSource = dtsLog
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
    end
    object cxGrid3Level1: TcxGridLevel
      GridView = cxGridLogDBTableView1
    end
  end
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 189
    Height = 579
    Align = alLeft
    BorderOuter = fsNone
    TabOrder = 1
    object RzGroupBox2: TRzGroupBox
      Left = 0
      Top = 426
      Width = 189
      Height = 153
      Align = alBottom
      Caption = 'Status Code'
      TabOrder = 0
      ExplicitLeft = 2
      ExplicitTop = 424
      ExplicitWidth = 185
      object cxGrid2: TcxGrid
        AlignWithMargins = True
        Left = 4
        Top = 17
        Width = 181
        Height = 132
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 177
        ExplicitHeight = 109
        object cxGridStatusDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnCellClick = cxGridStatusDBTableView1CellClick
          DataController.DataSource = dtsStatus
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridStatusDBTableView1
        end
      end
    end
    object RzGroupBox1: TRzGroupBox
      Left = 0
      Top = 0
      Width = 189
      Height = 426
      Align = alClient
      Caption = 'Tabelas'
      TabOrder = 1
      ExplicitLeft = 4
      ExplicitTop = 4
      ExplicitWidth = 185
      ExplicitHeight = 351
      object cxGrid1: TcxGrid
        AlignWithMargins = True
        Left = 4
        Top = 17
        Width = 181
        Height = 405
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 177
        ExplicitHeight = 330
        object cxGridTabelasDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dtsTabela
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGridTabelasDBTableView1
        end
      end
    end
  end
  object RESTClientTabela: TRESTClient
    Params = <>
    HandleRedirects = True
    Left = 752
    Top = 24
  end
  object RESTRequestTabela: TRESTRequest
    Client = RESTClientTabela
    Params = <>
    Response = RESTResponseTabela
    SynchronizedEvents = False
    Left = 752
    Top = 72
  end
  object RESTResponseTabela: TRESTResponse
    Left = 752
    Top = 120
  end
  object RESTResponseDataSetAdapterTabela: TRESTResponseDataSetAdapter
    Dataset = FDTabela
    FieldDefs = <>
    Response = RESTResponseTabela
    Left = 752
    Top = 176
  end
  object FDTabela: TFDMemTable
    AfterScroll = FDTabelaAfterScroll
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 272
    Top = 32
  end
  object FDStatus: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 272
    Top = 80
  end
  object FDLog: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 272
    Top = 128
  end
  object dtsTabela: TDataSource
    DataSet = FDTabela
    Left = 352
    Top = 32
  end
  object dtsStatus: TDataSource
    DataSet = FDStatus
    Left = 352
    Top = 80
  end
  object dtsLog: TDataSource
    DataSet = FDLog
    Left = 352
    Top = 128
  end
  object RESTClientStatus: TRESTClient
    Params = <>
    HandleRedirects = True
    Left = 760
    Top = 232
  end
  object RESTRequestStatus: TRESTRequest
    Client = RESTClientStatus
    Params = <>
    Response = RESTResponseStatus
    SynchronizedEvents = False
    Left = 760
    Top = 280
  end
  object RESTResponseStatus: TRESTResponse
    Left = 760
    Top = 328
  end
  object RESTResponseDataSetAdapterStatus: TRESTResponseDataSetAdapter
    AutoUpdate = False
    Dataset = FDStatus
    FieldDefs = <>
    Response = RESTResponseStatus
    Left = 760
    Top = 384
  end
  object RESTClientLog: TRESTClient
    Params = <>
    HandleRedirects = True
    Left = 528
    Top = 232
  end
  object RESTRequestLog: TRESTRequest
    Client = RESTClientLog
    Params = <>
    Response = RESTResponseLog
    SynchronizedEvents = False
    Left = 528
    Top = 280
  end
  object RESTResponseLog: TRESTResponse
    Left = 528
    Top = 328
  end
  object RESTResponseDataSetAdapterLog: TRESTResponseDataSetAdapter
    AutoUpdate = False
    Dataset = FDLog
    FieldDefs = <
      item
        Name = 'id'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'tabela'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'tiporequisicao'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'registro'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'horaintegracao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'horasincronismo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'statuscod'
        DataType = ftInteger
      end
      item
        Name = 'erro'
        DataType = ftBoolean
      end
      item
        Name = 'jsonretorno'
        DataType = ftMemo
      end
      item
        Name = 'jsonenvio'
        DataType = ftMemo
      end
      item
        Name = 'url'
        DataType = ftWideString
        Size = 200
      end
      item
        Name = 'parametros'
        DataType = ftMemo
      end
      item
        Name = 'mensagem'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'datalog'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'idconf'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'app'
        DataType = ftString
        Size = 20
      end>
    Response = RESTResponseLog
    Left = 528
    Top = 384
  end
end
