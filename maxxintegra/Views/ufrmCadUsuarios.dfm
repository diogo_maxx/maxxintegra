inherited frmCadUsuarios: TfrmCadUsuarios
  Caption = 'Usu'#225'rios'
  ClientHeight = 296
  ClientWidth = 449
  ExplicitWidth = 449
  ExplicitHeight = 296
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBordas: TRzPanel
    Width = 449
    Height = 296
    ExplicitWidth = 449
    ExplicitHeight = 296
    inherited pnlMaster: TRzPanel
      Width = 449
      Height = 252
      ExplicitWidth = 449
      ExplicitHeight = 252
      inherited pgMaster: TRzPageControl
        Width = 443
        Height = 161
        ExplicitWidth = 443
        ExplicitHeight = 161
        FixedDimension = 19
        inherited TabSheet1: TRzTabSheet
          ExplicitWidth = 443
          ExplicitHeight = 141
          object Label1: TLabel
            Left = 4
            Top = 11
            Width = 46
            Height = 13
            Caption = 'Descri'#231#227'o'
            FocusControl = mseUSU_DESCRICAO
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 4
            Top = 51
            Width = 80
            Height = 13
            Caption = 'Nome de usu'#225'rio'
            FocusControl = mseUSU_NOME
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 4
            Top = 91
            Width = 30
            Height = 13
            Caption = 'Senha'
            FocusControl = mseUSU_SENHA
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 197
            Top = 91
            Width = 79
            Height = 13
            Caption = 'Confirmar senha'
            FocusControl = mseUSU_CONFSENHA
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object mseUSU_DESCRICAO: TRzDBEdit
            Left = 4
            Top = 25
            Width = 381
            Height = 21
            DataSource = dtsMaster
            DataField = 'usu_descricao'
            TabOrder = 0
          end
          object mseUSU_NOME: TRzDBEdit
            Left = 4
            Top = 65
            Width = 381
            Height = 21
            DataSource = dtsMaster
            DataField = 'usu_nome'
            TabOrder = 1
          end
          object mseUSU_SENHA: TRzDBEdit
            Left = 4
            Top = 105
            Width = 188
            Height = 21
            DataSource = dtsMaster
            DataField = 'usu_senha'
            PasswordChar = '*'
            TabOrder = 2
          end
          object mseUSU_CONFSENHA: TRzMaskEdit
            Left = 197
            Top = 105
            Width = 188
            Height = 21
            PasswordChar = '*'
            TabOrder = 3
            Text = ''
          end
        end
      end
      inherited pnlCODIGO: TRzPanel
        Width = 449
        ExplicitWidth = 449
        inherited cbxINATIVO: TRzDBCheckBox
          Left = 393
          DataField = 'inativo'
          DataSource = dtsMaster
          ExplicitLeft = 393
        end
      end
      inherited pnlBotoes: TRzPanel
        Top = 214
        Width = 449
        ExplicitTop = 214
        ExplicitWidth = 449
        inherited btnCancelar: TRzRapidFireButton
          Left = 300
          ExplicitLeft = 388
        end
        inherited btnSalvar: TRzBitBtn
          Left = 376
          ExplicitLeft = 376
        end
        inherited btnNovo: TRzBitBtn
          Left = -80
          ExplicitLeft = -80
        end
        inherited btnAlterar: TRzBitBtn
          Left = -4
          ExplicitLeft = -4
        end
        inherited btnDeletar: TRzBitBtn
          Left = 72
          ExplicitLeft = 72
        end
        inherited btnPesquisar: TRzBitBtn
          Left = 148
          ExplicitLeft = 148
        end
        inherited btnOpcoes: TRzBitBtn
          Left = 224
          ExplicitLeft = 224
        end
      end
    end
    inherited pnlTitulo: TRzPanel
      Width = 449
      ExplicitWidth = 449
      inherited spbFechar: TsSpeedButton
        Left = 416
        ExplicitLeft = 504
      end
    end
    inherited stbSTATUS: TRzStatusBar
      Top = 277
      Width = 449
      ExplicitTop = 277
      ExplicitWidth = 449
      inherited stpNomeTela: TRzStatusPane
        Width = 96
        ExplicitWidth = 96
        ExplicitHeight = 19
      end
      inherited stpCriacao: TRzStatusPane
        Left = 96
        Width = 160
        Align = alClient
        ExplicitLeft = 194
        ExplicitTop = 0
        ExplicitWidth = 167
      end
      inherited stpModificacao: TRzStatusPane
        Left = 256
        Width = 193
        ExplicitLeft = 256
        ExplicitWidth = 193
      end
    end
  end
  inherited dtsMaster: TDataSource
    Left = 160
    Top = 72
  end
  inherited FDMaster: TFDQuery
    SQL.Strings = (
      'SELECT  u.idusuarios,'
      '        u.usu_descricao,'
      '        u.usu_nome,'
      '        u.usu_senha,'
      '        u.dt_criacao,'
      '        u.dt_modificacao,'
      '        u.usu_criacao,'
      '        u.usu_modificacao,'
      '        u.inativo,'
      '        u.status'
      'FROM &SCHEMA.usuarios u'
      'where u.idusuarios = :IDUSUARIOS'
      '  and u.status <> '#39'X'#39)
    Left = 160
    Top = 16
    ParamData = <
      item
        Name = 'IDUSUARIOS'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
      end>
    object FDMasteridusuarios: TIntegerField
      FieldName = 'idusuarios'
      Origin = 'idusuarios'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDMasterusu_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_descricao'
      Origin = 'usu_descricao'
      Size = 100
    end
    object FDMasterusu_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_nome'
      Origin = 'usu_nome'
      Size = 100
    end
    object FDMasterusu_senha: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_senha'
      Origin = 'usu_senha'
      Size = 40
    end
    object FDMasterdt_criacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_criacao'
      Origin = 'dt_criacao'
    end
    object FDMasterdt_modificacao: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'dt_modificacao'
      Origin = 'dt_modificacao'
    end
    object FDMasterusu_criacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_criacao'
      Origin = 'usu_criacao'
    end
    object FDMasterusu_modificacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_modificacao'
      Origin = 'usu_modificacao'
    end
    object FDMasterinativo: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'inativo'
      Origin = 'inativo'
      FixedChar = True
      Size = 1
    end
    object FDMasterstatus: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'status'
      Origin = '`status`'
      FixedChar = True
      Size = 1
    end
  end
end
