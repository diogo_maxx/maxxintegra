object frmLogDetalhes: TfrmLogDetalhes
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Log - Detalhes'
  ClientHeight = 419
  ClientWidth = 776
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 776
    Height = 419
    Align = alClient
    Color = 16053492
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 38
      Height = 13
      Caption = 'Tabela'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 330
      Top = 4
      Width = 102
      Height = 13
      Caption = 'Tipo de requisi'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 481
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Registro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 4
      Top = 44
      Width = 108
      Height = 13
      Caption = 'Hora da integra'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 167
      Top = 44
      Width = 115
      Height = 13
      Caption = 'Hora do sincronismo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 674
      Top = 4
      Width = 68
      Height = 13
      Caption = 'Status Code'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 330
      Top = 44
      Width = 22
      Height = 13
      Caption = 'URL'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 4
      Top = 84
      Width = 62
      Height = 13
      Caption = 'Mensagem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 4
      Top = 260
      Width = 61
      Height = 13
      Caption = 'JSON Envio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 390
      Top = 260
      Width = 53
      Height = 13
      Caption = 'Resposta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 6
      Top = 140
      Width = 62
      Height = 13
      Caption = 'Mensagem'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4210752
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RzDBEdit1: TRzDBEdit
      Left = 4
      Top = 18
      Width = 320
      Height = 21
      DataSource = frmLog.dtsLog
      DataField = 'tabela'
      ReadOnly = True
      Color = 16053492
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
      TabOrder = 0
    end
    object RzDBComboBox1: TRzDBComboBox
      Left = 330
      Top = 18
      Width = 145
      Height = 21
      DataField = 'tiporequisicao'
      DataSource = frmLog.dtsLog
      ReadOnly = True
      Color = 16053492
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
      TabOrder = 1
      Items.Strings = (
        'INSER'#199#195'O'
        'ALTERA'#199#195'O'
        'DELETE')
      Values.Strings = (
        'I'
        'A'
        'D')
    end
    object RzDBEdit2: TRzDBEdit
      Left = 481
      Top = 18
      Width = 187
      Height = 21
      DataSource = frmLog.dtsLog
      DataField = 'registro'
      ReadOnly = True
      Color = 16053492
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
      TabOrder = 2
    end
    object RzDBEdit3: TRzDBEdit
      Left = 674
      Top = 18
      Width = 96
      Height = 21
      DataSource = frmLog.dtsLog
      DataField = 'statuscod'
      ReadOnly = True
      Alignment = taRightJustify
      Color = 16053492
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      FramingPreference = fpCustomFraming
      ParentFont = False
      ReadOnlyColor = 16053492
      TabOrder = 3
    end
    object RzDBEdit4: TRzDBEdit
      Left = 330
      Top = 58
      Width = 440
      Height = 21
      DataSource = frmLog.dtsLog
      DataField = 'url'
      ReadOnly = True
      Color = 16053492
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
      TabOrder = 4
    end
    object RzDBMemo1: TRzDBMemo
      Left = 4
      Top = 98
      Width = 766
      Height = 39
      Color = 16053492
      DataField = 'mensagem'
      DataSource = frmLog.dtsLog
      ReadOnly = True
      TabOrder = 5
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
    end
    object RzDBMemo2: TRzDBMemo
      Left = 4
      Top = 276
      Width = 380
      Height = 138
      Color = 16053492
      DataField = 'jsonenvio'
      DataSource = frmLog.dtsLog
      ReadOnly = True
      TabOrder = 6
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
    end
    object RzDBMemo3: TRzDBMemo
      Left = 390
      Top = 276
      Width = 380
      Height = 138
      Color = 16053492
      DataField = 'jsonretorno'
      DataSource = frmLog.dtsLog
      ReadOnly = True
      TabOrder = 7
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
    end
    object RzDBEdit5: TRzDBEdit
      Left = 4
      Top = 58
      Width = 157
      Height = 21
      DataSource = frmLog.dtsLog
      DataField = 'horaintegracao'
      ReadOnly = True
      Color = 16053492
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
      TabOrder = 8
    end
    object RzDBEdit6: TRzDBEdit
      Left = 167
      Top = 58
      Width = 157
      Height = 21
      DataSource = frmLog.dtsLog
      DataField = 'horasincronismo'
      ReadOnly = True
      Color = 16053492
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
      TabOrder = 9
    end
    object RzDBMemo4: TRzDBMemo
      Left = 4
      Top = 154
      Width = 766
      Height = 102
      Color = 16053492
      DataField = 'parametros'
      DataSource = frmLog.dtsLog
      ReadOnly = True
      TabOrder = 10
      FramingPreference = fpCustomFraming
      ReadOnlyColor = 16053492
    end
  end
end
