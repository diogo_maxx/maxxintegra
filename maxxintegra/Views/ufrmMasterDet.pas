unit ufrmMasterDet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMaster, Vcl.StdCtrls, Vcl.Buttons, RzStatus, RzPanel, Vcl.ExtCtrls, acImage,
  sSpeedButton, RzButton, RzSpnEdt;

type
  TfrmMasterDet = class(TfrmMaster)
    pnlBotoes: TRzPanel;
    btnCancelar: TRzRapidFireButton;
    btnOK: TRzBitBtn;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ConfigurarFormulario; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMasterDet: TfrmMasterDet;

implementation

{$R *.dfm}

uses uValidacao, uConstantes;

procedure TfrmMasterDet.btnCancelarClick(Sender: TObject);
begin
  inherited;
  Close;
  ModalResult := mrCancel;
end;

procedure TfrmMasterDet.btnOKClick(Sender: TObject);
begin
  inherited;
  Close;
  ModalResult := mrOk;
end;

procedure TfrmMasterDet.ConfigurarFormulario;
begin
  inherited;
  pnlBotoes.Color              := LAYOUT_COR_FORM_TITULO;

  stpCriacao.Caption     := '';
  stpModificacao.Caption := '';
end;

procedure TfrmMasterDet.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  inherited;
  if ModalResult = mrOk then
    CanClose := ValidaObrigatorios(Self, ListaObrigatorios);
end;

end.
