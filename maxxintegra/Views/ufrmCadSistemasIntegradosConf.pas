unit ufrmCadSistemasIntegradosConf;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterDet, RzStatus, RzPanel, Vcl.ExtCtrls, acImage, Vcl.StdCtrls,
  sSpeedButton, RzButton, Vcl.Buttons, RzSpnEdt, Vcl.Mask, RzEdit, RzDBEdit, RzDBBnEd, Vcl.DBCtrls, RzTabs, RzCmboBx,
  RzDBCmbo, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzRadChk,
  RzDBChk;

type
  TfrmCadSistemasIntegradosConf = class(TfrmMasterDet)
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    Label1: TLabel;
    mseidtiposcripts: TRzDBButtonEdit;
    msetpscr_descricao: TRzDBEdit;
    Label2: TLabel;
    mseconf_tabela: TRzDBEdit;
    mseconf_tabelapai: TRzDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    mseconf_codigo: TRzDBEdit;
    Label5: TLabel;
    mseconf_codigopai: TRzDBEdit;
    Label6: TLabel;
    TabSheet5: TRzTabSheet;
    mmoconf_scriptbusca: TRzDBMemo;
    cbbconf_prioridade: TRzDBComboBox;
    RzPageControl2: TRzPageControl;
    TabSheet6: TRzTabSheet;
    mmoconf_baseurl: TRzDBMemo;
    TabSheet7: TRzTabSheet;
    mmoconf_endpoint: TRzDBMemo;
    TabSheet8: TRzTabSheet;
    RzDBMemo4: TRzDBMemo;
    Label7: TLabel;
    mseconf_codigosql: TRzDBEdit;
    TabSheet9: TRzTabSheet;
    RzPanel1: TRzPanel;
    btnAddConf: TRzBitBtn;
    btnEditConf: TRzBitBtn;
    btnDelConf: TRzBitBtn;
    cxGridFiliais: TcxGrid;
    cxGridFiliaisDBTableView: TcxGridDBTableView;
    cxGridFiliaisLevel: TcxGridLevel;
    cxGridFiliaisDBTableViewjson_camposql: TcxGridDBColumn;
    cxGridFiliaisDBTableViewjson_campojson: TcxGridDBColumn;
    cxGridFiliaisDBTableViewinativo: TcxGridDBColumn;
    TabSheet2: TRzTabSheet;
    mmoconf_endpointupdate: TRzDBMemo;
    TabSheet3: TRzTabSheet;
    mmoconf_endpointdelete: TRzDBMemo;
    Label8: TLabel;
    mseconf_nomejson: TRzDBEdit;
    cbxconf_encarray: TRzDBCheckBox;
    procedure mseidtiposcriptsExit(Sender: TObject);
    procedure mseidtiposcriptsButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAddConfClick(Sender: TObject);
    procedure btnEditConfClick(Sender: TObject);
    procedure btnDelConfClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigurarBotoesPadrao(Padroniza : Boolean = True);
  public
    { Public declarations }
  end;

var
  frmCadSistemasIntegradosConf: TfrmCadSistemasIntegradosConf;

implementation

{$R *.dfm}

uses ufrmCadSistemasIntegrados, uPesquisas, uTipoScript, ufrmCadSistemasIntegradosConfJSON;

procedure TfrmCadSistemasIntegradosConf.btnAddConfClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadSistemasIntegradosConfJSON, frmCadSistemasIntegradosConfJSON);

    frmCadSistemasIntegrados.FDConfJSON.Append;

    frmCadSistemasIntegradosConfJSON.ShowModal;

    if frmCadSistemasIntegradosConfJSON.ModalResult = mrOk then
      frmCadSistemasIntegrados.FDConfJSON.Post
    else frmCadSistemasIntegrados.FDConfJSON.Cancel;

  finally
    FreeAndNil(frmCadSistemasIntegradosConfJSON);
    ConfigurarBotoesPadrao;
  end;
end;

procedure TfrmCadSistemasIntegradosConf.btnDelConfClick(Sender: TObject);
begin
  inherited;
  frmCadSistemasIntegrados.FDConfJSON.Delete;
  ConfigurarBotoesPadrao;
end;

procedure TfrmCadSistemasIntegradosConf.btnEditConfClick(Sender: TObject);
begin
  inherited;
  try
    Application.CreateForm(TfrmCadSistemasIntegradosConfJSON, frmCadSistemasIntegradosConfJSON);

    frmCadSistemasIntegrados.FDConfJSON.edit;

    frmCadSistemasIntegradosConfJSON.ShowModal;

    if frmCadSistemasIntegradosConfJSON.ModalResult = mrOk then
      frmCadSistemasIntegrados.FDConfJSON.Post
    else frmCadSistemasIntegrados.FDConfJSON.Cancel;

  finally
    FreeAndNil(frmCadSistemasIntegradosConfJSON);
    ConfigurarBotoesPadrao;
  end;
end;

procedure TfrmCadSistemasIntegradosConf.ConfigurarBotoesPadrao(Padroniza: Boolean);
begin
  btnAddConf.Enabled  := True;
  btnEditConf.Enabled := (not frmCadSistemasIntegrados.FDConfJSON.IsEmpty);
  btnDelConf.Enabled  := (not frmCadSistemasIntegrados.FDConfJSON.IsEmpty);
  inherited;
end;

procedure TfrmCadSistemasIntegradosConf.FormCreate(Sender: TObject);
begin
  inherited;
  ListaObrigatorios.Add('mseidtiposcripts');
  ListaObrigatorios.Add('mseconf_tabela');
  ListaObrigatorios.Add('mseconf_codigo');
  ListaObrigatorios.Add('cbbconf_prioridade');
  ListaObrigatorios.Add('mseconf_codigosql');
//  ListaObrigatorios.Add('mmoconf_endpoint');
//  ListaObrigatorios.Add('mmoconf_endpointupdate');
//  ListaObrigatorios.Add('mmoconf_endpointdelete');

  RzPageControl1.ActivePageIndex := 0;
  RzPageControl2.ActivePageIndex := 0;

end;

procedure TfrmCadSistemasIntegradosConf.FormShow(Sender: TObject);
begin
  inherited;

  ConfigurarBotoesPadrao;
  stpCriacao.Caption     := 'Cria��o: ' + FormatDateTime('DD/MM/YYYY HH:MM',  frmCadSistemasIntegrados.FDSistemasIntegradosConf.FieldByName('dt_criacao').AsDateTime);
  stpModificacao.Caption := 'Atualiza��o: ' + FormatDateTime('DD/MM/YYYY HH:MM', frmCadSistemasIntegrados.FDSistemasIntegradosConf.FieldByName('dt_modificacao').AsDateTime);


  mseconf_nomejson.CharCase := ecNormal;
  mseconf_nomejson.SetFocus;
  mseidtiposcripts.SetFocus;

end;

procedure TfrmCadSistemasIntegradosConf.mseidtiposcriptsButtonClick(Sender: TObject);
begin
  inherited;
  Pesquisar(SQLPesquisaTipoScript, ['C�digo', 'Descri��o'], [mseidtiposcripts, msetpscr_descricao]);
end;

procedure TfrmCadSistemasIntegradosConf.mseidtiposcriptsExit(Sender: TObject);
begin
  inherited;
  if mseidtiposcripts.Text <> '' then
    Pesquisar(SQLPesquisaTipoScript, ' and idtiposcripts = ' + mseidtiposcripts.Text, ['C�digo', 'Descri��o'], [mseidtiposcripts, msetpscr_descricao]);
end;

end.
