unit ServerMethodsUnit1;

interface

uses System.SysUtils, System.Classes, System.Json,
    Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Phys.MySQLDef, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.Intf, FireDAC.Phys, FireDAC.Phys.MySQL, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, IniFiles, IWSystem, Variants, Winapi.Windows, Data.DBXPlatform,
  StrUtils, Rest.JSON, System.JSON.Types, System.JSON.Writers, System.Json.Builders, Forms;

type
{$METHODINFO ON}
  TServerMethods1 = class(TDataModule)
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDConnection: TFDConnection;
    FDConfJSON: TFDQuery;
    FDConf: TFDQuery;
    FDConfTabelas: TFDQuery;
    dtsConfTabelas: TDataSource;
    dtsConf: TDataSource;
  private
    function CriaConexao(FDCon: TFDConnection; Tansaction: TFDTransaction = nil; ComponenteRetorno: TComponent = nil): Boolean;
    procedure SalvarLog(Msg: String);
    procedure LimpaMemoriaAPP;
    { Private declarations }
  public
    { Public declarations }
    function GetConfiguracao(IDSistemasIntegrados: String): TJSONValue;
  end;
{$METHODINFO OFF}

implementation


{$R *.dfm}

uses uDatasetToJSON;

{ TServerMethods1 }


function TServerMethods1.GetConfiguracao(IDSistemasIntegrados: String): TJSONValue;
var
  JsonTabela, JsonJSON: TJSONValue;
  JsonConf: TJSONObject;
  DataSetToJson: TDatasetToJSON;
begin
  try
    GetInvocationMetadata().ResponseContentType := 'json';
    JsonConf       := TJSONObject.Create;
    JsonTabela     := TJSONValue.Create;
    JsonJSON       := TJSONValue.Create;
    DataSetToJson  := TDatasetToJSON.Create;

//    FDConf.DisableControls;
//    FDConfTabelas.DisableControls;
//    FDConfJSON.DisableControls;


    try
      if IDSistemasIntegrados <> '' then
      begin
        CriaConexao(FDConnection);

        FDConf.Close;
//        FDConf.Macros[0].AsString  := FDConnection.Params.Database;
        FDConf.Macros[0].AsString  := IDSistemasIntegrados;
        FDConf.Open;

        FDConfTabelas.Close;
        FDConfTabelas.Open;

        FDConfJSON.Close;
        FDConfJSON.Open;

        if not FDConf.IsEmpty then
        begin
          if FDConfTabelas.RecordCount > 1 then
            JsonTabela := TJSONArray.Create;

          while not FDConfTabelas.Eof do
          begin
            FDConfJSON.First;
            if not FDConfJSON.IsEmpty then
            begin
              JsonJSON := TJSONValue.Create;

              if FDConfJSON.RecordCount > 1 then
              begin
                JsonJSON := TJSONArray.Create;
                while not FDConfJSON.Eof do
                begin
                  DataSetToJson := TDatasetToJSON.Create;
                  TJSONArray(JsonJSON).AddElement(DataSetToJson.CriaJson(FDConfJSON));
                  FDConfJSON.Next;
                end;
              end
              else JsonJSON := DataSetToJson.CriaJson(FDConfJSON);
            end;

            if not FDConfJSON.IsEmpty then
              DataSetToJson := TDatasetToJSON.Create(True, JsonJSON, 'conf_json')
            else
              DataSetToJson := TDatasetToJSON.Create();

            if FDConfTabelas.RecordCount > 1 then
              TJSONArray(JsonTabela).AddElement(DataSetToJson.CriaJson(FDConfTabelas))
            else JsonTabela := DataSetToJson.CriaJson(FDConfTabelas);

            FreeAndNil(DataSetToJson);

            FDConfTabelas.Next;
          end;

          if JsonTabela.ToString <> '' then
            DataSetToJson := TDatasetToJSON.Create(True, JsonTabela, 'sistemasintegrados_conf')
          else
            DataSetToJson := TDatasetToJSON.Create;

          JsonConf.AddPair('sistemasintegrados', DataSetToJson.CriaJson(FDConf));
          GetInvocationMetadata().ResponseCode := 200;
          Result := JsonConf;
        end;
      end;

    except
      on E: Exception do
      begin
        SalvarLog(e.Message + ' SQL: ' + FDConf.SQL.Text);
      end;
    end;
  finally
    FDConf.Close;
    FDConfTabelas.Close;
    FDConfJSON.Close;
    LimpaMemoriaAPP;
  end;
end;

function TServerMethods1.CriaConexao(FDCon: TFDConnection; Tansaction: TFDTransaction = nil; ComponenteRetorno: TComponent = nil): Boolean;
var
  ArqIni: TIniFile;
  PASTACFG, DATABASE, HOST, PORTA: String;
begin
  try
    Result := True;
    try
      PASTACFG := StringReplace(UpperCase(gsAppPath), '\EXE\', '\Conf\', []);
      ArqIni := TIniFile.Create(PastaCFG + 'conf.cfg');

      DATABASE := ArqIni.ReadString('BANCO', 'Database', '');
      HOST     := ArqIni.ReadString('BANCO', 'Host',     '');
      PORTA    := ArqIni.ReadString('BANCO', 'Porta',    '');
    except
      on E: Exception do
      begin
        Result := False;
      end;
    end;

    if Result then
    begin
      try
        FDCon.Connected              := False;
        FDCon.LoginPrompt            := False;
        FDCon.UpdateOptions.LockWait := False;

        FDCon.Params.Clear;
        FDCon.Params.Add('DriverID=MySQL');
        FDCon.Params.Add('Database=' + DATABASE);
        FDCon.Params.Add('User_Name=root');
        FDCon.Params.Add('Password=root');
        FDCon.Params.Add('SERVER=' + HOST);
        FDCon.Params.Add('Port=' + PORTA);

        FDCon.Connected := True;
      except
        on E: Exception do
        begin
          SalvarLog('Criarconexao: ' +  E.Message);

          Result := False;
        end;
      end;
    end;
  finally
    if ArqIni <> nil then
      FreeAndNil(ArqIni);
  end;
end;

procedure TServerMethods1.SalvarLog(Msg: String);
var
   Arquivo: TextFile;
   Log : string;
begin
  Log :=  StringReplace(UpperCase(gsAppPath), '\EXE\', '\Conf\', []) + 'maxxintegra.log';

  try
    AssignFile(Arquivo, Log);
    if FileExists(Log) then
        Append(arquivo) { se existir, apenas adiciona linhas }
    else ReWrite(arquivo); { cria um novo se n�o existir }

    try
      WriteLn(arquivo, DateToStr(now) + ' - ' +  timetostr(now) + ' - ' +  msg);
    finally
      CloseFile(arquivo)
    end;
  except
  end;
end;

procedure TServerMethods1.LimpaMemoriaAPP;
var
  MainHandle : THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false,
    GetCurrentProcessID) ;
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(MainHandle) ;
  except
  end;
  Application.ProcessMessages;
end;

end.

