object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 410
  Width = 525
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    Left = 424
    Top = 112
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrAppWait
    Left = 424
    Top = 48
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=maxxintegra'
      'User_Name=root'
      'Password=root'
      'Server=192.168.25.3'
      'DriverID=MySQL')
    LoginPrompt = False
    Left = 48
    Top = 32
  end
  object FDConfJSON: TFDQuery
    IndexFieldNames = 'idconf;idsistemasintegrados'
    MasterSource = dtsConfTabelas
    MasterFields = 'idconf;idsistemasintegrados'
    DetailFields = 'idconf;idsistemasintegrados'
    Connection = FDConnection
    FetchOptions.AssignedValues = [evDetailCascade]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'SELECT * FROM &SCHEMA.conf_json c'
      'where c.idsistemasintegrados = :IDSISTEMASINTEGRADOS'
      '  and c.idconf = :IDCONF'
      '  and c.inativo = '#39'N'#39
      '  and c.status = '#39'A'#39)
    Left = 328
    Top = 288
    ParamData = <
      item
        Name = 'IDSISTEMASINTEGRADOS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'IDCONF'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
  end
  object FDConf: TFDQuery
    Connection = FDConnection
    FetchOptions.AssignedValues = [evDetailCascade]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'SELECT s.*'
      'FROM sistemasintegrados s'
      'where s.idsistemasintegrados = &FILTRO'
      '  and s.status = '#39'A'#39' '
      '  and s.inativo = '#39'N'#39' ')
    Left = 160
    Top = 288
    MacroData = <
      item
        Value = Null
        Name = 'FILTRO'
        DataType = mdIdentifier
      end>
  end
  object FDConfTabelas: TFDQuery
    IndexFieldNames = 'idsistemasintegrados'
    MasterSource = dtsConf
    MasterFields = 'idsistemasintegrados'
    DetailFields = 'idsistemasintegrados'
    Connection = FDConnection
    FetchOptions.AssignedValues = [evDetailCascade]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'SELECT s.*,'
      '       tp.tpscr_descricao'
      'FROM &SCHEMA.sistemasintegrados_conf s'
      
        'left join &SCHEMA.tiposcripts tp on (tp.idtiposcripts = s.idtipo' +
        'scripts)'
      'where s.idsistemasintegrados = :IDSISTEMASINTEGRADOS'
      '  and s.inativo = '#39'N'#39
      '  and s.status = '#39'A'#39)
    Left = 240
    Top = 288
    ParamData = <
      item
        Name = 'IDSISTEMASINTEGRADOS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    MacroData = <
      item
        Value = Null
        Name = 'SCHEMA'
        DataType = mdIdentifier
      end>
  end
  object dtsConfTabelas: TDataSource
    DataSet = FDConfTabelas
    Left = 240
    Top = 336
  end
  object dtsConf: TDataSource
    DataSet = FDConf
    Left = 160
    Top = 336
  end
end
