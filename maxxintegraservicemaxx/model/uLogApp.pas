unit uLogApp;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MongoDBDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Moni.Base, FireDAC.Moni.FlatFile, FireDAC.Phys.MongoDB, Data.DB,
  FireDAC.Comp.Client, System.Rtti, System.JSON.Types, System.JSON.Readers, System.JSON.BSON, System.JSON.Builders,
  FireDAC.Phys.MongoDBWrapper, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Phys.MongoDBDataSet, JSON, REST.JSON;


{$METHODINFO ON}
type
  TLogApp = class(TDataModule)
    FDMongoConnection: TFDConnection;
    FDPhysMongoDriverLink1: TFDPhysMongoDriverLink;
    FDMoniFlatFileClientLink1: TFDMoniFlatFileClientLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
  private
    { Private declarations }
  public
    { Public declarations }
     function Log: TJSONObject;
     procedure acceptLog(jObj: TJSONObject);
  end;
{$METHODINFO OFF}

var
  LogApp: TLogApp;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TLogApp }
procedure TLogApp.acceptLog(jObj: TJSONObject);
var
  Env: TMongoEnv;
  Doc: TMongoDocument;
  Con: TMongoConnection;
  I: Integer;
begin
  try
    try
      FDMongoConnection.Close;
      FDMongoConnection.Params.Clear;

      FDMongoConnection.Params.Add('DriverID=Mongo');
      FDMongoConnection.Params.Add('Server=192.168.25.3');
      FDMongoConnection.Params.Add('Port=27017');
      FDMongoConnection.Params.Add('Database=appLog');
      FDMongoConnection.Params.Add('User_Name=' + QuotedStr('root'));
      FDMongoConnection.Params.Add('Password=' + QuotedStr('root'));
//      FDMongoConnection.Params.Add('MongoAdvanced=authMechanism=MONGODB-X509');


      FDMongoConnection.Connected := True;

      Con := TMongoConnection(FDMongoConnection.CliObj);
      Env := Con.Env;

      Doc := Env.NewDoc;

      for I := 0 to Pred(jObj.Count) do
        Doc.Add(jObj.Pairs[I].JsonString.Value, jObj.Pairs[I].JsonValue.Value);

      Con['appLog']['log'].Insert(Doc);

    except
      on E: Exception do
        raise Exception.Create('Error Message ' + E.Message);
    end;
  finally

  end;
end;

function TLogApp.Log: TJSONObject;
begin
  try
    try

    except

    end;
  finally

  end;
end;

end.
