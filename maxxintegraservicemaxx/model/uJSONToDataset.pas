unit uJSONToDataset;

interface

uses FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, JSON, Rest.JSON, SysUtils, DateUtils, Variants, StrUtils, Winapi.Windows,
  System.Classes;


type
  TJSONToDataSet = class
  private
    FJsonObject: TJSONObject;

    FNomeQuery: TStringList;
    procedure SetJsonObject(const Value: TJSONObject);
    procedure PreencherObjeto(Nome: String; jObj: TJSONObject);
    procedure PercorrerArray(jArr: TJSONArray; Nome : String = '');
  public
    FArrayQuery: Array of TFDMemTable;
    constructor Create; overload;
    destructor Destroy; override;
    property JsonObject: TJSONObject read FJsonObject write SetJsonObject;

    function Converter: Boolean;


  end;

implementation

{ TJSONToDataSet }


function TJSONToDataSet.Converter: Boolean;
var
  I: Integer;
begin
  Result := True;
  try
    if FJsonObject <> nil then
    begin
      if FJsonObject.Count > 0 then
      begin
        for I := 0 to Pred(FJsonObject.Count) do
        begin
          if FJsonObject.Pairs[I].JsonString.Value = 'result' then
          begin
            PercorrerArray(TJSONArray(FJsonObject.Pairs[I].JsonValue));
          end;
        end;
      end
    end
    else
      Result := False;
  except
    on E: Exception do
    begin
      raise Exception.Create('Converter: ' + E.Message);
      Result := False;
    end;
  end;
end;

constructor TJSONToDataSet.Create;
begin
  inherited Create;
  FJsonObject := TJSONObject.Create;
  FNomeQuery  := TStringList.Create;
end;

destructor TJSONToDataSet.Destroy;
var
  I: Integer;
begin
  inherited Destroy;
//  FreeAndNil(FJsonObject);

  for I := 0 to Pred(Length(FArrayQuery)) do
    FreeAndNil(FArrayQuery[I]);
end;

procedure TJSONToDataSet.PercorrerArray(jArr: TJSONArray; Nome : String = '');
var
  I: Integer;
begin
  if Nome = '' then
  begin
    for I := 0 to Pred(jArr.Count) do
    begin
      if jArr.Items[I].ClassType = TJSONObject then
        PreencherObjeto(TJsonObject(jarr.Items[I]).Pairs[0].JsonString.Value, TJsonObject(TJsonObject(jarr.Items[I]).Pairs[0].JsonValue));
    end;
  end
  else
  begin
    for I := 0 to Pred(jArr.Count) do
     PreencherObjeto(Nome, TJSONObject(jArr.Items[I]));
  end;
end;

procedure TJSONToDataSet.PreencherObjeto(Nome: String; jObj: TJSONObject);
var
  I, Index: Integer;
begin
  try
    Index := FNomeQuery.IndexOf(Nome);


    if FNomeQuery.IndexOf(Nome) > -1 then //se j� percorreu o objeto
      FArrayQuery[FNomeQuery.IndexOf(Nome)].Append
    else
    begin
      FNomeQuery.Add(Nome);

      Index := FNomeQuery.IndexOf(Nome);


      SetLength(FArrayQuery, Index + 1);

      FArrayQuery[Index] := TFDMemTable.Create(nil);

      FArrayQuery[Index].Name := Nome;

      for I := 0 to Pred(jObj.Count) do
      begin
        if (jObj.Pairs[I].JsonValue.ClassType <> TJSONObject) and
           (jObj.Pairs[I].JsonValue.ClassType <> TJSONArray) then
        begin
          with FArrayQuery[Index].FieldDefs.AddFieldDef do
          begin
            Name     :=  jObj.Pairs[I].JsonString.Value;
            DataType := ftMemo;
          end;
        end;
      end;

      FArrayQuery[Index].Open;
    end;

    if FArrayQuery[Index].Fields.Count > 0 then
    begin
      FArrayQuery[Index].Append;

      for I := 0 to Pred(jObj.Count) do
      begin
        if (jObj.Pairs[I].JsonValue.ClassType <> TJSONObject) and
           (jObj.Pairs[I].JsonValue.ClassType <> TJSONArray) then
        begin
          with FArrayQuery[Index] do
            Fields[I].Value := jObj.Pairs[I].JsonValue.Value;
        end
        else if jObj.Pairs[I].JsonValue.ClassType = TJSONObject then
          PreencherObjeto(jObj.Pairs[I].JsonString.Value, TJSONObject(jObj.Pairs[I].JsonValue))
        else PercorrerArray(TJSONArray(jObj.Pairs[I].JsonValue), jObj.Pairs[I].JsonString.Value);
      end;
      FArrayQuery[Index].Post;
    end
    else
      raise Exception.Create('Erro ao criar os campos do DataSet!');
  except
    on E: Exception do
      raise Exception.Create('PreencherObjeto: ' + E.Message);
  end;
end;

procedure TJSONToDataSet.SetJsonObject(const Value: TJSONObject);
begin
  FJsonObject := Value;
end;

end.
