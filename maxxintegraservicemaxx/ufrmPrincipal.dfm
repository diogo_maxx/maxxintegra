object Form1: TForm1
  Left = 271
  Top = 114
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 272
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 417
    Height = 272
    Align = alClient
    BorderOuter = fsFlatBold
    TabOrder = 0
    ExplicitWidth = 423
    ExplicitHeight = 139
    object Label1: TLabel
      Left = 288
      Top = 59
      Width = 20
      Height = 13
      Caption = 'Port'
    end
    object ButtonStart: TButton
      Left = 24
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Iniciar'
      TabOrder = 0
      OnClick = ButtonStartClick
    end
    object ButtonStop: TButton
      Left = 24
      Top = 87
      Width = 75
      Height = 25
      Caption = 'Parar'
      TabOrder = 1
      OnClick = ButtonStopClick
    end
    object EditPort: TEdit
      Left = 288
      Top = 73
      Width = 121
      Height = 21
      TabStop = False
      Enabled = False
      TabOrder = 2
      Text = '1090'
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnMinimize = ApplicationEvents1Minimize
    Left = 256
    Top = 72
  end
  object TrayIcon1: TTrayIcon
    Hint = 'MaxxIntegra - Servi'#231'o para enviar script'
    OnClick = TrayIcon1Click
    Left = 440
    Top = 208
  end
end
