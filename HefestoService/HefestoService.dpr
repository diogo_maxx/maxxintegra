program HefestoService;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  ufrmPrincipal in 'ufrmPrincipal.pas' {Form1},
  wbModule in 'wbModule.pas' {WebModule1: TWebModule},
  uLog in 'model\uLog.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
