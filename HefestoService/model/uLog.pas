unit uLog;

interface

uses
  JSON, REST.JSON, System.SysUtils, System.Classes;


type
{$METHODINFO ON}
  Log = class (TComponent)
    private
      FVersao: String;
      FCliente: String;
      FSituacao: String;
      FMensagem: WideString;
      FUsuario: String;
      FApp: String;
      FData: TDateTime;
      procedure SetApp(const Value: String);
      procedure SetCliente(const Value: String);
      procedure SetData(const Value: TDateTime);
      procedure SetMensagem(const Value: WideString);
      procedure SetSituacao(const Value: String);
      procedure SetUsuario(const Value: String);
      procedure SetVersao(const Value: String);


      property App: String read FApp write SetApp;
      property Situacao: String read FSituacao write SetSituacao;
      property Data: TDateTime read FData write SetData;
      property Cliente: String read FCliente write SetCliente;
      property Versao: String read FVersao write SetVersao;
      property Usuario: String read FUsuario write SetUsuario;
      property Mensagem: WideString read FMensagem write SetMensagem;
    public
      function updateSalvarLog: String;

  end;
{$METHODINFO OFF}

implementation

{ Log }

function Log.updateSalvarLog: String;
begin
  try
    Result := 'Ok';
  finally

  end;
end;

procedure Log.SetApp(const Value: String);
begin
  FApp := Value;
end;

procedure Log.SetCliente(const Value: String);
begin
  FCliente := Value;
end;

procedure Log.SetData(const Value: TDateTime);
begin
  FData := Value;
end;

procedure Log.SetMensagem(const Value: WideString);
begin
  FMensagem := Value;
end;

procedure Log.SetSituacao(const Value: String);
begin
  FSituacao := Value;
end;

procedure Log.SetUsuario(const Value: String);
begin
  FUsuario := Value;
end;

procedure Log.SetVersao(const Value: String);
begin
  FVersao := Value;
end;

end.
