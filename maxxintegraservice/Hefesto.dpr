program Hefesto;
{$APPTYPE GUI}

{$R *.dres}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  uServerMethods in 'model\uServerMethods.pas' {ServerMethods: TDataModule},
  uWMPrincipal in 'model\uWMPrincipal.pas' {WMPrincipal: TWebModule},
  ufrmPrincipal in 'views\ufrmPrincipal.pas' {frmPrincipal},
  uConexao in 'model\uConexao.pas',
  uLog in 'model\uLog.pas',
  uDatasetToJSON in 'model\uDatasetToJSON.pas',
  uMaxxREST in 'model\uMaxxREST.pas',
  uCriarTabelas in 'model\uCriarTabelas.pas',
  uJSONToDataset in 'model\uJSONToDataset.pas',
  uExportarInformacoes in 'model\uExportarInformacoes.pas',
  uConstantes in 'model\uConstantes.pas',
  uLayoutFormulario in 'model\uLayoutFormulario.pas',
  ufrmMasterCad in 'views\ufrmMasterCad.pas' {frmMasterCad},
  ufrmCadEmpresa in 'views\ufrmCadEmpresa.pas' {frmCadEmpresa},
  uException in 'model\uException.pas',
  ufrmCadFilial in 'views\ufrmCadFilial.pas' {frmCadFilial},
  ufrmMasterPes in 'views\ufrmMasterPes.pas' {frmMasterPes},
  uPesquisar in 'model\uPesquisar.pas',
  ufrmLogDetalhes in 'views\ufrmLogDetalhes.pas' {frmLogDetalhes};

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
