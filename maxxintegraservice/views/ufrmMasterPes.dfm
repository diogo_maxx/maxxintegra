object frmMasterPes: TfrmMasterPes
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'frmMasterPes'
  ClientHeight = 205
  ClientWidth = 508
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 508
    Height = 205
    Align = alClient
    BorderOuter = fsFlatBold
    BorderColor = 6700580
    BorderHighlight = 6700580
    Color = 6700580
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 42
      Height = 13
      Caption = 'Pesquisa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object btnCancelar: TRzRapidFireButton
      AlignWithMargins = True
      Left = 347
      Top = 12
      Width = 76
      Height = 27
      Caption = 'Cancelar'
      OnClick = btnCancelarClick
    end
    object cxGridPesquisa: TcxGrid
      AlignWithMargins = True
      Left = 5
      Top = 45
      Width = 498
      Height = 155
      Align = alBottom
      TabOrder = 0
      object cxGridPesquisaDBTableView1: TcxGridDBTableView
        OnDblClick = cxGridPesquisaDBTableView1DblClick
        OnKeyPress = cxGridPesquisaDBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        FilterBox.Visible = fvNever
        DataController.DataSource = dtsPesquisa
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsSelection.CellSelect = False
        OptionsView.GridLines = glNone
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        Styles.Content = cxStyle2
        Styles.ContentEven = cxStyle1
      end
      object cxGridPesquisaLevel1: TcxGridLevel
        GridView = cxGridPesquisaDBTableView1
      end
    end
    object msePESQUISA: TRzMaskEdit
      Left = 4
      Top = 18
      Width = 333
      Height = 21
      Color = 6700580
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      FrameColor = clWhite
      FrameHotColor = clWhite
      FrameSides = [sdBottom]
      FrameVisible = True
      FramingPreference = fpCustomFraming
      ParentColor = True
      ParentFont = False
      TabOrder = 1
      Text = ''
      OnKeyPress = msePESQUISAKeyPress
      OnKeyUp = msePESQUISAKeyUp
    end
    object btnSalvar: TRzBitBtn
      AlignWithMargins = True
      Left = 429
      Top = 12
      Width = 70
      Height = 27
      HelpType = htKeyword
      HelpKeyword = 'Padrao'
      Caption = '&Ok'
      TabOrder = 2
      OnClick = btnSalvarClick
    end
  end
  object FDPesquisa: TFDQuery
    Left = 40
    Top = 80
  end
  object dtsPesquisa: TDataSource
    DataSet = FDPesquisa
    Left = 104
    Top = 80
  end
  object StyleGrid: TcxStyleRepository
    Left = 280
    Top = 32
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 16243131
    end
    object cxStyle2: TcxStyle
    end
  end
end
