unit ufrmPrincipal;

interface

uses Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, Web.HTTPApp, Vcl.ExtCtrls,
  Vcl.Mask,
  dxToggleSwitch, System.ImageList, Vcl.ImgList, acAlphaImageList, RzCmboBx, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxImageComboBox, IWSystem, dxActivityIndicator, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, RzButton, Vcl.Buttons, sSpeedButton, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, StrUtils, Json, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, IniFiles, FireDAC.Phys.FB, FireDAC.Phys.FBDef, JvComponentBase, JvThreadTimer, REST.Types,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, Vcl.DBCtrls, Vcl.ComCtrls, jsontreeview,
  jsondoc, acPNG, acImage, FireDAC.Phys.MSSQLDef, FireDAC.Phys.OracleDef, FireDAC.Phys.Oracle,
  FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, sSkinManager, dxSkinsForm, sPanel,
  sPageControl, acHeaderControl, Vcl.ToolWin, sToolBar, sTabControl, sBitBtn, sLabel, sMaskEdit, sCustomComboEdit,
  sCurrEdit, sMemo, sButton, sSkinProvider, System.Diagnostics, System.SyncObjs, sGroupBox, sTreeView, uConexao,
  acSlider, DateUtils, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxEdit, sEdit, Vcl.WinXCtrls;

Type
  TTaskHelper = class helper for TTask
  private type
    TUnsafeTaskEx = record
    private
      [Unsafe]
      // preciso de um record UNSAFE para nao incrementar o RefCount da Interface
      FTask: TTask;
    public
      property Value: TTask read FTask write FTask;
    end;
  public
    class function WaitForAllEx(AArray: Array of ITask;
    ATimeOut: int64 = INFINITE): boolean;
  end;


type
  TfrmPrincipal = class(TForm)
    FDConexao: TFDQuery;
    ThreadTimer: TJvThreadTimer;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    FDPhysOracleDriverLink1: TFDPhysOracleDriverLink;
    StyleGrid: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    FDEmpresa: TFDQuery;
    FDFilial: TFDQuery;
    dtsEmpresa: TDataSource;
    dtsFilial: TDataSource;
    FDEmpresaid: TFDAutoIncField;
    FDEmpresaCODEMP: TStringField;
    FDEmpresaRAZAO: TStringField;
    FDEmpresaFANTASIA: TStringField;
    FDEmpresaTIPO: TStringField;
    FDEmpresaCPFCNPJ: TStringField;
    FDEmpresaIE: TStringField;
    FDEmpresaEMAIL: TStringField;
    FDEmpresaTELEFONE: TStringField;
    FDEmpresaCEP: TStringField;
    FDEmpresaLOGRADOURO: TStringField;
    FDEmpresaCOMPLEMENTO: TStringField;
    FDEmpresaNUMERO: TStringField;
    FDEmpresaBAIRRO: TStringField;
    FDEmpresaCIDADE: TStringField;
    FDEmpresaESTADO: TStringField;
    FDEmpresaLOGOMARCA: TBlobField;
    FDFilialid: TFDAutoIncField;
    FDFilialIDEMPRESA: TIntegerField;
    FDFilialCODFILIAL: TStringField;
    FDFilialRAZAO: TStringField;
    FDFilialFANTASIA: TStringField;
    FDFilialTIPO: TStringField;
    FDFilialCPFCNPJ: TStringField;
    FDFilialIE: TStringField;
    FDFilialEMAIL: TStringField;
    FDFilialTELEFONE: TStringField;
    FDFilialCEP: TStringField;
    FDFilialLOGRADOURO: TStringField;
    FDFilialCOMPLEMENTO: TStringField;
    FDFilialNUMERO: TStringField;
    FDFilialBAIRRO: TStringField;
    FDFilialCIDADE: TStringField;
    FDFilialESTADO: TStringField;
    FDFilialLOGOMARCA: TBlobField;
    FDEmpresaDTCRIACAO: TSQLTimeStampField;
    FDEmpresaDUMANUT: TSQLTimeStampField;
    FDFilialDTCRIACAO: TSQLTimeStampField;
    FDFilialDUMANUT: TSQLTimeStampField;
    FDSchemaAdapter: TFDSchemaAdapter;
    sSkinManager1: TsSkinManager;
    sPanel1: TsPanel;
    sImage2: TsImage;
    sAlphaImageList1: TsAlphaImageList;
    pgcPrincipal: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet3: TsTabSheet;
    btnIniciar: TButton;
    btnParar: TButton;
    sLabel1: TsLabel;
    mseSistema: TsCalcEdit;
    sTabSheet2: TsTabSheet;
    sPageControl2: TsPageControl;
    sPageControl3: TsPageControl;
    sTabSheet4: TsTabSheet;
    sTabSheet5: TsTabSheet;
    sPanel2: TsPanel;
    btnAddEmp: TsButton;
    btnEditEmp: TsButton;
    btnDelEmp: TsButton;
    cxGridEmpresa: TcxGrid;
    cxGridEmpresaDBTableView: TcxGridDBTableView;
    cxGridEmpresaDBTableViewid: TcxGridDBColumn;
    cxGridEmpresaDBTableViewRAZAO: TcxGridDBColumn;
    cxGridEmpresaDBTableViewFANTASIA: TcxGridDBColumn;
    cxGridEmpresaDBTableViewTIPO: TcxGridDBColumn;
    cxGridEmpresaDBTableViewCPFCNPJ: TcxGridDBColumn;
    cxGridEmpresaLevel: TcxGridLevel;
    sPanel3: TsPanel;
    btnAddFil: TsButton;
    btnEditFil: TsButton;
    btnDelFil: TsButton;
    cxGridFiliais: TcxGrid;
    cxGridFiliaisDBTableView1: TcxGridDBTableView;
    cxGridFiliaisDBTableView1id: TcxGridDBColumn;
    cxGridFiliaisDBTableView1RAZAO: TcxGridDBColumn;
    cxGridFiliaisDBTableView1FANTASIA: TcxGridDBColumn;
    cxGridFiliaisDBTableView1TIPO: TcxGridDBColumn;
    cxGridFiliaisDBTableView1CPFCNPJ: TcxGridDBColumn;
    cxGridFiliaisLevel: TcxGridLevel;
    sSkinProvider1: TsSkinProvider;
    sGroupBox1: TsGroupBox;
    mmoLog: TsMemo;
    dxActivityIndicator1: TdxActivityIndicator;
    sPanel4: TsPanel;
    slStatusServico: TsSlider;
    sSlider2: TsSlider;
    sGroupBox4: TsGroupBox;
    FDLog: TFDQuery;
    dtsLog: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    FDLogid: TFDAutoIncField;
    FDLogtabela: TWideStringField;
    FDLogtiporequisicao: TWideStringField;
    FDLogregistro: TWideStringField;
    FDLoghoraintegracao: TSQLTimeStampField;
    FDLoghorasincronismo: TSQLTimeStampField;
    FDLogstatuscod: TIntegerField;
    FDLogerro: TBooleanField;
    FDLogjsonretorno: TWideMemoField;
    FDLogjsonenvio: TWideMemoField;
    FDLogurl: TWideStringField;
    FDLogparametros: TWideMemoField;
    FDLogmensagem: TWideStringField;
    FDLogdatalog: TSQLTimeStampField;
    cxGridDBTableView1registro: TcxGridDBColumn;
    cxGridDBTableView1horaintegracao: TcxGridDBColumn;
    cxGridDBTableView1horasincronismo: TcxGridDBColumn;
    cxGridDBTableView1statuscod: TcxGridDBColumn;
    cxGridDBTableView1mensagem: TcxGridDBColumn;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxGridDBTableView1tabela: TcxGridDBColumn;
    cxGridDBTableView1tiporequisicao: TcxGridDBColumn;
    sPanel5: TsPanel;
    msePESQUISA: TsEdit;
    btnAtualizar: TsButton;
    dxActivityIndicator2: TdxActivityIndicator;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel4MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnIniciarClick(Sender: TObject);
    procedure btnPararClick(Sender: TObject);
    procedure ThreadTimerTimer(Sender: TObject);
    procedure btnAddEmpClick(Sender: TObject);
    procedure btnEditEmpClick(Sender: TObject);
    procedure btnEditFilClick(Sender: TObject);
    procedure btnDelFilClick(Sender: TObject);
    procedure btnDelEmpClick(Sender: TObject);
    procedure btnAddFilClick(Sender: TObject);
    procedure msePESQUISAKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnAtualizarClick(Sender: TObject);
    procedure cxGridDBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
  private
    FServer: TIdHTTPWebBrokerBridge;
//    IAguarde: ITask;
    Conexao: TConexao;
    procedure Desconectar;
    procedure Iniciar;
    procedure PrimeiroUso;
    procedure ExecutarSincronismo;
    procedure LimparCache(Sender: TFDDataSet; AErrors: Integer);
    procedure PreencherEmpresaFilial;

//    procedure Timer(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
    IPMAXX, IPMAXXLOG: String;
    Preencher_PrimeiroUso, ErroBDCliente, ParouServico: Boolean;
    procedure LimpaMemoriaAPP;
    procedure PrepararParametrosUD(SQL: String);
    function PreencheSCHEMA: String;
  end;

var
  frmPrincipal: TfrmPrincipal;
  EmpresaPreenchida: Boolean;

implementation

{$R *.dfm}

uses
  WinApi.Windows, Winapi.ShellApi, Datasnap.DSSession, uLog, uMaxxREST,
  uCriarTabelas, uExportarInformacoes, uLayoutFormulario, ufrmMasterCad, ufrmCadEmpresa, uException, ufrmCadFilial,
  ufrmLogDetalhes;

procedure TerminateThreads;
begin
  if TDSSessionManager.Instance <> nil then
    TDSSessionManager.Instance.TerminateAllSessions;
end;

procedure TfrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Desconectar;

  FreeAndNil(Conexao);
  Application.Terminate;
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  Conexao := TConexao.GetInstance;

  FServer := TIdHTTPWebBrokerBridge.Create(Self);

  FDConexao.Connection  := Conexao.SQLite;
  FDEmpresa.Connection  := Conexao.SQLite;
  FDFilial.Connection   := Conexao.SQLite;
  FDLog.Connection      := Conexao.SQLite;


  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TFDQuery then
    begin
      //Configurando Mestre-Detalhe
      if (TFDQuery(Components[I]).MasterSource <> nil) and
         (TFDQuery(Components[I]).DetailFields = '')  and
         (TFDQuery(Components[I]).IndexFieldNames = '') then
      begin
        TFDQuery(Components[I]).DetailFields               := TFDQuery(Components[I]).MasterFields;
        TFDQuery(Components[I]).IndexFieldNames            := TFDQuery(Components[I]).MasterFields;
        TFDQuery(Components[I]).FetchOptions.DetailCascade := True;
        TFDQuery(Components[I]).FetchOptions.Cache         := [fiDetails, fiBlobs, fiMeta];
      end;
      //seta a procedure de p�s apply para comitar todos os registros.
      TFDQuery(Components[I]).AfterApplyUpdates := LimparCache;
    end;
  end;

end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
var
  ArqIni : TIniFile;
begin
  pgcPrincipal.ActivePageIndex := 0;

  try
    ArqIni := TIniFile.Create(StringReplace(UpperCase(gsAppPath), '\EXE\', '\Conf\', []) + 'conf.cfg');

    IPMAXX    := ArqIni.ReadString('SERVIDOR', 'IP', 'maxxsoft.no-ip.org');
    IPMAXXLOG := ArqIni.ReadString('SERVIDOR', 'IPLOG', 'maxxsoft.no-ip.org:1091');
  except
    on E: Exception do
    begin
      SalvarLog('Leitura de arquivo de conf: ' + E.Message);
      ArqIni.Free;
    end;
  end;

  if FileExists(StringReplace(UpperCase(gsAppPath), '\EXE\', '\BD\', []) + 'maxxintegra.db3') then
  begin
    Conexao.Conectar(True, False);

    FDConexao.Close;
    FDConexao.Open;

    FDEmpresa.Close;
    FDEmpresa.Open;
    FDFilial.Close;
    FDFilial.Open;

//    FDLog.Close;
//    FDLog.Open;

    mseSistema.Text := FDConexao.FieldByName('idsistemasintegrados').AsString;

    FDConexao.Close;
  end
  else pgcPrincipal.Pages[1].TabVisible := False;

//  PadronizarTela(Self);
  LimpaMemoriaAPP;
end;

procedure TfrmPrincipal.btnAddEmpClick(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmCadEmpresa, frmCadEmpresa);

    FDEmpresa.Append;
    FDEmpresaDTCRIACAO.AsDateTime := Now;
    FDEmpresaTIPO.AsString        := 'JURIDICA';

    frmCadEmpresa.ShowModal;

    if frmCadEmpresa.ModalResult = mrOk then
    begin
      FDEmpresaDUMANUT.AsDateTime := Now;
      FDEmpresa.Post;
      FDEmpresa.ApplyUpdates(0);
    end
    else FDEmpresa.Cancel;
  finally
    FreeAndNil(frmCadEmpresa);
  end;
end;

procedure TfrmPrincipal.btnDelEmpClick(Sender: TObject);
begin
  FDFilial.First;
  while not FDFilial.Eof do
    FDFilial.Delete;

  FDEmpresa.Delete;
  FDEmpresa.ApplyUpdates(0);
end;

procedure TfrmPrincipal.btnDelFilClick(Sender: TObject);
begin
  FDFilial.Delete;
  FDFilial.ApplyUpdates(0);
end;

procedure TfrmPrincipal.btnEditEmpClick(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmCadEmpresa, frmCadEmpresa);

    FDEmpresa.Edit;

    frmCadEmpresa.ShowModal;

    if frmCadEmpresa.ModalResult = mrOk then
    begin
      FDEmpresaDUMANUT.AsDateTime := Now;
      FDEmpresa.Post;
      FDEmpresa.ApplyUpdates(0);
    end
    else
      FDEmpresa.Cancel;
  finally
    FreeAndNil(frmCadEmpresa);
  end;
end;

procedure TfrmPrincipal.btnEditFilClick(Sender: TObject);
begin
  try
    Application.CreateForm(TfrmCadFilial, frmCadFilial);

    FDFilial.Edit;

    frmCadFilial.ShowModal;

    if frmCadFilial.ModalResult = mrOk then
    begin
      FDFilialDUMANUT.AsDateTime := Now;
      FDFilial.Post;
      FDFilial.ApplyUpdates(0);
    end
    else FDFilial.Cancel;
  finally
    FreeAndNil(frmCadFilial);
  end;
end;

procedure TfrmPrincipal.Iniciar;
begin
  try
    if not FServer.Active then
    begin
      FServer.Bindings.Clear;
      FServer.DefaultPort := 18080;
      FServer.Active      := True;

      btnIniciar.Enabled     := False;
      btnParar.Enabled       := True;
    end;

    slStatusServico.SliderOn := True;
    SalvarLog('Servi�o iniciado.');
    ParouServico := False;
  except
    on E: Exception do
    begin
      SalvarLog('Iniciar servi�o: ' + E.Message);
      SalvarLog('N�o foi poss�vel iniciar o servidor! ' + E.Message);
    end;
  end;
end;

procedure TfrmPrincipal.Panel4MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  if Button = mbleft then
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

procedure TfrmPrincipal.btnPararClick(Sender: TObject);
begin
  Desconectar;
end;

procedure TfrmPrincipal.cxGridDBTableView1CellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  try
    Application.CreateForm(TfrmLogDetalhes, frmLogDetalhes);
    frmLogDetalhes.ShowModal;
  finally
    FreeAndNil(frmLogDetalhes);
  end;
end;

procedure TfrmPrincipal.btnIniciarClick(Sender: TObject);
var
  IInicio: ITask;
begin
  try
    mmoLog.Lines.Clear;

    btnParar.Enabled := True;

    ErroBDCliente               := False;
    dxActivityIndicator1.Active := True;

    IInicio := TTask.Create(
    procedure()
    var
      MaxxREST: TMaxxRESTCliente;
      CriaTabelas: TCriarTabelas;
    begin
      try
        try
          MaxxREST               := TMaxxRESTCliente.GetInstance;
          MaxxREST.BaseURL       := IPMAXX + '/datasnap/rest/';
          MaxxREST.EndPoint      := 'TServerMethods1/GetConfiguracao/' + mseSistema.Text;

          CriaTabelas            := TCriarTabelas.Create;
          CriaTabelas.JSONTabela := TJSONObject(MaxxRest.BuscarConfiguracoes);

          if CriaTabelas.JSONTabela <> nil then
          begin
            if CriaTabelas.CriarTabelas(mmoLog.Lines) then
            begin
              Conexao.Conectar(False, True);

              if not CriaTabelas.CriarTabelaEmpresaFilial then
                Exit;

              if Conexao.BDCli.Connected then
              begin
                if not CriaTabelas.CriarTabelaIntegracao then
                  Exit;

                if not CriaTabelas.CriarTriggers(mmoLog.Lines) then
                  Exit;
              end
              else ErroBDCliente := True;
            end;
          end;

          Preencher_PrimeiroUso := CriaTabelas.TabelaPriUso;
        except
          on E: Exception do
            SalvarLog('IInicio: ' + E.Message);
        end;

        FDConexao.Close;
        FDConexao.Open;

        MaxxREST.BaseURL_Login  := frmPrincipal.FDConexao.FieldByName('sis_baseurl').AsString;
        MaxxREST.EndPoint_Login := frmPrincipal.FDConexao.FieldByName('sis_endpointlogin').AsString;
        MaxxREST.User           := frmPrincipal.FDConexao.FieldByName('sis_wsusername').AsString;
        MaxxREST.Password       := frmPrincipal.FDConexao.FieldByName('sis_wspassword').AsString;

      finally
        FreeAndNil(CriaTabelas);
      end;
    end);
    IInicio.Start;
    TTask.WaitForAllEx(IInicio);

    try
      if not ErroBDCliente then
      begin
        FDEmpresa.Close;
        FDEmpresa.Open;
        FDFilial.Close;
        FDFilial.Open;

        if not FDEmpresa.IsEmpty then
        begin
          PrimeiroUso;
          ThreadTimer.Enabled := True;
          Iniciar; //inicia o servi�o
        end
        else
        begin
          pgcPrincipal.Pages[1].TabVisible := True;

          mmoLog.Lines.Add('Inserindo pr� registros de EMPRESA / FILIAL...');

          PreencherEmpresaFilial;

          mmoLog.Lines.Add('Confira as informa��es de EMPRESA para iniciar o servi�o!!!!');
        end;
      end;
    except
      on E: Exception do
        SalvarLog('Abrindo empresas/filial: ' + E.Message);
    end;
  finally
    LimpaMemoriaAPP;
    dxActivityIndicator1.Active := False;
  end;
end;

procedure TfrmPrincipal.ExecutarSincronismo;
var
  ISincr: ITask;
  Erro: Boolean;
begin
  if not ErroBDCliente then
  begin
    dxActivityIndicator1.Active := True;
    ThreadTimer.Enabled         := False;


    ISincr := TTask.Create(
    procedure()
    var
      ExportarInformacoes: TExportarInformacoes;
    begin
      ExportarInformacoes     := TExportarInformacoes.Create;
      Exportarinformacoes.Log := mmoLog.Lines;

      if ExportarInformacoes.Log <> nil then
      begin
        ExportarInformacoes.Log := mmoLog.Lines;
        Erro := not ExportarInformacoes.Iniciar;

        FreeAndNil(ExportarInformacoes);
      end;
    end);

    ISincr.Start;
    TTask.WaitForAllEx(ISincr);

    LimpaMemoriaAPP;

    dxActivityIndicator1.Active := False;

    if Erro then
      Desconectar;

    ThreadTimer.Enabled         := not Erro;
  end;
end;

procedure TfrmPrincipal.ThreadTimerTimer(Sender: TObject);
begin
  ExecutarSincronismo;
end;

procedure TfrmPrincipal.Desconectar;
var
  I: Integer;
begin
  TerminateThreads;
  FServer.Active := False;
  FServer.Bindings.Clear;

  Conexao.BDCli.Connected := False;

  ThreadTimer.Enabled    := False;

  btnIniciar.Enabled     := True;
  btnParar.Enabled       := False;

  ParouServico           := True;

  SalvarLog('Servi�o parado.');
  slStatusServico.SliderOn := False;
  sSlider2.SliderOn        := False;
end;

procedure TfrmPrincipal.LimpaMemoriaAPP;
var
  MainHandle : THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false,
    GetCurrentProcessID) ;
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(MainHandle) ;
  except
    on E: Exception do
      SalvarLog('LimparMemoriaAPP: ' + E.Message);
  end;
  Application.ProcessMessages;
end;

procedure TfrmPrincipal.PrimeiroUso;
var
  IPrimeiroUso: ITask;
begin
  IPrimeiroUso := TTask.Create(
  procedure()
  var
    _FDInt, _FDBusca, _FDExec, _FDPriUso: TFDQuery;
    I, J, Count: Integer;
    Campo: TStringList;
    CODIGO_REG: String;
  begin
    try
      try
        /////////////////////////////////////////////////////////////
        ///    Criando querys para manipula��o das informa��es    ///
        /////////////////////////////////////////////////////////////
        _FDInt              := TFDQuery.Create(nil);
        _FDExec             := TFDQuery.Create(nil);
        _FDBusca            := TFDQuery.Create(nil);
        _FDPriUso           := TFDQuery.Create(nil);

        _FDInt.Connection    := Conexao.SQLite;
        _FDPriUso.Connection := Conexao.SQLite;
        _FDExec.Connection   := Conexao.BDCli;
        _FDBusca.Connection  := Conexao.BDCli;

        if Preencher_PrimeiroUso then
        begin
          _FDPriUso.Close;
          _FDPriUso.SQL.Clear;
          _FDPriUso.SQL.Add('CREATE TABLE [primeirouso]( ' +
                         '  [tabela] VARCHAR(50), ' +
                         '  [quant] INTEGER, ' +
                         '  [ult_reg] VARCHAR(50), ' +
                         '  [id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL);');
//          _FDPriUso.Prepare;
          _FDPriUso.ExecSQL;
          _FDPriUso.Close;
        end;

        //Desabilitando controles para deixar mais r�pido os loop's
        _FDInt.DisableControls;
        _FDExec.DisableControls;
        _FDBusca.DisableControls;
        _FDPriUso.DisableControls;
        /////////////////////////////////////////////////////////////
        ///    Localizando as configura��es das tabelas pai       ///
        /////////////////////////////////////////////////////////////
        _FDInt.Close;
        _FDInt.SQL.Clear;
        _FDInt.SQL.Add('select * from sistemasintegrados_conf ' +
                       'where (conf_tabelapai = '''' or conf_tabelapai is null) ' +
                       ' and (tpscr_descricao <> ''EMPRESA'' and tpscr_descricao <> ''FILIAL'') ' +
                       'order by conf_prioridade');
        _FDInt.Open;

        if not _FDInt.IsEmpty then
        begin
          /////////////////////////////////////////////////////////////
          ///   Loop para enviar todos os registros configurados    ///
          /////////////////////////////////////////////////////////////
          _FDInt.First;
          while not _FDInt.Eof do
          begin
            _FDPriUso.Close;
            _FDPriUso.SQL.Clear;
            _FDPriUso.SQL.Add('select * from primeirouso');
            _FDPriUso.Open;
            _FDPriUso.CachedUpdates := True;

            _FDPriUso.Filtered := False;
            _FDPriUso.Filter   := 'tabela like ' + QuotedStr(_FDInt.FieldByName('conf_tabela').AsString);
            _FDPriUso.Filtered := True;

            if _FDPriUso.IsEmpty then
            begin
              /////////////////////////////////////////////////////////////////
              ///   Capturando campos (no sql) para montagem dos filtros    ///
              /////////////////////////////////////////////////////////////////
              Campo                 := TStringList.Create;
              Campo.StrictDelimiter := False;
              Campo.Delimiter       := ';';
              Campo.DelimitedText   := _FDInt.FieldByName('conf_codigosql').AsString;
              Campo.StrictDelimiter := True;
              /////////////////////////////////////////////////////////////
              ///   Buscando as informa��es no banco do cliente         ///
              /////////////////////////////////////////////////////////////
              _FDBusca.FetchOptions.Mode := fmAll;
              _FDBusca.Close;
              _FDBusca.SQL.Clear;
              _FDBusca.SQL.Add(_FDInt.FieldByName('conf_scriptbusca').AsString);

              if _FDBusca.MacroCount > 0 then
              begin
                if _FDBusca.FindMacro('SCHEMA') <> nil then
                  _FDBusca.MacroByName('SCHEMA').AsRaw := frmPrincipal.PreencheSCHEMA;
              end;
              _FDBusca.Open;

              SalvarLog('Importando registros da tabela: ' + _FDInt.FieldByName('TPSCR_DESCRICAO').AsString);


              _FDBusca.First;
              /////////////////////////////////////////////////////////////
              ///   Preparando o insert da tabela de integra��o         ///
              /////////////////////////////////////////////////////////////
              _FDExec.SQL.Clear;
              _FDExec.SQL.Add('INSERT INTO MAXX_INTEGRACAO (TABELA_REG, TABELA_PAI, CODIGO_REG, CODIGO_PAI, DATA, TIPO_MOD, ENVIADO) ' +
                              'VALUES (:TABELA_REG, :TABELA_PAI, :CODIGO_REG, :CODIGO_PAI, :DATA, :TIPO_MOD, :ENVIADO)');

              _FDExec.Params[0].DataType := TFieldType.ftString;
              _FDExec.Params[1].DataType := TFieldType.ftString;
              _FDExec.Params[2].DataType := TFieldType.ftString;
              _FDExec.Params[3].DataType := TFieldType.ftString;
              _FDExec.Params[4].DataType := TFieldType.ftDateTime;
              _FDExec.Params[5].DataType := TFieldType.ftString;
              _FDExec.Params[6].DataType := TFieldType.ftString;

              _FDExec.Prepare;
              _FDExec.Params.ArraySize := _FDBusca.RecordCount; //setando o tamanho de registros de insert

              for I := 0 to Pred(_FDExec.Params.ArraySize) do
              begin
                CODIGO_REG := '';

                for J := 0 to Campo.Count -1 do
                begin
                  if CODIGO_REG <> '' then
                    CODIGO_REG := CODIGO_REG + ';';

                  CODIGO_REG := CODIGO_REG + _FDBusca.FieldByName(Campo[J]).AsString;
                end;

                _FDExec.Params[0].Values[I] := _FDInt.FieldByName('conf_tabela').AsString;
                _FDExec.Params[1].Values[I] := NULL;
                _FDExec.Params[2].Values[I] := CODIGO_REG;
                _FDExec.Params[3].Values[I] := NULL;
                _FDExec.Params[4].Values[I] := Now;
                _FDExec.Params[5].Values[I] := 'I';
                _FDExec.Params[6].Values[I] := 'N';

                _FDBusca.Next;
              end;

              if _FDExec.Params.ArraySize > 0 then
                _FDExec.Execute(_FDExec.Params.ArraySize);

              _FDExec.Close;

              _FDPriUso.Append;
//                _FDPriUso.FieldByName('id').AsInteger     := -1;
              _FDPriUso.FieldByName('tabela').AsString  := _FDInt.FieldByName('conf_tabela').AsString;
              _FDPriUso.FieldByName('quant').AsInteger  := _FDBusca.RecordCount;
              _FDPriUso.FieldByName('ult_reg').AsString := CODIGO_REG;
              _FDPriUso.Post;
              _FDPriUso.ApplyUpdates(0);

              _FDBusca.Close;

              FreeAndNil(Campo);
            end;

            _FDInt.Next;
          end;
        end;
      except
        on E: Exception do
        begin
          TThread.Synchronize(nil,
          procedure()
          begin
            SalvarLog('IPrimeiroUso: ' + E.Message);
          end);
        end;
      end;
    finally
      _FDInt.Close;
      _FDBusca.Close;
      _FDExec.Close;
      _FDPriUso.Close;

      FreeAndNil(_FDInt);
      FreeAndNil(_FDBusca);
      FreeAndNil(_FDExec);
      FreeAndNil(_FDPriUso);
    end;
  end);

  IPrimeiroUso.Start;
  TTask.WaitForAllEx(IPrimeiroUso);
end;

procedure TfrmPrincipal.msePESQUISAKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  AItemList: TcxFilterCriteriaItemList;
  I: Integer;
begin
  inherited;
  with cxGridDBTableView1.DataController.Filter do
  begin
    if msePESQUISA.Text <> '' then
    begin
      Active := False;
      BeginUpdate;
      try
        Root.Clear;
        Root.BoolOperatorKind := fboOr;

        AItemList := Root.AddItemList(fboOr);

        for I := 0 to cxGridDBTableView1.ColumnCount -1 do
          AItemList.AddItem(cxGridDBTableView1.Columns[I], foLike, '%' + msePESQUISA.Text + '%', msePESQUISA.Text);
      finally
        EndUpdate;
        Active := True;
      end;
    end
    else
    begin
      Active := False;
      BeginUpdate;
      Root.Clear;
      EndUpdate;
    end;
  end;
end;

procedure TfrmPrincipal.btnAddFilClick(Sender: TObject);
begin
   try
    Application.CreateForm(TfrmCadFilial, frmCadFilial);

    FDFilial.Append;
    FDFilialIDEMPRESA.AsInteger  := FDEmpresaid.AsInteger;
    FDFilialDTCRIACAO.AsDateTime := Now;
    FDFilialTIPO.AsString        := 'JURIDICA';

    frmCadFilial.ShowModal;

    if frmCadFilial.ModalResult = mrOk then
    begin
      FDFilialDUMANUT.AsDateTime := Now;
      FDFilial.Post;
      FDFilial.ApplyUpdates(0);
    end
    else FDFilial.Cancel;
  finally
    FreeAndNil(frmCadFilial);
  end;
end;

procedure TfrmPrincipal.btnAtualizarClick(Sender: TObject);
var
  IAtualizarLog: ITask;
begin
  dxActivityIndicator2.Active := True;

  IAtualizarLog := TTask.Create(
  procedure
  begin
    cxGridDBTableView1.DataController.DataSource := nil;

    FDLog.Close;
    FDLog.Open;

    TThread.Synchronize(nil,
    procedure
    begin
      cxGridDBTableView1.DataController.DataSource := dtsLog;
    end);
  end);

  IAtualizarLog.Start;
  TTask.WaitForAllEx(IAtualizarLog);

  dxActivityIndicator2.Active := False;
end;

procedure TfrmPrincipal.PrepararParametrosUD(SQL: String);
var
  I: Integer;
  Campo: String;
  MaxxREST: TMaxxRESTCliente;
  FDQry: TFDQuery;
begin
  try
    FDQry               := TFDQuery.Create(nil);
    FDQry.Connection    := Conexao.BDCli;
    MaxxREST            := TMaxxRESTCliente.GetInstance;
    MaxxREST.UrlNomes   := '';
    MaxxREST.URLValores := '';

    ////////////////////////////////////////////
    ///  Preparando os nomes dos parametros  ///
    ////////////////////////////////////////////
    I := 0;
    while I <= Length(MaxxREST.EndPoint) do
    begin
      if Copy(MaxxREST.EndPoint, I, 1) = '{' then
      begin
        Campo := '';
        Inc(I);
        while (Copy(MaxxREST.EndPoint, I, 1) <> '}') or (I <= Length(MaxxREST.UrlNomes)) do
        begin
          Campo := Campo + Copy(MaxxREST.EndPoint, I, 1);
          Inc(I);
        end;

        if MaxxREST.UrlNomes <> '' then
          MaxxREST.UrlNomes := MaxxREST.UrlNomes + ';';

        MaxxREST.UrlNomes := MaxxREST.UrlNomes + Campo;
      end;
      Inc(I);
    end;
    /////////////////////////////////////////////
    ///  Preenchendo os nomes dos parametros  ///
    /////////////////////////////////////////////
    FDQry.Close;
    FDQry.SQL.Clear;
    FDQry.SQL.Add(SQL);
    FDQry.Open;

    I     := 1;
    Campo := '';

    if MaxxREST.UrlNomes <> '' then
    begin
      while I <= Length(MaxxREST.UrlNomes) do
      begin
        Campo := '';

        if Pos(';', MaxxREST.UrlNomes) > 0 then
        begin
          while (Copy(MaxxREST.UrlNomes, I, 1) <> ';') or (I <= Length(MaxxREST.UrlNomes)) do
          begin
            Campo := Campo + Copy(MaxxREST.UrlNomes, I, 1);
            Inc(I);
          end;
        end
        else
        begin
          while (I <= Length(MaxxREST.UrlNomes)) do
          begin
            Campo := Campo + Copy(MaxxREST.UrlNomes, I, 1);
            Inc(I);
          end;
        end;

        if MaxxREST.URLValores <> '' then
          MaxxREST.URLValores := MaxxREST.URLValores + ';';

        MaxxREST.URLValores := MaxxREST.URLValores + FDQry.FieldByName(Campo).AsString;
        Inc(I);
      end;
    end;
  finally
    FDQry.Close;
    FreeAndNil(FDQry);
  end;
end;

function TfrmPrincipal.PreencheSCHEMA: String;
begin
  case AnsiIndexStr(Conexao.BDCli.Params.DriverID, ['MSSQL', 'Ora']) of
    0: //SQLServer
    begin
      Result := Conexao.BDCli.Params.Database + '.';

      if Conexao.BDCli.Params.Values['MetaCurSchema'] <> '' then
        Result := Result + Conexao.BDCli.Params.Values['MetaCurSchema'];
    end;
    1: //Oracle
      Result := Conexao.BDCli.Params.Values['MetaCurSchema'];
  end;
end;

procedure TfrmPrincipal.LimparCache(Sender: TFDDataSet; AErrors: Integer);
begin
  TFDQuery(Sender).CommitUpdates;
end;

procedure TfrmPrincipal.PreencherEmpresaFilial;
var
  FDConf, FDInt, FDBusca: TFDQuery;

  function RetornaValor(Campo: String): Variant;
  begin
    FDConf.Filtered := False;
    FDConf.Filter   := 'IDCONF LIKE ' + QuotedStr(FDInt.FieldByName('IDCONF').AsString) + ' AND JSON_CAMPOJSON LIKE ' + QuotedStr(Campo);
    FDConf.Filtered := True;

    if not FDConf.IsEmpty then
    begin
      if FDBusca.FindField(FDConf.FieldByName('JSON_CAMPOSQL').AsString) <> nil then
        Result := FDBusca.FieldByName(FDConf.FieldByName('JSON_CAMPOSQL').AsString).AsVariant
      else
        Result := NULL;
    end
    else
      Result := NULL;
  end;

begin
  try
    FDConf  := TFDQuery.Create(nil);
    FDInt   := TFDQuery.Create(nil);
    FDBusca := TFDQuery.Create(nil);

    FDFilial.DataSource := nil;

    FDConf.Connection  := Conexao.SQLite;
    FDInt.Connection   := Conexao.SQLite;
    FDBusca.Connection := Conexao.BDCli;

    FDInt.Close;
    FDInt.SQL.Clear;
    FDInt.SQL.Add('select * from sistemasintegrados_conf');
    FDInt.Open;

    FDConf.Close;
    FDConf.SQL.Clear;
    FDConf.SQL.Add('select * from conf_json');
    FDConf.Open;

    /////////////////////////////////////
    ///  Empresa                      ///
    /////////////////////////////////////
    FDInt.Filtered := False;
    FDInt.Filter   := 'TPSCR_DESCRICAO LIKE ' + QuotedStr('EMPRESA');
    FDInt.Filtered := True;

    if not FDInt.IsEmpty then
    begin
      FDBusca.Close;
      FDBusca.SQL.Clear;
      FDBusca.SQL.Add(FDInt.FieldByName('conf_scriptbusca').AsString);

      if FDBusca.MacroCount > 0 then
      begin
        if FDBusca.FindMacro('SCHEMA') <> nil then
          FDBusca.MacroByName('SCHEMA').AsRaw := PreencheSCHEMA;
      end;

      FDBusca.Open;

      FDBusca.First;
      while not FDBusca.Eof do
      begin
        FDEmpresa.Append;
        FDEmpresaCODEMP.AsVariant      := RetornaValor('C�digo');
        FDEmpresaRAZAO.AsVariant       := RetornaValor('Raz�o');
        FDEmpresaFANTASIA.AsVariant    := RetornaValor('Fantasia');
        FDEmpresaTIPO.AsVariant        := 'JURIDICA';
        FDEmpresaCPFCNPJ.AsVariant     := RetornaValor('CNPJ');
        FDEmpresaIE.AsVariant          := RetornaValor('I.E');
        FDEmpresaLOGRADOURO.AsVariant  := RetornaValor('Logradouro');
        FDEmpresaCOMPLEMENTO.AsVariant := RetornaValor('Complemento');
        FDEmpresaCEP.AsVariant         := RetornaValor('CEP');
        FDEmpresaNUMERO.AsVariant      := RetornaValor('N�mero');
        FDEmpresaBAIRRO.AsVariant      := RetornaValor('Bairro');
        FDEmpresaCIDADE.AsVariant      := RetornaValor('Munic�pio');
        FDEmpresaESTADO.AsVariant      := RetornaValor('UF');
        FDEmpresaEMAIL.AsVariant       := RetornaValor('E-mail');
        FDEmpresaLOGOMARCA.AsVariant   := RetornaValor('Logomarca');
        FDEmpresaTELEFONE.AsVariant    := RetornaValor('Telefone');
        FDEmpresaDTCRIACAO.AsDateTime  := Now;
        FDEmpresaDUMANUT.AsDateTime    := Now;
        FDEmpresa.Post;

        FDBusca.Next;
      end;
      FDEmpresa.ApplyUpdates(0);
    end;

    /////////////////////////////////////
    ///  Filial                       ///
    /////////////////////////////////////
    FDInt.Filtered := False;
    FDInt.Filter   := 'TPSCR_DESCRICAO LIKE ' + QuotedStr('FILIAL');
    FDInt.Filtered := True;

    if not FDInt.IsEmpty then
    begin
      FDBusca.Close;
      FDBusca.SQL.Clear;
      FDBusca.SQL.Add(FDInt.FieldByName('conf_scriptbusca').AsString);

      if FDBusca.MacroCount > 0 then
      begin
        if FDBusca.FindMacro('SCHEMA') <> nil then
          FDBusca.MacroByName('SCHEMA').AsRaw := PreencheSCHEMA;
      end;

      FDBusca.Open;

      FDBusca.First;
      while not FDBusca.Eof do
      begin
        FDFilial.Append;

        FDEmpresa.Filtered := False;
        FDEmpresa.Filter   := 'CODEMP LIKE ' + QuotedStr(RetornaValor('Empresa'));
        FDEmpresa.Filtered := True;

        FDFilialIDEMPRESA.AsVariant   := FDEmpresaid.AsVariant;
        FDFilialCODFILIAL.AsVariant   := RetornaValor('C�digo');
        FDFilialRAZAO.AsVariant       := RetornaValor('Raz�o');
        FDFilialFANTASIA.AsVariant    := RetornaValor('Fantasia');
        FDFilialTIPO.AsVariant        := 'JURIDICA';
        FDFilialCPFCNPJ.AsVariant     := RetornaValor('CNPJ');
        FDFilialIE.AsVariant          := RetornaValor('I.E');
        FDFilialLOGRADOURO.AsVariant  := RetornaValor('Logradouro');
        FDFilialCOMPLEMENTO.AsVariant := RetornaValor('Complemento');
        FDFilialCEP.AsVariant         := RetornaValor('CEP');
        FDFilialNUMERO.AsVariant      := RetornaValor('N�mero');
        FDFilialBAIRRO.AsVariant      := RetornaValor('Bairro');
        FDFilialCIDADE.AsVariant      := RetornaValor('Munic�pio');
        FDFilialESTADO.AsVariant      := RetornaValor('UF');
        FDFilialEMAIL.AsVariant       := RetornaValor('E-mail');
        FDFilialLOGOMARCA.AsVariant   := RetornaValor('Logomarca');
        FDFilialTELEFONE.AsVariant    := RetornaValor('Telefone');
        FDFilialDTCRIACAO.AsDateTime  := Now;
        FDFilialDUMANUT.AsDateTime    := Now;
        FDFilial.Post;

        FDEmpresa.Filtered := False;
        FDEmpresa.Filter   := '';

        FDBusca.Next;
      end;
      FDFilial.ApplyUpdates(0);
    end;
  finally
    FDConf.Close;
    FDInt.Close;
    FDBusca.Close;

    FreeAndNil(FDConf);
    FreeAndNil(FDInt);
    FreeAndNil(FDBusca);

    FDFilial.DataSource := dtsEmpresa;
  end;
end;






{ TTaskHelper }

class function TTaskHelper.WaitForAllEx(AArray: array of ITask; ATimeOut: int64): boolean;
var
  FEvent: TEvent;
  task: TUnsafeTaskEx;
  i: integer;
  taskInter: TArray<TUnsafeTaskEx>;
  completou: boolean;
  Canceled, Exceptions: boolean;
  ProcCompleted: TProc<ITask>;
  LHandle: THandle;
  LStop: TStopwatch;
begin
  LStop := TStopwatch.StartNew;
  ProcCompleted := procedure(ATask: ITask)
    begin
      FEvent.SetEvent;
    end;

  Canceled := false;
  Exceptions := false;
  result := true;
  try
    for i := low(AArray) to High(AArray) do
    begin
      task.Value := TTask(AArray[i]);
      if task.Value = nil then
        raise EArgumentNilException.Create('Wait Nil Task');

      completou := task.Value.IsComplete;
      if not completou then
      begin
        taskInter := taskInter + [task];
      end
      else
      begin
        if task.Value.HasExceptions then
          Exceptions := true
        else if task.Value.IsCanceled then
          Canceled := true;
      end;
    end;

    try
      FEvent := TEvent.Create();
      for task in taskInter do
      begin
        try
          FEvent.ResetEvent;
          if LStop.ElapsedMilliseconds > ATimeOut then
            break;
          LHandle := FEvent.Handle;
          task.Value.AddCompleteEvent(ProcCompleted);
          while not task.Value.IsComplete do
          begin
            try
              if LStop.ElapsedMilliseconds > ATimeOut then
                break;
                  if MsgWaitForMultipleObjectsEx(1, LHandle,
                    ATimeOut - LStop.ElapsedMilliseconds, QS_ALLINPUT, 0)
                    = WAIT_OBJECT_0 + 1 then
                    application.ProcessMessages;
            finally
            end;
          end;
          if task.Value.IsComplete then
          begin
            if task.Value.HasExceptions then
              Exceptions := true
            else if task.Value.IsCanceled then
              Canceled := true;
          end;
        finally
          task.Value.removeCompleteEvent(ProcCompleted);

        end;
      end;
    finally
      FEvent.Free;
    end;
  except
    result := false;
  end;

  if (not Exceptions and not Canceled) then
    Exit;
  if Exceptions or Canceled then
    raise EOperationCancelled.Create
      ('One Or More Tasks HasExceptions/Canceled');

end;

end.
