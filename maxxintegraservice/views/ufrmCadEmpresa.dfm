inherited frmCadEmpresa: TfrmCadEmpresa
  Caption = 'frmCadEmpresa'
  ClientHeight = 373
  ClientWidth = 792
  ExplicitWidth = 792
  ExplicitHeight = 373
  PixelsPerInch = 96
  TextHeight = 13
  object Label18: TLabel [0]
    Left = 121
    Top = 182
    Width = 45
    Height = 13
    Caption = 'Endere'#231'o'
  end
  inherited pnlBordas: TRzPanel
    Width = 792
    Height = 373
    ExplicitWidth = 792
    ExplicitHeight = 373
    inherited Panel4: TPanel
      Width = 788
      Caption = 'Hefesto Server - Cadastro de empresa'
      ExplicitWidth = 788
    end
    inherited RzPanel3: TRzPanel
      Top = 331
      Width = 788
      ExplicitTop = 331
      ExplicitWidth = 788
      inherited btnCancelar: TRzRapidFireButton
        Left = 633
        ExplicitLeft = 633
      end
      inherited btnSalvar: TRzBitBtn
        Left = 715
        ExplicitLeft = 715
      end
    end
    inherited RzPanel1: TRzPanel
      Width = 788
      Height = 304
      ExplicitWidth = 788
      ExplicitHeight = 304
      object Label4: TLabel [0]
        Left = 7
        Top = 54
        Width = 67
        Height = 13
        Caption = 'Cod. Empresa'
        FocusControl = mseCODEMP
      end
      object Label5: TLabel [1]
        Left = 113
        Top = 54
        Width = 60
        Height = 13
        Caption = 'Raz'#227'o Social'
        FocusControl = mseRAZAO
      end
      object Label6: TLabel [2]
        Left = 335
        Top = 54
        Width = 41
        Height = 13
        Caption = 'Fantasia'
        FocusControl = mseFANTASIA
      end
      object Label7: TLabel [3]
        Left = 7
        Top = 94
        Width = 20
        Height = 13
        Caption = 'Tipo'
      end
      object Label8: TLabel [4]
        Left = 113
        Top = 93
        Width = 54
        Height = 13
        Caption = 'CPF / CNPJ'
        FocusControl = mseCPFCNPJ
      end
      object Label9: TLabel [5]
        Left = 335
        Top = 93
        Width = 87
        Height = 13
        Caption = 'Inscri'#231#227'o Estadual'
        FocusControl = mseIE
      end
      object Label10: TLabel [6]
        Left = 113
        Top = 174
        Width = 45
        Height = 13
        Caption = 'Endere'#231'o'
        FocusControl = mseLOGRADOURO
      end
      object Label11: TLabel [7]
        Left = 7
        Top = 214
        Width = 65
        Height = 13
        Caption = 'Complemento'
        FocusControl = mseCOMPLEMENTO
      end
      object Label12: TLabel [8]
        Left = 229
        Top = 214
        Width = 37
        Height = 13
        Caption = 'N'#250'mero'
        FocusControl = mseNUMERO
      end
      object Label13: TLabel [9]
        Left = 335
        Top = 214
        Width = 28
        Height = 13
        Caption = 'Bairro'
        FocusControl = mseBAIRRO
      end
      object Label14: TLabel [10]
        Left = 7
        Top = 174
        Width = 19
        Height = 13
        Caption = 'CEP'
        FocusControl = mseCEP
      end
      object Label15: TLabel [11]
        Left = 7
        Top = 254
        Width = 33
        Height = 13
        Caption = 'Cidade'
        FocusControl = mseCIDADE
      end
      object Label16: TLabel [12]
        Left = 229
        Top = 254
        Width = 33
        Height = 13
        Caption = 'Estado'
        FocusControl = mseESTADO
      end
      object Label17: TLabel [13]
        Left = 7
        Top = 134
        Width = 28
        Height = 13
        Caption = 'E-mail'
        FocusControl = mseEMAIL
      end
      object Label19: TLabel [14]
        Left = 335
        Top = 134
        Width = 42
        Height = 13
        Caption = 'Telefone'
        FocusControl = mseTELEFONE
      end
      inherited RzPanel2: TRzPanel
        Width = 782
        ExplicitWidth = 782
        inherited Label2: TLabel
          Left = 611
          ExplicitLeft = 611
        end
        inherited Label3: TLabel
          Left = 444
          ExplicitLeft = 444
        end
        inherited mseDTCRIACAO: TRzDBEdit
          Left = 444
          DataSource = frmPrincipal.dtsEmpresa
          DataField = 'DTCRIACAO'
          ExplicitLeft = 444
        end
        inherited mseDUMANUT: TRzDBEdit
          Left = 611
          DataSource = frmPrincipal.dtsEmpresa
          DataField = 'DUMANUT'
          ExplicitLeft = 611
        end
        inherited mseID: TRzDBEdit
          DataSource = frmPrincipal.dtsEmpresa
          DataField = 'id'
        end
      end
      object mseCODEMP: TRzDBButtonEdit
        Left = 7
        Top = 68
        Width = 100
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'CODEMP'
        TabOrder = 1
        AltBtnWidth = 15
        ButtonWidth = 15
        OnButtonClick = mseCODEMPButtonClick
      end
      object mseRAZAO: TRzDBEdit
        Left = 113
        Top = 68
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'RAZAO'
        TabOrder = 2
      end
      object mseFANTASIA: TRzDBEdit
        Left = 335
        Top = 68
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'FANTASIA'
        TabOrder = 3
      end
      object cbbTIPO: TRzDBComboBox
        Left = 7
        Top = 107
        Width = 100
        Height = 21
        DataField = 'TIPO'
        DataSource = frmPrincipal.dtsEmpresa
        Style = csDropDownList
        TabOrder = 4
        OnExit = cbbTIPOExit
        Items.Strings = (
          'FISICA'
          'JURIDICA')
        Values.Strings = (
          'FISICA'
          'JURIDICA')
      end
      object mseCPFCNPJ: TRzDBEdit
        Left = 113
        Top = 107
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'CPFCNPJ'
        TabOrder = 5
      end
      object mseIE: TRzDBEdit
        Left = 335
        Top = 107
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'IE'
        TabOrder = 6
      end
      object mseLOGRADOURO: TRzDBEdit
        Left = 113
        Top = 188
        Width = 438
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'LOGRADOURO'
        TabOrder = 10
      end
      object mseCOMPLEMENTO: TRzDBEdit
        Left = 7
        Top = 228
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'COMPLEMENTO'
        TabOrder = 11
      end
      object mseNUMERO: TRzDBEdit
        Left = 229
        Top = 228
        Width = 100
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'NUMERO'
        TabOrder = 12
      end
      object mseBAIRRO: TRzDBEdit
        Left = 335
        Top = 228
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'BAIRRO'
        TabOrder = 13
      end
      object mseCEP: TRzDBButtonEdit
        Left = 7
        Top = 188
        Width = 100
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'CEP'
        TabOrder = 9
        AltBtnWidth = 15
        ButtonWidth = 15
        OnButtonClick = mseCEPButtonClick
      end
      object mseCIDADE: TRzDBEdit
        Left = 7
        Top = 268
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'CIDADE'
        TabOrder = 14
      end
      object mseESTADO: TRzDBEdit
        Left = 229
        Top = 268
        Width = 100
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'ESTADO'
        TabOrder = 15
      end
      object GroupBox1: TGroupBox
        Left = 560
        Top = 54
        Width = 225
        Height = 129
        Anchors = [akTop, akRight]
        Caption = 'Logomarca'
        TabOrder = 16
        object sSpeedButton1: TsSpeedButton
          Left = 4
          Top = 18
          Width = 34
          Height = 34
          Flat = True
          OnClick = sSpeedButton1Click
          Images = sAlphaImageList1
          ImageIndex = 0
        end
        object sSpeedButton2: TsSpeedButton
          Left = 4
          Top = 92
          Width = 34
          Height = 34
          Flat = True
          OnClick = sSpeedButton2Click
          Images = sAlphaImageList1
          ImageIndex = 1
        end
        object imgLOGOMARCA: TcxDBImage
          AlignWithMargins = True
          Left = 41
          Top = 18
          Align = alRight
          DataBinding.DataField = 'LOGOMARCA'
          DataBinding.DataSource = frmPrincipal.dtsEmpresa
          Properties.FitMode = ifmProportionalStretch
          Properties.GraphicClassName = 'TdxPNGImage'
          Properties.GraphicTransparency = gtTransparent
          TabOrder = 0
          Transparent = True
          Height = 106
          Width = 179
        end
      end
      object mseEMAIL: TRzDBEdit
        Left = 7
        Top = 148
        Width = 322
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'EMAIL'
        TabOrder = 7
      end
      object mseTELEFONE: TRzDBEdit
        Left = 335
        Top = 148
        Width = 216
        Height = 21
        DataSource = frmPrincipal.dtsEmpresa
        DataField = 'TELEFONE'
        TabOrder = 8
      end
    end
  end
  object sAlphaImageList1: TsAlphaImageList
    DrawingStyle = dsTransparent
    Height = 30
    Width = 30
    Items = <
      item
        ImageFormat = ifPNG
        ImageName = 'camera'
        ImgData = {
          89504E470D0A1A0A0000000D494844520000001E0000001E08060000003B30AE
          A20000000467414D410000B18F0BFC6105000000017352474200AECE1CE90000
          00206348524D00007A26000080840000FA00000080E8000075300000EA600000
          3A98000017709CBA513C00000006624B4744000000000000F943BB7F00000009
          7048597300000048000000480046C96B3E000001A64944415448C7ED94C16A13
          511486BF731385D46CECCA447D0129E32608DDE9BA7905759707286452DAD2B4
          090D4C02756BD67982E2C26DF7D645AAF8022D49565936149C7B5C3405496EEA
          9D32EEE68781E1FCE7FEDF9C33CC40A64CFF4992A4B91C0E0F1569BA3C8568D2
          0D765207DF077D08DC097ED6F8B125681FE579928D3874A5426D12055F170DE3
          7C1AD5CF2940015E88D2771966D58114A0777A9904BC5ACAA92A6FADD5A2B55A
          5423EF54E54BD2987C9266819D512F8816CA67C059A93EDC45E4387DB0723AEA
          05D1ABE6CFC7D36BDB16E1BD820283F58239F875B4D12987C34D45AA3E71DEAB
          56F804309DD99608215012280B34A633DB02B0E44E7CF3BCC1F19AF93E5FF787
          454FE023C06FF2E7A983FF9A5C978A420C606739EF1F92373877A395F9ED60C9
          B4B7B5474FAE2BBE79FE13C76C03AC17CC8142A4309A5FD1D335D3043056B67D
          E39CAB298517EAEC56DD1BF75E779C67EA17FB086D9737EE064B9C44DF3122C7
          E570B869C99DC405BE01989BF8CD7CD2AD2451C9C0802255C156F3B37B97F64F
          AD7AC7570F4A73EBD21BAC422D25F825AAB51487C89429D3B2FE00E2EC86DD49
          D383D10000002574455874646174653A63726561746500323031382D30352D32
          385431373A31313A34362B30303A3030733593ED000000257445587464617465
          3A6D6F6469667900323031382D30352D32385431373A31313A34362B30303A30
          3002682B5100000028744558747376673A626173652D7572690066696C653A2F
          2F2F746D702F6D616769636B2D4A6A456E364257332B2DCCA40000000049454E
          44AE426082}
      end
      item
        ImageFormat = ifPNG
        ImageName = 'delete'
        ImgData = {
          89504E470D0A1A0A0000000D494844520000001E0000001E08060000003B30AE
          A20000000467414D410000B18F0BFC6105000000017352474200AECE1CE90000
          00206348524D00007A26000080840000FA00000080E8000075300000EA600000
          3A98000017709CBA513C00000006624B4744000000000000F943BB7F00000009
          7048597300000048000000480046C96B3E000000734944415448C7ED91C10D80
          20100457E9C3879DD80C1D5C688184A2ECC48750C7E9DF87022131909D2F979B
          DC0010321A53C9B0736E51D51DC0FA783A8D319BF7FE68261691ABE6A210C2EB
          EEB93E56A764FD7169EEAFCCC08FA929A698628AC715A7829DB19958556DA63C
          02B04DD310D20D37FBBB187419050A0A0000002574455874646174653A637265
          61746500323031382D30352D32385431373A31323A34332B30303A3030CA3A07
          490000002574455874646174653A6D6F6469667900323031382D30352D323854
          31373A31323A34332B30303A3030BB67BFF500000028744558747376673A6261
          73652D7572690066696C653A2F2F2F746D702F6D616769636B2D716F6C72716F
          4A4FB48866690000000049454E44AE426082}
      end>
    Left = 688
    Top = 216
    Bitmap = {
      494C010100000800080001000100FFFFFFFF0400FFFFFFFFFFFFFFFF424D7600
      0000000000007600000028000000040000000100000001000400000000000400
      0000000000000000000000000000000000000000000000008000008000000080
      8000800000008000800080800000C0C0C000808080000000FF0000FF000000FF
      FF00FF000000FF00FF00FFFF0000FFFFFF0000000000}
  end
  object odlgImage: TOpenPictureDialog
    Filter = 'Portable network graphics (AlphaControls) (*.png)|*.png'
    Left = 618
    Top = 243
  end
end
