object frmMasterCad: TfrmMasterCad
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'frmMasterCad'
  ClientHeight = 338
  ClientWidth = 565
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBordas: TRzPanel
    Left = 0
    Top = 0
    Width = 565
    Height = 338
    Align = alClient
    BorderOuter = fsFlatBold
    TabOrder = 0
    object Panel4: TPanel
      Left = 2
      Top = 2
      Width = 561
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Hefesto Server'
      Color = 6700580
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      OnMouseDown = Panel4MouseDown
    end
    object RzPanel3: TRzPanel
      Left = 2
      Top = 296
      Width = 561
      Height = 40
      Align = alBottom
      BorderOuter = fsNone
      Color = 6700580
      Padding.Bottom = 3
      TabOrder = 1
      object btnCancelar: TRzRapidFireButton
        AlignWithMargins = True
        Left = 406
        Top = 3
        Width = 76
        Height = 31
        Align = alRight
        Caption = 'Cancelar'
        OnClick = btnCancelarClick
        ExplicitLeft = 568
        ExplicitHeight = 29
      end
      object btnSalvar: TRzBitBtn
        AlignWithMargins = True
        Left = 488
        Top = 3
        Width = 70
        Height = 31
        HelpType = htKeyword
        HelpKeyword = 'Padrao'
        Align = alRight
        Caption = '&Salvar'
        TabOrder = 0
        OnClick = btnSalvarClick
      end
    end
    object RzPanel1: TRzPanel
      Left = 2
      Top = 27
      Width = 561
      Height = 269
      Align = alClient
      BorderOuter = fsNone
      Padding.Right = 3
      TabOrder = 2
      object RzPanel2: TRzPanel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 555
        Height = 45
        Margins.Right = 0
        Align = alTop
        BorderOuter = fsFlat
        Padding.Right = 3
        TabOrder = 0
        DesignSize = (
          555
          45)
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 33
          Height = 13
          Caption = 'C'#243'digo'
          FocusControl = mseID
        end
        object Label2: TLabel
          Left = 384
          Top = 4
          Width = 82
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Data Modifica'#231#227'o'
          ExplicitLeft = 510
        end
        object Label3: TLabel
          Left = 217
          Top = 4
          Width = 70
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Data Cadastro'
          ExplicitLeft = 343
        end
        object mseDTCRIACAO: TRzDBEdit
          Left = 217
          Top = 18
          Width = 162
          Height = 21
          TabStop = False
          Anchors = [akTop, akRight]
          Enabled = False
          TabOrder = 1
        end
        object mseDUMANUT: TRzDBEdit
          Left = 384
          Top = 18
          Width = 162
          Height = 21
          TabStop = False
          Anchors = [akTop, akRight]
          Enabled = False
          TabOrder = 2
        end
        object mseID: TRzDBEdit
          Left = 4
          Top = 18
          Width = 121
          Height = 21
          TabStop = False
          Enabled = False
          TabOrder = 0
        end
      end
    end
  end
  object btnPesquisaImagem: TBitBtn
    Left = 240
    Top = 1000
    Width = 201
    Height = 25
    Caption = 'btnPesquisaImagem'
    Enabled = False
    Glyph.Data = {
      96090000424D9609000000000000360000002800000028000000140000000100
      18000000000060090000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFAFAFAA5A5A5C5C5C5FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFDFDFDCACACADDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFBFBFB9F9F9F606060A5A5A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE
      C5C5C59B9B9BCACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFB9E9E9E6464649F
      9F9FFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDC5C5C59D9D9DC5C5C5FCFC
      FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6D6
      D6D6D7D7D7E9E9E9FFFFFFFDFDFD9E9E9E6060609F9F9FFBFBFBFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0E9E9E9EAEA
      EAF2F2F2FFFFFFFEFEFEC4C4C49B9B9BC5C5C5FEFEFEFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E8A6A6A66F6F6F505050535353757575
      B7B7B7B6B6B65454549F9F9FFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFF2F2F2C7C7C7A4A4A4959595979797A7A7A7D0D0D0D4
      D4D4939393C5C5C5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFE4E4E4858585606060A3A3A3D1D1D1CECECE989898606060757575B5B5
      B5FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
      F0F0B3B3B39D9D9DC8C8C8E0E0E0DFDFDFC2C2C29E9E9EAAAAAAD4D4D4FEFEFE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE9D9D9D6464
      64CCCCCCFDFDFDFFFFFFFFFFFFFAFAFABFBFBF5F5F5FB7B7B7FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC2C2C2A1A1A1E4E4E4
      FFFFFFFFFFFFFFFFFFFEFEFEDCDCDC9E9E9ECFCFCFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDD707070A8A8A8FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFBFBFB999999717171E6E6E6FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFEBEBEBA4A4A4CCCCCCFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFEFEFEC1C1C1A8A8A8F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFC4C4C4565656DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFCDCDCD545454D8D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFDFDFDF979797EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF
      DFDF979797EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      C3C3C3545454E3E3E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1D1D15151
      51D6D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDE96
      9696ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1969696E9E9E9
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D76C6C6CB2B2
      B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDA3A3A36F6F6FE6E6E6FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E8A1A1A1D2D2D2FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C8A5A5A5F1F1F1FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC969696696969DADADAFFFFFFFF
      FFFFFFFFFFFEFEFECDCDCD5F5F5FA6A6A6FEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFDFDFDBDBDBDA5A5A5ECECECFFFFFFFFFFFFFFFF
      FFFFFFFFE4E4E49E9E9EC7C7C7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFDBDBDB7C7C7C696969B3B3B3E3E3E3DFDFDFA8A8A8
      656565868686E6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFEAEAEAADADADA4A4A4D2D2D2ECECECEAEAEACCCCCCA1A1A1B3
      B3B3F1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFDCDCDC9696966B6B6B5454545656567070709C9C9CE6E6E6FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFEBEBEBBDBDBDA2A2A2969696979797A4A4A4C2C2C2F1F1F1FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFDFDFDD7D7D7C2C2C2C4C4C4DCDCDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      E8E8E8DEDEDEDFDFDFEBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    NumGlyphs = 2
    TabOrder = 1
    TabStop = False
    Visible = False
  end
end
