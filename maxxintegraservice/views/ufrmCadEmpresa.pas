unit ufrmCadEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmMasterCad, RzDBBnEd, RzDBEdit, Vcl.StdCtrls, Vcl.Mask, RzEdit, RzButton,
  RzSpnEdt, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls, RzPanel, RzCmboBx, RzDBCmbo, JSON, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxImage, cxDBEdit, System.ImageList, Vcl.ImgList,
  acAlphaImageList, Vcl.ExtDlgs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TfrmCadEmpresa = class(TfrmMasterCad)
    Label4: TLabel;
    mseCODEMP: TRzDBButtonEdit;
    mseRAZAO: TRzDBEdit;
    Label5: TLabel;
    mseFANTASIA: TRzDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    cbbTIPO: TRzDBComboBox;
    Label8: TLabel;
    mseCPFCNPJ: TRzDBEdit;
    Label9: TLabel;
    mseIE: TRzDBEdit;
    Label10: TLabel;
    mseLOGRADOURO: TRzDBEdit;
    Label11: TLabel;
    mseCOMPLEMENTO: TRzDBEdit;
    Label12: TLabel;
    mseNUMERO: TRzDBEdit;
    Label13: TLabel;
    mseBAIRRO: TRzDBEdit;
    Label14: TLabel;
    mseCEP: TRzDBButtonEdit;
    Label15: TLabel;
    mseCIDADE: TRzDBEdit;
    Label16: TLabel;
    mseESTADO: TRzDBEdit;
    GroupBox1: TGroupBox;
    imgLOGOMARCA: TcxDBImage;
    sAlphaImageList1: TsAlphaImageList;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    Label17: TLabel;
    mseEMAIL: TRzDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    mseTELEFONE: TRzDBEdit;
    odlgImage: TOpenPictureDialog;
    procedure mseCEPButtonClick(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure mseCODEMPButtonClick(Sender: TObject);
    procedure cbbTIPOExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadEmpresa: TfrmCadEmpresa;

implementation

{$R *.dfm}

uses uMaxxREST, ufrmPrincipal, uPesquisar;

procedure TfrmCadEmpresa.cbbTIPOExit(Sender: TObject);
begin
  inherited;
  if frmPrincipal.FDEmpresaTIPO.Text = 'FISICA' then
    frmPrincipal.FDEmpresaCPFCNPJ.EditMask := '000\.000\.000\-00;0;_'
  else
    frmPrincipal.FDEmpresaCPFCNPJ.EditMask := '00\.000\.000\/0000\-00;0;_';
end;

procedure TfrmCadEmpresa.mseCEPButtonClick(Sender: TObject);
//var
//  MaxxREST: TMaxxRESTCliente;
//  JsonCEP : TJSONObject;
//  I: Integer;
begin
  inherited;
//  if mseCEP.Text <> '' then
//  begin
//    try
//      MaxxREST := TMaxxRESTCliente.GetInstance;
//      JsonCEP  := TJSONObject.Create;
//
//
//
//      JsonCEP  := (MaxxREST.BuscarCEP(StringReplace(mseCEP.Text, '-', '', [rfReplaceAll, rfIgnoreCase])) as TJSONObject);
//
//      mseLOGRADOURO.Field.AsString := JsonCEP.GetValue('logradouro').Value;
//      mseBAIRRO.Field.AsString     := JsonCEP.GetValue('bairro').Value;
//      mseCIDADE.Field.AsString     := TJSONObject(JsonCEP.GetValue('cidade')).GetValue('nome').Value;
//      mseESTADO.Field.AsString     := TJSONObject(JsonCEP.GetValue('estado')).GetValue('sigla').Value;
//    finally
//      FreeAndNil(JsonCEP);
//    end;
//  end;
end;

procedure TfrmCadEmpresa.mseCODEMPButtonClick(Sender: TObject);
var
  Pesquisa : TPesquisa;
begin
  inherited;
  Pesquisa                  := TPesquisa.Create(
  ['C�digo',
   'Raz�o',
   'Fantasia',
   'CNPJ',
   'I.E',
   'Logradouro',
   'Complemento',
   'CEP',
   'N�mero',
   'Bairro',
   'Munic�pio',
   'UF',
   'E-mail',
   'Logomarca',
   'Telefone'],
  [frmPrincipal.FDEmpresaCODEMP,
   frmPrincipal.FDEmpresaRAZAO,
   frmPrincipal.FDEmpresaFANTASIA,
   frmPrincipal.FDEmpresaCPFCNPJ,
   frmPrincipal.FDEmpresaIE,
   frmPrincipal.FDEmpresaLOGRADOURO,
   frmPrincipal.FDEmpresaCOMPLEMENTO,
   frmPrincipal.FDEmpresaCEP,
   frmPrincipal.FDEmpresaNUMERO,
   frmPrincipal.FDEmpresaBAIRRO,
   frmPrincipal.FDEmpresaCIDADE,
   frmPrincipal.FDEmpresaESTADO,
   frmPrincipal.FDEmpresaEMAIL,
   frmPrincipal.FDEmpresaLOGOMARCA,
   frmPrincipal.FDEmpresaTELEFONE]);

  Pesquisa.TipoPesquisa     := 'EMPRESA';
  Pesquisa.ComponentOrigem  := mseCODEMP;

  Pesquisa.Pesquisar;


end;

procedure TfrmCadEmpresa.sSpeedButton1Click(Sender: TObject);
begin
  inherited;
  if odlgImage.Execute then
    frmPrincipal.FDEmpresaLOGOMARCA.LoadFromFile(odlgImage.FileName);
end;

procedure TfrmCadEmpresa.sSpeedButton2Click(Sender: TObject);
begin
  inherited;
  frmPrincipal.FDEmpresaLOGOMARCA.Clear;
end;

end.
