unit ufrmMasterPes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RzButton, RzSpnEdt, RzPanel, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.StdCtrls, Vcl.Mask, RzEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TfrmMasterPes = class(TForm)
    RzPanel1: TRzPanel;
    cxGridPesquisaDBTableView1: TcxGridDBTableView;
    cxGridPesquisaLevel1: TcxGridLevel;
    cxGridPesquisa: TcxGrid;
    Label1: TLabel;
    msePESQUISA: TRzMaskEdit;
    btnSalvar: TRzBitBtn;
    btnCancelar: TRzRapidFireButton;
    FDPesquisa: TFDQuery;
    dtsPesquisa: TDataSource;
    StyleGrid: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    procedure FormShow(Sender: TObject);
    procedure msePESQUISAKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure msePESQUISAKeyPress(Sender: TObject; var Key: Char);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure cxGridPesquisaDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGridPesquisaDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMasterPes: TfrmMasterPes;

implementation

{$R *.dfm}

uses uLayoutFormulario;

procedure TfrmMasterPes.btnCancelarClick(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;

procedure TfrmMasterPes.btnSalvarClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOk;
end;

procedure TfrmMasterPes.cxGridPesquisaDBTableView1DblClick(Sender: TObject);
begin
  btnSalvar.Click;
end;

procedure TfrmMasterPes.cxGridPesquisaDBTableView1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnSalvar.Click
  else if Key = #27 then
    btnCancelar.Click;
end;

procedure TfrmMasterPes.FormShow(Sender: TObject);
begin
  PadronizarBotao(btnSalvar);
  PadronizarBotao(btnCancelar);
  msePESQUISA.SetFocus;
end;

procedure TfrmMasterPes.msePESQUISAKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    cxGridPesquisa.SetFocus
  else if Key = #27 then
    btnCancelar.Click;
end;

procedure TfrmMasterPes.msePESQUISAKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  AItemList: TcxFilterCriteriaItemList;
  I: Integer;
begin
  inherited;
  with cxGridPesquisaDBTableView1.DataController.Filter do
  begin
    if msePESQUISA.Text <> '' then
    begin
      Active := False;
      BeginUpdate;
      try
        Root.Clear;
        Root.BoolOperatorKind := fboOr;

        Options   := Options + [fcoCaseInsensitive];
        AItemList := Root.AddItemList(fboOr);

        for I := 0 to cxGridPesquisaDBTableView1.ColumnCount -1 do
          AItemList.AddItem(cxGridPesquisaDBTableView1.Columns[I], foLike, '%' + msePESQUISA.Text + '%', msePESQUISA.Text);
      finally
        EndUpdate;
        Active := True;
      end;
    end
    else
    begin
      Active := False;
      BeginUpdate;
      Root.Clear;
      EndUpdate;
    end;
  end;
end;

end.
