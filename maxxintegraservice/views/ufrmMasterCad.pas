unit ufrmMasterCad;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, sSpeedButton, Vcl.ExtCtrls, RzPanel, RzSpnEdt, RzButton,
  Vcl.StdCtrls, Vcl.Mask, RzEdit, RzDBEdit, RzDBBnEd;

type
  TfrmMasterCad = class(TForm)
    pnlBordas: TRzPanel;
    Panel4: TPanel;
    RzPanel3: TRzPanel;
    btnCancelar: TRzRapidFireButton;
    btnSalvar: TRzBitBtn;
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    mseDTCRIACAO: TRzDBEdit;
    mseDUMANUT: TRzDBEdit;
    btnPesquisaImagem: TBitBtn;
    mseID: TRzDBEdit;
    procedure Panel4MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMasterCad: TfrmMasterCad;

implementation

{$R *.dfm}

uses uLayoutFormulario;

procedure TfrmMasterCad.btnSalvarClick(Sender: TObject);
begin
  Close;
  ModalResult := mrOk;
end;

procedure TfrmMasterCad.FormKeyPress(Sender: TObject; var Key: Char);
begin
  try
    if Self.ActiveControl <> nil then
    begin
      if (Key = #13) and
         (Self.ActiveControl.ClassType <> TRzDBMemo) and
         (Self.ActiveControl.ClassType <> TMemo) and
         (not(Self.ActiveControl is TCustomMemo)) and
         (TEdit(Self.ActiveControl).Name <> 'mseCODIGO') then
      begin
         Perform(CM_DialogKey, VK_TAB, 0);
         Key := #0;
      end;
    end;

    if Key = #27 then
      btnCancelar.Click;
  except
  end;
end;

procedure TfrmMasterCad.FormShow(Sender: TObject);
begin
  PadronizarTela(Self);
end;

procedure TfrmMasterCad.Panel4MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  if Button = mbleft then
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

procedure TfrmMasterCad.btnCancelarClick(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;

end.
