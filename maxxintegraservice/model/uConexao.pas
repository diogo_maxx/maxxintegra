unit uConexao;

interface

uses IniFiles, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, IWSystem, RTTI, TypInfo, uLog, uException;

type
   TConexao = class
     strict protected
     class var Conexao : TConexao; //singleton
     protected
       FConSQLite: TFDConnection;
       FConBDCli: TFDConnection;
       FConfBDCli: TFDQuery;

       constructor CreatePrivate;
       destructor Destroy; override;

     public
       class function GetInstance: TConexao;
       function SQLite: TFDConnection;
       function BDCli: TFDConnection;
       function Conectar(SQLite: Boolean; BDCliente: Boolean): Boolean;
   end;

implementation

{ TConexao }

uses ufrmPrincipal;

function TConexao.BDCli: TFDConnection;
begin
  Result := FConBDCli;
end;

function TConexao.Conectar(SQLite: Boolean; BDCliente: Boolean): Boolean;
begin
  try
    if SQLite then
    begin
      if not FConSQLite.Connected then
      begin
        FConSQLite.Connected              := False;
        FConSQLite.LoginPrompt            := False;
        FConSQLite.UpdateOptions.LockWait := False;

        FConSQLite.Params.Clear;
        FConSQLite.Params.Add('DriverID=SQLite');
        FConSQLite.Params.Add('MetaCaseInsCat=True');
        FConSQLite.Params.Add('Database=' + StringReplace(UpperCase(gsAppPath), '\EXE\', '\BD\', []) + 'maxxintegra.db3');

        FConSQLite.Connected := True;
      end;
    end;


    if BDCliente then
    begin
      if FConSQLite.Connected then
      begin
        //verifica se existe a tabela de configura��o para buscar as informa��es de conex�o com o cliente
        with FConfBDCli do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM sqlite_master where tbl_name = ' + QuotedStr('sistemasintegrados'));
          Open;

          if not IsEmpty then
          begin
            Close;
            SQL.Clear;
            SQL.Add('select * from sistemasintegrados');
            Open;
          end
          else
            Close;
        end;
      end;

      if not FConBDCli.Connected then
      begin
        //se a configura��o estiver carregada
        if FConfBDCli.State <> dsInactive then
        begin
          FConBDCli.Connected := False;
          FConBDCli.Params.Clear;

          case AnsiIndexStr(FConfBDCli.FieldByName('sis_driverid').AsString, ['F', 'M', 'O', 'S']) of
            0: //Firebird
              FConBDCli.Params.Add('DriverID=FB');
            1: //MySQL
              FConBDCli.Params.Add('DriverID=MySQL');
            2: //Oracle
              FConBDCli.Params.Add('DriverID=Ora');
            3: //SQLServer
              FConBDCli.Params.Add('DriverID=MSSQL');
          end;

          FConBDCli.Params.Add('MetaCaseInsCat=True');
          FConBDCli.Params.Add('Database=' +  FConfBDCli.FieldByName('sis_database').AsString);
          FConBDCli.Params.Add('User_Name=' + FConfBDCli.FieldByName('sis_username').AsString);
          FConBDCli.Params.Add('Password=' +  FConfBDCli.FieldByName('sis_password').AsString);
          FConBDCli.Params.Add('CharacterSet=WE8MSWIN1252');
          if FConfBDCli.FieldByName('sis_server').AsString <> '' then FConBDCli.Params.Add('SERVER='        + FConfBDCli.FieldByName('sis_server').AsString);
          if FConfBDCli.FieldByName('sis_port').AsString <> '' then   FConBDCli.Params.Add('Port='          + FConfBDCli.FieldByName('sis_port').AsString);
          if FConfBDCli.FieldByName('sis_schema').AsString <> '' then FConBDCli.Params.Add('MetaCurSchema=' + FConfBDCli.FieldByName('sis_schema').AsString);

          FConBDCli.Connected := True;

          frmPrincipal.sSlider2.SliderOn := FConBDCli.Connected;

        end;
      end;
    end;
  except
    on E: Exception do
    begin
      SalvarLog('Conex�o: ' + E.Message);

      FConBDCli.Connected            := False;
      frmPrincipal.sSlider2.SliderOn := FConBDCli.Connected;
    end;
  end;
end;

constructor TConexao.CreatePrivate;
begin
  inherited Create;

  FConSQLite := TFDConnection.Create(nil);
  FConBDCli  := TFDConnection.Create(nil);
  FConfBDCli := TFDQuery.Create(nil);

  FConfBDCli.Connection := FConSQLite;
end;

destructor TConexao.Destroy;
begin
  inherited Destroy;

  FConSQLite.Connected := False;
  FConBDCli.Connected  := False;

  FreeAndNil(FConSQLite);
  FreeAndNil(FConBDCli);
end;

class function TConexao.GetInstance: TConexao;
begin
  if not Assigned(Conexao) then
    Conexao := TConexao.CreatePrivate;

  Result := Conexao;
end;

function TConexao.SQLite: TFDConnection;
begin
  Result := FConSQLite;
end;

end.
