unit uCriarTabelas;

interface

uses FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, RzButton, Vcl.Buttons, sSpeedButton, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, StrUtils, Json, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, IniFiles, FireDAC.Phys.FB, FireDAC.Phys.FBDef, SysUtils, System.Classes, uJSONToDataset,
  FireDAC.Comp.ScriptCommands, FireDAC.Stan.Util, FireDAC.Comp.Script, uConexao;

type
  TCriarTabelas = class
   private
    FQuery     : TFDQuery;
    FJSONTabela: TJSONObject;
    FJsonToDataSet: TJSONToDataSet;
    FTabelaPriUso: Boolean;
    Conexao       : TConexao;
    procedure SetJSONTabela(const Value: TJSONObject);
    function PrepareDDLTrigger(FDQry: TFDQuery; Trigger, Tipo, BD: String): String;
    procedure SetTabelaPriUso(const Value: Boolean);
   public
    constructor Create; overload;
    destructor Destroy; override;
    property JSONTabela: TJSONObject read FJSONTabela write SetJSONTabela;
    property TabelaPriUso: Boolean read FTabelaPriUso write SetTabelaPriUso;
    function CriarTabelas(str: TStrings): Boolean;
    function CriarTriggers(str: TStrings): Boolean;
    function CriarTabelaIntegracao: Boolean;
    function CriarTabelaEmpresaFilial: Boolean;
end;

implementation

{ TCriarTabelas }

uses uLog, ufrmPrincipal, uException;

constructor TCriarTabelas.Create;
begin
  inherited Create;
  FQuery         := TFDQuery.Create(nil);
  FJSONTabela    := TJSONObject.Create;
  FJsonToDataSet := TJSONToDataSet.Create;

  Conexao        := TConexao.GetInstance;
end;

function TCriarTabelas.CriarTabelaEmpresaFilial: Boolean;
var
  FDExec: TFDQuery;
  Empresa, Filial: Boolean;
begin
  try
    Result := True;

    FDExec := TFDQuery.Create(nil);

    try
      FDExec.Connection := Conexao.SQLite;
      ///////////////////////////////////////
      ///  verificando se a tabela existe ///
      ///////////////////////////////////////
      FDExec.Close;
      FDExec.SQL.Clear;
      FDExec.SQL.Add('SELECT * FROM sqlite_master where tbl_name = :TABELA');
      FDExec.Params[0].AsString := 'EMPRESA';
      FDExec.Open;

      Empresa := FDExec.IsEmpty;

      FDExec.Close;
      FDExec.Params[0].AsString := 'FILIAL';
      FDExec.Open;

      Filial := FDExec.IsEmpty;
      //////////////////////////////////////
      ///  Criando tabelas               ///
      //////////////////////////////////////
      if Empresa then
      begin
        FDExec.Close;
        FDExec.SQL.Clear;
        FDExec.SQL.Add(
            'CREATE TABLE [EMPRESA]( ' +
            '  [id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ' +
            '  [CODEMP] VARCHAR(100) NOT NULL, ' +
            '  [RAZAO] VARCHAR(100), ' +
            '  [FANTASIA] VARCHAR(100), ' +
            '  [TIPO] VARCHAR(20), ' +
            '  [CPFCNPJ] VARCHAR(20), ' +
            '  [IE] VARCHAR(50), ' +
            '  [EMAIL] VARCHAR(100), ' +
            '  [TELEFONE] VARCHAR(20), ' +
            '  [CEP] VARCHAR(9), ' +
            '  [LOGRADOURO] VARCHAR(100), ' +
            '  [COMPLEMENTO] VARCHAR(100), ' +
            '  [NUMERO] VARCHAR(20), ' +
            '  [BAIRRO] VARCHAR(100), ' +
            '  [CIDADE] VARCHAR(100), ' +
            '  [ESTADO] VARCHAR(20), ' +
            '  [DTCRIACAO] TIMESTAMP, ' +
            '  [DUMANUT] TIMESTAMP, ' +
            '  [LOGOMARCA] IMAGE); ');
        FDExec.ExecSQL;
      end;

      if Filial then
      begin
        FDExec.Close;
        FDExec.SQL.Clear;
        FDExec.SQL.Add(
            'CREATE TABLE [FILIAL]( ' +
            '  [id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ' +
            '  [IDEMPRESA] INTEGER NOT NULL, ' +
            '  [CODFILIAL] VARCHAR(100) NOT NULL, ' +
            '  [RAZAO] VARCHAR(100), ' +
            '  [FANTASIA] VARCHAR(100), ' +
            '  [TIPO] VARCHAR(20), ' +
            '  [CPFCNPJ] VARCHAR(20), ' +
            '  [IE] VARCHAR(50), ' +
            '  [EMAIL] VARCHAR(100), ' +
            '  [TELEFONE] VARCHAR(20), ' +
            '  [CEP] VARCHAR(9), ' +
            '  [LOGRADOURO] VARCHAR(100), ' +
            '  [COMPLEMENTO] VARCHAR(100), ' +
            '  [NUMERO] VARCHAR(20), ' +
            '  [BAIRRO] VARCHAR(100), ' +
            '  [CIDADE] VARCHAR(100), ' +
            '  [ESTADO] VARCHAR(20), ' +
            '  [DTCRIACAO] TIMESTAMP, ' +
            '  [DUMANUT] TIMESTAMP, ' +
            '  [LOGOMARCA] IMAGE); ');
        FDExec.ExecSQL;
      end;
    except
      on E: Exception do
      begin
        SalvarLog('CriarTabelaEmpresaFilial: ' + E.Message);
        Result := False;
      end;
    end;
  finally
    FDExec.Close;
    FreeAndNil(FDExec);
  end;
end;

function TCriarTabelas.CriarTabelaIntegracao: Boolean;
var
  _Qry: TFDQuery;
  Tabela, Generator, Trigger: String;

  function CriaTabelaIntegracaoExecutar(SQL: String): Boolean;
  begin
    _Qry.SQL.Clear;
    _Qry.SQL.Add(SQL);

    if _Qry.Macros.Count > 0 then
    begin
      if _Qry.FindMacro('SCHEMA') <> nil then
      begin
        _Qry.MacroByName('SCHEMA').AsRaw := frmPrincipal.PreencheSCHEMA;
        SalvarLog('SCHEMA: ' + _Qry.MacroByName('SCHEMA').AsRaw);
      end;
    end;

    _Qry.ExecSQL;
  end;

begin
  try
    try
      Result := True;

      _Qry           := TFDQuery.Create(nil);

      _Qry.Connection := Conexao.BDCli;
      Generator       := '';
      Trigger         := '';

      _Qry.Close;
      _Qry.SQL.Clear;

      case AnsiIndexStr(Conexao.BDCli.Params.DriverID, ['FB', 'MySQL', 'Ora', 'MSSQL']) of
        0: //firebird
        begin
          _Qry.SQL.Add(
          'select rdb$field_name from RDB$RELATION_FIELDS ' +
          'where rdb$relation_name = ' + QuotedStr('MAXX_INTEGRACAO'));

          Tabela := 'CREATE TABLE MAXX_INTEGRACAO ( ' +
                    '    ID INTEGER NOT NULL PRIMARY KEY, ' +
                    '    TABELA_REG VARCHAR(100), ' +
                    '    TABELA_PAI VARCHAR(100), ' +
                    '    CODIGO_REG VARCHAR(100), ' +
                    '    CODIGO_PAI VARCHAR(100), ' +
                    '    DATA TIMESTAMP, ' +
                    '    TIPO_MOD CHAR(1), ' +
                    '    ENVIADO CHAR(1))';
//          ChavePrimaria := 'ALTER TABLE MAXX_INTEGRACAO ADD CONSTRAINT PK_MAXX_INTEGRACAO PRIMARY KEY (ID)';
          Generator := 'CREATE SEQUENCE GEN_MAXX_INTEGRACAO_ID';

          Trigger := 'create trigger maxx_integracao_bi for maxx_integracao ' +
                     'active before insert position 0 ' +
                     'as ' +
                     'begin ' +
                     '  if (new.id is null) then ' +
                     '    new.id = gen_id(gen_maxx_integracao_id,1);  ' +
                     'end';

        end;
        1: // MySQL
        begin
          _Qry.SQL.Add(
          'SELECT * ' +
          'FROM INFORMATION_SCHEMA.TABLES ' +
          'WHERE TABLE_NAME=' + QuotedStr('MAXX_INTEGRACAO'));

          Tabela := 'CREATE TABLE maxx_integracao ( ' +
                    '  ID INT NOT NULL AUTO_INCREMENT, ' +
                    '  TABELA_REG VARCHAR(100) NULL, ' +
                    '  TABELA_PAI VARCHAR(100) NULL, ' +
                    '  CODIGO_REG VARCHAR(100) NULL, ' +
                    '  CODIGO_PAI VARCHAR(100) NULL, ' +
                    '  DATA TIMESTAMP NULL, ' +
                    '  TIPO_MOD CHAR(1) NULL, ' +
                    '  ENVIADO CHAR(1) NULL, ' +
                    '  PRIMARY KEY (ID))';
        end;
        2: //Ora
        begin
          _Qry.SQL.Add(
          'select * from all_tables where table_name =' + QuotedStr('MAXX_INTEGRACAO'));

          Tabela := 'CREATE TABLE &SCHEMA.maxx_integracao ( ' +
                    '  ID NUMBER NOT NULL, ' +
                    '  TABELA_REG VARCHAR2(100) NULL, ' +
                    '  TABELA_PAI VARCHAR2(100) NULL, ' +
                    '  CODIGO_REG VARCHAR2(100) NULL, ' +
                    '  CODIGO_PAI VARCHAR2(100) NULL, ' +
                    '  DATA DATE NULL, ' +
                    '  TIPO_MOD CHAR(1) NULL, ' +
                    '  ENVIADO CHAR(1) NULL, ' +
                    '  PRIMARY KEY (ID))';

          Generator := 'CREATE SEQUENCE &SCHEMA.SEQMAXX_INTEGRACAO START WITH 1';

          Trigger := 'create trigger tr_maxxintegracao ' +
                     '     before insert on &SCHEMA.maxx_integracao ' +
                     '     for each row ' +
                     'begin ' +
                     '     select &SCHEMA.seqmaxx_integracao.nextval ' +
                     '       into :new.id ' +
                     '       from dual; ' +
                     'end;';

        end;
        3: //SQLServer
        begin
          _Qry.SQL.Add('SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ' + QuotedStr('MAXX_INTEGRACAO'));

          Tabela := 'CREATE TABLE &SCHEMA.MAXX_INTEGRACAO ( ' +
                    '    ID int IDENTITY(1,1) PRIMARY KEY, ' +
                    '    TABELA_REG VARCHAR(100), ' +
                    '    TABELA_PAI VARCHAR(100), ' +
                    '    CODIGO_REG VARCHAR(100), ' +
                    '    CODIGO_PAI VARCHAR(100), ' +
                    '    DATA DATETIME, ' +
                    '    TIPO_MOD CHAR(1), ' +
                    '    ENVIADO CHAR(1))';
        end;
      end;

      _Qry.Open;

      if _Qry.IsEmpty then
      begin
        CriaTabelaIntegracaoExecutar(Tabela);

        if Generator <> '' then
          CriaTabelaIntegracaoExecutar(Generator);
        if Trigger <> '' then
          CriaTabelaIntegracaoExecutar(Trigger);
      end;


    except
      on E: Exception do
      begin
        SalvarLog('CriaTabelaIntegracao: ' + E.Message);
        Result := False;
      end;
    end;
  finally
    FreeAndNil(_Qry);
  end;
end;

function TCriarTabelas.CriarTabelas(str: TStrings): Boolean;
var
  I, J, K: Integer;
  FDExec, FDBusca: TFDQuery;
  DDL, Valores: String;
begin
  try
    try
      Result := True;

      FDExec  := TFDQuery.Create(nil);
      FDBusca := TFDQuery.Create(nil);

      SalvarLog('Iniciando cria��o das tabelas...', nil, False);

      FJsonToDataSet.JsonObject := FJSONTabela;
      if FJsonToDataSet.Converter then
      begin
        Conexao.Conectar(True, False);
        FDExec.Connection  := Conexao.SQLite;
        FDBusca.Connection := Conexao.SQLite;
        //criando tabelas
        for I := 0 to Pred(Length(FJsonToDataSet.FArrayQuery)) do
        begin
          FDBusca.Close;
          FDBusca.SQL.Clear;
          FDBusca.SQL.Add('SELECT * FROM sqlite_master where tbl_name = ' + QuotedStr(FJsonToDataSet.FArrayQuery[I].Name));
          FDBusca.Open;

          if not FDBusca.IsEmpty then
          begin
            FDExec.Close;
            FDExec.SQL.Clear;
            FDExec.SQL.Add('drop table ' + FJsonToDataSet.FArrayQuery[I].Name);
            FDExec.ExecSQL;

            FDExec.Close;
          end;

          DDL := 'CREATE TABLE [' + FJsonToDataSet.FArrayQuery[I].Name + ']( [ID] INTEGER PRIMARY KEY AUTOINCREMENT';

          for J := 0 to Pred(FJsonToDataSet.FArrayQuery[I].FieldCount) do
            DDL := DDL + ', [' + FJsonToDataSet.FArrayQuery[I].Fields[J].FieldName + '] TEXT';

          DDL := DDL + ');';

          SalvarLog('Criando tabela ' + FJsonToDataSet.FArrayQuery[I].Name, nil, False);

          FDExec.Close;
          FDExec.SQL.Clear;
          FDExec.SQL.Add(DDL);
          FDExec.ExecSQL;

          ////////////////////////////////////////
          ///  preenchendo a tabela            ///
          ////////////////////////////////////////
          DDL     := '';
          Valores := '';
//          DDL     := 'INSERT INTO ' + FJsonToDataSet.FArrayQuery[I].Name + ' (';

          for J := 0 to Pred(FJsonToDataSet.FArrayQuery[I].FieldCount) do
          begin
            if DDL <> '' then
              DDL := DDL + ', ';

            if Valores <> '' then
              Valores := Valores + ', ';

            DDL     := DDL + FJsonToDataSet.FArrayQuery[I].Fields[J].FieldName;
            Valores := Valores + ':' + FJsonToDataSet.FArrayQuery[I].Fields[J].FieldName;
          end;

          DDL := 'INSERT INTO ' + FJsonToDataSet.FArrayQuery[I].Name + ' (' + DDL + ') VALUES (' + Valores + ')';

          FDExec.Close;
          FDExec.SQL.Clear;
          FDExec.SQL.Add(DDL);

          /////////////////////////////////////////////
          ///  configurando os tipos de par�metros  ///
          /////////////////////////////////////////////
          for J := 0 to Pred(FDExec.Params.Count) do
          begin
            FDExec.Params[J].ParamType := ptInput;
            FDExec.Params[J].DataType  := ftMemo;
          end;

          FDExec.Prepare;
          FDExec.Params.ArraySize := FJsonToDataSet.FArrayQuery[I].RecordCount;

          FJsonToDataSet.FArrayQuery[I].First;

          for J := 0 to Pred(FDExec.Params.ArraySize) do
          begin
            for K := 0 to Pred(FDExec.Params.Count) do
               FDExec.Params[K].Values[J] := FJsonToDataSet.FArrayQuery[I].Fields[K].Value;
            FJsonToDataSet.FArrayQuery[I].Next;
          end;

          if FDExec.Params.ArraySize > 0 then
            FDExec.Execute(FDExec.Params.ArraySize);

          SalvarLog('  - Inserido(s) ' + InttoStr(FJsonToDataSet.FArrayQuery[I].RecordCount) + ' registro(s).', nil, False);
          FDExec.Close;
          FDBusca.Close;
        end;

        FDBusca.Close;
        FDBusca.SQL.Clear;
        FDBusca.SQL.Add('SELECT * FROM sqlite_master where tbl_name = ' + QuotedStr('primeirouso'));
        FDBusca.Open;

        FTabelaPriUso := FDBusca.IsEmpty;

        /////////////////////////////////////////////
        ///  Criando tabela de log                ///
        /////////////////////////////////////////////
        FDBusca.Close;
        FDBusca.SQL.Clear;
        FDBusca.SQL.Add('SELECT * FROM sqlite_master where tbl_name = ' + QuotedStr('log'));
        FDBusca.Open;

        if FDBusca.IsEmpty then
        begin
          FDExec.Close;
          FDExec.SQL.Clear;
          FDExec.SQL.Add(
            'CREATE TABLE [main].[log]( ' +
            '            [id] INTEGER PRIMARY KEY AUTOINCREMENT, ' +
            '            [tabela] NVARCHAR(100), ' +
            '            [tiporequisicao] NCHAR(1), ' +
            '            [registro] NVARCHAR(100), ' +
            '            [horaintegracao] TIMESTAMP, ' +
            '            [horasincronismo] TIMESTAMP, ' +
            '            [statuscod] INT, ' +
            '            [erro] BOOLEAN, ' +
            '            [jsonretorno] TEXT, ' +
            '            [jsonenvio] TEXT, ' +
            '            [url] NVARCHAR(100), ' +
            '            [parametros] TEXT, ' +
            '            [mensagem] NVARCHAR, ' +
            '            [datalog] TIMESTAMP); ');
          FDExec.ExecSQL;
          SalvarLog('Criando tabela LOG', nil, False);
        end;
      end
      else
        SalvarLog('Falha ao criar tabelas... verifique o log!', nil, False);
    except
      on E: Exception do
      begin
        Result := False;
        SalvarLog('CriarTAbelas: ' + E.Message);
      end;
    end;
  finally
    FDExec.Close;
    FDBusca.Close;

    FreeAndNil(FDExec);
    FreeAndNil(FDBusca);
  end;
end;

function TCriarTabelas.CriarTriggers(str: TStrings): Boolean;
var
  FDSis, FDConf, FDExec: TFDQuery;
  FConBD: TFDConnection;


  function Executar(Trigger, Tipo: String): Boolean;
  var
    TriggerName: String;
  begin
    try
      try
        case AnsiIndexStr(FDSis.FieldByName('sis_driverid').AsString, ['S']) of
          0: //SQLServer
          begin
            case AnsiIndexStr(Tipo, ['I', 'A', 'D']) of
              0: TriggerName := 'MAXXINTEGRA_' + FDConf.FieldByName('conf_tabela').AsString + '_AFTINS';
              1: TriggerName := 'MAXXINTEGRA_' + FDConf.FieldByName('conf_tabela').AsString + '_AFTUPD';
              2: TriggerName := 'MAXXINTEGRA_' + FDConf.FieldByName('conf_tabela').AsString + '_BEFDEL';
            end;

            FDExec.Close;
            FDExec.SQL.Clear;
            FDExec.SQL.Add('IF OBJECT_ID(N' + QuotedStr(TriggerName) + ') IS NOT NULL exec sp_executesql N' + QuotedStr('DROP TRIGGER ' + TriggerName));
            FDExec.ExecSQL;
          end;
        end;

        FDExec.Close;
        FDExec.SQL.Clear;
        FDExec.SQL.Add(PrepareDDLTrigger(FDConf, Trigger, Tipo, FDSis.FieldByName('sis_driverid').AsString));

        if FDExec.Macros.Count > 0 then
        begin
          if FDExec.FindMacro('SCHEMA') <> nil then
            FDExec.MacroByName('SCHEMA').AsRaw := frmPrincipal.PreencheSCHEMA;
        end;

        FDExec.ExecSQL;

        case AnsiIndexStr(Tipo, ['I', 'A','D']) of
          0: SalvarLog('Executado script trigger ' + FDConf.FieldByName('TPSCR_DESCRICAO').AsString + ' - insert.', nil, False);
          1: SalvarLog('Executado script trigger ' + FDConf.FieldByName('TPSCR_DESCRICAO').AsString + ' - update.', nil, False);
          2: SalvarLog('Executado script trigger ' + FDConf.FieldByName('TPSCR_DESCRICAO').AsString + ' - delete.', nil, False);
        end;
      except
        on E: Exception do
        begin
          SalvarLog('Executar DDL Trigger: ' + E.Message);
          SalvarLog(FDExec.SQL.Text);
          Result := false;
        end;
      end;
    finally
    end;
  end;

begin
  try
    try
      Result         := True;
      FConBD         := TFDConnection.Create(nil);
      FConBD.OnError := TErrorFDConnection.ConexaoError;

      FDSis  := TFDQuery.Create(nil);
      FDConf := TFDQuery.Create(nil);
      FDExec := TFDQuery.Create(nil);


      FDSis.Connection  := Conexao.SQLite;
      FDConf.Connection := Conexao.SQLite;
      FDExec.Connection := Conexao.BDCli;

      FDSis.Close;
      FDSis.SQL.Clear;
      FDSis.SQL.Add('select * from sistemasintegrados');
      FDSis.Open;

      FDConf.Close;
      FDConf.SQL.Clear;
      FDConf.SQL.Add('select * from sistemasintegrados_conf ' +
                     'where (tpscr_descricao <> ''EMPRESA'' and tpscr_descricao <> ''FILIAL'')');
      FDConf.Open;

      FDConf.First;
      while not FDConf.Eof do
      begin
        if FDConf.FieldByName('conf_tabelapai').AsString = '' then
        begin
          Executar(FDSis.FieldByName('sis_triggerinsert').AsString, 'I');
          Executar(FDSis.FieldByName('sis_triggerupdate').AsString, 'A');
          Executar(FDSis.FieldByName('sis_triggerdelete').AsString, 'D');
        end
        else
          Executar(FDSis.FieldByName('sis_triggerdelete').AsString, 'D');

        FDConf.Next;
      end;
    except
      on E: Exception do
      begin
        SalvarLog('CriarTriggers: ' + E.Message);
        Result := False;
      end;
    end;
  finally
    FDSis.Close;
    FDConf.Close;
    FDExec.Close;
    FConBD.Close;

    FreeAndNil(FDSis);
    FreeAndNil(FDConf);
    FreeAndNil(FDExec);
    FreeAndNil(FConBD);
  end;
end;

destructor TCriarTabelas.Destroy;
begin
  inherited Destroy;
  FQuery.Close;
  FreeAndNil(FQuery);
  FreeAndNil(FJsonToDataSet);
end;

function TCriarTabelas.PrepareDDLTrigger(FDQry: TFDQuery; Trigger, Tipo, BD: String): String;

    function RetornaCodigo(Codigo, Tipo: String): String;
    var
      StrCodigo: TStringList;
      I: Integer;
    begin
      StrCodigo    := TStringList.Create;
      try
        StrCodigo.StrictDelimiter := False;
        StrCodigo.Delimiter       := ';';
        StrCodigo.DelimitedText   := Codigo;

        case AnsiIndexStr(BD, ['S', 'O'] ) of
          0: //SQLServer
          begin
            for I := 0 to StrCodigo.Count -1 do
            begin
              if Result <> '' then
                Result := Result + ' + ' + QuotedStr(',') + ' + ';

              Result := Result + ifthen(Tipo = 'D', 'D.', 'I.') + StrCodigo[I];
            end;
          end;
          1: //Oracle
          begin
            for I := 0 to StrCodigo.Count -1 do
            begin
              if Result <> '' then
                Result := Result + ' || ' + QuotedStr(';') + ' || ';

              Result := Result + ifthen(Tipo = 'D', ':OLD.', ':NEW.') + StrCodigo[I];
            end;
          end
        else
          begin
            for I := 0 to StrCodigo.Count -1 do
            begin
              if Result <> '' then
                Result := Result + ' || ' + QuotedStr(';') + ' || ';

              Result := Result + ifthen(Tipo = 'D', 'OLD.', 'NEW.') + StrCodigo[I];
            end;
          end;
        end;
      finally
        StrCodigo.Free;
      end;
    end;

begin
  try

    Result := StringReplace(Trigger, '/*TABELA_REG*/', FDQry.FieldByName('conf_tabela').AsString, [rfReplaceAll, rfIgnoreCase]);
    Result := StringReplace(Result, '/*CTABELA_REG*/', QuotedStr(FDQry.FieldByName('conf_tabela').AsString), [rfReplaceAll, rfIgnoreCase]);

    if FDQry.FieldByName('conf_tabelapai').AsString <> '' then
      Result := StringReplace(Result, '/*TABELA_PAI*/', QuotedStr(FDQry.FieldByName('conf_tabelapai').AsString), [rfReplaceAll, rfIgnoreCase])
    else
      Result := StringReplace(Result, '/*TABELA_PAI*/', 'null', [rfReplaceAll, rfIgnoreCase]);

    Result := StringReplace(Result, '/*CODIGO_REG*/', RetornaCodigo(FDQry.FieldByName('conf_codigo').AsString, Tipo), [rfReplaceAll, rfIgnoreCase]);

    if FDQry.FieldByName('conf_codigopai').AsString <> '' then
      Result := StringReplace(Result, '/*CODIGO_PAI*/', RetornaCodigo(FDQry.FieldByName('conf_codigopai').AsString, Tipo), [rfReplaceAll, rfIgnoreCase])
    else
      Result := StringReplace(Result, '/*CODIGO_PAI*/', 'null', [rfReplaceAll, rfIgnoreCase]);


  except
    on E: Exception do
      SalvarLog('PrepareDDLTrigger: ' + E.Message);
  end;
end;

procedure TCriarTabelas.SetJSONTabela(const Value: TJSONObject);
begin
  FJSONTabela := Value;
end;

procedure TCriarTabelas.SetTabelaPriUso(const Value: Boolean);
begin
  FTabelaPriUso := Value;
end;

end.
