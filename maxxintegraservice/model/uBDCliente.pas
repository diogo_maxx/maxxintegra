unit uBDCliente;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, IWSystem, StrUtils, Rest.JSON, System.JSON.Types, System.JSON.Writers, System.Json.Builders, Json,
  Data.DBXPlatform, Variants, System.Diagnostics, IPPeerClient, REST.Client,
  System.SyncObjs, Winapi.Windows, Vcl.Forms, uDatasetToJSON;

//  function CDSToJSONValue(FDQry: TFDQuery; NomeNo: String; JsonValue: TJsonValue = nil; NomeFilho: String = ''): TJSONValue;
  function CriarFiltros(FDQry, FDQryIntegracao: TFDQuery; CodigoReg: String = ''; CCodigo: String = ''): String; overload;
  function CriarFiltros(DataSet: TDataSet; Campos: String; CamposPai: String = ''): String; overload;
  procedure RetornaValor(Valor: String; StrList: TStringList; Quoted: Boolean = True);
  function CDSToJSONValue(FDQry: TFDQuery; AFilho: array of TJsonFilho): TJSONValue;
  function CriarTabelaIntegracao(FDCon: TFDConnection): Boolean;
  function CriarTriggers(FDCon: TFDConnection):Boolean;
  function Exportar(Tabela, Filtro, Script: String; FDCliente: TFDConnection = nil; FDIntegra: TFDConnection = nil): TJsonValue;


Type
  TTaskHelper = class helper for TTask
  private type
    TUnsafeTaskEx = record
    private
      [Unsafe]
      // preciso de um record UNSAFE para nao incrementar o RefCount da Interface
      FTask: TTask;
    public
      property Value: TTask read FTask write FTask;
    end;
  public
    class function WaitForAllEx(AArray: Array of ITask;
    ATimeOut: int64 = INFINITE): boolean;
  end;

implementation

uses uLog, uConexao, ufrmPrincipal, uException;

function CriarTabelaIntegracao(FDCon: TFDConnection): Boolean;
var
  _Qry: TFDQuery;
  Tabela, ChavePrimaria: String;
begin
  try
    try
      //cria uma nova conex�o com o banco do cliente

//      CriaConexaoBDCliente(FDCon);
      _Qry := TFDQuery.Create(nil);
      _Qry.Connection := FDCon;

      ChavePrimaria := '';

      _Qry.Close;
      _Qry.SQL.Clear;

      case AnsiIndexStr(FDCon.Params.DriverID, ['FB', 'MySQL', 'Ora', 'MSSQL']) of
        0: //firebird
        begin
          _Qry.SQL.Add(
          'select rdb$field_name from RDB$RELATION_FIELDS ' +
          'where rdb$relation_name = ' + QuotedStr('MAXX_INTEGRACAO'));

          Tabela := 'CREATE TABLE MAXX_INTEGRACAO ( ' +
                    '    ID INTEGER NOT NULL PRIMARY KEY, ' +
                    '    TABELA_REG VARCHAR(100), ' +
                    '    TABELA_PAI VARCHAR(100), ' +
                    '    CODIGO_REG VARCHAR(100), ' +
                    '    CODIGO_PAI VARCHAR(100), ' +
                    '    DATA TIMESTAMP, ' +
                    '    TIPO_MOD CHAR(1), ' +
                    '    EXEC CHAR(1))';
//          ChavePrimaria := 'ALTER TABLE MAXX_INTEGRACAO ADD CONSTRAINT PK_MAXX_INTEGRACAO PRIMARY KEY (ID)';
          ChavePrimaria := 'CREATE SEQUENCE GEN_MAXX_INTEGRACAO_ID';
        end;
        1: // MySQL
        begin
          _Qry.SQL.Add(
          'SELECT * ' +
          'FROM INFORMATION_SCHEMA.TABLES ' +
          'WHERE TABLE_NAME=' + QuotedStr('MAXX_INTEGRACAO'));

          Tabela := 'CREATE TABLE maxx_integracao ( ' +
                    '  ID INT NOT NULL AUTO_INCREMENT, ' +
                    '  TABELA_REG VARCHAR(100) NULL, ' +
                    '  TABELA_PAI VARCHAR(100) NULL, ' +
                    '  CODIGO_REG VARCHAR(100) NULL, ' +
                    '  CODIGO_PAI VARCHAR(100) NULL, ' +
                    '  DATA TIMESTAMP NULL, ' +
                    '  TIPO_MOD CHAR(1) NULL, ' +
                    '  EXEC CHAR(1) NULL, ' +
                    '  PRIMARY KEY (ID))';
        end;
        2: //Ora
        begin
          _Qry.SQL.Add(
          'select * from all_tables where table_name =' + QuotedStr('MAXX_INTEGRACAO'));
        end;
        3: //SQLServer
        begin
          _Qry.SQL.Add('SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ' + QuotedStr('MAXX_INTEGRACAO'));
        end;
      end;

      _Qry.Open;

      if _Qry.IsEmpty then
      begin
        _Qry.SQL.Clear;
        _Qry.SQL.Add(Tabela);
        _Qry.ExecSQL;

        if ChavePrimaria <> '' then
        begin
           _Qry.SQL.Clear;
          _Qry.SQL.Add(ChavePrimaria);
          _Qry.ExecSQL;
        end;
      end;
    except
      on E: Exception do
      begin
        if E.Message <> 'Operation aborted' then
          SalvarLog('CriaConexao: ' + E.Message);
        Result := False;
      end;
    end;
  finally
    FreeAndNil(_Qry);
  end;
end;

function CriarTriggers(FDCon: TFDConnection):Boolean;
var
  _FDBD: TFDConnection;
  _Qry : TFDQuery;
  Insert, Update, Delete : String;

  function ConfDDLTrigger(Trigger, Tipo: String): String;

    function RetornaCodigo(Codigo, Tipo: String): String;
    var
      StrCodigo: TStringList;
      I: Integer;
    begin
      StrCodigo    := TStringList.Create;

      try
        StrCodigo.StrictDelimiter := False;
        StrCodigo.Delimiter       := ';';
        StrCodigo.DelimitedText   := Codigo;

        for I := 0 to StrCodigo.Count -1 do
        begin
          if Result <> '' then
            Result := Result + ' || ' + QuotedStr(';') + ' || ';

          Result := Result + ifthen(Tipo = 'D', 'OLD.', 'NEW.') + StrCodigo[I];
        end;
      finally
        StrCodigo.Free;
      end;
    end;

  begin
    Result := StringReplace(Trigger, '/*TABELA_REG*/', _Qry.FieldByName('conf_tabela').AsString, [rfReplaceAll, rfIgnoreCase]);
    Result := StringReplace(Result, '/*CTABELA_REG*/', QuotedStr(_Qry.FieldByName('conf_tabela').AsString), [rfReplaceAll, rfIgnoreCase]);

    if _Qry.FieldByName('conf_tabelapai').AsString <> '' then
      Result := StringReplace(Result, '/*TABELA_PAI*/', QuotedStr(_Qry.FieldByName('conf_tabelapai').AsString), [rfReplaceAll, rfIgnoreCase])
    else
      Result := StringReplace(Result, '/*TABELA_PAI*/', 'null', [rfReplaceAll, rfIgnoreCase]);

    Result := StringReplace(Result, '/*CODIGO_REG*/', RetornaCodigo(_Qry.FieldByName('conf_codigo').AsString, Tipo), [rfReplaceAll, rfIgnoreCase]);

    if _Qry.FieldByName('conf_codigopai').AsString <> '' then
      Result := StringReplace(Result, '/*CODIGO_PAI*/', RetornaCodigo(_Qry.FieldByName('conf_codigopai').AsString, Tipo), [rfReplaceAll, rfIgnoreCase])
    else
      Result := StringReplace(Result, '/*CODIGO_PAI*/', 'null', [rfReplaceAll, rfIgnoreCase]);
  end;

  function Executar(DDL: String): Boolean;
  var
    _Exec : TFDQuery;
  begin
    try
      _Exec            := TFDQuery.Create(nil);
      _Exec.Connection := FDCon;

      if DDL <> '' then
      begin
        _Exec.Close;
        _Exec.SQL.Clear;
        _Exec.SQL.Add(DDL);
        _Exec.ExecSQL;
      end;
    finally
      FreeAndNil(_Exec);
    end;
  end;
begin
  try
    _FDBD           := TFDConnection.Create(nil);
    _FDBD.OnError   := TErrorFDConnection.ConexaoError;
    _Qry            := TFDQuery.Create(nil);
    _Qry.Connection := _FDBD;

    CriaConexao(_FDBD);

    _Qry.Close;
    _Qry.SQL.Clear;
    _Qry.SQL.Add('select * from sistemasintegrados_conf');
    _Qry.Open;

    if not _Qry.IsEmpty then
    begin
      _Qry.First;
      while not _Qry.Eof do
      begin
        Executar(ConfDDLTrigger(_Qry.FieldByName('conf_triggerinsert').AsString, 'I'));
        Executar(ConfDDLTrigger(_Qry.FieldByName('conf_triggerupdate').AsString, 'U'));
        Executar(ConfDDLTrigger(_Qry.FieldByName('conf_triggerdelete').AsString, 'D'));
        _Qry.Next;
      end;
    end;

    if FDCon.Params.DriverID = 'FB' then
    begin
      Executar(' create or alter trigger maxx_integracao_IDBEFINS for maxx_integracao ' +
               'active before insert position 0 ' +
               'as  ' +
               'begin ' +
               '  if (new.ID is null) then ' +
               '    new.ID = gen_id(gen_maxx_integracao_id,1); end');
    end;

  finally
    FreeAndNil(_FDBD);
    FreeAndNil(_Qry);
  end;
end;

function Exportar(Tabela, Filtro, Script: String; FDCliente: TFDConnection = nil; FDIntegra: TFDConnection = nil): TJsonValue;
var
  FDBD, FDInt: TFDConnection;
  FDQry, FDQryInt, FDConf: TFDQuery;
  JSonArray : TJsonArray;
  AFilho: Array of TJsonFilho;
  DataSetToJson : TDatasetToJSON;

  procedure BuscaSQL(FD: TFDQuery; SQL: String);
  begin
    FD.Close;
    FD.SQL.Clear;
    FD.SQL.Add(SQL);
    FD.Prepare;
    FD.Open;
  end;

  procedure Filtrar(FD: TFDQuery; Filtro: String);
  begin
    FD.Filtered := False;
    FD.Filter   := Filtro;
    FD.Filtered := True;
  end;

begin
  try
    try
      if FDCliente <> nil then
        FDBD := FDCliente
      else
      begin
        FDBD         := TFDConnection.Create(nil);
        FDBD.OnError := TErrorFDConnection.ConexaoError;
        CriaConexaoBDCliente(FDBD);
      end;

      if FDIntegra <> nil then
        FDInt := FDIntegra
      else
      begin
        FDInt         := TFDConnection.Create(nil);
        FDInt.OnError := TErrorFDConnection.ConexaoError;
        CriaConexao(FDInt);
      end;

      FDQryInt := TFDQuery.Create(nil);
      FDQry    := TFDQuery.Create(nil);
      FDConf   := TFDQuery.Create(nil);

      FDQry.DisableControls;
      FDQryInt.DisableControls;
      FDConf.DisableControls;

      FDQryInt.Connection := FDInt;
      FDQry.Connection    := FDBD;
      FDConf.Connection   := FDInt;

      //verificando se existe filhos
        //buscando configura��es
      BuscaSQL(FDQryInt, 'select * from sistemasintegrados_conf where conf_tabelapai = ' + QuotedStr(Tabela));

      //executando a busca no banco de dados
      Script := StringReplace(Script, '&SCHEMA', frmPrincipal.PreencheSCHEMA, [rfReplaceAll, rfIgnoreCase]);
      Script := StringReplace(Script, '&FILTRO', Filtro, [rfReplaceAll, rfIgnoreCase]);

      frmPrincipal.PrepararParametrosUD(Script);

      BuscaSQL(FDQry, Script);

      if not FDQry.IsEmpty then
      begin
        if FDQry.RecordCount > 1 then
          JSonArray  := TJSONArray.Create;

        FDQry.First;
        while not FDQry.Eof do
        begin
          if not FDQryInt.IsEmpty then
          begin
            SetLength(AFilho, FDQryInt.RecordCount);

            FDQryInt.First;
            while not FDQryInt.Eof do
            begin
              AFilho[FDQryInt.RecNo -1].JsonFilho := TJSONValue.Create;
              AFilho[FDQryInt.RecNo -1].Nome      := ifthen(FDQryInt.FieldByName('conf_nomejson').AsString <> '',
                                                            FDQryInt.FieldByName('conf_nomejson').AsString,
                                                            FDQryInt.FieldByName('conf_tabela').AsString);
              AFilho[FDQryInt.RecNo -1].JsonFilho := Exportar(FDQryInt.FieldByName('conf_tabela').AsString,
                                                       CriarFiltros(FDQry, FDQryInt.FieldByName('conf_codigopai').AsString,
                                                                           FDQryInt.FieldByName('conf_codigosql').AsString),
                                                       FDQryInt.FieldByName('conf_scriptbusca').AsString, FDBD, FDInt);
              FDQryInt.Next;
            end;
          end;

          try
            try
              BuscaSQL(FDQryInt, 'select * from sistemasintegrados_conf where conf_tabela = ' + QuotedStr(Tabela));

              FDConf.Close;
              FDConf.SQL.Clear;
              FDConf.SQL.Add('SELECT * FROM conf_json ' +
                             'where idsistemasintegrados = ' + FDQryInt.FieldByName('idsistemasintegrados').AsString +
                             '  and idconf = ' + FDQryInt.FieldByName('idconf').AsString);
              FDConf.Open;

              DataSetToJson                := TDatasetToJSON.Create(False, AFilho);
              DataSetToJson.FormatoData    := 'yyyy-MM-dd''T''HH:mm:ss.zzz';
              DataSetToJson.IncluiTimeZone := True;
//              Result                    := DataSetToJson.Converter(FDQry);


              if (FDQryInt.FieldByName('conf_tabelapai').AsString <> '') then
              begin
                if JSonArray = nil then
                   JSonArray := TJSONArray.Create;

                 JSonArray.AddElement(DataSetToJson.CriaJson(FDQry, FDConf))
              end
              else
              begin
                if FDQry.RecordCount > 1 then
                  JSonArray.AddElement(DataSetToJson.CriaJson(FDQry, FDConf))
                else Result := DataSetToJson.CriaJson(FDQry, FDConf);
              end;

            finally
              FreeAndNil(DataSetToJson);
            end;
          except
            on E: Exception do
              SalvarLog('Erro CDSjson: ' + E.Message);
          end;

          FDQry.Next;
        end;

        if FDQryInt.FieldByName('conf_tabelapai').AsString <> '' then
          Result := JSonArray
        else
        if FDQry.RecordCount > 1 then
          Result := JSonArray;
      end;
    except
      on E: Exception do
      begin
        SalvarLog('Exportar: ' + E.Message);
      end;
    end;
  finally
    FDQry.Close;
    FDQryInt.Close;
    FDConf.Close;

    FreeAndNil(FDQry);
    FreeAndNil(FDQryInt);
    FreeAndNil(FDConf);

    if FDCliente = nil then
    begin
      FDBD.Connected := False;
      FreeAndNil(FDBD);
    end;

    if FDIntegra = nil then
    begin
      FDInt.Connected := False;
      FreeAndNil(FDInt);
    end;

    frmPrincipal.LimpaMemoriaAPP;
  end;
end;

function CDSToJSONValue(FDQry: TFDQuery; AFilho: array of TJsonFilho): TJSONValue;
var
  DataSetToJson: TDatasetToJSON;
begin
//  try
//    try
//      if Length(AFilho) > 0 then
//        DataSetToJson := TDatasetToJSON.Create(False, AFilho)
//      else DataSetToJson := TDatasetToJSON.Create;
//
//      DataSetToJson.FormatoData := 'yyyy-MM-dd''T''HH:mm.zzz';
//      Result                    := DataSetToJson.Converter(FDQry);
//    finally
//      FreeAndNil(DataSetToJson);
//    end;
//  except
//    on E: Exception do
//      SalvarLog('Erro CDSjson: ' + E.Message);
//  end;
end;


procedure RetornaValor(Valor: String; StrList: TStringList; Quoted: Boolean = True);
var
  Str: TStringList;
  I: Integer;
begin
  try
    Str                 := TStringList.Create;
    Str.StrictDelimiter := False;
    Str.Delimiter       := ';';
    Str.DelimitedText   := Valor;

    for I := 0 to Str.Count -1 do
      if Str[I] <> '' then
        StrList.Add(ifthen(Quoted, QuotedStr(Str[I]), Str[I]));
  finally
    FreeAndNil(Str);
  end;
end;

function CriarFiltros(FDQry, FDQryIntegracao: TFDQuery; CodigoReg: String = ''; CCodigo: String = ''): String;
var
  Campos, Filtro, Valores: TStringList;
  I: Integer;
begin
  try
    Filtro                 := TStringList.Create;
    Campos                 := TStringList.Create;
    Campos.StrictDelimiter := False;
    Campos.Delimiter       := ';';
    Campos.DelimitedText   := ifthen(CCodigo = '', FDQryIntegracao.FieldByName('conf_codigo').AsString, CCodigo);
    Campos.StrictDelimiter := True;

    //adiciona os campos para o filtro
    for I := 0 to Campos.Count -1 do
    begin
      if Campos[I] <> '' then
        Filtro.Add(' and ' + Campos[I] + ' in (');
    end;

    if CodigoReg = '' then
    begin
      FDQry.First;

      while not FDQry.Eof do
      begin
        Valores := TStringList.Create;

        RetornaValor(FDQry.FieldByName('CODIGO_REG').AsString, Valores);
        for I := 0 to Filtro.Count -1 do
          Filtro[I] := Filtro[I] + ifthen((FDQry.RecNo = FDQry.RecordCount), Valores[I] + ') ', Valores[I] + ',');

        FreeAndNil(Valores);
        FDQry.Next;
      end;
    end
    else
    begin
      Valores := TStringList.Create;

      RetornaValor(CodigoReg, Valores);
      for I := 0 to Filtro.Count -1 do
        Filtro[I] := Filtro[I] + Valores[I] + ')';

      FreeAndNil(Valores);
    end;

    Result := '';
    for I := 0 to Filtro.Count -1 do
      Result := Result + Filtro[I];
  finally
    FreeAndNil(Valores);
    FreeAndNil(Filtro);
    FreeAndNil(Campos);
  end;
end;

function CriarFiltros(DataSet: TDataSet; Campos: String; CamposPai: String = ''): String;
var
  Campo, CampoPai: TStringList;
  I: Integer;
begin
  try
    Campo     := TStringList.Create;
    CampoPai  := TStringList.Create;

    Campo.StrictDelimiter := False;
    Campo.Delimiter       := ';';
    Campo.DelimitedText   := Campos;
    Campo.StrictDelimiter := True;

    if CamposPai <> '' then
    begin
      CampoPai.StrictDelimiter := False;
      CampoPai.Delimiter       := ';';
      CampoPai.DelimitedText   := CamposPai;
      CampoPai.StrictDelimiter := True;
    end;


    Result := '';

    for I := 0 to Campo.Count -1 do
    begin
      if Campo[I] <> '' then
      begin
        if CamposPai <> '' then
          Result := Result + ' and ' + Campo[I] + ' in(' + DataSet.FieldByName(CampoPai[I]).AsString + ')'
        else
          Result := Result + ' and ' + Campo[I] + ' in(' + DataSet.FieldByName(Campo[I]).AsString + ')'
      end;
    end;
  finally
    FreeAndNil(CampoPai);
    FreeAndNil(Campo);
  end;


//  Result.StrictDelimiter := False;
//  Result.Delimiter       := ';';
//  Result.DelimitedText   := Campos;
end;

 { TTaskHelper }

class function TTaskHelper.WaitForAllEx(AArray: array of ITask; ATimeOut: int64): boolean;
var
  FEvent: TEvent;
  task: TUnsafeTaskEx;
  i: integer;
  taskInter: TArray<TUnsafeTaskEx>;
  completou: boolean;
  Canceled, Exceptions: boolean;
  ProcCompleted: TProc<ITask>;
  LHandle: THandle;
  LStop: TStopwatch;
begin
  LStop := TStopwatch.StartNew;
  ProcCompleted := procedure(ATask: ITask)
    begin
      FEvent.SetEvent;
    end;

  Canceled := false;
  Exceptions := false;
  result := true;
  try
    for i := low(AArray) to High(AArray) do
    begin
      task.Value := TTask(AArray[i]);
      if task.Value = nil then
        raise EArgumentNilException.Create('Wait Nil Task');

      completou := task.Value.IsComplete;
      if not completou then
      begin
        taskInter := taskInter + [task];
      end
      else
      begin
        if task.Value.HasExceptions then
          Exceptions := true
        else if task.Value.IsCanceled then
          Canceled := true;
      end;
    end;

    try
      FEvent := TEvent.Create();
      for task in taskInter do
      begin
        try
          FEvent.ResetEvent;
          if LStop.ElapsedMilliseconds > ATimeOut then
            break;
          LHandle := FEvent.Handle;
          task.Value.AddCompleteEvent(ProcCompleted);
          while not task.Value.IsComplete do
          begin
            try
              if LStop.ElapsedMilliseconds > ATimeOut then
                break;
                  if MsgWaitForMultipleObjectsEx(1, LHandle,
                    ATimeOut - LStop.ElapsedMilliseconds, QS_ALLINPUT, 0)
                    = WAIT_OBJECT_0 + 1 then
                    application.ProcessMessages;
            finally
            end;
          end;
          if task.Value.IsComplete then
          begin
            if task.Value.HasExceptions then
              Exceptions := true
            else if task.Value.IsCanceled then
              Canceled := true;
          end;
        finally
          task.Value.removeCompleteEvent(ProcCompleted);

        end;
      end;
    finally
      FEvent.Free;
    end;
  except
    result := false;
  end;

  if (not Exceptions and not Canceled) then
    Exit;
  if Exceptions or Canceled then
    raise EOperationCancelled.Create
      ('One Or More Tasks HasExceptions/Canceled');

end;



end.
