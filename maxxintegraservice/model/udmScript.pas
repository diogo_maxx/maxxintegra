unit udmScript;

interface

uses
  System.SysUtils, System.Classes, JSON, Data.DBXPlatform, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Phys.IBDef, FireDAC.Phys.FBDef,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.MySQL, FireDAC.Phys.FB, FireDAC.Phys.IBBase,
  FireDAC.Phys.IB, FireDAC.Stan.StorageJSON, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, Variants, Winapi.Windows, Math, StrUtils, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope;


{$METHODINFO ON}
type
  TdmScript = class(TDataModule)
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDPhysIBDriverLink1: TFDPhysIBDriverLink;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
  private
    { Private declarations }
     function ExecutaScript(TipoScript: String): TJSonArray;
     function RetornaTimeZone: String;
  public
    { Public declarations }
//    function Teste: TJSONObject;
     procedure Teste;
  end;
{$METHODINFO OFF}

var
  dmScript: TdmScript;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uConexao, uLog;

{$R *.dfm}

{ TdmScript }

procedure TdmScript.Teste;
var
  Resultado: TJSONArray;
begin
//  Result := Resultado;

  Resultado := ExecutaScript('');


  if Resultado = nil then
  begin
    GetInvocationMetadata().ResponseCode    := 500;
    GetInvocationMetadata().ResponseContent := 'Erro ao rodar o script';
  end
  else
  begin
    GetInvocationMetadata().ResponseCode    := 200;
    GetInvocationMetadata().ResponseContent := Resultado.ToString;
  end;
end;

function TdmScript.ExecutaScript(TipoScript: String): TJSonArray;
var
  ConLocal, ConBD : TFDConnection;
  FDQryLocal, FDQryBD: TFDQuery;
  JSonObject: TJSONObject;
  JSonArray: TJSONArray;
  I: Integer;
begin
  try
    Result := nil;

    JSonArray  := TJSONArray.Create;
    ////////////////////////
    /// BUSCANDO SCRIPT  ///
    ////////////////////////
    ConLocal := TFDConnection.Create(nil);
    CriaConexao(ConLocal);

    FDQryLocal            := TFDQuery.Create(nil);
    FDQryLocal.Connection := ConLocal;

    FDQryLocal.Close;
    FDQryLocal.SQL.Clear;
    FDQryLocal.SQL.Add('select * from scripts');
    FDQryLocal.Open;


    try
      ConBD := TFDConnection.Create(nil);

      ConBD.Connected              := False;
      ConBD.LoginPrompt            := False;
      ConBD.UpdateOptions.LockWait := False;

      ConBD.Params.Clear;
      ConBD.Params.Add('DriverID=FB');
      ConBD.Params.Add('Database=' + FDQryLocal.FieldByName('scr_database').AsString);
      ConBD.Params.Add('User_Name=' + FDQryLocal.FieldByName('scr_username').AsString);
      ConBD.Params.Add('Password=' + FDQryLocal.FieldByName('scr_password').AsString);
      ConBD.Params.Add('SERVER=' + FDQryLocal.FieldByName('scr_server').AsString);
      ConBD.Params.Add('Port=' + FDQryLocal.FieldByName('scr_port').AsString);

      ConBD.Connected := True;

      FDQryBD            := TFDQuery.Create(nil);
      FDQryBD.Connection := ConBD;

      FDQryBD.Close;
      FDQryBD.SQL.Clear;
      FDQryBD.SQL.Add(FDQryLocal.FieldByName('SCR_SCRIPT').AsString);
      FDQryBD.Open;

      if not FDQryBD.IsEmpty then
      begin
        FDQryBD.First;

        while not FDQryBD.Eof do
        begin
          JSonObject := TJSONObject.Create;

          with FDQryBD do
          begin
            for I := 0 to FieldCount -1 do
            begin
              if not VarIsNull(Fields[I].Value) then
              begin
                if Fields[I].DataType in [TFieldType.ftString, TFieldType.ftWideString, TFieldType.ftMemo, TFieldType.ftFixedChar] then
                  JSonObject.AddPair(Fields[I].FieldName, TJSONString.Create(Fields[I].Value))
                else
                if Fields[I].DataType in [TFieldType.ftSmallint, TFieldType.ftInteger, TFieldType.ftFloat,
                                          TFieldType.ftCurrency, TFieldType.ftBCD, TFieldType.ftLargeint,
                                          TFieldType.ftFMTBcd] then
                  JSonObject.AddPair(Fields[I].FieldName, TJSONNumber.Create(Fields[I].Value))
                else
                if Fields[I].DataType in [TFieldType.ftDate, TFieldType.ftDateTime, TFieldType.ftTimeStamp] then
                  JSonObject.AddPair(Fields[I].FieldName, TJSONString.Create(FormatDateTime('yyyy-MM-dd''T''HH:mm.zzz', Fields[I].Value) + RetornaTimeZone));
              end;
            end;
          end;

          JSonArray.Add(JSonObject);
          FDQryBD.Next;
        end;
        Result := JSonArray;
      end
      else
        Result := nil;
    except
      on E: Exception do
        SalvarLog('Executa script: ' + E.Message);
    end;
  finally
    ConLocal.Connected := False;
    ConBD.Connected    := False;
    FreeAndNil(ConLocal);
    FreeAndNil(ConBD);
    FreeAndNil(FDQryLocal);
    FreeAndNil(FDQryBD);
  end;
end;

function TdmScript.RetornaTimeZone: String;
var
  TimeZone: TTimeZoneInformation;
  Time: String;

  //yyyy-MM-dd'T'HH:mm:ss.SSSZ formatdo da hora
begin
  GetTimeZoneInformation(TimeZone);

  Time := IntToStr(TimeZone.Bias div 60);

  if Length(Time) = 1 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60) + '00'
  else
  if Length(Time) = 2 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60) + '0'
  else
  if Length(Time) = 3 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60)
  else
  if Length(Time) = 4 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-', '+') + FloatToStr(TimeZone.Bias div 60);

end;


end.
