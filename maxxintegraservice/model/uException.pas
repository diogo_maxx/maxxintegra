unit uException;

interface

uses
  Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, Web.HTTPApp, RzLstBox, RzChkLst, Vcl.ExtCtrls, RzPanel, RzEdit,
  Vcl.Mask, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxCheckBox,
  dxToggleSwitch, System.ImageList, Vcl.ImgList, acAlphaImageList, RzCmboBx, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxImageComboBox, IWSystem, dxActivityIndicator, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, RzButton, Vcl.Buttons, sSpeedButton, IPPeerClient, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, StrUtils, Json, FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef,
  FireDAC.Stan.ExprFuncs, IniFiles, FireDAC.Phys.FB, FireDAC.Phys.FBDef, JvComponentBase, JvThreadTimer, REST.Types,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, Vcl.DBCtrls, RzDBEdit, Vcl.ComCtrls, jsontreeview,
  RzTabs, jsondoc, acPNG, acImage, FireDAC.Phys.MSSQLDef, FireDAC.Phys.OracleDef, FireDAC.Phys.Oracle,
  FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL, RzBckgnd, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid;

  type TErrorFDConnection = class
    class procedure ConexaoError(ASender, AInitiator: TObject; var AException: Exception);
    class procedure ConexaoLost(Sender: TObject);
  end;

  type TErrorFDQuery = class
    class procedure PostErro(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
  end;


implementation

uses ufrmPrincipal, uLog;


class procedure TErrorFDConnection.ConexaoError(ASender, AInitiator: TObject; var AException: Exception);
begin
  SalvarLog('N�o foi poss�vel conectar ao Banco de Dados!');
  if Pos('the password has expired', AException.Message) > 0 then
    SalvarLog('Motivo: A senha do banco de dados est� expirada!')
  else
  if Pos('TNS:could not resolve the connect identifier specified', AException.Message) > 0 then
    SalvarLog('Motivo: Falha na configura��o do TNS. Contate o DBA.')
  else
    SalvarLog('Mensagem n�o tratada: ' + AException.Message);
end;


{ TErrorFDQuery }

class procedure TErrorFDQuery.PostErro(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  SalvarLog(DataSet.Name + ': ' + E.Message);
end;

class procedure TErrorFDConnection.ConexaoLost(Sender: TObject);
begin
  SalvarLog('A conex�o foi perdida!');
end;

end.
