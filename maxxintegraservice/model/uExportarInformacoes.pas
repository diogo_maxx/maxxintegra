unit uExportarInformacoes;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, IWSystem, StrUtils, Rest.JSON, System.JSON.Types, System.JSON.Writers, System.Json.Builders, Json,
  Data.DBXPlatform, Variants, System.Diagnostics, IPPeerClient, REST.Client, REST.Types,
  System.SyncObjs, Winapi.Windows, Vcl.Forms, uDatasetToJSON, uConexao, uMaxxREST;

type
  TExportarInformacoes = class
  private
    FQryConfiguracao   : TFDQuery;
    FQryBuscaBDCliente : TFDQuery;
    FQryMaxxIntegracao : TFDQuery;
    FQryConfJSON       : TFDQuery;
    FLimite            : Integer;
    FMaxxREST          : TMaxxRESTCliente;
    FDataSetToJSON     : TDatasetToJSON;
    Conexao            : TConexao;
    FLog: TStrings;

    function PrepararQrys: Boolean;
    procedure VerificaLimite;
    function PreencherFilhos(Tabela, FiltroPai: String; ID: Integer): TJSONValue;
    function FiltroFilho(DataSet: TDataSet; Campos: String; CamposPai: String): String;
    function GerarJSON(Tipo: String): String;
    function SincronizarEmpresaFilial: Boolean;
    procedure PrepararURLUD(SQL: String);
    procedure ConfigurarEnvio(Tipo: String);
    procedure LimparMemoria;
    function ConverterDataSet(FDBusca: TFDQuery; IDSistemas, IDConf: String; AFilhos : Array of TJsonFilho): TJSONValue;
  public
    property Log: TStrings read FLog write FLog;
    constructor Create; overload;
    destructor Destroy; override;
    function Iniciar: Boolean;
  end;



implementation

{ TExportarInformacoes }

uses uLog, ufrmPrincipal, uException;

constructor TExportarInformacoes.Create;
begin
  inherited Create;

  Conexao                   := TConexao.GetInstance;
  FQryConfiguracao          := TFDQuery.Create(nil);
  FQryBuscaBDCliente        := TFDQuery.Create(nil);
  FQryMaxxIntegracao        := TFDQuery.Create(nil);
  FQryConfJSON              := TFDQuery.Create(nil);
  FMaxxREST                 := TMaxxRESTCliente.GetInstance;

  FQryConfiguracao.Connection      := Conexao.SQLite;
  FQryConfJSON.Connection          := Conexao.SQLite;
  FQryBuscaBDCliente.Connection    := Conexao.BDCli;
  FQryMaxxIntegracao.Connection    := Conexao.BDCli;

  FQryMaxxIntegracao.CachedUpdates     := True;
  FQryMaxxIntegracao.FetchOptions.Mode := fmAll;

  FQryConfiguracao.DisableControls;
  FQryBuscaBDCliente.DisableControls;
//  FQryMaxxIntegracao.DisableControls;

  FQryMaxxIntegracao.OnPostError := TErrorFDQuery.PostErro;

end;

destructor TExportarInformacoes.Destroy;
begin
  inherited Destroy;
  FQryMaxxIntegracao.Close;
  FQryConfiguracao.Close;
  FQryConfJSON.Close;
  FQryBuscaBDCliente.Close;

  FreeAndNil(FQryMaxxIntegracao);
  FreeAndNil(FQryConfiguracao);
  FreeAndNil(FQryConfJSON);
  FreeAndNil(FQryBuscaBDCliente);
  FreeAndNil(FDataSetToJSON);
end;

function TExportarInformacoes.FiltroFilho(DataSet: TDataSet; Campos: String; CamposPai: String): String;
var
  Campo, CampoPai: TStringList;
  I: Integer;
begin
  try
    try
      Campo     := TStringList.Create;
      CampoPai  := TStringList.Create;

      Campo.StrictDelimiter := False;
      Campo.Delimiter       := ';';
      Campo.DelimitedText   := Campos;
      Campo.StrictDelimiter := True;

      CampoPai.StrictDelimiter := False;
      CampoPai.Delimiter       := ';';
      CampoPai.DelimitedText   := CamposPai;
      CampoPai.StrictDelimiter := True;


      Result := '';

      for I := 0 to Campo.Count -1 do
      begin
        if Campo[I] <> '' then
          Result := Result + ' and ' + Campo[I] + ' in(' + DataSet.FieldByName(CampoPai[I]).AsString + ')'
      end;
    except
      on E: Exception do
        SalvarLog('FiltroFilho: ' + E.Message);
    end;
  finally
    FreeAndNil(CampoPai);
    FreeAndNil(Campo);
    LimparMemoria;
  end;
end;

function TExportarInformacoes.Iniciar: Boolean;
begin
  try
    ////////////////////////////////////////////////////
    ///  Prepara as querys com as buscas nos bancos  ///
    ///  Caso haja registro pra sincronizar retorna  ///
    ///  true                                        ///
    ////////////////////////////////////////////////////
    if PrepararQrys then
    begin
      if not FQryMaxxIntegracao.IsEmpty then
      begin
        if not FMaxxREST.IniciarTransacao then
          Exit;
      end;

      if SincronizarEmpresaFilial then
      begin
        FQryMaxxIntegracao.First;
        while not FQryConfiguracao.Eof do
        begin
          if FQryConfiguracao.FieldByName('conf_tabelapai').AsString = '' then
          begin
            FQryMaxxIntegracao.Filtered := False;
            FQryMaxxIntegracao.Filter   := 'TABELA_REG LIKE ' + QuotedStr(FQryConfiguracao.FieldByName('CONF_TABELA').AsString);
            FQryMaxxIntegracao.Filtered := True;

            FMaxxREST.BaseURL  := '';
            FMaxxREST.EndPoint := '';

            ////////////////////////////////////////////////////
            ///  Se existir registros para esta configura��o ///
            ////////////////////////////////////////////////////
            FQryMaxxIntegracao.First;
            while not FQryMaxxIntegracao.Eof do
            begin
              if frmPrincipal.ParouServico then
                Exit;

              SalvarLog(GerarJSON('I'));
              SalvarLog(GerarJSON('A'));
              SalvarLog(GerarJSON('D'));

              FQryMaxxIntegracao.Next;
            end;
          end;
          FQryConfiguracao.Next;
        end;
      end;
    end;

    FQryMaxxIntegracao.Filtered := False;

    FQryBuscaBDCliente.Close;
    FQryBuscaBDCliente.SQL.Clear;

    case AnsiIndexStr(FQryBuscaBDCliente.Connection.Params.DriverID, ['FB']) of
      0: FQryBuscaBDCliente.SQL.Add('delete from maxx_integracao where enviado = ' + QuotedStr('S'));
    else
      begin
        FQryBuscaBDCliente.SQL.Add('delete from ' + frmPrincipal.PreencheSCHEMA + '.maxx_integracao where enviado = ' + QuotedStr('S'));
      end;
    end;

    FQryBuscaBDCliente.ExecSQL;

    if not FQryMaxxIntegracao.IsEmpty then
      SalvarLog('Sincroniza��o finalizada!');

    Result := True;

    LimparMemoria;
  except
    on E: Exception do
    begin
      Result := False;
      SalvarLog('Iniciar: ' + E.Message);
    end;
  end;
end;

procedure TExportarInformacoes.LimparMemoria;
var
  MainHandle : THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false,
    GetCurrentProcessID) ;
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(MainHandle) ;
  except
  end;
  Application.ProcessMessages;
end;

function TExportarInformacoes.GerarJSON(Tipo: String): String;
var
  Registros, I, J         : Integer;
  Campos, Valores, Filtro : TStringList;
  FiltroMontado, SQL      : String;
  FQryFilho               : TFDQuery;
  AFilho                  : Array of TJsonFilho;
  JsonValue, JsonEnvio    : TJSONValue;
  InserirLog              : TLog;
begin
  try
    try
      FQryFilho            := TFDQuery.Create(nil);
      FQryFilho.Connection := Conexao.SQLite;

      if frmPrincipal.ParouServico then
        Exit;

      FQryMaxxIntegracao.Filtered := False;
      FQryMaxxIntegracao.Filter   := 'TABELA_REG LIKE ' + QuotedStr(FQryConfiguracao.FieldByName('CONF_TABELA').AsString) + ' and TIPO_MOD = ' + QuotedStr(Tipo);
      FQryMaxxIntegracao.Filtered := True;

      if not FQryMaxxIntegracao.IsEmpty then
      begin
        if frmPrincipal.ParouServico then
          Exit;

        ConfigurarEnvio(Tipo);

        FQryMaxxIntegracao.First;

        Registros := FQryMaxxIntegracao.RecordCount;

        Campos := TStringList.Create;
        Campos.StrictDelimiter := False;
        Campos.Delimiter       := ';';
        Campos.DelimitedText   := FQryConfiguracao.FieldByName('conf_codigo').AsString;
        Campos.StrictDelimiter := True;

        while (Registros > 0) do
        begin
          if FQryMaxxIntegracao.Eof then
            Break;

          if frmPrincipal.ParouServico then
            Break;

          FiltroMontado := '';
          SQL           := '';
          Filtro        := TStringList.Create;
          if FLimite > 1 then
               JsonEnvio := TJSONArray.Create
          else JsonEnvio := TJSONValue.Create;
          ////////////////////////////////////
          ///  Criando filtros             ///
          ////////////////////////////////////
//          for I := 0 to Pred(FLimite) do
//          begin
//            if not FQryMaxxIntegracao.Eof then
//            begin
//              Valores                 := TStringList.Create;
//              Valores.StrictDelimiter := False;
//              Valores.Delimiter       := ';';
//              Valores.DelimitedText   := FQryMaxxIntegracao.FieldByName('CODIGO_REG').AsString;
//              Valores.StrictDelimiter := True;
//
//              for J := 0 to Pred(Campos.Count) do
//              begin
//                if Filtro.Count < Campos.Count then
//                  Filtro.Add(QuotedStr(Valores[J]))
//                else
//                  Filtro[J] := Filtro[J] + ', ' + QuotedStr(Valores[J]);
//              end;
//
//              FreeAndNil(Valores);
//
//              FQryMaxxIntegracao.Edit;
//              FQryMaxxIntegracao.FieldByName('ENVIADO').AsString := 'S';
//              FQryMaxxIntegracao.Post;
//
//              FQryMaxxIntegracao.Next;
//            end
//            else Break;
//          end;

          Valores                 := TStringList.Create;
          Valores.StrictDelimiter := False;
          Valores.Delimiter       := ';';
          Valores.DelimitedText   := FQryMaxxIntegracao.FieldByName('CODIGO_REG').AsString;
          Valores.StrictDelimiter := True;

          for J := 0 to Pred(Campos.Count) do
          begin
            if Filtro.Count < Campos.Count then
              Filtro.Add(QuotedStr(Valores[J]))
            else
              Filtro[J] := Filtro[J] + ', ' + QuotedStr(Valores[J]);
          end;

          FreeAndNil(Valores);

          FQryMaxxIntegracao.Edit;
          FQryMaxxIntegracao.FieldByName('ENVIADO').AsString := 'S';
          ////////////////////////////////////
          ///  Montando filtros            ///
          ////////////////////////////////////
          for I := 0 to Pred(Campos.Count) do
            FiltroMontado := FiltroMontado + ' and ' + Campos[I] + ' in (' + Filtro[I] + ')';

          //////////////////////////////////////////////
          ///  Realizando a busca no banco de dados  ///
          //////////////////////////////////////////////
          SQL := FQryConfiguracao.FieldByName('conf_scriptbusca').AsString;
          SQL := StringReplace(SQL, '&SCHEMA', frmPrincipal.PreencheSCHEMA, [rfReplaceAll, rfIgnoreCase]);
          SQL := StringReplace(SQL, '&FILTRO', FiltroMontado, [rfReplaceAll, rfIgnoreCase]);

          FQryBuscaBDCliente.Close;
          FQryBuscaBDCliente.SQL.Clear;
          FQryBuscaBDCliente.SQL.Add(SQL);
          FQryBuscaBDCliente.Open;

          FQryBuscaBDCliente.First;
          while not FQryBuscaBDCliente.Eof do
          begin
            if frmPrincipal.ParouServico then
             Exit;
            ////////////////////////////////////
            ///  Verificando se tem filhos   ///
            ////////////////////////////////////
            FQryFilho.Close;
            FQryFilho.SQL.Clear;
            FQryFilho.SQL.Add('select * from sistemasintegrados_conf where conf_tabelapai like ' +
                              QuotedStr(FQryConfiguracao.FieldByName('conf_tabela').AsString) +
                              ' and idconf <> ' + IntToStr(FQryConfiguracao.FieldByName('idconf').AsInteger));
            FQryFilho.Open;

            if not FQryFilho.IsEmpty then
            begin
              SetLength(AFilho, FQryFilho.RecordCount);

              FQryFilho.First;
              while not FQryFilho.Eof do
              begin
                AFilho[FQryFilho.RecNo -1].JsonFilho := TJSONValue.Create;

                AFilho[FQryFilho.RecNo -1].Nome      := ifthen(FQryFilho.FieldByName('conf_nomejson').AsString <> '',
                                                               FQryFilho.FieldByName('conf_nomejson').AsString,
                                                               FQryFilho.FieldByName('conf_tabela').AsString);

                AFilho[FQryFilho.RecNo -1].JsonFilho := PreencherFilhos(FQryFilho.FieldByName('conf_tabela').AsString,
                                                        FiltroFilho(FQryBuscaBDCliente,
                                                                    FQryFilho.FieldByName('conf_codigopai').AsString,
                                                                    FQryConfiguracao.FieldByName('conf_codigosql').AsString),
                                                               FQryFilho.FieldByName('idconf').AsInteger);


                FQryFilho.Next;
              end;
            end;

            if FMaxxREST.Metodo = TRESTRequestMethod.rmPUT then
              PrepararURLUD(SQL);

            if FQryConfiguracao.FieldByName('conf_encarray').AsString = 'S' then
                 TJSONArray(JsonEnvio).AddElement(ConverterDataSet(FQryBuscaBDCliente, FQryConfiguracao.FieldByName('idsistemasintegrados').AsString,
                                                                   FQryConfiguracao.FieldByName('idconf').AsString, AFilho))
            else JsonEnvio := ConverterDataSet(FQryBuscaBDCliente, FQryConfiguracao.FieldByName('idsistemasintegrados').AsString,
                                               FQryConfiguracao.FieldByName('idconf').AsString, AFilho);

            FQryBuscaBDCliente.Next;
          end;

          ///////////////////////////////////////////
          ///  Gerando LOG                        ///
          ///////////////////////////////////////////
          InserirLog                 := TLog.Create;
          InserirLog.Tabela          := FQryConfiguracao.FieldByName('TPSCR_DESCRICAO').AsString;
          InserirLog.Registro        := FQryMaxxIntegracao.FieldByName('codigo_reg').AsString;
          InserirLog.HoraSincronismo := Now;
          InserirLog.HoraIntegrado   := FQryMaxxIntegracao.FieldByName('DATA').AsDateTime;
          InserirLog.JSONEnvio       := JsonEnvio.ToString;

          case AnsiIndexStr(Tipo, ['I', 'A', 'D']) of
            0: InserirLog.TipoRequisicao := TTipoRequisicao.tprINSERT;
            1: InserirLog.TipoRequisicao := TTipoRequisicao.tprUPDATE;
            2: InserirLog.TipoRequisicao := TTipoRequisicao.tprDELETE;
          end;

          FMaxxREST.Enviar(JsonEnvio);

          InserirLog.StatusCod   := FMaxxREST.StatusCode;
          if FMaxxREST.Response.JSONValue <> nil then
            InserirLog.JSONRetorno := FMaxxREST.Response.JSONValue.ToString;
          InserirLog.URL         := FMaxxREST.Response.FullRequestURI;

          for I := 0 to Pred(FMaxxREST.Request.Params.Count) do
          begin
            if FMaxxREST.Request.Params[I].Kind = TRESTRequestParameterKind.pkHTTPHEADER then
              InserirLog.Parametros.Add(FMaxxREST.Request.Params[I].name + ': ' + FMaxxREST.Request.Params[I].Value);
          end;

          InserirLog.Parametros.Add(FMaxxREST.Response.Headers.Text);

          case FMaxxREST.StatusCode of
            200,201: // OK, created
            begin
              SalvarLog('Sincronismo da tabela: ' + FQryConfiguracao.FieldByName('TPSCR_DESCRICAO').AsString + ' | registro: ' +
                                                    FQryMaxxIntegracao.FieldByName('codigo_reg').AsString + ' ' +
                                                    ifthen(Tipo = 'I', 'inserido.', ifthen(Tipo = 'A', 'alterado.', 'deletado')), nil, True, False);

              InserirLog.Mensagem := ifthen(Tipo = 'I', 'Inserido', ifthen(Tipo = 'A', 'Alterado', 'Deletado')) + ' com sucesso!';
              InserirLog.Erro     := False;

              FQryMaxxIntegracao.Post;
              FQryMaxxIntegracao.Next;
            end;
//            409: //Conflict
//            begin
//              SalvarLog('Modificando sincronismo da tabela: ' + FQryConfiguracao.FieldByName('TPSCR_DESCRICAO').AsString + ' | registro: ' +
//                                                                FQryMaxxIntegracao.FieldByName('codigo_reg').AsString + ' para altera��o', nil, True, False);
//
//              FQryMaxxIntegracao.Edit;
//              FQryMaxxIntegracao.FieldByName('TIPO_MOD').AsString := 'A';
//              FQryMaxxIntegracao.FieldByName('ENVIADO').AsString  := 'N';
//              FQryMaxxIntegracao.Post;
//
//              InserirLog.Erro     := True;
//              InserirLog.Mensagem := 'Modificado para altera��o';
//            end;
            400,401: //Bad request, not authorized
            begin
              SalvarLog('Falha na requisi��o.');
              InserirLog.Mensagem := 'Falha na requisi��o';
              InserirLog.Erro     := True;
              InserirLog.GravarLog;

              FQryMaxxIntegracao.Cancel;
              Exit;
            end;
            404: //NOT FOUND
            begin
              SalvarLog('Falha na requisi��o. StatusCode: 404');
              InserirLog.Mensagem := 'Falha na requisi��o';
              InserirLog.Erro     := True;

              FQryMaxxIntegracao.FieldByName('ENVIADO').AsString := 'E';

//              FQryMaxxIntegracao.Cancel;
              FQryMaxxIntegracao.Post;
              FQryMaxxIntegracao.Next;
            end;
            500: //Internal server error
            begin
              SalvarLog('Falha na requisi��o. Houve uma situa��o n�o tratada no servidor de terceiros.');
              InserirLog.Mensagem := 'Falha na requisi��o. Houve uma situa��o n�o tratada no servidor de terceiros.';
              InserirLog.Erro     := True;
              InserirLog.JSONRetorno := FMaxxREST.Response.JSONText;

              FQryMaxxIntegracao.FieldByName('ENVIADO').AsString := 'N';
              FQryMaxxIntegracao.Post;
              FQryMaxxIntegracao.Next;
            end
          else
            begin
              InserirLog.Erro     := False;
              InserirLog.Mensagem := 'N�o tratado';
              FQryMaxxIntegracao.Post;
              FQryMaxxIntegracao.Next;
            end;
          end;
          InserirLog.GravarLog;
          FreeAndNil(InserirLog);

          Dec(Registros);

          LimparMemoria;
        end;
        if not frmPrincipal.ParouServico then
        begin
          FQryMaxxIntegracao.ApplyUpdates(0);
          Result := ' - Sincronizado(s): (' + IntToStr(FQryMaxxIntegracao.RecordCount) + ') registro(s)';
        end;
      end;
    except
      on E: Exception do
        SalvarLog('GerarJSON: ' + E.Message);
    end;
  finally
    FQryBuscaBDCliente.Close;

    FQryFilho.Close;
    FreeAndNil(FQryFilho);
    FreeAndNil(JsonValue);
    FreeAndNil(JsonEnvio);
    FreeAndNil(Campos);
    FreeAndNil(Valores);
    FreeAndNil(Filtro);

    LimparMemoria;
  end;
end;

function TExportarInformacoes.PreencherFilhos(Tabela, FiltroPai: String; ID: Integer): TJSONValue;
var
  FBusca, FConf, FConfFilho: TFDQuery;
  JsonArray: TJSONArray;
  JsonValue: TJSONValue;
  SQL : String;
  AFilho : Array of TJsonFilho;
  I: Integer;
begin
  try
    try
      FBusca     := TFDQuery.Create(nil);
      FConf      := TFDQuery.Create(nil);
      FConfFilho := TFDQuery.Create(nil);
      JsonArray  := TJSONArray.Create;
      JsonValue  := TJSONValue.Create;

      FConf.Connection      := Conexao.SQLite;
      FConfFilho.Connection := Conexao.SQLite;
      FBusca.Connection     := Conexao.BDCli;

      FConf.Close;
      FConf.SQL.Clear;
      FConf.SQL.Add('select * from sistemasintegrados_conf where idconf = ' + IntToStr(ID));
      FConf.Open;

      SQL := '';
      SQL := FConf.FieldByName('conf_scriptbusca').AsString;
      SQL := StringReplace(SQL, '&SCHEMA', frmPrincipal.PreencheSCHEMA, [rfReplaceAll, rfIgnoreCase]);
      SQL := StringReplace(SQL, '&FILTRO', FiltroPai, [rfReplaceAll, rfIgnoreCase]);

      FBusca.Close;
      FBusca.SQL.Clear;
      FBusca.SQL.Add(SQL);
      FBusca.Open;

      FBusca.First;
      while not FBusca.Eof do
      begin
        ////////////////////////////////////
        ///  Verificando se tem filhos   ///
        ////////////////////////////////////
        FConfFilho.Close;
        FConfFilho.SQL.Clear;
        FConfFilho.SQL.Add('select * from sistemasintegrados_conf where conf_tabelapai like ' +
                          QuotedStr(FConf.FieldByName('conf_tabela').AsString) + ' and idconf <> ' + IntToStr(ID));
        FConfFilho.Open;

        if not FConfFilho.IsEmpty then
        begin
          SetLength(AFilho, FConfFilho.RecordCount);

          FConfFilho.First;
          while not FConfFilho.Eof do
          begin
            AFilho[FConfFilho.RecNo -1].JsonFilho := TJSONValue.Create;

            AFilho[FConfFilho.RecNo -1].Nome      := ifthen(FConfFilho.FieldByName('conf_nomejson').AsString <> '',
                                                            FConfFilho.FieldByName('conf_nomejson').AsString,
                                                            FConfFilho.FieldByName('conf_tabela').AsString);

            AFilho[FConfFilho.RecNo -1].JsonFilho := PreencherFilhos(FConfFilho.FieldByName('conf_tabela').AsString,
                                                         FiltroFilho(FBusca,
                                                                     FConfFilho.FieldByName('conf_codigopai').AsString,
                                                                     FConf.FieldByName('conf_codigosql').AsString),
                                                                     FConfFilho.FieldByName('idconf').AsInteger);


            FConfFilho.Next;
          end;
        end;

        if (FBusca.RecordCount > 1) then
           JsonArray.AddElement(ConverterDataSet(FBusca, FConf.FieldByName('idsistemasintegrados').AsString,
                                                         FConf.FieldByName('idconf').AsString, AFilho))
        else JsonValue := ConverterDataSet(FBusca, FConf.FieldByName('idsistemasintegrados').AsString,
                                                         FConf.FieldByName('idconf').AsString, AFilho);

        FBusca.Next;
      end;
      if (FBusca.RecordCount > 1) or (FConf.FieldByName('conf_encarray').AsString = 'S') then
      begin
        if JsonArray.Count = 0 then
          JsonArray.AddElement(JsonValue);

        Result := JsonArray;
      end
      else
        Result := JsonValue;
    except
      on E: Exception do
        SalvarLog('PreencherFilhos: ' + E.Message);
    end;
  finally
    FBusca.Close;
    FConf.Close;
    FConfFilho.Close;

    FreeAndNil(FBusca);
    FreeAndNil(FConf);
    FreeAndNil(FConfFilho);
    LimparMemoria;
  end;
end;

function TExportarInformacoes.PrepararQrys: Boolean;
begin
  try
    Result := True;

    FQryConfiguracao.Close;
    FQryConfiguracao.SQL.Clear;
    FQryConfiguracao.SQL.Add('select * from sistemasintegrados_conf order by conf_prioridade, conf_tabela');
    FQryConfiguracao.Open;

    FQryMaxxIntegracao.Close;
    FQryMaxxIntegracao.SQL.Clear;

    case AnsiIndexStr(FQryBuscaBDCliente.Connection.Params.DriverID, ['FB']) of
      0: FQryMaxxIntegracao.SQL.Add('select * from maxx_integracao where enviado = ' + QuotedStr('N'))
    else
      begin
        FQryMaxxIntegracao.SQL.Add('select * from ' + frmPrincipal.PreencheSCHEMA + '.maxx_integracao where enviado = ' + QuotedStr('N'));
      end;
    end;

    FQryMaxxIntegracao.Open;

    FQryConfiguracao.First;
    FQryMaxxIntegracao.First;

    Result := not FQryMaxxIntegracao.IsEmpty;
  except
    on E: Exception do
    begin
      SalvarLog('PrepararQrys: ' + E.Message);
      Result := False;
    end;
  end;
end;

procedure TExportarInformacoes.PrepararURLUD(SQL: String);
var
  I: Integer;
  Campo: String;
  MaxxREST: TMaxxRESTCliente;
  FDQry: TFDQuery;
begin
  try
    FDQry               := TFDQuery.Create(nil);
    FDQry.Connection    := Conexao.BDCli;
    MaxxREST            := TMaxxRESTCliente.GetInstance;
    MaxxREST.UrlNomes   := '';
    MaxxREST.URLValores := '';

    ////////////////////////////////////////////
    ///  Preparando os nomes dos parametros  ///
    ////////////////////////////////////////////
    I := 0;
    while I <= Length(MaxxREST.EndPoint) do
    begin
      if Copy(MaxxREST.EndPoint, I, 1) = '{' then
      begin
        Campo := '';
        Inc(I);
        while (Copy(MaxxREST.EndPoint, I, 1) <> '}') or (I <= Length(MaxxREST.UrlNomes)) do
        begin
          Campo := Campo + Copy(MaxxREST.EndPoint, I, 1);
          Inc(I);
        end;

        if MaxxREST.UrlNomes <> '' then
          MaxxREST.UrlNomes := MaxxREST.UrlNomes + ';';

        MaxxREST.UrlNomes := MaxxREST.UrlNomes + Campo;
      end;
      Inc(I);
    end;
    /////////////////////////////////////////////
    ///  Preenchendo os nomes dos parametros  ///
    /////////////////////////////////////////////
    FDQry.Close;
    FDQry.SQL.Clear;
    FDQry.SQL.Add(SQL);
    FDQry.Open;

    I     := 1;
    Campo := '';

    if MaxxREST.UrlNomes <> '' then
    begin
      while I <= Length(MaxxREST.UrlNomes) do
      begin
        Campo := '';

        if Pos(';', MaxxREST.UrlNomes) > 0 then
        begin
          while (Copy(MaxxREST.UrlNomes, I, 1) <> ';') or (I <= Length(MaxxREST.UrlNomes)) do
          begin
            Campo := Campo + Copy(MaxxREST.UrlNomes, I, 1);
            Inc(I);
          end;
        end
        else
        begin
          while (I <= Length(MaxxREST.UrlNomes)) do
          begin
            Campo := Campo + Copy(MaxxREST.UrlNomes, I, 1);
            Inc(I);
          end;
        end;

        if MaxxREST.URLValores <> '' then
          MaxxREST.URLValores := MaxxREST.URLValores + ';';

        MaxxREST.URLValores := MaxxREST.URLValores + FDQry.FieldByName(Campo).AsString;
        Inc(I);
      end;
    end;
  finally
    FDQry.Close;
    FreeAndNil(FDQry);
  end;
end;

function TExportarInformacoes.SincronizarEmpresaFilial: Boolean;

function RetornaTimeZone: String;
var
  TimeZone: TTimeZoneInformation;
  Time: String;
begin
  GetTimeZoneInformation(TimeZone);

  Time := IntToStr(TimeZone.Bias div 60);
  if Length(Time) = 1 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60) + '00'
  else
  if Length(Time) = 2 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60) + '0'
  else
  if Length(Time) = 3 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60)
  else
  if Length(Time) = 4 then
     Result := IfThen((TimeZone.Bias * -1) < 0, '-', '+') + FloatToStr(TimeZone.Bias div 60);
end;

var
  JsonObject, JsonEndereco: TJSONObject;
begin
  try
    Result := True;

    try
      with frmPrincipal do
      begin
        SalvarLog('Iniciando sincronismo de EMPRESA/FILIAL...');


        FDEmpresa.First;
        ////////////////////////////////////////////
        ///  configura��o de envio para EMPRESA  ///
        ////////////////////////////////////////////
        while not FDEmpresa.Eof do
        begin
          FQryConfiguracao.Filtered := False;
          FQryConfiguracao.Filter   := 'TPSCR_DESCRICAO LIKE ' + QuotedStr('EMPRESA');
          FQryConfiguracao.Filtered := True;

          JsonObject   := TJSONObject.Create;
          JsonEndereco := TJSONObject.Create;

          with JsonEndereco do
          begin
            AddPair('estado',      FDEmpresaESTADO.AsString);
            AddPair('municipio',   FDEmpresaCIDADE.AsString);
            AddPair('bairro',      FDEmpresaBAIRRO.AsString);
            AddPair('descricao',   FDEmpresaLOGRADOURO.AsString);
            AddPair('cep',         FDEmpresaCEP.AsString);
            AddPair('numero',      FDEmpresaNUMERO.AsString);
            AddPair('complemento', FDEmpresaCOMPLEMENTO.AsString);
          end;

          with JsonObject do
          begin
            AddPair('sync',         FDEmpresaCODEMP.AsString);
            AddPair('razao',        FDEmpresaRAZAO.AsString);
            AddPair('fantasia',     FDEmpresaFANTASIA.AsString);
            AddPair('endereco',     JsonEndereco);
            AddPair('cnpj',         FDEmpresaCPFCNPJ.AsString);
            AddPair('inscEstadual', FDEmpresaIE.AsString);
            AddPair('telefone',     FDEmpresaTELEFONE.AsString);
            AddPair('email',        FDEmpresaEMAIL.AsString);
            AddPair('dtSync',       TJSONString.Create(FormatDateTime('yyyy-MM-dd''T''HH:mm:ss.zzz', Now) + RetornaTimeZone));
          end;

          FMaxxREST.BaseURL  := FQryConfiguracao.FieldByName('conf_baseurl').AsString;
          FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpoint').AsString;
          FMaxxREST.Metodo   := TRESTRequestMethod.rmPOST;

          if not FMaxxREST.Enviar(JsonObject) then
          begin
            case FMaxxREST.StatusCode of
              409: //conflito
              begin
                FMaxxREST.BaseURL  := FQryConfiguracao.FieldByName('conf_baseurl').AsString;
                FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpointupdate').AsString;
                FMaxxREST.Metodo   := TRESTRequestMethod.rmPUT;

                FMaxxREST.URLValores := FDEmpresaCODEMP.AsString;
                FMaxxREST.UrlNomes   := 'sync';

                if FMaxxREST.Enviar(JsonObject) then
                  SalvarLog('Sincronismo da tabela: EMPRESA | registro: ' + FDEmpresaCODEMP.AsString + ' atualizado');
              end;
            end;
          end
          else
            SalvarLog('Sincronismo da tabela: EMPRESA | registro: ' + FDEmpresaCODEMP.AsString + ' inserido');
          ////////////////////////////////////////////
          ///  configura��o de envio para FILIAL   ///
          ////////////////////////////////////////////
          FQryConfiguracao.Filtered := False;
          FQryConfiguracao.Filter   := 'TPSCR_DESCRICAO LIKE ' + QuotedStr('FILIAL');
          FQryConfiguracao.Filtered := True;

          if not FQryConfiguracao.IsEmpty then
          begin
            FDFilial.First;

            while not FDFilial.Eof do
            begin
              JsonObject   := TJSONObject.Create;
              JsonEndereco := TJSONObject.Create;

              with JsonEndereco do
              begin
                AddPair('estado',      FDFilialESTADO.AsString);
                AddPair('municipio',   FDFilialCIDADE.AsString);
                AddPair('bairro',      FDFilialBAIRRO.AsString);
                AddPair('descricao',   FDFilialLOGRADOURO.AsString);
                AddPair('cep',         FDFilialCEP.AsString);
                AddPair('numero',      FDFilialNUMERO.AsString);
                AddPair('complemento', FDFilialCOMPLEMENTO.AsString);
              end;

              with JsonObject do
              begin
                AddPair('sync',         FDFilialCODFILIAL.AsString);
                AddPair('matriz',       FDEmpresaCODEMP.AsString);
                AddPair('razao',        FDFilialRAZAO.AsString);
                AddPair('fantasia',     FDFilialFANTASIA.AsString);
                AddPair('endereco',     JsonEndereco);
                AddPair('cnpj',         FDFilialCPFCNPJ.AsString);
                AddPair('inscEstadual', FDFilialIE.AsString);
                AddPair('telefone',     FDFilialTELEFONE.AsString);
                AddPair('email',        FDFilialEMAIL.AsString);
                AddPair('dtSync',       TJSONString.Create(FormatDateTime('yyyy-MM-dd''T''HH:mm:ss.zzz', Now) + RetornaTimeZone));
              end;

              FMaxxREST.BaseURL  := FQryConfiguracao.FieldByName('conf_baseurl').AsString;
              FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpoint').AsString;
              FMaxxREST.Metodo   := TRESTRequestMethod.rmPOST;

              if not FMaxxREST.Enviar(JsonObject) then
              begin
                case FMaxxREST.StatusCode of
                  409: //conflito
                  begin
                    FMaxxREST.BaseURL  := FQryConfiguracao.FieldByName('conf_baseurl').AsString;
                    FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpointupdate').AsString;
                    FMaxxREST.Metodo   := TRESTRequestMethod.rmPUT;

                    FMaxxREST.URLValores := FDFilialCODFILIAL.AsString;
                    FMaxxREST.UrlNomes   := 'sync';

                    if FMaxxREST.Enviar(JsonObject) then
                      SalvarLog('Sincronismo da tabela: FILIAL | registro: ' + FDFilialCODFILIAL.AsString + ' atualizado');
                  end;
                end;
              end
              else
                SalvarLog('Sincronismo da tabela: FILIAL | registro: ' + FDFilialCODFILIAL.AsString + ' inserido');
              FDFilial.Next;
            end;
          end;
          FDEmpresa.Next;
        end;
      end;
    except
      on E: Exception do
      begin
        SalvarLog('SincronizarEmpresaFilial: ' + E.Message);
        Result := False;
      end;
    end;
  finally
    FQryConfiguracao.Filtered := False;
    FQryConfiguracao.Filter   := '';
  end;
end;

procedure TExportarInformacoes.VerificaLimite;
begin
  ////////////////////////////////////////////////////////////////
  ///  Verificar se existe algum limite pra envio no endpoint  ///
  ////////////////////////////////////////////////////////////////
  try
    FLimite := 1;
//    if FQryConfiguracao.FieldByName('conf_maxqntpost').AsString <> '' then
//    begin
//      //cria objeto
//      FMaxxREST.BaseURL  := FQryConfiguracao.FieldByName('conf_baseurl').AsString;
//      FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_maxqntpost').AsString;
//      FMaxxREST.Metodo   := TRESTRequestMethod.rmGET;
//      FLimite            := FMaxxREST.QuantPost;
//    end;
  except

  end;
end;

procedure TExportarInformacoes.ConfigurarEnvio(Tipo: String);
begin
  try
    FLimite := 1;

    FMaxxREST.BaseURL  := FQryConfiguracao.FieldByName('conf_baseurl').AsString;

    case AnsiIndexStr(Tipo, ['I', 'A', 'D']) of
      0:
      begin
        VerificaLimite;

        FLog.Add(FQryConfiguracao.FieldByName('TPSCR_DESCRICAO').AsString + ': ' + IntToStr(FQryMaxxIntegracao.RecordCount) + ' inser��es');
        FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpoint').AsString;
        FMaxxREST.Metodo   := TRESTRequestMethod.rmPOST;

        if FLimite > 1 then
        begin
          if Copy(FMaxxREST.EndPoint, Length(FMaxxREST.EndPoint), 1) = '/' then
            FMaxxREST.EndPoint := FMaxxREST.EndPoint + 'synchronization'
          else
            FMaxxREST.EndPoint := FMaxxREST.EndPoint + '/synchronization';

          FMaxxREST.Metodo   := TRESTRequestMethod.rmPUT;
        end;
      end;
      1:
      begin
        FLog.Add(FQryConfiguracao.FieldByName('TPSCR_DESCRICAO').AsString + ': ' + IntToStr(FQryMaxxIntegracao.RecordCount) + ' altera��es');
        FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpointupdate').AsString;
        FMaxxREST.Metodo   := TRESTRequestMethod.rmPUT;
//        PrepararParametrosUD(FQryConfiguracao.FieldByName('conf_scriptbusca').AsString);
      end;
      2:
      begin
        FLog.Add(FQryConfiguracao.FieldByName('TPSCR_DESCRICAO').AsString + ': ' + IntToStr(FQryMaxxIntegracao.RecordCount) + ' exclus�es');
        FMaxxREST.EndPoint := FQryConfiguracao.FieldByName('conf_endpointdelete').AsString;
        FMaxxREST.Metodo   := TRESTRequestMethod.rmDELETE;
//        PrepararParametrosUD(FQryConfiguracao.FieldByName('conf_scriptbusca').AsString);
      end;
    end;
  except
    on E: Exception do
     SalvarLog('ConfigurarEnvio: ' + E.Message);
  end;
end;

function TExportarInformacoes.ConverterDataSet(FDBusca: TFDQuery; IDSistemas, IDConf: String; AFilhos: array of TJsonFilho): TJSONValue;
begin
  ////////////////////////////////////////////
  ///  Convertendo o dataset em json       ///
  ///  e abrindo configura��o dos campos   ///
  ////////////////////////////////////////////
  FDataSetToJson                := TDatasetToJSON.Create(False, AFilhos);
  FDataSetToJson.FormatoData    := 'yyyy-MM-dd''T''HH:mm:ss.zzz';
  FDataSetToJson.IncluiTimeZone := True;

  FQryConfJSON.Close;
  FQryConfJSON.SQL.Clear;
  FQryConfJSON.SQL.Add('SELECT * FROM sqlite_master where tbl_name = ' + QuotedStr('conf_json'));
  FQryConfJSON.Open;

  if not FQryConfJSON.IsEmpty then
  begin
    FQryConfJSON.Close;
    FQryConfJSON.SQL.Clear;
    FQryConfJSON.SQL.Add('SELECT * FROM conf_json ' +
                         'where idsistemasintegrados = ' + IDSistemas +
                         '  and idconf = '               + IDConf);
    FQryConfJSON.Open;

    Result := FDataSetToJson.CriaJson(FDBusca, FQryConfJSON);
  end
  else
    Result := FDataSetToJson.CriaJson(FDBusca);
end;

end.
