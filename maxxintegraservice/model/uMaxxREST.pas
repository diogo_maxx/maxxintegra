unit uMaxxREST;

interface

uses REST.Client, REST.Types, Data.Bind.Components, Data.Bind.ObjectScope, SysUtils, JSON, REST.JSON, System.Classes, JSONTreeView, jsondoc;

type TMaxxRESTCliente = class
  strict protected
     class var MaxxREST : TMaxxRESTCliente; //singleton
  private
    FRClient: TRESTClient;
    FRRequest: TRESTRequest;
    FRResponse: TRESTResponse;
    FMetodo: TRESTRequestMethod;
    FToken: String;
    FBaseURL: String;
    FEndPoint: String;
    FEndPoint_Login: String;
    FBaseURL_Login: String;
    FPassword: String;
    FUser: String;
    FInTransaction: Boolean;
    FURLValores: String;
    FUrlNomes: String;
    FStatusCode: Integer;
    FSucesso: Boolean;

    constructor CreatePrivate;
    destructor Destroy; override;

    procedure SetBaseURL(const Value: String);
    procedure SetEndPoint(const Value: String);
    procedure SetMetodo(const Value: TRESTRequestMethod);
    function Configurar(Login: Boolean = False): Boolean;
    procedure SetBaseURL_Login(const Value: String);
    procedure SetEndPoint_Login(const Value: String);
    procedure SetPassword(const Value: String);
    procedure SetUser(const Value: String);
    procedure SetURLValores(const Value: String);
    procedure SetUrlNomes(const Value: String);
    procedure SetStatusCode(const Value: Integer);
  public
    property Metodo : TRESTRequestMethod read FMetodo     write SetMetodo default TRESTRequestMethod.rmGET;
    property BaseURL: String             write SetBaseURL;
    property EndPoint: String            read FEndPoint   write SetEndPoint;
    property BaseURL_Login: String       write SetBaseURL_Login;
    property EndPoint_Login: String      write SetEndPoint_Login;
    property User: String                write SetUser;
    property Password: String            write SetPassword;
    property URLValores: String          read FURLValores write SetURLValores;
    property UrlNomes: String            read FUrlNomes   write SetUrlNomes;
    property StatusCode: Integer         read FStatusCode write SetStatusCode;
    property Response: TRESTResponse     read FRResponse;
    property Request: TRESTRequest       read FRRequest;

    class function GetInstance: TMaxxRESTCliente;
    constructor Create;

    function Enviar(jObj: TJSONValue):Boolean;
    function QuantPost: Integer;
    function IniciarTransacao: Boolean;
    function BuscarConfiguracoes: TJSONValue;
    procedure FecharTransacao;
    function BuscarCEP(CEP: String): TJSONValue;
end;

implementation

{ TMaxxREST }

uses uLog;
constructor TMaxxRESTCliente.Create;
begin
  raise Exception.Create('Utilize GetInstance para criar o objeto!');
end;

constructor TMaxxRESTCliente.CreatePrivate;
begin
  inherited Create;
  FRClient   := TRESTClient.Create(nil);
  FRRequest  := TRESTRequest.Create(nil);
  FRResponse := TRESTResponse.Create(nil);

  FRRequest.Client   := FRClient;
  FRRequest.Response := FRResponse;
end;

destructor TMaxxRESTCliente.Destroy;
begin
  inherited Destroy;
  FreeAndNil(FRClient);
  FreeAndNil(FRRequest);
  FreeAndNil(FRResponse);

end;

class function TMaxxRESTCliente.GetInstance: TMaxxRESTCliente;
begin
  if not Assigned(MaxxREST) then
    MaxxREST := TMaxxRESTCliente.CreatePrivate;

  Result := MaxxREST;
end;

procedure TMaxxRESTCliente.SetBaseURL(const Value: String);
begin
  FBaseURL := Value;
end;

procedure TMaxxRESTCliente.SetBaseURL_Login(const Value: String);
begin
  FBaseURL_Login := Value;
end;

procedure TMaxxRESTCliente.SetEndPoint(const Value: String);
begin
  FEndPoint := Value;
end;

procedure TMaxxRESTCliente.SetEndPoint_Login(const Value: String);
begin
  FEndPoint_Login := Value;
end;

procedure TMaxxRESTCliente.SetMetodo(const Value: TRESTRequestMethod);
begin
  FMetodo := Value;
end;

procedure TMaxxRESTCliente.SetPassword(const Value: String);
begin
  FPassword := Value;
end;


procedure TMaxxRESTCliente.SetStatusCode(const Value: Integer);
begin
  FStatusCode := Value;
end;

procedure TMaxxRESTCliente.SetUrlNomes(const Value: String);
begin
  FUrlNomes := Value;
end;

procedure TMaxxRESTCliente.SetURLValores(const Value: String);
begin
  FURLValores := Value;
end;

procedure TMaxxRESTCliente.SetUser(const Value: String);
begin
  FUser := Value;
end;

function TMaxxRESTCliente.BuscarCEP(CEP: String): TJSONValue;
begin
  //buscando CEP utilizando a API CEPAberto
  //TOken: 204d624189ae08c6efa3696d846fb8f4
  try
    FRRequest.Params.Clear;
    FRClient.BaseURL   := 'http://www.cepaberto.com/api/v3/';
    FRRequest.Resource := 'cep?cep=' + CEP;
    FRRequest.Method   := TRESTRequestMethod.rmGET;


    FRRequest.AddAuthParameter('Authorization', 'Token token="204d624189ae08c6efa3696d846fb8f4"', TRESTRequestParameterKind.pkHTTPHEADER);


    FRRequest.Accept := 'application/json; charset=utf-8';


//    with FRRequest.Params.AddItem do
//    begin
//      Kind        := pkHTTPHEADER;
//      Value       := 'Token token=204d624189ae08c6efa3696d846fb8f4';
//      name        := 'Authorization';
//      ContentType := TRESTContentType.ctAPPLICATION_JSON;
//    end;

//    with FRRequest.Params.AddItem do
//    begin
//      Kind        := pkHTTPHEADER;
//      Value       := 'CepAberto';
//      name        := 'User-Agent';
//      ContentType := TRESTContentType.ctAPPLICATION_JSON;
//    end;

//    with FRRequest.Params.AddItem do
//    begin
//      Kind        := pkHTTPHEADER;
//      Value       := 'application/json';
//      name        := 'Accept';
//      ContentType := TRESTContentType.ctAPPLICATION_JSON;
//    end;

    FRRequest.Execute;

    Result := FRResponse.JSONValue;
  except
    on E: Exception do
      SalvarLog('BuscarCEP: ' + E.Message);
  end;
end;

function TMaxxRESTCliente.BuscarConfiguracoes: TJSONValue;
begin
  try
    FMetodo   := TRESTRequestMethod.rmGET;
    Configurar;

    FRRequest.Execute;

    Result := FRResponse.JSONValue;
  except
    on E: Exception do
      SalvarLog('BuscarConfiguracoes: ' + E.Message);
  end;
end;

function TMaxxRESTCliente.Configurar(Login: Boolean = False): Boolean;
begin
  try
    Result := True;

    FRClient.RaiseExceptionOn500 := False;

    FRRequest.Params.Clear;
    FRRequest.Timeout := 10000;

    if FToken <> '' then
      FRRequest.AddParameter('Authorization', FToken, pkHTTPHEADER);

    //Particularidade servi�o do CRM
    FRRequest.AddParameter('SerializeType', 'sync', pkHTTPHEADER);
//    FRRequest.Client.SetHTTPHeader('User-Agent', 'DeskTop-Integracao');
    FRRequest.AddParameter('User-Agent', 'DeskTop-Integracao', pkHTTPHEADER);


//    FRClient.UserAgent := 'DeskTop-Integracao';

    FRResponse.ResetToDefaults;

    if Login then
    begin
      FRRequest.Method := rmPOST;

      if FBaseURL_Login <> '' then
        FRClient.BaseURL := FBaseURL_Login
      else
      begin
        SalvarLog('Configurar: BaseURL_Login vazio na configura��o do MaxxREST!');
        Result := False;
      end;

      if FEndPoint_Login <> '' then
        FRRequest.Resource := FEndPoint_Login
      else
      begin
        SalvarLog('Configurar: EndPoint_Login vazio na configura��o do MaxxREST!');
        Result := False;
      end;
    end
    else
    begin
      FRRequest.Timeout := 360000;

      FRRequest.Method := FMetodo;

      if FBaseURL <> '' then
        FRClient.BaseURL := FBaseURL
      else
      begin
        SalvarLog('Configurar: BaseURL vazio na configura��o do MaxxREST!');
        Result := False;
      end;

      if FEndPoint <> '' then
        FRRequest.Resource := FEndPoint
      else
      begin
        SalvarLog('Configurar: EndPoint vazio na configura��o do MaxxREST!');
        Result := False;
      end;
    end;

  except
    on E: Exception do
    begin
      SalvarLog('Configurar: ' + E.Message);
      Result := False;
    end;
  end;
end;

function TMaxxRESTCliente.IniciarTransacao: Boolean;
var
  jBody, jResponse: TJSONObject;
  I: Integer;
begin
  try
    try
      SalvarLog('Iniciando transa��o...');

      if Configurar(True) then //Configurando as url's
      begin
        //criando json para o body com usuario e senha
        jBody := TJSONObject.Create;

        jBody.AddPair('password', FPassword);
        jBody.AddPair('username', FUser);

        //criando body
      //  FRRequest.Body.ClearBody;
      //  FRRequest.Body.Add(jBody);
        FRRequest.ClearBody;
        FRRequest.AddBody(jBody.ToString, TRESTContentType.ctAPPLICATION_JSON);
        //executando login
        FRRequest.Execute;

        jResponse := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(FRResponse.JSONText), 0) as TJSONObject;

        if jResponse.GetValue('token') <> nil then
        begin

          for I := 0 to Pred(jResponse.Count) do
          begin
            if jResponse.Pairs[I].JsonString.Value = 'token'  then
              FToken := jResponse.Pairs[I].JsonValue.Value;
          end;
          FRRequest.AddParameter('Authorization', FToken, pkHTTPHEADER);
        end
        else
        begin
          FToken := '';

          for I := 0 to Pred(jResponse.Count) do
            SalvarLog(jResponse.Pairs[I].JsonString.Value + ': ' + jResponse.Pairs[I].JsonValue.Value);
        end;
      end
      else FToken := '';

      if FToken <> '' then
        SalvarLog('Transa��o iniciada!')
      else
        SalvarLog('Falha ao iniciar a transa��o!');

      FInTransaction := FToken <> '';
      Result         := FToken <> '';
    except
      on E: Exception do
      begin
        SalvarLog('IniciarTransacao: ' + E.Message, nil, False);
        SalvarLog('Motivo: ', FRResponse.JSONValue, False);

        SalvarLog('N�o foi poss�vel conectar ao servi�o de terceiros! ');

        if jResponse <> nil then
        begin
          jResponse := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(FRResponse.JSONText), 0) as TJSONObject;

          for I := 0 to Pred(jResponse.Count) do
           SalvarLog(jResponse.Pairs[I].JsonString.Value + ': ' + jResponse.Pairs[I].JsonValue.Value);
        end;
      end;
    end;
  finally
    FreeAndNil(jBody);
    FreeAndNil(jResponse);
  end;
end;

function TMaxxRESTCliente.QuantPost: Integer;
begin
  Configurar;

  if not FInTransaction then
  begin
    raise Exception.Create('N�o � poss�vel enviar as informa��es sem antes iniciar uma transa��o!');
    Abort;
  end;

  FRRequest.Execute;

  if FRResponse.StatusCode in [200, 201] then
     Result := StrToInt(FRResponse.JsonText)
  else Result := 1;
end;

procedure TMaxxRESTCliente.FecharTransacao;
begin
  FToken             := '';
  FRClient.BaseURL   := '';
  FRRequest.Resource := '';
  FRRequest.Params.Clear;

  FInTransaction     := False;
end;

function TMaxxRESTCliente.Enviar(jObj: TJSONValue): Boolean;
var
  JsonResult         : TJSONObject;
  I                  : Integer;
//  JsonDocument       : TJSONDocument;
//  JsonDocumentEnvio  : TJSONDocument;
  StrNome, StrValores: TStringList;
begin
  try
    JsonResult := TJSONObject.Create;
    StrNome    := TStringList.Create;
    StrValores := TStringList.Create;

    if Configurar then
    begin
      if (FMetodo = TRESTRequestMethod.rmPUT) or (FMetodo = TRESTRequestMethod.rmDELETE) then
      begin

        StrNome.StrictDelimiter := False;
        StrNome.DelimitedText   := FUrlNomes;
        StrNome.Delimiter       := ';';
        StrNome.StrictDelimiter := True;

        StrValores.StrictDelimiter := False;
        StrValores.DelimitedText   := FURLValores;
        StrValores.Delimiter       := ';';
        StrValores.StrictDelimiter := True;

        for I := 0 to Pred(StrNome.Count) do
          FRRequest.Params.AddUrlSegment(StrNome[I], StrValores[I]);
      end;


      if jObj <> nil then
      begin
        with FRRequest.Params.AddItem do
        begin
          ContentType := ctAPPLICATION_JSON;
          Kind        := pkREQUESTBODY;
          Value       := jObj.ToJSON;
        end;
      end;

  //     FRRequest.Body.Add(jObj.ToJSON, TRESTContentType.ctAPPLICATION_JSON);

      FRRequest.Execute;


      FStatusCode := FRResponse.StatusCode;

      if FRResponse.StatusCode in [200, 201] then
        Result := True
      else
      begin
        Result := False;
        case FStatusCode of
          401:
          begin
            IniciarTransacao;
            Result := Enviar(jObj);
          end;
        else
          if FStatusCode <> 409 then
            SalvarLog('Enviar: ' +  IntToStr(FRResponse.StatusCode) + ' - ' + FRResponse.JSONText, jObj, False);
        end;
      end;
    end
    else
    begin
      FStatusCode := 400;
      Result      := False;
    end;


  except
    on E: Exception do
      SalvarLog('Enviar: ' + E.Message);
  end;
end;

end.
