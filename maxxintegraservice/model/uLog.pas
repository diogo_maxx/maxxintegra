unit uLog;

interface

uses IniFiles, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, RzPanel, Vcl.StdCtrls, IWSystem, Threading, JSON, REST.JSON,
  Vcl.ComCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, uMaxxREST, REST.Client, REST.Types;

type
   TTipoRequisicao = (tprINSERT, tprUPDATE, tprDELETE);

  TLog = class
  private
    FErro: Boolean;
    FJSONRetorno: String;
    FJSONEnvio: String;
    FRegistro: String;
    FParametros: TStrings;
    FStatusCod: Integer;
    FTipoRequisicao: TTipoRequisicao;
    FTabela: String;
    FHoraSincronismo: TDateTime;
    FHoraIntegrado: TDateTime;
    FMensagem: String;
    FURL: String;
    procedure SetErro(const Value: Boolean);
    procedure SetHoraIntegrado(const Value: TDateTime);
    procedure SetHoraSincronismo(const Value: TDateTime);
    procedure SetJSONEnvio(const Value: String);
    procedure SetJSONRetorno(const Value: String);
    procedure SetMensagem(const Value: String);
    procedure SetParametros(const Value: TStrings);
    procedure SetRegistro(const Value: String);
    procedure SetStatusCod(const Value: Integer);
    procedure SetTabela(const Value: String);
    procedure SetTipoRequisicao(const Value: TTipoRequisicao);
    procedure SetURL(const Value: String);
    procedure EnviarLog(JSONObject: TJSONValue);
    { private declarations }
  protected
    { protected declarations }
  public
    destructor Destroy; override;
    { public declarations }

  published
    { published declarations }
    property Tabela: String read FTabela write SetTabela;
    property TipoRequisicao: TTipoRequisicao read FTipoRequisicao write SetTipoRequisicao;
    property Registro: String read FRegistro write SetRegistro;
    property HoraIntegrado: TDateTime read FHoraIntegrado write SetHoraIntegrado;
    property HoraSincronismo: TDateTime read FHoraSincronismo write SetHoraSincronismo;
    property StatusCod: Integer read FStatusCod write SetStatusCod;
    property Erro: Boolean read FErro write SetErro;
    property JSONRetorno: String read FJSONRetorno write SetJSONRetorno;
    property JSONEnvio: String read FJSONEnvio write SetJSONEnvio;
    property URL: String read FURL write SetURL;
    property Parametros: TStrings read FParametros write SetParametros;
    property Mensagem: String read FMensagem write SetMensagem;
    procedure GravarLog;

    constructor Create; overload;

  end;




procedure LogTelaPrincipal(Msg: String; PulaLinha: Boolean = False);
procedure SalvarLog(MSG: String; json: TJSONValue = nil; MostrarTela: Boolean = True; SalvarArquivo: Boolean = True);

implementation

uses ufrmPrincipal, uConexao, uDatasetToJSON;

procedure SalvarLog(Msg: String; json: TJSONValue = nil; MostrarTela: Boolean = True; SalvarArquivo: Boolean = True);
var
  ILog: ITask;
begin
  TThread.Synchronize(nil,
  procedure
  var
    Arquivo: TextFile;
    Log : string;
  begin
    if SalvarArquivo then
    begin
      Log :=  StringReplace(UpperCase(gsAppPath), '\EXE\', '\Conf\', []) + 'maxxintegra.log';

      try
        AssignFile(Arquivo, Log);
        if FileExists(Log) then
            Append(arquivo) { se existir, apenas adiciona linhas }
        else ReWrite(arquivo); { cria um novo se n�o existir }

        try
          WriteLn(arquivo, DateToStr(now) + ' - ' +  timetostr(now) + ' - ' +  msg);

          if json <> nil then
            WriteLn(arquivo, DateToStr(now) + ' - ' +  timetostr(now) + ' - ' +  json.ToJSON);
        finally
          CloseFile(arquivo)
        end;


      except
        on E: Exception do
          SalvarLog('SalvarLog: ' + E.Message);
      end;

    end;
  end);

  if MostrarTela then
    LogTelaPrincipal(Msg);
end;

procedure LogTelaPrincipal(Msg: String; PulaLinha: Boolean = False);
begin
  TThread.Synchronize(nil,
  procedure
  begin
    if PulaLinha then
      frmPrincipal.mmoLog.Lines.Add('');


    if Pos('[FireDAC][Phys][Ora] ORA-03113: end-of-file on communication channel', Msg) > 0 then
    begin
      frmPrincipal.mmoLog.Lines.Add(FormatDateTime('DD/MM/YYYY HH:MM:SS', Now) + ': ' + 'Perda de conex�o com o BD!');
      frmPrincipal.mmoLog.Lines.Add('Verifique sua conex�o de rede.');
      frmPrincipal.mmoLog.Lines.Add('Verifique se o servi�o do banco de dados est� ativo.');
      frmPrincipal.mmoLog.Lines.Add('Tente novamente!');
      frmPrincipal.mmoLog.Lines.Add('Caso persista a falha na conex�o, entre em contato com o DBA.');
    end
    else if Pos('[FireDAC][Phys][Ora] ORA-12154: TNS:could not resolve the connect identifier specified', Msg) > 0 then
    begin
      frmPrincipal.mmoLog.Lines.Add(FormatDateTime('DD/MM/YYYY HH:MM:SS', Now) + ': ' + 'N�o foi poss�vel conectar com o BD!');
      frmPrincipal.mmoLog.Lines.Add('TNS n�o encontrou o servi�o especificado!');
      frmPrincipal.mmoLog.Lines.Add('Tente novamente!');
      frmPrincipal.mmoLog.Lines.Add('Caso persista a falha na conex�o, entre em contato com o DBA.');
    end
    else if Pos('[FireDAC][Phys][Ora] ORA-12170: TNS:Connect timeout occurred', Msg) > 0 then
      frmPrincipal.mmoLog.Lines.Add(FormatDateTime('DD/MM/YYYY HH:MM:SS', Now) + ': ' + 'Time out de conex�o!')
    else
    if Msg <> '' then
      frmPrincipal.mmoLog.Lines.Add(FormatDateTime('DD/MM/YYYY HH:MM:SS', Now) + ': ' + Msg);
  end);
end;

{ TLog }

constructor TLog.Create;
begin
  inherited Create;
  FParametros := TStringList.Create;
end;

destructor TLog.Destroy;
begin
  inherited Destroy;
end;

procedure TLog.EnviarLog(JSONObject: TJSONValue);
var
  FClient: TRESTClient;
  FRequest: TRESTRequest;
  FResponse: TRESTResponse;
  jObj: TJSONObject;
begin
  FClient   := TRESTClient.Create(frmPrincipal.IPMAXXLOG);
  FRequest  := TRESTRequest.Create(nil);
  FResponse := TRESTResponse.Create(nil);

  TJSONObject(JSONObject).AddPair('idconf', frmPrincipal.mseSistema.Text);
  TJSONObject(JSONObject).AddPair('app', 'Hefesto');



  FRequest.Client             := FClient;
  FRequest.Response           := FResponse;
  FClient.RaiseExceptionOn500 := False;

  FRequest.Resource := '/log/';
  FRequest.Method   := TRESTRequestMethod.rmPOST;

  with FRequest.Params.AddItem do
  begin
    ContentType := ctAPPLICATION_JSON;
    Kind        := pkREQUESTBODY;
    Value       := JSONObject.ToJSON;
  end;

  FRequest.Execute;

end;

procedure TLog.GravarLog;
var
  FLog: TFDQuery;
  Conexao: TConexao;
  DataSetJson: TDatasetToJSON;
  Len : Integer;
begin
  try
    try
      Conexao            := TConexao.GetInstance;
      FLog               := TFDQuery.Create(nil);
      FLog.Connection    := Conexao.SQLite;
      FLog.CachedUpdates := True;

      FLog.Close;
      FLog.SQL.Add('select * from log');
      FLog.Open;

      FLog.Append;
      FLog.FieldByName('Tabela').AsString            := FTabela;

      case (TipoRequisicao) of
        tprINSERT: FLog.FieldByName('TipoRequisicao').AsString := 'I';
        tprUPDATE: FLog.FieldByName('TipoRequisicao').AsString := 'A';
        tprDELETE: FLog.FieldByName('TipoRequisicao').AsString := 'D';
      end;

      FLog.FieldByName('Registro').AsString          := FRegistro;
      FLog.FieldByName('Horaintegracao').AsDateTime  := FHoraIntegrado;
      FLog.FieldByName('HoraSincronismo').AsDateTime := FHoraSincronismo;
      FLog.FieldByName('StatusCod').AsInteger        := FStatusCod;
      FLog.FieldByName('Erro').AsBoolean             := FErro;
      FLog.FieldByName('JSONRetorno').Value          := JSONRetorno;
      FLog.FieldByName('JSONEnvio').Value            := JSONEnvio;
      FLog.FieldByName('URL').AsString               := FURL;
      FLog.FieldByName('Parametros').AsString        := FParametros.Text;
      FLog.FieldByName('Mensagem').AsString          := FMensagem;
      FLog.FieldByName('datalog').AsDateTime         := Now;
      FLog.Post;

      DataSetJson                := TDatasetToJSON.Create;
      DataSetJson.FormatoData    := 'yyyy-MM-dd''T''HH:mm:ss.zzz';
      DataSetJson.IncluiTimeZone := True;

      EnviarLog(DataSetJson.CriaJson(FLog));

      FLog.Edit;
      FLog.FieldByName('Parametros').Clear;
      FLog.Post;

      FLog.ApplyUpdates(0);

    except
      on E: Exception do
        SalvarLog('GravarLog: ' + E.Message);
    end;
  finally
    FLog.Close;
    FreeAndNil(FLog);
  end;
end;

procedure TLog.SetErro(const Value: Boolean);
begin
  FErro := Value;
end;

procedure TLog.SetHoraIntegrado(const Value: TDateTime);
begin
  FHoraIntegrado := Value;
end;

procedure TLog.SetHoraSincronismo(const Value: TDateTime);
begin
  FHoraSincronismo := Value;
end;

procedure TLog.SetJSONEnvio(const Value: String);
begin
  FJSONEnvio := Value;
end;

procedure TLog.SetJSONRetorno(const Value: String);
begin
  FJSONRetorno := Value;
end;

procedure TLog.SetMensagem(const Value: String);
begin
  FMensagem := Value;
end;

procedure TLog.SetParametros(const Value: TStrings);
begin
  FParametros := Value;
end;

procedure TLog.SetRegistro(const Value: String);
begin
  FRegistro := Value;
end;

procedure TLog.SetStatusCod(const Value: Integer);
begin
  FStatusCod := Value;
end;

procedure TLog.SetTabela(const Value: String);
begin
  FTabela := Value;
end;

procedure TLog.SetTipoRequisicao(const Value: TTipoRequisicao);
begin
  FTipoRequisicao := Value;
end;

procedure TLog.SetURL(const Value: String);
begin
  FURL := Value;
end;

end.
