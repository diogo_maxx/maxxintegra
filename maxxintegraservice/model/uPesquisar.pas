unit uPesquisar;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, sSpeedButton, RzPanel, Vcl.StdCtrls, RzStatus, RzCommon,
  acImage, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, StrUtils, Forms, RTTI, Threading, uConexao;

type
   TRetornaPesquisa = record
     Componente: TComponent;
     FieldQry: String;
   end;

type
   TTipoFiltro = (tpSQL, tpPesquisa);

type
   TPesquisa = class
  private
    FRetornaPesquisa: Array of TRetornaPesquisa;
    FTipoPesquisa: String;
    FComponentOrigem: TCustomEdit;
    FQryIntegracao: TFDQuery;
    FConfBusca: TFDQuery;
    FQryBD: TFDQuery;
    FSQLBusca: String;
    FFiltro: String;
    FTipoFiltro: TTipoFiltro;
    Conexao : TConexao;
    procedure SetTipoPesquisa(const Value: String);
    constructor Create; overload;
    procedure SetComponentOrigem(const Value: TCustomEdit);
    procedure SetSQLBusca(const Value: String);
    function PreencheSCHEMA: String;
    procedure SetFiltro(const Value: String);
    procedure PreencherPesquisa;
    procedure SetarValorComponente(Component: TComponent; Valor: Variant);
    procedure SetTipoFiltro(const Value: TTipoFiltro);
  public
    destructor Destroy; override;
  published
     property TipoPesquisa: String            read FTipoPesquisa     write SetTipoPesquisa;
     property ComponentOrigem: TCustomEdit    read FComponentOrigem  write SetComponentOrigem;
     property SQLBusca: String                read FSQLBusca         write SetSQLBusca;
     property Filtro : String                 read FFiltro           write SetFiltro;
     property TipoFiltro: TTipoFiltro         read FTipoFiltro       write SetTipoFiltro;

     constructor Create(Field: Array of String; Components: Array of TComponent); overload;
     function Pesquisar: Boolean;

   end;

implementation

{ TPesquisa }

uses uException, ufrmMasterPes, uLog;

function TPesquisa.Pesquisar: Boolean;
var
  IPesquisa: ITask;
  I: Integer;
begin
  try
    try
      Result := True;

      if SQLBusca = '' then
      begin
        FQryIntegracao.Close;
        FQryIntegracao.SQL.Clear;
        FQryIntegracao.SQL.Add('select * from sistemasintegrados_conf where tpscr_descricao like ' + QuotedStr(FTipoPesquisa));
        FQryIntegracao.Open;

        if not FQryIntegracao.IsEmpty then
        begin
          SQLBusca := FQryIntegracao.FieldByName('conf_scriptbusca').AsString;

          FConfBusca.Close;
          FConfBusca.SQL.Clear;
          FConfBusca.SQL.Add('select * from conf_json where idconf like ' + FQryIntegracao.FieldByName('IDCONF').AsString);
          FConfBusca.Open;
        end;
      end;
      //cria tela de pesquisa
      //seta o formulario do componente origem como owner da tela
      frmMasterPes      := TfrmMasterPes.Create(ComponentOrigem.Owner);
      frmMasterPes.Top  := (TForm(FComponentOrigem.Owner).Top + FComponentOrigem.Top + FComponentOrigem.Height + 27);
      frmMasterPes.Left := (TForm(FComponentOrigem.Owner).Left + FComponentOrigem.Left);

      with frmMasterPes do
      begin
        cxGridPesquisaDBTableView1.OptionsView.NoDataToDisplayInfoText := 'Localizando registros...';

        FDPesquisa.Connection := Conexao.BDCli;

        FDPesquisa.Close;
        FDPesquisa.SQL.Clear;
        FDPesquisa.SQL.Add(FSQLBusca);

        if FDPesquisa.MacroCount > 0 then
        begin
          if FDPesquisa.FindMacro('SCHEMA') <> nil then
            FDPesquisa.MacroByName('SCHEMA').AsRaw := PreencheSCHEMA;

          if (FTipoFiltro = tpSQL) and (FFiltro <> '') then
          begin
            if FDPesquisa.FindMacro('FILTRO') <> nil then
              FDPesquisa.MacroByName('FILTRO').AsRaw := FFiltro;
          end;
        end;
        FDPesquisa.Open;

        if (FTipoFiltro = tpPesquisa) and (FFiltro <> '') then
          msePESQUISA.Text := Filtro;

        if not FDPesquisa.IsEmpty then
        begin
          cxGridPesquisaDBTableView1.DataController.CreateAllItems;

          if not FConfBusca.IsEmpty then
          begin
            for I := 0 to Pred(cxGridPesquisaDBTableView1.ColumnCount) do
            begin
              FConfBusca.Filtered := False;
              FConfBusca.Filter   := 'JSON_CAMPOSQL like ' + QuotedStr(cxGridPesquisaDBTableView1.Columns[I].Caption);
              FConfBusca.Filtered := True;

              if not FConfBusca.IsEmpty then
                cxGridPesquisaDBTableView1.Columns[I].Caption := FConfBusca.FieldByName('JSON_CAMPOJSON').AsString;

              FConfBusca.Filtered := False;
              FConfBusca.Filter   := '';
            end;
          end;

          cxGridPesquisa.Refresh;
        end;

        cxGridPesquisaDBTableView1.OptionsView.NoDataToDisplayInfoText := '<No data to display>';
      end;

      frmMasterPes.ShowModal;

      if frmMasterPes.ModalResult = mrOk then
        PreencherPesquisa;
    except
      on E: Exception do
      begin
        SalvarLog(E.Message);
        Result := False;
      end;
    end;
  finally
    if frmMasterPes <> nil then
      FreeAndNil(frmMasterPes);
  end;
end;

procedure TPesquisa.PreencherPesquisa;
var
  Contexto : TRttiContext;
  Tipo     : TRttiType;
  Metodo   : TValue;
  I,J      : Integer;
begin
  try
    for I := 0 to Length(FRetornaPesquisa) -1 do
    begin
      /////////////////////////////////////////////////////////////////////////////////////
      ///  Se o campo for um c�digo, geralmente o que est� fazendo a busca...           ///
      ///  retira-se o evento de exit do mesmo para que n�o fa�a 2 pesquisas no banco   ///
      ///  e avan�a o foco da tela e seta o restante dos valores.                       ///
      /////////////////////////////////////////////////////////////////////////////////////
//      if FRetornaPesquisa[I].FieldQry = 'C�digo' then
//      begin
//        try
//          Contexto := TRttiContext.Create;
//          Tipo     := Contexto.GetType(FRetornaPesquisa[I].Componente.ClassInfo);
//          //retirando o evento
//          if Tipo.GetProperty('OnExit') <> nil then
//          begin
//            if not Tipo.GetProperty('OnExit').GetValue(FRetornaPesquisa[I].Componente).IsEmpty then
//            begin
//               Metodo := Tipo.GetProperty('OnExit').GetValue(FRetornaPesquisa[I].Componente);
//               Tipo.GetProperty('OnExit').SetValue(FRetornaPesquisa[I].Componente, nil);
//            end;
//          end;
//          //setando o valor no componente
//          if frmMasterPes.FDPesquisa.FindField(FRetornaPesquisa[I].FieldQry) <> nil then
//            SetarValorComponente(FRetornaPesquisa[I].Componente, frmMasterPes.FDPesquisa.FindField(FRetornaPesquisa[I].FieldQry).AsVariant);
//          //avan�ando o foco
//          TForm(FRetornaPesquisa[I].Componente.Owner).Perform(CM_DialogKey, VK_TAB, 0);
//          //seta novamente o evento
//          if (not Metodo.IsEmpty) then
//            Tipo.GetProperty('OnExit').SetValue(FRetornaPesquisa[I].Componente, Metodo);
//        finally
//          Contexto.Free;
//        end;
//      end
//      else
//      begin
        for J := 0 to Pred(frmMasterPes.cxGridPesquisaDBTableView1.ColumnCount) do
          if FRetornaPesquisa[I].FieldQry = frmMasterPes.cxGridPesquisaDBTableView1.Columns[J].Caption then
             SetarValorComponente(FRetornaPesquisa[I].Componente,
             frmMasterPes.cxGridPesquisaDBTableView1.Columns[J].DataBinding.Field.AsVariant);
//      end;
    end;
  except
    on E: Exception do
      SalvarLog(E.Message);
  end;
end;

function TPesquisa.PreencheSCHEMA: String;
begin
  case AnsiIndexStr(Conexao.BDCli.Params.DriverID, ['MSSQL', 'Ora']) of
    0: //SQLServer
    begin
      Result := Conexao.BDCli.Params.Database + '.';

      if Conexao.BDCli.Params.Values['MetaCurSchema'] <> '' then
        Result := Result + Conexao.BDCli.Params.Values['MetaCurSchema'];
    end;
    1: //Oracle
      Result := Conexao.BDCli.Params.Values['MetaCurSchema'];
  end;
end;

constructor TPesquisa.Create;
begin
  inherited Create;

  Conexao           := TConexao.GetInstance;
  FQryIntegracao    := TFDQuery.Create(nil);
  FQryBD            := TFDQuery.Create(nil);
  FConfBusca        := TFDQuery.Create(nil);

  FQryIntegracao.Connection := Conexao.SQLite;
  FQryBD.Connection         := Conexao.BDCli;
  FConfBusca.Connection     := Conexao.SQLite;
end;

constructor TPesquisa.Create(Field: array of String; Components: array of TComponent);
var
  I: Integer;
begin
  Create;

  SetLength(FRetornaPesquisa, Length(Field));

  for I := 0 to Pred(Length(Field)) do
  begin
    FRetornaPesquisa[I].FieldQry   := Field[I];
    FRetornaPesquisa[I].Componente := Components[I];
  end;
end;

destructor TPesquisa.Destroy;
begin
  inherited Destroy;
  FQryBD.Close;
  FQryIntegracao.Close;

  FreeAndNil(FQryBD);
  FreeAndNil(FQryIntegracao);
  FreeAndNil(FConfBusca);
end;

procedure TPesquisa.SetComponentOrigem(const Value: TCustomEdit);
begin
  FComponentOrigem := Value;
end;

procedure TPesquisa.SetFiltro(const Value: String);
begin
  FFiltro := Value;
end;

procedure TPesquisa.SetSQLBusca(const Value: String);
begin
  FSQLBusca := Value;
end;

procedure TPesquisa.SetTipoFiltro(const Value: TTipoFiltro);
begin
  FTipoFiltro := Value;
end;

procedure TPesquisa.SetTipoPesquisa(const Value: String);
begin
  FTipoPesquisa := Value;
end;

procedure TPesquisa.SetarValorComponente(Component: TComponent; Valor: Variant);
var
  Contexto  : TRttiContext;
  Tipo      : TRttiType;
begin
  if Valor <> Null then
  begin
    try
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Component.ClassType);

        if not(VarIsEmpty(Valor) or VarIsNull(Valor)) then
        begin
          if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TField)) then
          begin
            if Component.ClassType <> TBlobField then
              Tipo.GetProperty('AsVariant').SetValue(Component, TValue.From(Valor))
            else TField(Component).Value := Valor;
          end
          else
          if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) then
          begin
            if Tipo.GetProperty('Field') = nil then
            begin
              if Tipo.GetProperty('Value') <> nil then
                 Tipo.GetProperty('Value').SetValue(Component, TValue.From(Valor))
              else if Tipo.GetProperty('Text') <> nil then
                 Tipo.GetProperty('Text').SetValue(Component, vartostr(Valor));
            end
            else
              SetarValorComponente(TField(Tipo.GetProperty('Field').GetValue(Component).AsObject), Valor);
          end;
        end
        else
        begin
          if Tipo.GetMethod('Parent') <> nil then
          begin
            if Tipo.GetMethod('Clear') <> nil then
              Tipo.GetMethod('Clear').Invoke(Component, []);
          end
          else
          begin
            if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TField)) then
              Tipo.GetProperty('AsVariant').SetValue(Component, TValue.From(''))
            else
            if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomEdit)) then
            begin
              if Tipo.GetProperty('Field') = nil then
              begin
                if Tipo.GetProperty('Value') <> nil then
                   Tipo.GetProperty('Value').SetValue(Component, TValue.From(''))
                else if Tipo.GetProperty('Text') <> nil then
                   Tipo.GetProperty('Text').SetValue(Component, vartostr(''));
              end
              else
                SetarValorComponente(TField(Tipo.GetProperty('Field').GetValue(Component).AsObject), '');
            end;
          end;
        end;
      finally
        Contexto.Free;
      end;
    except
      on E : Exception do
      begin
//        Mensagem('E', 'Erro ao setar o valor no componente: ' + E.Message + #13 + Tipo.GetProperty('Name').GetValue(Component).AsString);
        Contexto.Free;
      end;
    end;
  end;
end;

end.
