unit udmBuscaBD;

interface

uses
  System.SysUtils, System.Classes, Json, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, Data.DBXPlatform;

{$METHODINFO ON}
type
  TdmBuscaBD = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function BuscaBD(Tabela, Filtro: String): TJsonValue;
  end;
{$METHODINFO OFF}

var
  dmBuscaBD: TdmBuscaBD;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uConexao, uBDCliente, ufrmPrincipal, uException;

{$R *.dfm}

{ TdmBuscaBD }

function TdmBuscaBD.BuscaBD(Tabela, Filtro: String): TJsonValue;
var
  FDInt: TFDConnection;
  FDQry_Int: TFDQuery;
  JsonObject : TJsonObject;
begin
  try
    frmPrincipal.ThreadTimer.Enabled := False;

    FDInt                := TFDConnection.Create(nil);
    FDInt.OnError        := TErrorFDConnection.ConexaoError;
    FDQry_Int            := TFDQuery.Create(nil);

    FDQry_Int.DisableControls;

    FDQry_Int.Connection := FDInt;
    JsonObject           := TJSONObject.Create;

    CriaConexao(FDInt);

    GetInvocationMetadata().ResponseContentType := 'json';

    FDQry_Int.Close;
    FDQry_Int.SQL.Clear;
    FDQry_Int.SQL.Add('select * from sistemasintegrados_conf where conf_tabela = ' + UpperCase(QuotedStr(Tabela)));
    FDQry_Int.Open;

    if not FDQry_Int.IsEmpty then
      JsonObject.AddPair(Tabela, Exportar(UpperCase(Tabela), Filtro, FDQry_Int.FieldByName('conf_scriptbusca').AsString, nil, FDInt));


    if JsonObject.Count > 0 then
      GetInvocationMetadata().ResponseCode := 200
    else
      GetInvocationMetadata().ResponseCode := 204;
  finally
    frmPrincipal.ThreadTimer.Enabled := True;
    Result := JsonObject;
    FDQry_Int.Close;
    FDInt.Connected := False;
    FreeAndNil(FDQry_Int);
    FreeAndNil(FDInt);
    frmPrincipal.LimpaMemoriaAPP;
  end;
end;

procedure TdmBuscaBD.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(dmBuscaBD);
end;

end.
