unit uLayoutFormulario;

interface

uses Graphics, Classes, RTTI, RzDBBnEd, RzDBEdit, RzButton, RzDBChk, RzEdit, RzCmboBx, RZCommon, RzSpnEdt, StrUtils,
     SysUtils, Forms, StdCtrls, Buttons, TypInfo, Vcl.Controls, Winapi.Windows, dxStatusBar, ExtCtrls, RzRadChk, RzTabs,
     sSpeedButton, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
     cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;


   type
     RzDateTimeEditEvent = class
     class procedure RzDateTimeEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
     class procedure RzDateTimeEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
     class procedure RzDateTimeEditInvalidDate(Sender: TObject; var KeepFocused, KeepInvalidText: Boolean; var NewDate: TDateTime);
     class procedure RzDateTimeEditInvalidTime(Sender: TObject; var KeepFocused, KeepInvalidText: Boolean; var NewTime: TDateTime);
   end;
   type
     RzMemoEvent = class (TObject)
         class procedure RzMemoEventKeyPress(Sender: TObject; var Key: Char);
   end;


procedure PadronizarBotao(Component: TComponent);
procedure PadronizarEdit(Component: TComponent);
procedure PadronizarLabel(Component: TComponent; ListaObrigatorios: TStringList = nil);
procedure PadronizarTabs(Component: TComponent);
procedure PadronizarGrid(Component: TComponent);
procedure PadronizarTela(Component: TForm; ListaObrigatorios: TStringList = nil);

implementation

uses uConstantes, ufrmPrincipal;

{RzDateTimeEditEvent}
class procedure RzDateTimeEditEvent.RzDateTimeEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
//  OutputDebug('LayoutFormulario -> Passou pela RzDateTimeEditKeyDown');
  ///////////////////////////
  ///  Teclas num�ricas   ///
  ///////////////////////////
  //verifica se o que foi digitado s�o os n�meros no numpad ou n�meros que est�o em cima das letras
  // devido a alguns notebooks n�o terem o numpad
  if Key in [48..57, 96..105] then
  begin
    ///////////////////
    //Campo de hora ///
    ///////////////////

    if TRzCustomEdit(Sender).HelpKeyword = 'Hora' then
    begin
      if Length(TRzCustomEdit(Sender).Text) > 4 then
      begin
        //Validando se o a posi��o selecionada no texto � ":"
        TRzCustomEdit(Sender).SelLength := 1;
        //caso seja ":" acrescentar pular e alterar o pr�ximo
        if TRzCustomEdit(Sender).SelText = ':' then TRzCustomEdit(Sender).SelStart := TRzCustomEdit(Sender).SelStart +1;
        //seleciona um caracter a frente e substitui
        TRzCustomEdit(Sender).SelLength := 1;
        TRzCustomEdit(Sender).SetSelText('');
      end
      else //caso seja na posi��o 2, inserir os dois pontos
      if (Length(TRzCustomEdit(Sender).Text) = 2) then
      begin
        //insere a barra
        TRzCustomEdit(Sender).Text := TRzCustomEdit(Sender).Text + ':';
        //posiciona o cursos do mouse no final do texto
        TRzCustomEdit(Sender).SelStart := Length(TRzCustomEdit(Sender).Text);
      end;
    end

    ////////////////////
    //Campo de data  ///
    ////////////////////

    else
    begin
      if Length(TRzCustomEdit(Sender).Text) > 9 then
      begin
        //Validando se o a posi��o selecionada no texto � "/"
        TRzCustomEdit(Sender).SelLength := 1;
        //caso seja "/" acrescentar pular e alterar o pr�ximo
        if TRzCustomEdit(Sender).SelText = '/' then TRzCustomEdit(Sender).SelStart := TRzCustomEdit(Sender).SelStart +1;
        //seleciona um caracter a frente e substitui
        TRzCustomEdit(Sender).SelLength := 1;
        TRzCustomEdit(Sender).SetSelText('');
      end
      else //caso seja na posi��o 2 e 5, inserir a barra
      if (Length(TRzCustomEdit(Sender).Text) = 2) or (Length(TRzCustomEdit(Sender).Text) = 5) then
      begin
        //insere a barra
        TRzCustomEdit(Sender).Text := TRzCustomEdit(Sender).Text + '/';
        //posiciona o cursos do mouse no final do texto
        TRzCustomEdit(Sender).SelStart := Length(TRzCustomEdit(Sender).Text);
      end;
    end;
  end
  else

  //////////////////////
  ///  Backspace    ////
  //////////////////////
  if Key = 8 then
  begin
    ///////////////////
    //Campo de hora ///
    ///////////////////

    if TRzCustomEdit(Sender).HelpKeyword = 'Hora' then
    begin
      with TRzCustomEdit(Sender) do
      begin
        //Verifica se o valor selecionado � ":"
        if Copy(Text, SelStart, 1) = ':' then
        begin
          //Posiciona uma posi��o atr�s dos ":"
          SelStart  := SelStart -1;
        end;
        SetSelText('');
      end;

    end

    ////////////////////
    //Campo de data  ///
    ////////////////////

    else
    begin
      with TRzCustomEdit(Sender) do
      begin
        //Verifica se o valor selecionado � "/"
        if (Copy(Text, SelStart, 1) = '/') and (Length(Text) > SelStart) then
        begin
          //Posiciona uma posi��o atr�s dos "/"
          SelStart  := SelStart -1;
        end;
        SetSelText('');
      end;
    end;
  end
  else

  //////////////////////
  ///  Voltar       ////
  //////////////////////
  if Key = 37 then
  begin
    if TRzDateTimeEdit(Sender).SelLength = Length(TRzDateTimeEdit(Sender).Text) then
    begin
      TRzDateTimeEdit(Sender).SelStart := 0;
    end;
  end;

end;

class procedure RzDateTimeEditEvent.RzDateTimeEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  //verifica se n�o est� navegando entre os valores do campo
  if (Key <> VK_LEFT) and (Key <> VK_RIGHT) and (Key <> VK_BACK) then
  begin

    if ((Copy(TRzCustomEdit(Sender).Text, TRzCustomEdit(Sender).SelStart +1, 1) = '/') and
        ((TRzCustomEdit(Sender).SelStart > 2) and
         (Copy(TRzCustomEdit(Sender).Text, TRzCustomEdit(Sender).SelStart -2, 1) = '/')) or
        ((TRzCustomEdit(Sender).SelStart = 2)))

       or

       ((Copy(TRzCustomEdit(Sender).Text, TRzCustomEdit(Sender).SelStart +1, 1) = ':') and
        ((TRzCustomEdit(Sender).SelStart > 2) and
         (Copy(TRzCustomEdit(Sender).Text, TRzCustomEdit(Sender).SelStart -2, 1) = ':')) or
        ((TRzCustomEdit(Sender).SelStart = 2))) then
       TRzCustomEdit(Sender).SelStart := TRzCustomEdit(Sender).SelStart + 1;

    if Key = VK_ESCAPE then
    begin
      if TRzDateTimeEdit(Sender).Text = '' then
        TRzDateTimeEdit(Sender).Date := Now;
    end;
  end;
end;

class procedure RzDateTimeEditEvent.RzDateTimeEditInvalidDate(Sender: TObject; var KeepFocused, KeepInvalidText: Boolean;
                                                                var NewDate: TDateTime);
begin
  //A data � inv�lida, mostra a mensagem, continua com o foco e o texto inv�lido
  if TRzCustomEdit(Sender).Enabled then
  begin
//    TfrmMensagens.Mensagem('A data digitada � inv�lida!' + #13 + 'Verifique.', 'E', 'Valida��o de data', [mbOk]);
    KeepFocused     := True;
    KeepInvalidText := True;
  end;
end;

class procedure RzDateTimeEditEvent.RzDateTimeEditInvalidTime(Sender: TObject; var KeepFocused: Boolean; var KeepInvalidText: Boolean;
                                                              var NewTime: TDateTime);
begin
  //A hora � inv�lida, mostra a mensagem, continua com o foco e o texto inv�lido
  if TRzCustomEdit(Sender).Enabled then
  begin
//    TfrmMensagens.Mensagem('A hora digitada � inv�lida!' + #13 + 'Verifique.', 'E', 'Valida��o de hora', [mbOk]);
    KeepFocused     := True;
    KeepInvalidText := True;
  end;
end;


procedure PadronizarBotao(Component: TComponent);
var
  I : Integer;
  Contexto : TRTTIContext;
  Tipo     : TRTTIType;
  Enabled  : Boolean;
  Owner    : String;
begin
  try
//    OutputDebug('LayoutFormulario -> Passou pela PadronizarBotao');

    if Component = nil then
      Exit;

    if Component is TForm then
    begin
      for I := 0 to TForm(Component).ComponentCount -1 do
      begin
        if (TForm(Component).Components[i] is TSpeedButton) or
           (TForm(Component).Components[i] is TRzCustomButton) then
          PadronizarBotao(TForm(Component).Components[i]);
      end;
    end
    else
    begin
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Component.ClassType);

        Enabled := False;

        //Configurando bot�es
        if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRzButton)) then
        begin
          Enabled := Tipo.GetProperty('Enabled').GetValue(Component).AsBoolean;
          if Tipo.GetProperty('TabStop') <> nil then  Tipo.GetProperty('TabStop').SetValue(Component, False);
          if Tipo.GetProperty('ThemeAware') <> nil then Tipo.GetProperty('ThemeAware').SetValue(Component, False);
          if Tipo.GetProperty('HotTrack') <> nil   then Tipo.GetProperty('HotTrack').SetValue(Component, True);
          if Tipo.GetProperty('HotTrackColor') <> nil then
             Tipo.GetProperty('HotTrackColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_HABILITADO), InttoStr(LAYOUT_CORBOTAO_DESTAQUE))));
          if Tipo.GetProperty('HighlightColor') <> nil then
             Tipo.GetProperty('HighlightColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_HABILITADO), InttoStr(LAYOUT_CORBOTAO_DESTAQUE))));
          if Tipo.GetProperty('Glyph') <> nil then
             Tipo.GetProperty('Glyph').SetValue(Component, nil);

          if Tipo.GetProperty('HelpKeyWord') <> nil then
          begin
            Owner := TRzCustomButton(Component).Parent.Name;


            if Tipo.GetProperty('HelpKeyWord').GetValue(Component).AsString = 'Padrao' then
            begin
              if Tipo.GetProperty('Color') <> nil then
                 Tipo.GetProperty('Color').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_DESTAQUE),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));
              if Tipo.GetProperty('FrameColor') <> nil then
                 Tipo.GetProperty('FrameColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_DESTAQUE),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));
              if Tipo.GetProperty('HotTrackColor') <> nil then
                 Tipo.GetProperty('HotTrackColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_DESTAQUE),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));
              if Tipo.GetProperty('HighlightColor') <> nil then
                 Tipo.GetProperty('HighlightColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_DESTAQUE),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));

              if Tipo.GetProperty('Font') <> nil then
              begin
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Color := LAYOUT_CORBOTAO_FONTEDESTAQUE;
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Style := [fsBold];
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Size  := 8;
              end;
            end
            else if Tipo.GetProperty('HelpKeyWord').GetValue(Component).AsString = 'Normal' then
            begin
              if Tipo.GetProperty('Color') <> nil then
                 Tipo.GetProperty('Color').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_NORMAL),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));
              if Tipo.GetProperty('FrameColor') <> nil then
                 Tipo.GetProperty('FrameColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_NORMAL),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));
              if Tipo.GetProperty('HotTrackColor') <> nil then
                 Tipo.GetProperty('HotTrackColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_NORMAL),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));
              if Tipo.GetProperty('HighlightColor') <> nil then
                 Tipo.GetProperty('HighlightColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_NORMAL),
                                                                        ifthen(Owner = 'pnlBotoes', InttoStr(LAYOUT_CORBOTAO_DESABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADODET)))));

              if Tipo.GetProperty('Font') <> nil then
              begin
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Color := LAYOUT_CORBOTAO_FONTENORMAL;
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Style := [fsBold];
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Size  := 8;
              end;
            end
            else
            begin
              if Tipo.GetProperty('Color') <> nil then
                 Tipo.GetProperty('Color').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_HABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADO))));
              if Tipo.GetProperty('FrameColor') <> nil then
                 Tipo.GetProperty('FrameColor').SetValue(Component, StrtoInt(ifthen(Enabled, InttoStr(LAYOUT_CORBOTAO_HABILITADO), InttoStr(LAYOUT_CORBOTAO_DESABILITADO))));

              if Tipo.GetProperty('Font') <> nil then
              begin
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Color := LAYOUT_CORBOTAO_FONTEHABILITADO;
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Style := [fsBold];
                TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Size  := 8;
              end;
            end;
          end;
        end
        else if (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRZRapidFireButton)) then
        begin
          if Tipo.GetProperty('Flat') <> nil then Tipo.GetProperty('Flat').SetValue(Component, True);
          if Tipo.GetProperty('Font') <> nil then
          begin
            TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Color := LAYOUT_CORBOTAO_FONTEDESTAQUE;
            TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Style := [fsBold];
            TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Size  := 8;
          end;
        end;

      finally
        Contexto.Free;
      end;
    end;
  except
    on E : Exception do

  end;
end;

procedure PadronizarEdit(Component: TComponent);
var
  I : Integer;
  Contexto : TRTTIContext;
  Tipo     : TRTTIType;
begin
  try
    if Component is TForm then
    begin
      for I := 0 to TForm(Component).ComponentCount -1 do
        PadronizarEdit(TForm(Component).Components[I]);
    end
    else
    begin
      //Configurando edits
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Component.ClassType);
        if (Tipo is TRttiInstanceType) and ((TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRzCustomEdit)) or
                                            (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRzCustomComboBox)) or
                                            (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRzCustomCheckBox)) or
                                            (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomMemo))) then
        begin
          if Tipo.GetProperty('ButtonGlyph') <> nil       then Tipo.GetProperty('ButtonGlyph').SetValue(Component, TBitBtn(Component.
                                                                                             Owner.FindComponent('btnPesquisaImagem')).Glyph);
          if Tipo.GetProperty('DisabledColor') <> nil     then Tipo.GetProperty('DisabledColor').SetValue(Component, LAYOUT_COREDIT_DESABILITADO);
          if Tipo.GetProperty('Color') <> nil             then Tipo.GetProperty('Color').SetValue(Component, LAYOUT_COREDIT_NORMAL);
          if Tipo.GetProperty('FlatButtonColor') <> nil   then Tipo.GetProperty('FlatButtonColor').SetValue(Component, LAYOUT_FRAME_SELECIONADO);
          if Tipo.GetProperty('FlatButtons') <> nil       then Tipo.GetProperty('FlatButtons').SetValue(Component, True);
          if Tipo.GetProperty('FocusColor') <> nil        then Tipo.GetProperty('FocusColor').SetValue(Component, LAYOUT_COREDIT_SELECIONADO);
          if Tipo.GetProperty('FrameColor') <> nil        then Tipo.GetProperty('FrameColor').SetValue(Component, LAYOUT_FRAME);
          if Tipo.GetProperty('FrameVisible') <> nil      then Tipo.GetProperty('FrameVisible').SetValue(Component, True);
          if Tipo.GetProperty('FrameHotColor') <> nil     then Tipo.GetProperty('FrameHotColor').SetValue(Component, LAYOUT_FRAME_SELECIONADO);
          if Tipo.GetProperty('FrameHotTrack') <> nil     then Tipo.GetProperty('FrameHotTrack').SetValue(Component, True);
          if Tipo.GetProperty('ReadOnlyColor') <> nil     then Tipo.GetProperty('ReadOnlyColor').SetValue(Component, LAYOUT_COREDIT_DESABILITADO);
          if Tipo.GetProperty('CalculatorVisible') <> nil then Tipo.GetProperty('CalculatorVisible').SetValue(Component, True);
          if Tipo.GetProperty('UseFormatToParse') <> nil  then Tipo.GetProperty('UseFormatToParse').SetValue(Component, True);
          if Tipo.GetProperty('FramingPreference') <> nil then SetPropValue(Component, 'FramingPreference', fpCustomFraming);
          if Tipo.GetProperty('FrameSides') <> nil        then Tipo.GetProperty('FrameSides').SetValue(Component, TValue.From([sdBottom]));
          if Tipo.GetProperty('Style') <> nil             then Tipo.GetProperty('Style').SetValue(Component, TValue.From(csOwnerDrawFixed));
  //        if Tipo.GetProperty('AllowBlank') <> nil        then Tipo.GetProperty('AllowBlank').SetValue(Component, False);
          if Tipo.GetProperty('BiDiMode') <> nil          then Tipo.GetProperty('BiDiMode').SetValue(Component, TValue.From(bdLeftToRight));

          if not(TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRzCustomComboBox)) then
          begin
            if Tipo.GetProperty('PasswordChar') <> nil then
            begin
              if Tipo.GetProperty('PasswordChar').GetValue(Component).AsString = #0 then
                if Tipo.GetProperty('CharCase') <> nil then Tipo.GetProperty('CharCase').SetValue(Component, TValue.From(ecUpperCase));
            end
            else
              if Tipo.GetProperty('CharCase') <> nil   then Tipo.GetProperty('CharCase').SetValue(Component, TValue.From(ecUpperCase));
          end;

          if Tipo.GetProperty('Enabled') <> nil then
          begin
            if Tipo.GetProperty('Enabled').GetValue(Component).AsBoolean then
              TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Color := LAYOUT_CORFONT_EDIT_ATIVO
            else
            begin
              TFont(Tipo.GetProperty('Font').GetValue(Component).AsObject).Color := LAYOUT_CORFONT_EDIT_INATIVO;

              if (not TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TRzCustomCheckBox)) then
                if Tipo.GetProperty('Color') <> nil             then Tipo.GetProperty('Color').SetValue(Component, LAYOUT_COREDIT_DESABILITADO);
            end;
          end;

          if Tipo.GetProperty('HelpKeyword') <> nil then
          begin
            case AnsiIndexStr(Tipo.GetProperty('HelpKeyword').GetValue(Component).AsString, ['Hora', 'Integer']) of
              0: //campos de hora
              begin
                if Tipo.GetProperty('Format') <> nil    then Tipo.GetProperty('Format').SetValue(Component, 'hh:nn');
                if Tipo.GetProperty('EditType') <> nil  then Tipo.GetProperty('EditType').SetValue(Component, TValue.From(TRzDTEditType.etTime));
                if Tipo.GetProperty('MaxLength') <> nil then Tipo.GetProperty('MaxLength').SetValue(Component, 5);

                if Component is TRzDBDateTimeEdit then
                begin
                  TRzDBDateTimeEdit(Component).OnKeyUp       := RzDateTimeEditEvent.RzDateTimeEditKeyUp;
                  TRzDBDateTimeEdit(Component).OnKeyDown     := RzDateTimeEditEvent.RzDateTimeEditKeyDown;
                  TRzDBDateTimeEdit(Component).OnInvalidTime := RzDateTimeEditEvent.RzDateTimeEditInvalidTime;
                end;
                if Component is TRzDateTimeEdit then
                begin
                  TRzDateTimeEdit(Component).OnKeyUp       := RzDateTimeEditEvent.RzDateTimeEditKeyUp;
                  TRzDateTimeEdit(Component).OnKeyDown     := RzDateTimeEditEvent.RzDateTimeEditKeyDown;
                  TRzDateTimeEdit(Component).OnInvalidTime := RzDateTimeEditEvent.RzDateTimeEditInvalidTime;
                end;
              end;
              1: //campos de inteiro
              begin
                if Tipo.GetProperty('IntegersOnly') <> nil  then Tipo.GetProperty('IntegersOnly').SetValue(Component, True);
                if Tipo.GetProperty('DisplayFormat') <> nil then Tipo.GetProperty('DisplayFormat').SetValue(Component, '');
              end;
            else //caso vazio
              begin
                if Tipo.GetProperty('Format') <> nil           then Tipo.GetProperty('Format').SetValue(Component, 'dd/mm/yyyy');
                if Tipo.GetProperty('EditType') <> nil         then Tipo.GetProperty('EditType').SetValue(Component, TValue.From(TRzDTEditType.etDate));
                if Tipo.GetProperty('UseFormatToParse') <> nil then Tipo.GetProperty('UseFormatToParse').SetValue(Component, True);

                if Tipo.GetProperty('IntegersOnly') <> nil     then
                begin
                  Tipo.GetProperty('IntegersOnly').SetValue(Component, False);
                  if Tipo.GetProperty('Alignment') <> nil then
                     Tipo.GetProperty('Alignment').SetValue(Component, TValue.From(taRightJustify));
                end;

                if Tipo.GetProperty('DisplayFormat') <> nil then
                begin
                  if Tipo.GetProperty('DisplayFormat').GetValue(Component).AsString = ',0;(,0)' then
                     Tipo.GetProperty('DisplayFormat').SetValue(Component, ',0.00');
                end;

                //campo de data
                if Tipo.GetProperty('Date') <> nil then
                begin
                  if Tipo.GetProperty('MaxLength') <> nil then Tipo.GetProperty('MaxLength').SetValue(Component, 10);
                  if Tipo.GetProperty('Format') <> nil    then Tipo.GetProperty('Format').SetValue(Component, 'dd/mm/yyyy');


                  if Component is TRzDBDateTimeEdit then
                  begin
                    TRzDBDateTimeEdit(Component).OnKeyUp       := RzDateTimeEditEvent.RzDateTimeEditKeyUp;
                    TRzDBDateTimeEdit(Component).OnKeyDown     := RzDateTimeEditEvent.RzDateTimeEditKeyDown;
                    TRzDBDateTimeEdit(Component).OnInvalidDate := RzDateTimeEditEvent.RzDateTimeEditInvalidDate;
                  end;
                  if Component is TRzDateTimeEdit then
                  begin
                    TRzDateTimeEdit(Component).OnKeyUp       := RzDateTimeEditEvent.RzDateTimeEditKeyUp;
                    TRzDateTimeEdit(Component).OnKeyDown     := RzDateTimeEditEvent.RzDateTimeEditKeyDown;
                    TRzDateTimeEdit(Component).OnInvalidDate := RzDateTimeEditEvent.RzDateTimeEditInvalidDate;
                  end;
                end;
              end;
            end;
          end;

          if TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomMemo) then
            TRzMemo(Component).OnKeyPress := RzMemoEvent.RzMemoEventKeyPress;


          if Tipo.GetProperty('ButtonWidth') <> nil then
          begin
            if Tipo.GetProperty('ButtonWidth').GetValue(Component).AsVariant > 0 then
            begin
              if Tipo.GetProperty('ButtonShortCut') <> nil then
                 Tipo.GetProperty('ButtonShortCut').SetValue(Component, 114); //Codigo F3
              if Tipo.GetProperty('ButtonWidth') <> nil then
                 Tipo.GetProperty('ButtonWidth').SetValue(Component, 24); //Tamanho bot�o pesquisa
            end;
          end;
        end;
      finally
        Contexto.Free;
      end;
    end;
  except
    on E : Exception do

  end;
end;

procedure PadronizarLabel(Component: TComponent; ListaObrigatorios: TStringList = nil);
var
  I : Integer;
  Contexto : TRTTIContext;
  Tipo     : TRTTIType;
begin
  try
    if Component is TForm then
    begin
      for I := 0 to TForm(Component).ComponentCount -1 do
        if TForm(Component).Components[I].ClassType = TLabel then
           PadronizarLabel(TForm(Component).Components[I]);
    end
    else
    begin
      if TLabel(Component).Name <> 'lblTitulo' then
      begin
        if TLabel(Component).FocusControl <> nil then
        begin
          if ListaObrigatorios <> nil then
          begin
            if ListaObrigatorios.IndexOf(TLabel(Component).FocusControl.Name) > -1 then
            begin
              TLabel(Component).Font.Color := LAYOUT_CORLABEL_OBRIGATORIO;
              TLabel(Component).Font.Style := [fsBold];
            end
            else
            begin
              TLabel(Component).Font.Color := LAYOUT_CORLABEL_NORMAL;
               TLabel(Component).Font.Style := [];
            end;
          end;
        end
        else TLabel(Component).Font.Color := LAYOUT_CORLABEL_NORMAL;
      end;
    end;
  except
  end;
end;

procedure PadronizarTabs(Component: TComponent);
var
  I : Integer;
  Contexto : TRTTIContext;
  Tipo     : TRTTIType;
begin
  try
//    OutputDebug('LayoutFormulario -> Passou pela PadronizarTabs');
    if Component is TForm then
    begin
      for I := 0 to TForm(Component).ComponentCount - 1 do
      begin
        if TForm(Component).Components[i] is TCustomControl then
          PadronizarTabs(Tform(Component).Components[i]);
      end;
    end
    else
    begin
//      try
//        Contexto := TRttiContext.Create;
//        Tipo     := Contexto.GetType(Component.ClassType);
//        if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TCustomControl)) then
//        begin
////          if Tipo.GetProperty('TabStyle') <> nil         then SetPropValue(Component, 'TabStyle', );;
//
//        end;
//      finally
//        Contexto.Free;
//      end;
       TRzPageControl(Component).TextColors.Selected   := LAYOUT_CORLABEL_NORMAL;
       TRzPageControl(Component).TextColors.Unselected := LAYOUT_CORLABEL_NORMAL;
    end;
  except
    on E : Exception do

  end;
end;

procedure PadronizarGrid(Component: TComponent);
var
  I : Integer;
  Contexto : TRTTIContext;
  Tipo     : TRTTIType;
begin
  try
    if Component is TForm then
    begin
      for I := 0 to TForm(Component).ComponentCount -1 do
        if (TForm(Component).Components[I] is TcxGrid) or
           (TForm(Component).Components[I] is TcxGridLevel) or
           (TForm(Component).Components[I] is TcxGridDBTableView) or
           (TForm(Component).Components[I] is TcxGridDBColumn) then
           PadronizarGrid(TForm(Component).Components[I]);
    end
    else
    begin
      try
        Contexto := TRttiContext.Create;
        Tipo     := Contexto.GetType(Component.ClassInfo);

        if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TcxGrid)) then
        begin
          if Tipo.GetProperty('BorderStyle') <> nil then
            SetPropValue(Component, 'BorderStyle', TcxControlBorderStyle.cxcbsNone);
        end;

        if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TcxGridDBColumn)) then
        begin
          if Tipo.GetProperty('HeaderAlignmentHorz') <> nil then
            SetPropValue(Component, 'HeaderAlignmentHorz', taCenter);
        end;

        if (Tipo is TRttiInstanceType) and (TRttiInstanceType(Tipo).MetaclassType.InheritsFrom(TcxGridDBTableView)) then
        begin
          TcxGridTableView(Component).OptionsView.GridLines       := TcxGridLines.glNone;
          TcxGridTableView(Component).OptionsView.Indicator       := True;
          TcxGridTableView(Component).OptionsSelection.CellSelect := False;
          TcxGridTableView(Component).Styles.Content              := frmPrincipal.cxStyle2;
          TcxGridTableView(Component).Styles.ContentEven          := frmPrincipal.cxStyle1;
        end;
      finally
        Contexto.Free;
      end;
    end;
  except
    on E: Exception do
  end;
end;

procedure PadronizarTela(Component: TForm; ListaObrigatorios: TStringList = nil);
var
  I: Integer;
begin
  for I := 0 to Component.ComponentCount -1 do
  begin
    if Component.Components[I] is TLabel then
      PadronizarLabel(Component.Components[I], ListaObrigatorios)
    else
    if (Component.Components[I] is TRzCustomEdit) or
       (Component.Components[I] is TRzCustomComboBox) or
       (Component.Components[I] is TRzCustomCheckBox) or
       (Component.Components[I] is TCustomMemo) then
      PadronizarEdit(Component.Components[I])
    else
    if (Component.Components[i] is TSpeedButton) or
       (Component.Components[i] is TRzCustomButton) then
      PadronizarBotao(Component.Components[I])
    else
    if Component.Components[i] is TRzCustomTabControl then
      PadronizarTabs(Component.Components[I])
    else
    if (Component.Components[I] is TcxGrid) or
       (Component.Components[I] is TcxGridLevel) or
       (Component.Components[I] is TcxGridDBTableView) or
       (Component.Components[I] is TcxGridDBColumn) then
           PadronizarGrid(Component.Components[I]);
  end;


end;


{ RzMemoEvent }

class procedure RzMemoEvent.RzMemoEventKeyPress(Sender: TObject; var Key: Char);
var
  I: Integer;
begin
  try
    if (key = #13) then
    begin
      if (TCustomMemo(Sender).Lines[TCustomMemo(Sender).Lines.Count -1].IsEmpty) then
      begin
        TForm(TCustomMemo(Sender).Owner).Perform(CM_DialogKey, VK_TAB, 0);

        TCustomMemo(Sender).Text := Trim(TCustomMemo(Sender).Text);
      end;
    end;
  except
  end;
end;


end.
