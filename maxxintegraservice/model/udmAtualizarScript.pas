unit udmAtualizarScript;

interface

uses
  System.SysUtils, System.Classes, IPPeerClient, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, StrUtils, JSON,
  Threading, System.Diagnostics, System.SyncObjs, Winapi.Windows, Vcl.Forms, Rest.JSON, System.JSON.Types,
  System.JSON.Readers;


Type
  TTaskHelper = class helper for TTask
  private type
    TUnsafeTaskEx = record
    private
      [Unsafe]
      // preciso de um record UNSAFE para nao incrementar o RefCount da Interface
      FTask: TTask;
    public
      property Value: TTask read FTask write FTask;
    end;
  public
    class function WaitForAllEx(AArray: Array of ITask;
    ATimeOut: int64 = INFINITE): boolean;
  end;

type
  TdmAtualizarScript = class(TDataModule)
    RESTClientMaxx: TRESTClient;
    RESTResponseMaxx: TRESTResponse;
    RESTRequestMaxx: TRESTRequest;
    FDConCliente: TFDConnection;
    RESTClientTabelas: TRESTClient;
    RESTResponseTabelas: TRESTResponse;
    RESTRequestTabelas: TRESTRequest;
    RESTClientConexao: TRESTClient;
    RESTResponseConexao: TRESTResponse;
    RESTRequestConexao: TRESTRequest;
  private
    function Atualiza(Atualiza��o: String; FDQry: TFDQuery; RestClient: TRestClient; RestRequest: TRestRequest;
      RestResponse: TRestResponse): Boolean;
    { Private declarations }
  public
    { Public declarations }
//    function AtualizarScripts(IDSistema, IDCliente, IDFilial: String): Boolean;
    function AtualizarTabelas(IDSistema: String): Boolean;
    function AtualizarConexao(IDSistema: String): Boolean;
  end;

var
  dmAtualizarScript: TdmAtualizarScript;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses ufrmPrincipal, uLog, uMaxxREST;

{$R *.dfm}

{ TdmAtualizarScript }

function TdmAtualizarScript.AtualizarConexao(IDSistema: String): Boolean;
begin
  RESTRequestConexao.Params.AddUrlSegment('SISTEMA', IDSistema);

  Atualiza('conexao', frmPrincipal.FDConexao, RESTClientConexao, RESTRequestConexao, RESTResponseConexao);
end;

//function TdmAtualizarScript.AtualizarScripts(IDSistema, IDCliente, IDFilial: String): Boolean;
//begin
//  RESTRequestMaxx.Params.AddUrlSegment('CLIENTE', IDCliente);
//  RESTRequestMaxx.Params.AddUrlSegment('FILIAL', ifthen(IDFilial <> '0', '/' + IDFilial, ''));
//  RESTRequestMaxx.Params.AddUrlSegment('SISTEMA', IDSistema);
//
//  Atualiza('script', frmPrincipal.FDScripts, RESTClientMaxx, RESTRequestMaxx, RESTResponseMaxx);
//end;


function TdmAtualizarScript.Atualiza(Atualiza��o: String; FDQry: TFDQuery; RestClient: TRestClient; RestRequest:
                                     TRestRequest; RestResponse: TRestResponse): Boolean;
var
  Task: ITask;
begin
  Task := TTask.Create(
  procedure
  var
    JsonScript, JsonObject, JsonItem: TJsonObject;
    JsonArray, JsonResult: TJsonArray;
    I,J,K: Integer;
    MaxxREST: TMaxxRESTCliente;
  begin
    try
      try
        RestClient.BaseURL := frmPrincipal.IPMAXX + '/datasnap/rest/TServerMethods1';
        RestRequest.Execute;

        JSONObject := TJSONObject(TJSONObject.ParseJSONValue(RestResponse.JSONText));

        case RestResponse.StatusCode of
          200: //OK
          begin
            if JsonObject <> nil then
            begin
              //Apagando os scripts antigos e atualizando
              FDQry.Close;
              FDQry.Open();

              FDQry.First;
              while not FDQry.Eof do
                FDQry.Delete;

              FDQry.ApplyUpdates(0);

              FDQry.Close;
              FDQry.Open();

              //pegando resultados do resultado
              JsonResult := TJSONArray(JsonObject.Values['result']);
              if JsonResult <> nil then
              begin
                for I := 0 to JsonResult.Count -1 do
                begin
                  JsonArray := TJsonArray(TJsonObject(JsonResult.Items[I]).Values[Atualiza��o]);

                  for J := 0 to JsonArray.Count -1 do
                  begin
                    JsonScript := TJSONObject(JsonArray.Items[J]);

                    FDQry.Append;

                    for K := 0 to JsonScript.Count -1 do
                    begin
                      JsonItem := TJsonObject(JsonScript.Pairs[K]);

                      if FDQry.FindField(TJSONPair(JsonScript.Pairs[K]).JsonString.Value) <> nil then
                      begin
                        with FDQry.FindField(TJSONPair(JsonScript.Pairs[K]).JsonString.Value) do
                        begin
                          if DataType = ftTimeStamp then
                            Value := StrToDateTime(TJSONPair(JsonScript.Pairs[K]).JsonValue.Value)
                          else
                            Value := TJSONPair(JsonScript.Pairs[K]).JsonValue.Value;
                        end;
                      end;
                    end;
                    FDQry.Post;
                  end;
                end;
              end;
              FDQry.ApplyUpdates(0);

              TThread.Synchronize(nil,
              procedure()
              begin
                frmPrincipal.mmoDetalhes.Lines.Add(Atualiza��o + ': Inserido(s) (' + IntToStr(FDQry.RecordCount) + ') registro(s)!');
              end);
            end;
          end;
          204: //Vazio
          begin
            TThread.Synchronize(nil,
            procedure()
            begin
              frmPrincipal.mmoDetalhes.Lines.Add(Atualiza��o + ': N�o foram encontrados resultados com estes par�metros!');
            end);
          end;
          400: //Bad Request
          begin
            TThread.Synchronize(nil,
            procedure()
            begin
              frmPrincipal.mmoDetalhes.Lines.Add(Atualiza��o + ': A requisi��o n�o foi completada corretamente! Verifique os par�metros.');
            end);
          end;
          500: // Erro servidor
          begin
            TThread.Synchronize(nil,
            procedure()
            begin
              frmPrincipal.mmoDetalhes.Lines.Add(Atualiza��o + ': Foi encontrado algum erro no servidor! Informe a equipe de desenvolvimento.');
            end);
          end;
        end;
      except
        on E: Exception do
        begin
          TThread.Synchronize(nil,
          procedure()
          begin
            frmPrincipal.mmoDetalhes.Lines.Clear;
            frmPrincipal.mmoDetalhes.Lines.Add(Atualiza��o + ': ' + E.Message);
          end);
        end;
      end;
    finally

    end;
  end);

  Task.Start;
  TTask.WaitForAllEx(Task);
end;


function TdmAtualizarScript.AtualizarTabelas(IDSistema: String): Boolean;
begin
//  RESTRequestTabelas.Params.AddUrlSegment('SISTEMA', IDSistema);
//  Atualiza('tabela', frmPrincipal.FDTabelas, RESTClientTabelas, RESTRequestTabelas, RESTResponseTabelas);
end;

{ TTaskHelper }

class function TTaskHelper.WaitForAllEx(AArray: array of ITask; ATimeOut: int64): boolean;
var
  FEvent: TEvent;
  task: TUnsafeTaskEx;
  i: integer;
  taskInter: TArray<TUnsafeTaskEx>;
  completou: boolean;
  Canceled, Exceptions: boolean;
  ProcCompleted: TProc<ITask>;
  LHandle: THandle;
  LStop: TStopwatch;
begin
  LStop := TStopwatch.StartNew;
  ProcCompleted := procedure(ATask: ITask)
    begin
      FEvent.SetEvent;
    end;

  Canceled := false;
  Exceptions := false;
  result := true;
  try
    for i := low(AArray) to High(AArray) do
    begin
      task.Value := TTask(AArray[i]);
      if task.Value = nil then
        raise EArgumentNilException.Create('Wait Nil Task');

      completou := task.Value.IsComplete;
      if not completou then
      begin
        taskInter := taskInter + [task];
      end
      else
      begin
        if task.Value.HasExceptions then
          Exceptions := true
        else if task.Value.IsCanceled then
          Canceled := true;
      end;
    end;

    try
      FEvent := TEvent.Create();
      for task in taskInter do
      begin
        try
          FEvent.ResetEvent;
          if LStop.ElapsedMilliseconds > ATimeOut then
            break;
          LHandle := FEvent.Handle;
          task.Value.AddCompleteEvent(ProcCompleted);
          while not task.Value.IsComplete do
          begin
            try
              if LStop.ElapsedMilliseconds > ATimeOut then
                break;
                  if MsgWaitForMultipleObjectsEx(1, LHandle,
                    ATimeOut - LStop.ElapsedMilliseconds, QS_ALLINPUT, 0)
                    = WAIT_OBJECT_0 + 1 then
                    application.ProcessMessages;
            finally
            end;
          end;
          if task.Value.IsComplete then
          begin
            if task.Value.HasExceptions then
              Exceptions := true
            else if task.Value.IsCanceled then
              Canceled := true;
          end;
        finally
          task.Value.removeCompleteEvent(ProcCompleted);

        end;
      end;
    finally
      FEvent.Free;
    end;
  except
    result := false;
  end;

  if (not Exceptions and not Canceled) then
    Exit;
  if Exceptions or Canceled then
    raise EOperationCancelled.Create
      ('One Or More Tasks HasExceptions/Canceled');

end;

end.
