unit uDatasetToJSON;

interface

uses FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, Threading, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, JSON, Rest.JSON, SysUtils, DateUtils, Variants, StrUtils, Winapi.Windows;

type TJsonFilho = record
  Nome: String;
  JsonFilho: TJSONValue;
end;

//default 'yyyy-MM-dd''T''HH:mm:ss.zzz'

type
    TDatasetToJSON = class
  private
    FNomeFilho: String;
    FJsonFilho: TJsonValue;
    FPercorreTudo: Boolean;
    FFilho: Array of TJsonFilho;
    FFormatoData: String;
    FIncluiTimeZone: Boolean;

    procedure SetFilho(AIndex: Integer; const Value: TJsonFilho);
    function GetFilho(AIndex: Integer): TJsonFilho;
    procedure SetIncluiTimeZone(const Value: Boolean);
    function RetornaTimeZone: String;

    property Filho[AIndex: Integer]: TJsonFilho read GetFilho        write SetFilho;
    property JsonFilho   : TJsonValue           read FJsonFilho      write FJsonFilho;
    property NomeFilho   : String               read FNomeFilho      write FNomeFilho;
    property PercorreTudo: Boolean              read FPercorreTudo   write FPercorreTudo;
  public
    property FormatoData : String               read FFormatoData    write FFormatoData;
    property IncluiTimeZone: Boolean            read FIncluiTimeZone write SetIncluiTimeZone default True;

    constructor Create; overload;
    constructor Create(Percorrer: Boolean); overload;
    constructor Create(Percorrer: Boolean; jVal: TJSONValue; Nome: String); overload;
    constructor Create(Percorrer: Boolean; AFilho: array of TJsonFilho); overload;
    destructor Destroy; overload;
//    function Converter(DataSet: TDataSet): TJsonValue;
    function CriaJson(DataSet: TDataSet; Campos: TDataSet = nil): TJSONValue;
  end;

implementation

{ TDatasetToJSON }

uses uLog;

constructor TDatasetToJSON.Create(Percorrer: Boolean);
begin
  inherited Create;
  FPercorreTudo := Percorrer;
end;

constructor TDatasetToJSON.Create(Percorrer: Boolean; jVal: TJSONValue; Nome: String);
begin
  FPercorreTudo := Percorrer;
  FJsonFilho    := jVal;
  FNomeFilho    := Nome;
  inherited Create;
end;

constructor TDatasetToJSON.Create(Percorrer: Boolean; AFilho: array of TJsonFilho);
var
  I: Integer;
begin
  inherited Create;
  PercorreTudo := Percorrer;

  SetLength(FFilho, Length(AFilho));

  for I := 0 to Pred(Length(AFilho)) do
  begin
    FFilho[I].Nome      := AFilho[I].Nome;
    FFilho[I].JsonFilho := AFilho[I].JsonFilho;
  end;
end;

function TDatasetToJSON.CriaJson(DataSet: TDataSet; Campos: TDataSet = nil): TJSONValue;
var
  jObj: TJSONObject;
  jPar: TJSONPair;
  I: Integer;

  function RetornaNome(Nome: String): String;
  begin
    if Campos <> nil then
    begin
      try
        if Campos.RecordCount > 0 then
        begin
          Campos.Filtered := False;
          Campos.Filter   := 'json_camposql like ' + QuotedStr(Nome);
          Campos.Filtered := True;

          if (not Campos.IsEmpty) and (Campos.FieldByName('json_campojson').AsString <> '') then
            Result := Campos.FieldByName('json_campojson').AsString
          else Result := Nome;
        end
        else
          Result := Nome;
      except
        on E: Exception do
          SalvarLog('retornanome: ' + E.Message);

      end;

    end
    else Result := Nome;
  end;

begin
  try
    jObj := TJSONObject.Create;

    for I := 0 to Pred(DataSet.FieldCount) do
    begin
//      if DataSet.Fields[I].Value <> Null then
//      begin
        if DataSet.Fields[I].DataType in [ftString, ftWideString, ftMemo, ftFixedChar, ftWideMemo] then
          jObj.AddPair(RetornaNome(DataSet.Fields[I].FieldName), TJSONString.Create(StringReplace(Trim(DataSet.Fields[I].AsString), '\', '\\', [rfReplaceAll, rfIgnoreCase])))
        else
        if DataSet.Fields[I].DataType in [ftSmallint, ftInteger, ftFloat, ftCurrency, ftBCD, ftLargeint, ftFMTBcd] then
        begin
          if DataSet.Fields[I].Value <> Null then
          begin
            case DataSet.Fields[I].DataType of
              ftBCD, ftFMTBcd:
                jObj.AddPair(RetornaNome(DataSet.Fields[I].FieldName), TJSONNumber.Create(DataSet.Fields[I].AsFloat));
            else
              begin
                jObj.AddPair(RetornaNome(DataSet.Fields[I].FieldName), TJSONNumber.Create(DataSet.Fields[I].AsString));
              end;
            end;

          end;
        end
        else
        if DataSet.Fields[I].DataType in [ftDate, ftDateTime, ftTimeStamp] then
        begin
          if DataSet.Fields[I].Value <> Null then
          begin
            if FormatoData <> '' then
              jObj.AddPair(RetornaNome(DataSet.Fields[I].FieldName), TJSONString.Create(FormatDateTime(FormatoData, DataSet.Fields[I].AsDateTime) + RetornaTimeZone))
            else
              jObj.AddPair(RetornaNome(DataSet.Fields[I].FieldName), TJSONString.Create(DateToStr(DataSet.Fields[I].AsDateTime)));
          end;
        end;
//      end;
    end;

    if (NomeFilho <> '') and (not JsonFilho.Null) then
      jObj.AddPair(NomeFilho, JsonFilho);

    if Length(FFilho) > 0 then
    begin
      for I := 0 to Pred(Length(FFilho)) do
        jObj.AddPair(Filho[I].Nome, Filho[I].JsonFilho);
    end;

    Result := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(jObj.ToString),0);
  except
    on E: Exception do
      SalvarLog('CriaJson: ' + E.Message);
  end;
end;

destructor TDatasetToJSON.Destroy;
var
  I: Integer;
begin
  inherited Destroy;
  FreeAndNil(FJsonFilho);

  for I := 0 to Pred(Length(FFilho)) do
    FFilho[I].JsonFilho.Free;

  SetLength(FFilho, 0);
end;

function TDatasetToJSON.GetFilho(AIndex: Integer): TJsonFilho;
begin
  Result.Nome      := FFilho[AIndex].Nome;
  Result.JsonFilho := FFilho[AIndex].JsonFilho;
end;

function TDatasetToJSON.RetornaTimeZone: String;
var
  TimeZone: TTimeZoneInformation;
  Time: String;
begin
  if FIncluiTimeZone then
  begin
    GetTimeZoneInformation(TimeZone);

    Time := IntToStr(TimeZone.Bias div 60);
    if Length(Time) = 1 then
       Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60) + '00'
    else
    if Length(Time) = 2 then
       Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60) + '0'
    else
    if Length(Time) = 3 then
       Result := IfThen((TimeZone.Bias * -1) < 0, '-0', '+0') + FloatToStr(TimeZone.Bias div 60)
    else
    if Length(Time) = 4 then
       Result := IfThen((TimeZone.Bias * -1) < 0, '-', '+') + FloatToStr(TimeZone.Bias div 60);
  end
  else Result := '';
end;

constructor TDatasetToJSON.Create;
begin
  inherited;
  PercorreTudo := True;
  NomeFilho    := '';
  JsonFilho    := TJSONValue.Create;
  FormatoData  := '';
end;

procedure TDatasetToJSON.SetFilho(AIndex: Integer; const Value: TJsonFilho);
begin
  FFilho[AIndex].Nome      := Value.Nome;
  FFilho[AIndex].JsonFilho := Value.JsonFilho;
end;

procedure TDatasetToJSON.SetIncluiTimeZone(const Value: Boolean);
begin
  FIncluiTimeZone := Value;
end;

//function TDatasetToJSON.Converter(DataSet: TDataSet): TJsonValue;
//begin
//  try
//    if PercorreTudo then
//    begin
//      Result := TJSONArray.Create;
//
//      DataSet.First;
//
//      while not DataSet.Eof do
//      begin
//        TJsonArray(Result).AddElement(CriaJson(DataSet));
//        DataSet.Next;
//      end;
//    end
//    else CriaJson(DataSet);
//  except
//    raise Exception.Create('Erro ao converter o DataSetToJSON ');
//  end;
//end;

end.
