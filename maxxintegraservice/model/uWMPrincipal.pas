unit uWMPrincipal;

interface

uses
  System.SysUtils, System.Classes, Web.HTTPApp, Datasnap.DSHTTPCommon,
  Datasnap.DSHTTPWebBroker, Datasnap.DSServer,
  Web.WebFileDispatcher, Web.HTTPProd,
  DataSnap.DSAuth,
  Datasnap.DSProxyJavaScript, IPPeerServer, Datasnap.DSMetadata, Datasnap.DSServerMetadata, Datasnap.DSClientMetadata,
  Datasnap.DSCommonServer, Datasnap.DSHTTP, Data.DBXCommon, Data.DBCommonTypes;

type
  TWMPrincipal = class(TWebModule)
    DSHTTPWebDispatcher1: TDSHTTPWebDispatcher;
    DSServer: TDSServer;
    ServerFunctionInvoker: TPageProducer;
    ReverseString: TPageProducer;
    WebFileDispatcher1: TWebFileDispatcher;
    DSProxyGenerator1: TDSProxyGenerator;
    DSServerMetaDataProvider1: TDSServerMetaDataProvider;
    procedure WebModuleDefaultAction(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure DSServerDisconnect(DSConnectEventObject: TDSConnectEventObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWMPrincipal;

implementation


{$R *.dfm}

uses uServerMethods, Web.WebReq, uLog, ufrmPrincipal;

procedure TWMPrincipal.DSServerDisconnect(DSConnectEventObject: TDSConnectEventObject);
begin
  frmPrincipal.LimpaMemoriaAPP;
end;

procedure TWMPrincipal.WebModuleDefaultAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
//  if (Request.InternalPathInfo = '') or (Request.InternalPathInfo = '/')then
//    Response.Content := ReverseString.Content
//  else
  Response.SendRedirect(Request.InternalScriptName + '/');
end;

initialization
finalization
  Web.WebReq.FreeWebModules;

end.

