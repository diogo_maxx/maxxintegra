object dmAtualizarScript: TdmAtualizarScript
  OldCreateOrder = False
  Height = 336
  Width = 695
  object RESTClientMaxx: TRESTClient
    BaseURL = 'http://localhost:9090/datasnap/rest/TServerMethods1'
    Params = <>
    HandleRedirects = True
    Left = 582
    Top = 98
  end
  object RESTResponseMaxx: TRESTResponse
    Left = 582
    Top = 194
  end
  object RESTRequestMaxx: TRESTRequest
    Client = RESTClientMaxx
    Params = <
      item
        Kind = pkURLSEGMENT
        name = 'CLIENTE'
        Options = [poAutoCreated]
      end
      item
        Kind = pkURLSEGMENT
        name = 'FILIAL'
        Options = [poAutoCreated]
      end
      item
        Kind = pkURLSEGMENT
        name = 'SISTEMA'
        Options = [poAutoCreated]
      end>
    Resource = 'GetScripts/{SISTEMA}/{CLIENTE}{FILIAL}'
    Response = RESTResponseMaxx
    SynchronizedEvents = False
    Left = 582
    Top = 146
  end
  object FDConCliente: TFDConnection
    Left = 584
    Top = 32
  end
  object RESTClientTabelas: TRESTClient
    BaseURL = 'http://localhost:9090/datasnap/rest/TServerMethods1'
    Params = <>
    HandleRedirects = True
    Left = 438
    Top = 98
  end
  object RESTResponseTabelas: TRESTResponse
    Left = 438
    Top = 194
  end
  object RESTRequestTabelas: TRESTRequest
    Client = RESTClientTabelas
    Params = <
      item
        Kind = pkURLSEGMENT
        name = 'SISTEMA'
        Options = [poAutoCreated]
      end>
    Resource = 'GetTabelas/{SISTEMA}'
    Response = RESTResponseTabelas
    SynchronizedEvents = False
    Left = 438
    Top = 146
  end
  object RESTClientConexao: TRESTClient
    BaseURL = 'http://localhost:9090/datasnap/rest/TServerMethods1'
    Params = <>
    HandleRedirects = True
    Left = 230
    Top = 98
  end
  object RESTResponseConexao: TRESTResponse
    Left = 230
    Top = 194
  end
  object RESTRequestConexao: TRESTRequest
    Client = RESTClientConexao
    Params = <
      item
        Kind = pkURLSEGMENT
        name = 'SISTEMA'
        Options = [poAutoCreated]
      end>
    Resource = 'GetConexao/{SISTEMA}'
    Response = RESTResponseConexao
    SynchronizedEvents = False
    Left = 230
    Top = 146
  end
end
